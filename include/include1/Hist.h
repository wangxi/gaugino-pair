#pragma once
#include <TH1.h>
#include <functional> 

// 1D Histgram with its fill position 
class Hist
{
public:
	static const bool USE_OVERFLOW = true;
	double trigwei = 1;

	Hist(TH1* hist, std::function<double()> fillPosition, bool fillOverflow = false,std::function<double()> trig_weight = [&] {return 1;}) {
		hist->Sumw2();
		this->hist = hist;
		this->fillPosition = fillPosition;
		this->fillOverflow = fillOverflow;
	}
	//In this way, you can only use TH1F
	Hist(std::string histname, int nBin, double binStart, double binEnd, std::function<double()> fillPosition, bool fillOverflow = false,  std::function<double()> trig_weight =[&] {return 1;}) {
		TH1* hist = new TH1F(histname.c_str(), histname.c_str(), nBin, binStart, binEnd);
		hist->Sumw2();
		this->hist = hist;
		this->fillPosition = fillPosition;
		this->fillOverflow = fillOverflow;
	}
	void Fill(double weight) {
		double posi = fillPosition();
		if (fillOverflow) {
			double lastbinCenter = hist->GetBinCenter(hist->GetNbinsX());
			posi = (posi > lastbinCenter) ? lastbinCenter : posi;
		}
		hist->Fill(fillPosition(),weight);
	}
	int getBinNum() {
		return hist->GetNbinsX();
	}
	double getBinUp() {
		return hist->GetBinLowEdge(hist->GetNbinsX() + 1);
	}
	double getBinDown() {
		return hist->GetBinLowEdge(1);
	}
	void Write(){
		hist->Write();
	}
	~Hist(){
		/*Note: In root framework, when deleting a file, all objects owned by this file will be deleted(See Root chapter 10: ObjectOwnership). 
		So it's kind of dangerous to delete the histograms or trees by hand. Though may cause mermory leak, won't do anything when destructing
		if (hist != nullptr) {
			delete hist;
			hist = nullptr;
		}*/
	}
private:
	TH1 * hist;
	bool fillOverflow;
	std::function<double()> fillPosition;
};

//A class that allow you to create many Hist with same bin definition and fill Position
class HistTemplate
{

public:

	HistTemplate(std::function<double()> fillValue, int binNum, double binStart, double binEnd, bool useOverflow = false) {
		this->fillValue = fillValue;
		this->binNum = binNum;
		this->binStart = binStart;
		this->binEnd = binEnd;
		this->useOverflow = useOverflow;
	}
	Hist* createHist(std::string name){
		return new Hist(name, binNum, binStart, binEnd, fillValue, useOverflow);
	}
	~HistTemplate() {};

private:
	std::function<double()> fillValue;
	bool useOverflow;
	int binNum;
	double binStart, binEnd;

};



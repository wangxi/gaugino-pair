#pragma once

#include <TLorentzVector.h>
#include <map>
#include <iostream>
#include <vector>
#include <algorithm>
#include <functional>
#include <initializer_list>


/**************************************************************
**********Steal and modify from SimpleAnalysis package*********
****https://gitlab.cern.ch/atlas-phys-susy-wg/SimpleAnalysis***
**************************************************************/

enum AnalysisObjectType {
  ELECTRON, MUON, TAU, PHOTON, JET, MET, COMBINED
};

enum AnalysisElectronID {
  EVeryLooseLH = 1 << 0, 
  ELooseLH = 1 << 1, 
  EMediumLH = 1 << 2, 
  ETightLH = 1 << 3, 
  EIsoLoose = 1 << 12, 
  EIsoGradient = 1 << 13, 
  EIsSignal = 1 << 20,
  EGood = EVeryLooseLH | ELooseLH | EMediumLH | ETightLH | EIsSignal,
  EIsoGood = EGood | EIsoLoose | EIsoGradient
};

enum AnalysisMuonID {
	MuVeryLoose = 1 << 0,
	MuLoose = 1 << 1,
	MuMedium = 1 << 2,
	MuTight = 1 << 3,
    MuHighPt = 1 << 4, 
    MuIsoLoose = 1 << 12, 
    MuIsoGradient = 1 << 13, 
	MuIsoGradientLoose = 1 << 14,
	MuIsSignal = 1 << 20,
    MuGood = MuLoose | MuMedium | MuTight | MuVeryLoose | MuIsSignal, 
    MuIsoGood = MuGood | MuIsoGradientLoose | MuIsoLoose 
};

enum AnalysisTauID {
	TauVeryLoose = 0,
    TauLoose = 1 << 0, 
    TauMedium = 1 << 1, 
    TauTight = 1 << 2, 
    TauOneProng = 1 << 10, 
    TauThreeProng = 1 << 11, 
	TauIsSignal = 1 << 20,
    TauGood = TauLoose | TauMedium | TauTight | TauIsSignal,
    TauIsoGood = TauGood
};

enum AnalysisPhotonID {
    PhotonLoose = 1 << 0, 
    PhotonTight = 1 << 1, 
    PhotonIsoFixedCutLoose = 1 << 8, 
    PhotonIsoFixedCutTight = 1 << 9, 
    PhotonGood = PhotonLoose | PhotonTight, 
    PhotonIsoGood = PhotonGood | PhotonIsoFixedCutLoose | PhotonIsoFixedCutTight
};

enum AnalysisJetID {
    LightJet = 1 << 1, 
	GoodBJet = 1 << 2,
	GoodCJet = 1 << 3,
    ForwardJet = 1 << 4, 
    LessThan3Tracks = 1 << 8, // most signal jets should fail this
	JetIsSignal = 1 << 20,
	GoodJet = LightJet | GoodBJet | GoodCJet | ForwardJet | JetIsSignal,
    TrueLightJet = 1 << 27, //These should not be used for actual analysis selection, only for understanding
    TrueCJet = 1 << 28, 
    TrueBJet = 1 << 29, 
    TrueTau = 1 << 30,

};

enum ParticleIDs {
    //PDGIDs
    Neutralino1 = 1000022,
    Neutralino2 = 1000023,
    Neutralino3 = 1000025,
    Neutralino4 = 1000035,
    Chargino1 = 1000024,
    Chargino2 = 1000037,
    //Status codes
    StablePart = 1<<24,
    DecayedPart = 2<<24
};

class AnaObj: public TLorentzVector {
    public:
        AnaObj(AnalysisObjectType type, double Px, double Py, double Pz, double E, int charge, int id, double Mt = 0) :
                    TLorentzVector(Px, Py, Pz, E),
		    _charge(charge),
                    _id(id),
                    _type(type),
					_mt(Mt){
        };
        AnaObj(AnalysisObjectType type, TLorentzVector tlv, int charge, int id, double Mt = 0) :
                    TLorentzVector(tlv),
                    _charge(charge),
                    _id(id),
                    _type(type),
					_mt(Mt){
        };
		virtual void trigStatus(std::string trigName, bool trigMatch) {
			trigStat[trigName] = trigMatch;
		};
        virtual bool passID(int id) const {
			return (id & _id) == id;
        };
		virtual bool passTrig(std::string trigName) {
			if (trigStat.find(trigName) != trigStat.end()){
				return trigStat.at(trigName);
			}else {
				std::cerr << "Warnning! No trigger named " << trigName << "!" << std::endl;
				return true;
			}
			
		};
        virtual int charge() const {
            return _charge;
        };
        virtual AnalysisObjectType type() const {
            return _type;
        };
        virtual int id() const {
            return _id;
        };
		//The MT we use most of times, it is calculated by Pt and MET
		virtual double Mt() const {
			return _mt;
		}
		//In case, we create a func to return lorentz Mt
		virtual double lorentzMt() const {
			TLorentzVector tlv;
			tlv.SetPtEtaPhiM(Pt(), Eta(), Phi(), M());
			return tlv.Mt();
		}
		virtual void setMt(double Mt) {
			_mt = Mt;
		}
        virtual TLorentzVector lorentzVector() const {
            TLorentzVector tlv;
            tlv.SetPtEtaPhiM(Pt(), Eta(), Phi(), M());
            return tlv;
        };

		bool isOS(AnaObj o2) {
			return (this->_charge * o2.charge() < 0);
		}


    private:
        int _charge; //not used for jets or photons
        int _id;
		double _mt;
		std::map<std::string, bool> trigStat;
        AnalysisObjectType _type;
};

AnaObj operator+(const AnaObj& lhs, const AnaObj& rhs) {
	const TLorentzVector &tlhs = lhs;
	return AnaObj(COMBINED, tlhs + rhs, lhs.charge() + rhs.charge(), 0, lhs.Mt() + rhs.Mt());
}

class AnaObjs : public std::vector<AnaObj> {
	using std::vector<AnaObj>::vector; //reuse vector initializers
									   //force range check when accessing objects directly
public:
	AnaObjs() {};
	AnaObjs(AnalysisObjectType type, std::vector<double> PtVec, std::vector<double> EtaVec, std::vector<double> PhiVec, 
		std::vector<double> EVec, std::vector<int> chargeVec, std::vector<int> idVec, std::vector<double> MtVec){
		AnaObjs objs;
		if (PtVec.size() != EtaVec.size() != PhiVec.size() != EVec.size() != chargeVec.size() != idVec.size() != MtVec.size())
		{
			std::cerr << "Warnning! There is a event which the vectors size are different!!! Please Check!! Will return an empty Object collection" << std::endl;
		}
		else
		{
			for (unsigned int i = 0; i < PtVec.size(); i++)
			{
				TLorentzVector tlv;
				tlv.SetPtEtaPhiE(PtVec[i], EtaVec[i], PhiVec[i], EVec[i]);
				AnaObj o(type, tlv, chargeVec[i], idVec[i], MtVec[i]);
				objs.emplace_back(o);
			}
		}
		new (this)AnaObjs(objs);
	};

	AnaObjs(AnalysisObjectType type, std::vector<double> PtVec, std::vector<double> EtaVec, std::vector<double> PhiVec,
		std::vector<double> EVec, std::vector<int> chargeVec, std::vector<int> idVec) {
		AnaObjs objs;
		if (PtVec.size() != EtaVec.size() != PhiVec.size() != EVec.size() != chargeVec.size() != idVec.size())
		{
			std::cerr << "Warnning! There is a event which the vectors size are different!!! Please Check!! Will return an empty Object collection" << std::endl;
		}
		else
		{
			for (unsigned int i = 0; i < PtVec.size(); i++)
			{
				TLorentzVector tlv;
				tlv.SetPtEtaPhiE(PtVec[i], EtaVec[i], PhiVec[i], EVec[i]);
				AnaObj o(type, tlv, chargeVec[i], idVec[i]);
				objs.emplace_back(o);
			}
		}
		new (this)AnaObjs(objs);
	};

	AnaObjs(AnalysisObjectType type, std::vector<TLorentzVector> tlvVec, std::vector<int> chargeVec, std::vector<int> idVec, std::vector<double> MtVec) {
		AnaObjs objs;
		if (tlvVec.size() != chargeVec.size() != idVec.size() != MtVec.size())
		{
			std::cerr << "Warnning! There is a event which the vectors size are different!!! Please Check!! Will return an empty Object collection" << std::endl;
		}
		else
		{
			for (unsigned int i = 0; i < tlvVec.size(); i++)
			{
				AnaObj o(type, tlvVec[i], chargeVec[i], idVec[i], MtVec[i]);
				objs.emplace_back(o);
			}
		}
		new (this)AnaObjs(objs);
	};

	AnaObjs(AnalysisObjectType type, std::vector<TLorentzVector> tlvVec, std::vector<int> chargeVec, std::vector<int> idVec) {
		AnaObjs objs;
		if (tlvVec.size() != chargeVec.size() != idVec.size())
		{
			std::cerr << "Warnning! There is a event which the vectors size are different!!! Please Check!! Will return an empty Object collection" << std::endl;
		}
		else
		{
			for (unsigned int i = 0; i < tlvVec.size(); i++)
			{
				AnaObj o(type, tlvVec[i], chargeVec[i], idVec[i]);
				objs.emplace_back(o);
			}
		}
		new (this)AnaObjs(objs);
	};

	AnaObj& operator[](std::vector<AnaObj>::size_type ii) { return this->at(ii); };
	const AnaObj& operator[](std::vector<AnaObj>::size_type ii) const { return this->at(ii); };
	void sortByPt() {
		std::sort(this->begin(), this->end(), [](AnaObj a, AnaObj b) {return a.Pt() > b.Pt(); });
	}
	AnaObjs selectObjs(float ptCut, float etaCut = 1e10, int idCut = 0);
};

AnaObjs operator+(const AnaObjs& lhs, const AnaObjs& rhs) {
	AnaObjs combined;
	for (const auto &cand : lhs)
		combined.emplace_back(cand);
	for (const auto &cand : rhs)
		combined.emplace_back(cand);
	combined.sortByPt();
	return combined;
}

AnaObjs AnaObjs::selectObjs(float ptCut, float etaCut, int idCut) {
	AnaObjs newObjs;
	for (auto obj : *this)
	{
		if (obj.Pt() > ptCut && fabs(obj.Eta()) <= etaCut && obj.id() == (obj.id() | idCut))
		{
			newObjs.emplace_back(obj);
		}
	}
	return newObjs;
}

void printObject(const AnaObj& obj) {
	std::cout << " Pt: " << obj.Pt() << " Phi: " << obj.Phi() << " Eta:" << obj.Eta()
		<< " charge: " << obj.charge() << " id: " << obj.id() << " type:" << obj.type() << std::endl;
}

void printObjects(const AnaObjs& objs) {
	int ii = 0;
	for (const auto& obj : objs) {
		std::cout << " " << ii;
		printObject(obj);
		ii++;
	}
}

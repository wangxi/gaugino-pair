#pragma once
#include <TH1.h>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <functional>
#include <vector>
#include <string>
#include <algorithm>
#include <map>
#include <cstdlib>
#include <math.h>
#include <TFile.h>
#include "Hist.h"
#include "Utils.h"
#include <TTree.h>

//Will store all of the information we needed for a single cut
class CutIterator {
public:
	double sum;
	double err;
	std::string name;
	double rate;
	bool passed;
	std::function<bool()> cut;
    double trigweight =1;
	//All single cut contains N-1 plot for this cut, plots *before* this cut and other plots *after* this cut.
	Hist* hist_N_1;
	Hist* hist_beforeCut;
	std::vector<Hist*> hists;

	CutIterator(std::function<bool()> cut) {
		sum = 0;
		err = 0;
		rate = 100;
		passed = true;
		this->cut = cut;
		hist_N_1 = nullptr;
		hist_beforeCut = nullptr;
	};

	//Set N_1 plot for this cut
	void set_N_1_Hist(TH1* hist, std::function<double()> fillPosition, bool useOverflow = false,  std::function<double()> trig_weight = [&] {return 1;}) {
		hist_N_1 = new Hist(hist, fillPosition, useOverflow);
	//	trigweight = (trigweight !=1)? trigweight:trig_weight;
		trigweight = trig_weight();
	}
	//Set before cut plot for this cut
	void set_Before_Hist(TH1* hist, std::function<double()> fillPosition, bool useOverflow = false,  std::function<double()> trig_weight = [&] {return 1;}) {
		hist_beforeCut = new Hist(hist, fillPosition, useOverflow);
//		trigweight = (trigweight !=1)? trigweight:trig_weight;
		trigweight = trig_weight();
	}
	//Add other hists if you want to use
	void addHist(TH1* hist, std::function<double()> fillPosition, bool useOverflow = false,  std::function<double()>  trig_weight  = [&] {return 1;}) {
		hists.emplace_back(new Hist(hist, fillPosition, useOverflow,trig_weight));
	//	trigweight = (trigweight !=1)? trigweight:trig_weight;
		trigweight = trig_weight();
	}

	void addHist(std::string histname, int nBin, double binStart, double binEnd, std::function<double()> fillPosition, bool useOverflow = false,  std::function<double()> trig_weight  = [&] {return 1;}) {
		hists.emplace_back(new Hist(histname, nBin, binStart, binEnd, fillPosition, useOverflow,trig_weight));
//		trigweight = (trigweight !=1)? trigweight:trig_weight;
		trigweight = trig_weight();
	}

	~CutIterator() {
		if (hist_N_1 != nullptr) {
			delete hist_N_1;
			hist_N_1 = nullptr;
		}
		if (hist_beforeCut != nullptr) {
			delete hist_beforeCut;
			hist_beforeCut = nullptr;
		}
		for (auto hist : hists)
		{
			if (hist != nullptr) {
				delete hist;
				hist = nullptr;
			}
		}
	};
};

class Cutflow
{
public:
	static const bool SHOW_RATIO = true;
	static const bool NO_RATIO = false;

	static const bool SHOW_HIST = true;
	static const bool NO_HIST = false;

	static const int CSV = 0;
	static const int LATEX = 1;

	Cutflow() { 
		Cutflow("");
		willFillTree = false;
		outFile = new TFile("out.root", "recreate");
	};
	//If the program will deal with many files, you can set a name (like filename) to distinguish them
	Cutflow(std::string name) {
		willFillTree = false;

		//check if str end with ".root"
		std::string s = ".root";
		std::string::size_type idx = name.rfind(s);
		if ( idx != std::string::npos && name.size() == idx + s.size()){					// if yes
			outFile = new TFile(name.c_str(), "recreate");
			Name = name.erase(idx, name.size());
		}else {																				// if not
			Name = name;
			outFile = new TFile((name + ".root").c_str(), "recreate");
		}
	}
	~Cutflow() {
		outFile->Close();
		for (auto cut : CutQueue) {
			if (cut != nullptr) {
				delete cut;
				cut = nullptr;
			}
		}
		if (outFile != nullptr) {
			delete outFile;
			outFile = nullptr;
		}
	}

	void checkCutName(std::string cutname);
	//Here we return Object CutIterator so we can further do something for this cut step. Such as adding more histogram
	CutIterator* registerCut(std::string cutname, std::function<bool()> cut,std::function<double()> trig_weight = [&] {return 1;} );
	CutIterator* registerCut(std::string cutname, std::function<bool()> cut, std::string histname, int nBin, double binStart, double binEnd, std::function<double()> fillPosition, bool useOverflow = false, std::function<double()> trig_weight = [&] {return 1;});

	void setWeight(std::function<double()> weight) {
		this->weight = weight;
	}
	void setFillTree(TTree* tree){
		this->outTree = tree;
		this->willFillTree = true;
	}

	void startCut(bool willFillHist = SHOW_HIST);
	void PrintOut(bool CoutRatio = NO_RATIO, int OutputStyle = CSV);
	void PrintOut(std::string name, bool CoutRatio = NO_RATIO, int OutputStyle = CSV);

private:
	void PrintOutCsv(std::string name, bool CoutRatio = NO_RATIO);
	void PrintOutLatex(std::string name, bool CoutRatio = NO_RATIO);

	std::vector<CutIterator*> CutQueue;
	std::string Name;
	std::function<double()> weight;
	std::function<bool()> finalCut;
	TFile* outFile;
	TTree* outTree;
	bool willFillTree;
};

void Cutflow::checkCutName(std::string cutname) {
	for (unsigned int i = 0; i < CutQueue.size(); i++)
	{
		if (cutname == CutQueue.at(i)->name) {
			std::cerr << "In case you regster a cut into a loop which may cause a disater in mermory,"
				<< "You can't register two cut with same name" << std::endl
				<< "The wrong cut name is: " << cutname << std::endl;
			std::exit(-3);
		}
	}
}

//if disabled histogram fill
CutIterator* Cutflow::registerCut(std::string cutname, std::function<bool()> cut,std::function<double()> trig_weight) {
	checkCutName(cutname);
	CutIterator* newCut = new CutIterator(cut);
	newCut->name = cutname;
	newCut->trigweight = trig_weight();
	CutQueue.emplace_back(newCut);
	return newCut;
}

//if enabled histogram fill
 CutIterator* Cutflow::registerCut(std::string cutname, std::function<bool()> cut, std::string histname, int nBin, double binStart, double binEnd, std::function<double()> fillPosition, bool useOverflow, std::function<double()> trig_weight) {
	checkCutName(cutname);
	CutIterator* newCut = new CutIterator(cut);
	newCut->set_Before_Hist(new TH1F(histname.c_str(), histname.c_str(), nBin, binStart, binEnd), fillPosition, useOverflow, trig_weight);
	std::string histname_N_1 = histname + "_N_1";
	newCut->set_N_1_Hist(new TH1F(histname_N_1.c_str(), histname_N_1.c_str(), nBin, binStart, binEnd), fillPosition, useOverflow, trig_weight);
	newCut->name = cutname;
	newCut->trigweight = trig_weight();
	CutQueue.emplace_back(newCut);
	return newCut;
}

void Cutflow::startCut(bool willFillHist) {
	int falseTimes(0), falsePosition(0);
	double fillNumber = weight();
	double weight_event = fillNumber;
    double triwei = 1;
	for (unsigned int i = 0; i <CutQueue.size(); i++)
	{
	for (unsigned int k = 0; k <= i; k++)
	{
		if (CutQueue[k]->trigweight !=1)
			triwei = CutQueue[k]->trigweight;
//		triwei = (CutQueue[k]->trigweight !=1)? CutQueue[k]->trigweight:1;
//		cout<<"triwei  "<<triwei<<" K "<<k<<endl;
    }
//	   weight_event = fillNumber*triwei;
	   weight_event = fillNumber;
//	   cout<<"fillNumber  "<<fillNumber<<"  trigweight  "<< CutQueue[i]->trigweight<<endl;
		//Fill hists before this cut if doesn't fail previous cuts and we have defined hist for this cut
		if (willFillHist && CutQueue[i]->hist_beforeCut != nullptr && falseTimes == 0) {
			CutQueue[i]->hist_beforeCut->Fill(weight_event);
		}
		//Do cut
		if (CutQueue[i]->cut())
		{
			//Check if it fails previous cuts
			if (falseTimes == 0)
			{
				CutQueue[i]->sum += weight_event;
				CutQueue[i]->err = sqrt(pow(CutQueue[i]->err,2) + pow(weight_event, 2));

				//if enable fill hist, we will fill cutflow histogram
				auto hists = CutQueue[i]->hists;
				if (willFillHist && hists.size() > 0)
				{
					for (unsigned int i = 0; i < hists.size(); i++)
					{
						hists[i]->Fill(weight_event);
					} 
				}
			}
		} else if (!willFillHist) {
			//if willFillHist set to 'false' particularly, it means we won't fill histgram and we won't need
			//to do following thing. So we just return to save time
			return;
		} else {
			falseTimes++;
			falsePosition = i;
		}
	}

	//fill N-1 Histogram
	//If this event fails >=2 cut, there is no any historgram need to fill
	if (falseTimes < 2 && willFillHist)
	{
		//If this event fails 1 cut, we noly need to fill that historgram
		if (falseTimes == 1) {
			if (CutQueue[falsePosition]->hist_N_1 != nullptr){
				CutQueue[falsePosition]->hist_N_1->Fill(fillNumber);
			}
		}
		//If all pass, will Fill all of the N-1 hists we defined previously. Besides
		else{
			for (unsigned int i = 0	; i < CutQueue.size(); i++)
			{
				if (CutQueue[i]->hist_N_1 != nullptr) {
					CutQueue[i]->hist_N_1->Fill(fillNumber);
				}
			}
		}
	}

	//Fill output tree if pass all of the cuts
	if (falseTimes == 0 && willFillTree) {
		outTree->Fill();
	}
}

void Cutflow::PrintOut(bool CoutRatio, int OutputStyle) {
	PrintOut("cutflow",CoutRatio,OutputStyle);
}

void Cutflow::PrintOut(std::string name, bool coutRatio, int OutputStyle) {
	if (OutputStyle == CSV){
		PrintOutCsv(name, coutRatio);
	}else if (OutputStyle == LATEX) {
		PrintOutLatex(name, coutRatio);
	}else {
		std::cerr << "Unknown output type. Currrently only support CSV, LATEX. Choose CSV as default." << std::endl;
		PrintOutCsv(name, coutRatio);
	}
	outFile->Write();

}

void Cutflow::PrintOutCsv(std::string name, bool coutRatio) {
	unsigned int i;
	std::string outname = name + ".csv";
	std::ofstream cutcout(outname, std::ios::app);
	std::ofstream yield("yield.csv", std::ios::app);

	//print out cutflow
	cutcout << "Cut name, " << CutQueue[0]->name << ", ";
	for (i = 1; i < CutQueue.size() - 1; i++)
	{
		CutQueue[i]->rate = (CutQueue[i]->sum) / (CutQueue[i - 1]->sum) * 100;
		cutcout << CutQueue[i]->name << ", ";
	}
	CutQueue[i]->rate = (CutQueue[i]->sum) / (CutQueue[i - 1]->sum) * 100;
	cutcout << CutQueue[i]->name << std::endl;

	std::string writeName = (Name != "") ? Name : "Event remain";

	cutcout << writeName << ", ";
	for (i = 0; i < CutQueue.size() - 1; i++)
	{
		Utils::roundCSV(cutcout, CutQueue[i]->sum, CutQueue[i]->err);
		cutcout << " , ";
	}
	Utils::roundCSV(cutcout, CutQueue[i]->sum, CutQueue[i]->err);
	cutcout <<  '\n' << std::endl;

	//if enable ratio print
	if (coutRatio)
	{
		cutcout << "Ratio, ";
		for (i = 0; i < CutQueue.size() - 1; i++)
		{
			cutcout << std::fixed << std::setprecision(2) << CutQueue[i]->rate << "%, ";
		}
		cutcout << std::fixed << std::setprecision(2) << CutQueue[i]->rate << "%" << std::endl;
	}

	//printout yield
	auto it = *CutQueue.rbegin();
	yield << writeName << " , "; 
	Utils::roundCSV(yield, it->sum, it->err);
	yield <<std::endl;

}

void Cutflow::PrintOutLatex(std::string name, bool coutRatio) {
	//Todo
	unsigned int i;
	std::string outname = name + ".tex";
	std::ofstream cutcout(outname, std::ios::app);
	std::ofstream yield("yield.tex", std::ios::app);

	//print out cutflow
	cutcout << "Cut name  &  " << CutQueue[0]->name << "  &  ";
	for (i = 1; i < CutQueue.size() - 1; i++)
	{
		CutQueue[i]->rate = (CutQueue[i]->sum) / (CutQueue[i - 1]->sum) * 100;
		cutcout << CutQueue[i]->name << "  &  ";
	}
	CutQueue[i]->rate = (CutQueue[i]->sum) / (CutQueue[i - 1]->sum) * 100;
	cutcout << CutQueue[i]->name << "  \\\\ " << std::endl;

	std::string writeName = (Name != "") ? Name : "Event remain";

	cutcout << writeName << "  &  ";
	for (i = 0; i < CutQueue.size() - 1; i++)
	{
		Utils::roundLatex(cutcout, CutQueue[i]->sum, CutQueue[i]->err);
		cutcout << "  &  ";
	}
	Utils::roundLatex(cutcout, CutQueue[i]->sum, CutQueue[i]->err);

	//if enable ratio print
	if (coutRatio)
	{
		cutcout << "Ratio  &  ";
		for (i = 0; i < CutQueue.size() - 1; i++)
		{
			cutcout << std::fixed << std::setprecision(2) << CutQueue[i]->rate << "%  &  ";
		}
		cutcout << std::fixed << std::setprecision(2) << CutQueue[i]->rate << "%  \\\\" << std::endl;
	}

	//printout yield
	auto it = *CutQueue.rbegin();
	yield << writeName << "  &  ";
	Utils::roundLatex(yield, it->sum, it->err);
	yield << "  \\\\" << std::endl;
}

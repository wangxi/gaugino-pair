#pragma once

#include <TLorentzVector.h>
#include "AnaObjs.h"
#include "mt2_bisect.h"
#include <RooStats/RooStatsUtils.h>

class PhyUtils
{
public:
	PhyUtils();
	/*
	*Notice: For functions like xxMin(), xxMax(), one should provide an AnaObjs collection with all objs you want to cal in it. 
	*For example, if you want to calculate MT2Max for tau container, you can use MT2Max(tauContainers, met);
	*If you want calculate MT2Max for tau and electron container, you can ues MT2Max(tauContainers + eleContainers, met);
	*/
	//DeltaR, DeltaPhi
//	static inline double deltaR(double eta1, double phi1, double eta2, double phi2); -> disabled because it is recommended to use rapidity not eta to calculate the deltaR
	static inline double deltaR(const TLorentzVector& v1, const TLorentzVector& v2);
	static inline double deltaPhi(double phi1, double phi2);
	static inline double deltaPhi(const TLorentzVector &o1, const TLorentzVector& o2);
	static double minDphi(const AnaObjs& cands, double ptCut = 0);
	static double minDR(const AnaObjs& cands, double ptCut = 0);

	//MCT,MT,Mll
	static inline double calcMCT(double Et1, double pt1, double phi1, double Et2, double pt2, double phi2);
	static inline double calcMCT(const TLorentzVector& v1, const TLorentzVector& v2);
	static inline double calcMT(double pt1, double phi1, double pt2, double phi2);
	static inline double calcMT(const TLorentzVector &lepton, const TLorentzVector &met);
	static double calcMTmin(const AnaObjs& cands, const AnaObj& met);
	static inline double calcMll(double pt1, double eta1, double phi1, double E1,
		double pt2, double eta2, double phi2, double E2);

	static inline double calcMll(TLorentzVector tlv1, TLorentzVector tlv2);

	//MT2
	static double MT2(double pt1, double eta1, double phi1, double E1,
					double pt2, double eta2, double phi2, double E2,
					double missET, double missphi);
	static double MT2(double pt1, double eta1, double phi1, double E1,
					double pt2, double eta2, double phi2, double E2,
					double pxmiss, double pymiss, double minv);
	static double MT2(const TLorentzVector &o1, const TLorentzVector &o2, const TLorentzVector &met, double minv = 0);
//	static double MT2Max(const AnaObjs& cands, const TLorentzVector &met, double minv = 0);
    static double MT2Max( vector<TLorentzVector> cands, const TLorentzVector &met, double minv =0);

	//Top Tagger related
	static bool toptag0j(TLorentzVector v1l,TLorentzVector v2l, Double_t met, Double_t evt_MET_phi);
	static bool toptag2j(double ptjcut, double meffcut,
                TLorentzVector v1l,TLorentzVector v2l ,
                TLorentzVector v1j, TLorentzVector v2j,
		        Double_t met, int iopt,Double_t evt_MET_phi );

	static int GetTopPairs(double ptjcut, double meffcut, int njetscan,
                TLorentzVector v1l,TLorentzVector v2l,
				std::vector<TLorentzVector> jets, double_t met,double_t evt_met_phi);

	//Calculate Zn
	double calZn(double signalExp, double backgroundExp, double relativeBkgUncert);

	//Other useful utils
	static inline double calWeight(double lumi, double Xsection, double Nnorm, double extraWeight = 1);

	~PhyUtils();

private:

};

PhyUtils::PhyUtils()
{
}

PhyUtils::~PhyUtils()
{
}

/********************************************************
 ******************DeltaR and DeltaPhi*******************
 ********************************************************/
double PhyUtils::deltaPhi(double phi1, double phi2){
	double diffphi = fabs(phi1 - phi2);
	return (diffphi > M_PI)?fabs(2*M_PI - diffphi):diffphi;
}

double PhyUtils::deltaPhi(const TLorentzVector& v1, const TLorentzVector& v2){
	double diffphi = fabs(v1.Phi() - v2.Phi());
	return (diffphi > M_PI)?fabs(2*M_PI - diffphi):diffphi;
}
/*
double PhyUtils::deltaR(double eta1, double phi1, double eta2, double phi2){
	double diffphi = fabs(phi1 - phi2);
	while (diffphi > M_PI) diffphi = 2*M_PI - diffphi;
	return sqrt(pow((eta1 - eta2) ,2) + pow(diffphi ,2));
}
*/
double PhyUtils::deltaR(const TLorentzVector& v1, const TLorentzVector& v2){
	double diffphi = fabs(v1.Phi() - v2.Phi());
	while (diffphi > M_PI) diffphi = fabs(2*M_PI - diffphi);
	return sqrt(pow((v1.Rapidity() - v2.Rapidity()) ,2) + pow(diffphi ,2));;
}

double PhyUtils::minDphi(const AnaObjs& cands, double ptCut) {
	double dPhiMin = 1e10;
	if (cands.size() < 2) {
		return dPhiMin;
	}
	for (unsigned int i = 1; i < cands.size(); i++){
		if (cands[i].Pt() < ptCut) continue;
		for (unsigned int j = 0; j < i; j++) {
			if (cands[j].Pt() < ptCut) continue;
			double dPhiTmp = deltaPhi(cands[i], cands[j]);
			if (dPhiMin > dPhiTmp){
				dPhiMin = dPhiTmp;
			}
		}
	}
	return dPhiMin;
}

double PhyUtils::minDR(const AnaObjs& cands, double ptCut) {
	double dRMin = 1e10;
	if (cands.size() < 2) {
		return dRMin;
	}
	for (unsigned int i = 1; i < cands.size(); i++) {
		if (cands[i].Pt() < ptCut) continue;
		for (unsigned int j = 0; j < i; j++) {
			if (cands[j].Pt() < ptCut) continue;
			double dRTmp = deltaR(cands[i], cands[j]);
			if (dRMin > dRTmp) {
				dRMin = dRTmp;
			}
		}
	}
	return dRMin;
}


/********************************************************
 ************************MCT, MT, Mll********************
 ********************************************************/
double PhyUtils::calcMCT(double Et1, double pt1, double phi1, double Et2, double pt2, double phi2){
	double px1 = pt1 * cos(phi1);
	double py1 = pt1 * sin(phi1);
	double px2 = pt2 * cos(phi2);
	double py2 = pt2 * sin(phi2);
	double mCT = pow(Et1 + Et2, 2) - pow(px1 - px2, 2) - pow(py1 - py2, 2);
	return (mCT >= 0.) ? sqrt(mCT) : sqrt(-mCT);
}

double PhyUtils::calcMCT(const TLorentzVector& v1, const TLorentzVector& v2){
    double mCT = pow(v1.Et() + v2.Et(), 2) - pow(v1.Px() - v2.Px(), 2) - pow(v1.Py() - v2.Py(), 2);
    return (mCT >= 0.) ? sqrt(mCT) : sqrt(-mCT);
}

double PhyUtils::calcMT(double pt1, double phi1, double pt2, double phi2){
	double mT = 2 * pt1 * pt2 * (1 - TMath::Cos(phi1 - phi2));
	return (mT >= 0.) ? sqrt(mT) : sqrt(-mT);
}

double PhyUtils::calcMT(const TLorentzVector &lepton, const TLorentzVector &met){
    double mT = 2 * lepton.Pt() * met.Et() * (1 - TMath::Cos(lepton.Phi() - met.Phi()));
    return (mT >= 0.) ? sqrt(mT) : sqrt(-mT);
}

double PhyUtils::calcMTmin(const AnaObjs& cands, const AnaObj& met) {
	double mtmin = 0;
	if (cands.size() < 1) {
		return 0;
	}
	for (unsigned int i = 0; i < cands.size(); i++) {
		double mtTmp = calcMT(cands[i], met);
		if (mtmin < mtTmp) {
			mtmin = mtTmp;
		}
	}
	return mtmin;
}

double PhyUtils::calcMll(double pt1, double eta1, double phi1, double E1,
	double pt2, double eta2, double phi2, double E2) {
	TLorentzVector tlv1, tlv2;
	tlv1.SetPtEtaPhiE(pt1, eta1, phi1, E1);
	tlv2.SetPtEtaPhiE(pt2, eta2, phi2, E2);
	return (tlv1 + tlv2).M();
}

double PhyUtils::calcMll(TLorentzVector tlv1, TLorentzVector tlv2) {
	return (tlv1 + tlv2).M();
}

/********************************************************
 ***************************MT2**************************
 ********************************************************/
double PhyUtils::MT2(const TLorentzVector& o1, const TLorentzVector& o2, const TLorentzVector& met, double minv){
	double pa[3] = { o1.M(), o1.Px(), o1.Py() };
	double pb[3] = { o2.M(), o2.Px(), o2.Py() };
	double pmiss[3] = { 0, met.Px(), met.Py() };
	double mn = minv;
	mt2_bisect::mt2 mt2_event;
	mt2_event.set_momenta(pa,pb,pmiss);
	mt2_event.set_mn(mn);
	return mt2_event.get_mt2();
}

double PhyUtils::MT2(double pt1, double eta1, double phi1, double E1,
 		 double pt2, double eta2, double phi2, double E2,
 		 double missET, double missphi){
	double pxmiss = missET * cos(missphi);
	double pymiss = missET * sin(missphi);
	return MT2(pt1, eta1, phi1, E1, pt2, eta2, phi2, E2, pxmiss, pymiss, 0);
}

double PhyUtils::MT2(double pt1, double eta1, double phi1, double E1,
		 double pt2, double eta2, double phi2, double E2,
		 double pxmiss, double pymiss, double minv){
	TLorentzVector tlv_1;
	TLorentzVector tlv_2;
	tlv_1.SetPtEtaPhiE(pt1,eta1,phi1,E1);
	tlv_2.SetPtEtaPhiE(pt2,eta2,phi2,E2);
	double pa[3] = { tlv_1.M(), tlv_1.Px(), tlv_1.Py() };
	double pb[3] = { tlv_2.M(), tlv_2.Px(), tlv_2.Py() };
	double pmiss[3] = { 0, pxmiss, pymiss };
	double mn = minv;
	mt2_bisect::mt2 mt2_event;
	mt2_event.set_momenta(pa,pb,pmiss);
	mt2_event.set_mn(mn);
	return mt2_event.get_mt2();
}

//double PhyUtils::MT2Max(const AnaObjs& cands, const TLorentzVector &met, double minv) {
double PhyUtils::MT2Max( vector<TLorentzVector> cands, const TLorentzVector &met, double minv) {
	double mt2 = 0;
	if (cands.size() < 2){
		return 0;
	}
	for (unsigned int i = 1; i < cands.size(); i++){
		for (unsigned int j = 0; j < i; j++) {
			double mt2Tmp = MT2(cands[i], cands[j], met, minv);
			if (mt2 < mt2Tmp){
				mt2 = mt2Tmp;
			}
		}
	}
	return mt2;
}

double PhyUtils::calWeight(double lumi, double Xsection, double Nnorm, double extraWeight){
	return lumi * Xsection * extraWeight / Nnorm;
}

double PhyUtils::calZn(double signalExp, double backgroundExp, double relativeBkgUncert)
{
	double mainInf = signalExp + backgroundExp;  //Given
	double tau = 1. / backgroundExp / (relativeBkgUncert*relativeBkgUncert);
	double auxiliaryInf = backgroundExp * tau;  //Given

	double P_Bi = TMath::BetaIncomplete(1. / (1. + tau), mainInf, auxiliaryInf + 1);
	return RooStats::PValueToSignificance(P_Bi);
}
/**************************************************************
 * *******************Top Tagger Related **********************
 * ***********************************************************/
bool PhyUtils::toptag0j(TLorentzVector v1l,TLorentzVector v2l, Double_t met, Double_t evt_MET_phi) {

    const Double_t wmass = 80400.;
	TLorentzVector vecMet;
	vecMet.SetPtEtaPhiM(met,0,evt_MET_phi,0);
	TLorentzVector vecAll = v1l + v2l + vecMet;

    double mctll=calcMCT(v1l,v2l);

    double rr=vecAll.Pt()/(2.*wmass);
    double fact=rr+sqrt(1+rr*rr);

    //cout << "mctll " << mctll << " cut value " << wmass*fact << std::endl;
    return mctll < wmass*fact ? true: false;
}

bool PhyUtils::toptag2j(double ptjcut, double meffcut,
                TLorentzVector v1l,TLorentzVector v2l ,
                TLorentzVector v1j, TLorentzVector v2j,
        Double_t met, int iopt,Double_t evt_MET_phi ) {
        //
        static const Double_t tmass = 172500.;
        static const Double_t mljcut = 155000.;    // The max mass l+j could get from a top decay
        static const Double_t mctjcut = 137000.;
        //
        TLorentzVector vjb[2];
        double ml1j[2];
        double ml2j[2];
        //
    double metx = met*cos(evt_MET_phi);  double mety = met*sin(evt_MET_phi);
        double ptl1=v1l.Pt();   double ptl2=v2l.Pt();
        double ptj1=v1j.Pt();   double ptj2=v2j.Pt();
    double pxus=v1l.Pt()*cos(v1l.Phi())+v2l.Pt()*cos(v2l.Phi())+metx;
    double pyus=v1l.Pt()*sin(v1l.Phi())+v2l.Pt()*sin(v2l.Phi())+mety;
        double mefftop=ptj1+ptj2+ptl1+ptl2;

        if(ptj2 < ptjcut) return false;
        if(iopt == 0 && mefftop < meffcut) return false;

        // First part:
        //-------------
        vjb[0]=v1j;
        vjb[1]=v2j;
        double mctjj=calcMCT(vjb[0],vjb[1]);
    double pxusjl=vjb[0].Pt()*cos(vjb[0].Phi())+vjb[1].Pt()*cos(vjb[1].Phi())+pxus;
    double pyusjl=vjb[0].Pt()*sin(vjb[0].Phi())+vjb[1].Pt()*sin(vjb[1].Phi())+pyus;
        double rrj=sqrt(pxusjl*pxusjl+pyusjl*pyusjl)/(2.*tmass);
        double factj=rrj+sqrt(1+rrj*rrj);

        bool imct = mctjj < mctjcut*factj ? true : false;
        if(iopt==1) return imct;

        // Second part:
        //--------------
        for(int i=0;i<2;++i) {
                ml1j[i]=(v1l+vjb[i]).M();
                ml2j[i]=(v2l+vjb[i]).M();
        }
        int ncou=0;
        int icou1[2];
        int icou2[2];
        for(int i=0;i<2;++i) {
                for(int j=0;j<2;++j) {
                        if(i != j) {
                                if(ml1j[i]<mljcut && ml2j[j]<mljcut) {
                                        icou1[ncou]=i;
                                        icou2[ncou]=j;
                                        ncou++;
                                }
                        }
                }
        }
        bool imjl = ncou>0 ? true : false;

        // Third part: finally mct(ql,ql) for each coupling passing the mjl cut
        //-------------exploit the dependence of mct((jl),(jl)) on the maximum
        //             mass^2 of the two jl combinations 
        int ngcou=0;
        for(int i=0;i<ncou;++i) {

                int ij1=icou1[i];
                int ij2=icou2[i];
                TLorentzVector vtot1=vjb[ij1]+v1l;
                TLorentzVector vtot2=vjb[ij2]+v2l;
                double mctjl=calcMCT(vtot1,vtot2);
                double mm1=(vjb[ij1]+v1l).M();
                double mm2=(vjb[ij2]+v2l).M();
                double mmax2 = mm1>mm2 ? (mm1*mm1) : (mm2*mm2);
                double upl=mmax2/(tmass)+(tmass);
                //cout << " i " << " mctjl " << mctjl << " mmax2 " << mmax2 << 
                //" upl " << upl << endl;
                if(mctjl < upl*factj )  ngcou++;
        }

        bool imctjl = ngcou>0 ? true : false;
        //cout << " ll " << imctll << " ct " << imct << " jl " << imjl <<
        //" ctlj " << imctjl << endl;

        return imct & imjl & imctjl;
}

int PhyUtils::GetTopPairs(double ptjcut, double meffcut, int njetscan,
                TLorentzVector v1l,TLorentzVector v2l,
        std::vector<TLorentzVector> jets, Double_t met,Double_t evt_MET_phi) {

        // iopt = 0 - paper-like algorithm
        //      = 1 - consider only combinations including the most energetic jet
        int iopt = 0;

//        bool itopll = toptag0j(v1l, v2l, met, evt_MET_phi);
//        if(!itopll) return 0;

        bool itop=false;
        int npairs = 0;
        int njet=jets.size();
        if(njet<njetscan) njetscan=njet;

        for (int i1=0; i1<njetscan-1; ++i1) {
                if(i1 == 0 || iopt == 0){
                        for (int i2=i1+1; i2<njetscan; ++i2) {
                                TLorentzVector v1j=jets.at(i1);
                                TLorentzVector v2j=jets.at(i2);
                                itop = toptag2j(ptjcut, meffcut, v1l, v2l, v1j, v2j, met, iopt, evt_MET_phi);
                                if(itop)npairs++;
                        }
                }
        }
        return npairs;
}


#pragma once
#include <iostream>
#include <thread>
#include <atomic>
#include <vector>
#include <mutex>

/****************Thread Pool********************
*A class which manage a task pool and a worker pool.
*The worker pool will take out the tasks from the worker pool
* and run them in parallel
*/
class threadPool
{
	using Task = std::function<void()>;
private:
	std::vector<std::thread> taskPool;
	std::vector<Task> workerPool;
	std::mutex m_lock;							//synchronous lock
	std::condition_variable cv_task;			//Blocking condition
	bool isStoped;								//If it's stopped. 
	std::atomic<unsigned int> idlThreadNum;				//Idle thread num. Must use atomic to avoid potential synchronous bug
	const int MAX_THREAD_NUM = 32;				// Max Thread Num
	const bool FORCE_STOP = true;

public:
	threadPool(unsigned int threadNum = 8);
	~threadPool();
	//Stop all jobs. Will wait all running jobs done by default. If 'forceStop' is set to true, will abandon all running jobs.
	void stop(bool forceStop = false) {
		isStoped = true;
		if (forceStop) {
			for (std::thread& thread : taskPool) {
				thread.detach();
			}
		}else{
			for (std::thread& thread : taskPool) {
				if (thread.joinable())	thread.join();
			}
		}
	}
	//Only set the flag and don't do anything to the running jobs
	void setFlagStop() {
		isStoped = true;
	}
	void reStart() {
		isStoped = false;
	}
	unsigned int getidlThreadNum() {
		return idlThreadNum;
	}

};

threadPool::threadPool(unsigned int num)
{
	this->isStoped = false;
	if (num < 1) {
		num = 1;
	}
	else if (num > MAX_THREAD_NUM) {
		std::cout << "ThreadNum too large, set to Max: " << this->MAX_THREAD_NUM << std::endl;
		num = MAX_THREAD_NUM;
	}
	this->idlThreadNum = num;
	for (unsigned int i = 0; i < num; i++)
	{
		taskPool.emplace_back();
	}

}

threadPool::~threadPool()
{
}

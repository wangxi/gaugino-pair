#pragma once

#include "AnaObjs.h"
#include "OutputHandler.h"

/**************************************************************
**********Steal and modify from SimpleAnalysis package*********
****https://gitlab.cern.ch/atlas-phys-susy-wg/SimpleAnalysis***
**************************************************************/

class AnaEvent
{
public:
	AnaEvent(const std::string &name) : _name(name), _output(0) { getAnalysisList()->emplace_back(this); };
	AnaEvent() {};
	virtual void Init() {};
	virtual void Final() {};
	virtual ~AnaEvent() {};
	virtual const std::string& name() { return _name; };
	virtual void setOutput(OutputHandler *output) { _output = output; };
	OutputHandler* getOutput() { return _output; };
	virtual void addRegion(const std::string &label) { _output->addEntry(label); };
	virtual void addRegions(const std::vector<std::string> &labels) { _output->addEntries(labels); };
	virtual void addHistogram(const std::string &label, int bins, float min, float max) { _output->addHistogram(label, bins, min, max); };
	virtual void addHistogram(const std::string &label, int bins, float *edges) { _output->addHistogram(label, bins, edges); }
	virtual void addHistogram(const std::string &label, std::vector<float> &edges) { _output->addHistogram(label, edges); }
	virtual void addHistogram(const std::string &label, int binsX, float minX, float maxX,
		int binsY, float minY, float maxY) {
		_output->addHistogram(label, binsX, minX, maxX, binsY, minY, maxY);
	};
	virtual void accept(const std::string &name, double weight = 1) { _output->pass(name, weight); };
	virtual void fill(const std::string &name, double x) { _output->fillHistogram(name, x); };
	virtual void fill(const std::string &name, double x, double y) { _output->fillHistogram(name, x, y); };
	void adjustEventWeight(double weight) { _output->adjustEventWeight(weight); };
	void ntupVar(const std::string &label, int value) { _output->ntupVar(label, value); };
	void ntupVar(const std::string &label, float value) { _output->ntupVar(label, value); };
	void ntupVar(const std::string &label, double value) { _output->ntupVar(label, (float)value); };
	void ntupVar(const std::string &label, std::vector<int> values) { _output->ntupVar(label, values); };
	void ntupVar(const std::string &label, std::vector<float> values) { _output->ntupVar(label, values); };
	void ntupVar(const std::string &label, AnaObj &object, bool saveMass = false, bool saveType = false, bool saveVtx = false);
	void ntupVar(const std::string &label, AnaObjs &objects, bool saveMass = false, bool saveType = false, bool saveVtx = false);


	static float calcMTauTau(const AnaObj &o1, const AnaObj &o2, const AnaObj &met);

	// Helper functions
	std::vector<float> fakeJER(const AnaObjs &jets);
	static AnaObjs overlapRemoval(const AnaObjs &cands,
		const AnaObjs &others,
		float deltaR, int passId = 0);

	static AnaObjs overlapRemoval(const AnaObjs &cands,
		const AnaObjs &others,
		std::function<float(const AnaObj&, const AnaObj&)> radiusFunc,
		int passId = 0);

	static AnaObjs lowMassRemoval(const AnaObjs & cand, std::function<bool(const AnaObj&, const AnaObj&)>, float MinMass = 0, float MaxMass = FLT_MAX, int type = -1);

	static AnaObjs filterObjects(const AnaObjs& cands,
		float ptCut, float etaCut = 100., int id = 0, unsigned int maxNum = 10000);

	static AnaObjs filterCrack(const AnaObjs& cands, float minEta = 1.37, float maxEta = 1.52);

	static int countObjects(const AnaObjs& cands,
		float ptCut, float etaCut = 100., int id = 0);

	static float sumObjectsPt(const AnaObjs& cands,
		unsigned int maxNum = 10000, float ptCut = 0);

	static float sumObjectsM(const AnaObjs& cands,
		unsigned int maxNum = 10000, float mCut = 0);

	static float aplanarity(const AnaObjs& jets);
	static float sphericity(const AnaObjs& jets);

	static bool IsSFOS(const AnaObj& L, const AnaObj& L1);
	static std::pair<float, float> DiZSelection(const AnaObjs& electrons, const AnaObjs& muons);
	static bool PassZVeto(const AnaObjs& electrons, const AnaObjs &Muons, float Window = 10);

protected:
	std::string _name;
	OutputHandler *_output;

};

#define DefineAnalysis(ANALYSISNAME)					\
  class ANALYSISNAME : public AnaEvent {				\
  public:								\
  ANALYSISNAME() : AnaEvent(#ANALYSISNAME) {};			\
    void Init();							\
    void ProcessEvent(AnalysisEvent *event);				\
  };									\
  static const AnaEvent * ANALYSISNAME_instance __attribute__((used)) = new ANALYSISNAME();

std::string FindFile(const std::string& name);

// debugging functions

void printObject(const AnaObj& obj);

void printObjects(const AnaObjs& objs);

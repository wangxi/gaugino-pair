#pragma once
#include <string>
#include <regex>
#include <vector>
#include <sstream>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <dirent.h>       //------------ temporary solution for filesystem 
//#include<filesystem>    ---------- will be use after upgrading to c++17

class Utils{
public:
	//********* Round the num and err simply***************
	static void roundCSV(std::ofstream& stream, double num, double err);
	static void roundLatex(std::ofstream& stream, double num, double err);
	
	//********* String to double ********************
	static double sTod(const std::string& str);
	
	/********* convert anyType to a specific type *****
	 * To use eg: std::string s = "34553.353";
	 * double d = Tools::convert<double>(s);
	 **************************************************/
	template <class out_type, class in_type> static out_type convert(in_type& src);

	/********* find the position in the list where *****
	 * "value > list[posi] && value <= list[posi + 1]
	 * ************************************************/
	static int positionSearch(double value, std::vector<double> list);

	//******* erase string form a certain str to the end***
	static std::string eraseToEnd(std::string target, std::string src);

	//******* split a string by a char symbol **************
	static std::vector<std::string> splitStrBy(std::string names, char symbol);

	/******* Check if a 'ip' matches the regex 'pattern' **********
	 * Will return a string vector. the vec[0] will be the matched string if match
	 * Then following the captured words if regex contains
	 * Will show the match info if the 'showInfo' is turn on
	 */
	static std::vector<std::string> regexMatch(std::regex pattern, std::string ip, bool showInfo = false);

	//******* select files which name matches the regex in the folder
	static std::vector<std::string> getFiles(std::regex pattern, std::string folder);
	//******* select files which name matches the regex in the folder with the a 
	//******* vector to collect the extra Capture 
	static std::vector<std::string> getFiles(std::regex pattern, std::string folder, std::vector<std::vector<std::string>>& extraCapture);
};

void Utils::roundCSV(std::ofstream& stream, double num, double err) {
	// if err >  0.095, will round the num and err to 2 digist after the point.
	if (err >= 0.095 && err < 9999.945)
	{
		// if num > 9999.945, will not round sum
		if (num >= 9999.945) 
		{
			stream << std::resetiosflags(std::ios::fixed) << std::setprecision(6) << num << " +- " << std::fixed << std::setprecision(2) << err;
		}
		else 
		{
			stream << std::fixed << std::setprecision(2) << num << " +- " <<  err;
		}
	}
	else 
	{
		//else, will be kind of difficult and will round by hand
		stream << std::resetiosflags(std::ios::fixed) << std::setprecision(6) << num << " +- " << err;
	}
}

void Utils::roundLatex(std::ofstream& stream, double num, double err) {
	// if err >  0.095, will round the num and err to 2 digist after the point.
	if (err >= 0.095 && err < 9999.945) {
		// if num > 9999.945, will not round sum
		if (num >= 9999.945) {
			stream << "$  " << std::resetiosflags(std::ios::fixed) << std::setprecision(6) << num << " \\pm " << std::fixed << std::setprecision(2) << err << "  $";
		}
		else {
			stream << std::fixed << std::setprecision(2) << "$  " << num << " \\pm " << err << "  $";
		}
	}
	else {
		//else, will be kind of difficult and will round by hand
		stream << std::resetiosflags(std::ios::fixed) << std::setprecision(6) << "$  " << num << " \\pm " << err << "  $";
	}
}

double Utils::sTod(const std::string& str) {
	double dest;
	std::stringstream ss(str);
	ss >> dest;
	return dest;
}

template <class out_type, class in_type> out_type Utils::convert(in_type& src) {
	std::stringstream ss;
	out_type result;
	ss << src;
	ss >> result;
	return result;
}

int Utils::positionSearch(double value, std::vector<double> list) {
	int right = list.size() - 1;
	int left = 0;

	//first, make sure the value is not larger than list[right] or lower than list[left]
	if (value > list[right]) return right;
	if (value <= list[left]) return -1;

	while (right - left != 1) {
		int mid = (left + right) / 2;
		if (value > list[mid]) {
			left = mid;
		}
		else {
			right = mid;
		}
	}
	return left;
}

std::string Utils::eraseToEnd(std::string target, std::string src){
	int pos = target.rfind(src);
	if(pos > -1){
		int sEnd = target.size();
		return target.erase(pos,sEnd);
	} else{
		std::cout<<"Warnning: target string \""<<target<<"\" dosen't contain \""<<src<<"\" ! Return target string"<<std::endl;
	}
	return target;
}

std::vector<std::string> Utils::splitStrBy(std::string names, char symbol){
	std::vector<std::string> result;
	std::stringstream ss(names);
    while (ss.good()) {
        std::string substr;
        getline(ss, substr, symbol);
        result.emplace_back(substr);
    }
	return result;
}

std::vector<std::string> Utils::regexMatch(std::regex pattern, std::string ip, bool showInfo) 
{
    std::smatch result;
    std::vector<std::string> ret;
    bool valid = std::regex_match(ip,result,pattern); 
	if(showInfo){
		std::cout<<ip<<"  "<<(valid?"valid":"invalid")<<std::endl;
	}
    if(valid)
    {
    	for(unsigned int i = 0; i < result.size(); i++){
        	ret.emplace_back(result[i]);
        }   
	}   
    return ret;
}

std::vector<std::string> Utils::getFiles(std::regex pattern, std::string folder){
	if(folder[folder.length()-1] != '/'){
		folder.append(1,'/');
	}

	std::vector<std::string> strVec;
	DIR* dp;
	dp = opendir(folder.c_str());
	if (dp){
		while (auto file = readdir(dp)){
			std::string filename = (std::string)file->d_name;
			if (filename != "." && filename != ".."){			// ignore '.' and '..'
				std::vector<std::string> result = regexMatch(pattern, filename);
				if(result.size() != 0){
					strVec.emplace_back(folder + result[0]);
				}
			}
		}
		sort(strVec.begin(),strVec.end());
	}else{
		std::cerr << "Dir not found!" << std::endl;
	}
	closedir(dp);
	return strVec;
}

std::vector<std::string> Utils::getFiles(std::regex pattern, std::string folder, std::vector<std::vector<std::string>>& extraCapture){
	if(folder[folder.length()-1] != '/'){
		folder.append(1,'/');
	}

	std::vector<std::string> strVec;
	DIR* dp;
	dp = opendir(folder.c_str());
	bool hasVec(false);
	if (dp){
		while (auto file = readdir(dp)){
			std::string filename = (std::string)file->d_name;
			if (filename != "." && filename != ".."){			// ignore '.' and '..'
				std::vector<std::string> result = regexMatch(pattern, filename);
				if(result.size() != 0){
					hasVec = true;
					strVec.emplace_back(folder + result[0]);
					extraCapture.emplace_back(result);
				}
			}
		}
		if(hasVec){
			sort(strVec.begin(),strVec.end());
			sort(extraCapture.begin(),extraCapture.end(),[](std::vector<std::string> a, std::vector<std::string> b){return a[0] < b[0];});
		}
	}else{
		std::cerr << "Dir not found!" << std::endl;
	}
	closedir(dp);
	return strVec;
}


//std::vector<std::string> Utils::getFiles(std::regex pattern, std::string folder){
//	if(folder[folder.length()-1] != '/'){
//		folder.append(1,'/');
//	}
//
//	std::vector<std::string> strVec;
//	for (auto&fe : fs::directory_iterator(folder))
//	{
//			auto fp = fe.path();
//			auto fname = fp.filename();
//			std::vector<std::string> result = regexMatch(pattern, fname);
//			if(result.size() != 0){
//				strVec.emplace_back(folder + result[0]);
//			}
//	}
//	return strVec;
//}
//
//std::vector<std::string> Utils::getFiles(std::regex pattern, std::string folder, std::vector<std::vector<std::string>>& extraCapture){
//  if(folder[folder.length()-1] != '/'){
//		folder.append(1,'/');
//  }
//
//	std::vector<std::string> strVec;
//	for (auto&fe : fs::directory_iterator(folder))
//	{
//			auto fp = fe.path();
//			auto fname = fp.filename();
//			std::vector<std::string> result = regexMatch(pattern, fname);
//			if(result.size() != 0){
//				strVec.emplace_back(folder + result[0]);
//				extraCapture.emplace_back(result);
//			}
//	}
//	return strVec;
//}

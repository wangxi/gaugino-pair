//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Mon Nov 12 14:11:49 2018 by ROOT version 6.10/06
// from TTree Staus_Nominal/SmallTree for fast analysis
// found on file: Bkg/Higgs.mc16e.root
//////////////////////////////////////////////////////////

#ifndef selector_h
#define selector_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include "vector"

using namespace std;

class selector {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain
   TTree          *newTree;

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   Int_t           mcChannelNumber;
   Double_t        Weight_mc;
   Double_t        GenWeight;
   Double_t        GenWeight_LHE_1001;
   Double_t        GenWeight_LHE_1002;
   Double_t        GenWeight_LHE_1003;
   Double_t        GenWeight_LHE_1004;
   Double_t        GenWeight_LHE_1005;
   Double_t        GenWeight_LHE_1006;
   Double_t        GenWeight_LHE_1007;
   Double_t        GenWeight_LHE_1008;
   Double_t        GenWeight_LHE_1009;
   Double_t        GenWeight_LHE_1010;
   Double_t        GenWeight_LHE_1011;
   Double_t        GenWeight_LHE_1012;
   Double_t        GenWeight_LHE_1013;
   Double_t        GenWeight_LHE_1014;
   Double_t        GenWeight_LHE_1015;
   Double_t        GenWeight_LHE_1016;
   Double_t        GenWeight_LHE_1017;
   Double_t        GenWeight_LHE_1018;
   Double_t        GenWeight_LHE_1019;
   Double_t        GenWeight_LHE_1020;
   Double_t        GenWeight_LHE_1021;
   Double_t        GenWeight_LHE_1022;
   Double_t        GenWeight_LHE_1023;
   Double_t        GenWeight_LHE_1024;
   Double_t        GenWeight_LHE_1025;
   Double_t        GenWeight_LHE_1026;
   Double_t        GenWeight_LHE_1027;
   Double_t        GenWeight_LHE_1028;
   Double_t        GenWeight_LHE_1029;
   Double_t        GenWeight_LHE_1030;
   Double_t        GenWeight_LHE_1031;
   Double_t        GenWeight_LHE_1032;
   Double_t        GenWeight_LHE_1033;
   Double_t        GenWeight_LHE_1034;
   Double_t        GenWeight_LHE_1035;
   Double_t        GenWeight_LHE_1036;
   Double_t        GenWeight_LHE_1037;
   Double_t        GenWeight_LHE_1038;
   Double_t        GenWeight_LHE_1039;
   Double_t        GenWeight_LHE_1040;
   Double_t        GenWeight_LHE_1041;
   Double_t        GenWeight_LHE_1042;
   Double_t        GenWeight_LHE_1043;
   Double_t        GenWeight_LHE_1044;
   Double_t        GenWeight_LHE_1045;
   Double_t        GenWeight_LHE_1046;
   Double_t        GenWeight_LHE_1047;
   Double_t        GenWeight_LHE_1048;
   Double_t        GenWeight_LHE_1049;
   Double_t        GenWeight_LHE_1050;
   Double_t        GenWeight_LHE_1051;
   Double_t        GenWeight_LHE_1052;
   Double_t        GenWeight_LHE_1053;
   Double_t        GenWeight_LHE_1054;
   Double_t        GenWeight_LHE_1055;
   Double_t        GenWeight_LHE_1056;
   Double_t        GenWeight_LHE_1057;
   Double_t        GenWeight_LHE_1058;
   Double_t        GenWeight_LHE_1059;
   Double_t        GenWeight_LHE_1060;
   Double_t        GenWeight_LHE_1061;
   Double_t        GenWeight_LHE_1062;
   Double_t        GenWeight_LHE_1063;
   Double_t        GenWeight_LHE_1064;
   Double_t        GenWeight_LHE_1065;
   Double_t        GenWeight_LHE_1066;
   Double_t        GenWeight_LHE_1067;
   Double_t        GenWeight_LHE_1068;
   Double_t        GenWeight_LHE_1069;
   Double_t        GenWeight_LHE_1070;
   Double_t        GenWeight_LHE_1071;
   Double_t        GenWeight_LHE_1072;
   Double_t        GenWeight_LHE_1073;
   Double_t        GenWeight_LHE_1074;
   Double_t        GenWeight_LHE_1075;
   Double_t        GenWeight_LHE_1076;
   Double_t        GenWeight_LHE_1077;
   Double_t        GenWeight_LHE_1078;
   Double_t        GenWeight_LHE_1079;
   Double_t        GenWeight_LHE_1080;
   Double_t        GenWeight_LHE_1081;
   Double_t        GenWeight_LHE_1082;
   Double_t        GenWeight_LHE_1083;
   Double_t        GenWeight_LHE_1084;
   Double_t        GenWeight_LHE_1085;
   Double_t        GenWeight_LHE_1086;
   Double_t        GenWeight_LHE_1087;
   Double_t        GenWeight_LHE_1088;
   Double_t        GenWeight_LHE_1089;
   Double_t        GenWeight_LHE_1090;
   Double_t        GenWeight_LHE_1091;
   Double_t        GenWeight_LHE_1092;
   Double_t        GenWeight_LHE_1093;
   Double_t        GenWeight_LHE_1094;
   Double_t        GenWeight_LHE_1095;
   Double_t        GenWeight_LHE_1096;
   Double_t        GenWeight_LHE_1097;
   Double_t        GenWeight_LHE_1098;
   Double_t        GenWeight_LHE_1099;
   Double_t        GenWeight_LHE_1100;
   Double_t        GenWeight_LHE_1101;
   Double_t        GenWeight_LHE_1102;
   Double_t        GenWeight_LHE_1103;
   Double_t        GenWeight_LHE_1104;
   Double_t        GenWeight_LHE_1105;
   Double_t        GenWeight_LHE_1106;
   Double_t        GenWeight_LHE_1107;
   Double_t        GenWeight_LHE_1108;
   Double_t        GenWeight_LHE_1109;
   Double_t        GenWeight_LHE_1110;
   Double_t        GenWeight_LHE_1111;
   Double_t        GenWeight_LHE_1112;
   Double_t        GenWeight_LHE_1113;
   Double_t        GenWeight_LHE_1114;
   Double_t        GenWeight_LHE_1115;
   Double_t        GenWeight_LHE_1116;
   Double_t        GenWeight_LHE_1117;
   Double_t        GenWeight_LHE_1118;
   Double_t        GenWeight_LHE_1119;
   Double_t        GenWeight_LHE_1120;
   Double_t        GenWeight_LHE_1121;
   Double_t        GenWeight_LHE_1122;
   Double_t        GenWeight_LHE_1123;
   Double_t        GenWeight_LHE_1124;
   Double_t        GenWeight_LHE_1125;
   Double_t        GenWeight_LHE_1126;
   Double_t        GenWeight_LHE_1127;
   Double_t        GenWeight_LHE_1128;
   Double_t        GenWeight_LHE_1129;
   Double_t        GenWeight_LHE_1130;
   Double_t        GenWeight_LHE_1131;
   Double_t        GenWeight_LHE_1132;
   Double_t        GenWeight_LHE_1133;
   Double_t        GenWeight_LHE_1134;
   Double_t        GenWeight_LHE_1135;
   Double_t        GenWeight_LHE_1136;
   Double_t        GenWeight_LHE_1137;
   Double_t        GenWeight_LHE_1138;
   Double_t        GenWeight_LHE_1139;
   Double_t        GenWeight_LHE_1140;
   Double_t        GenWeight_LHE_1141;
   Double_t        GenWeight_LHE_1142;
   Double_t        GenWeight_LHE_1143;
   Double_t        GenWeight_LHE_1144;
   Double_t        GenWeight_LHE_1145;
   Double_t        GenWeight_LHE_1146;
   Double_t        GenWeight_LHE_1147;
   Double_t        GenWeight_LHE_1148;
   Double_t        GenWeight_LHE_1149;
   Double_t        GenWeight_LHE_1150;
   Double_t        GenWeight_LHE_1151;
   Double_t        GenWeight_LHE_1152;
   Double_t        GenWeight_LHE_1153;
   Double_t        GenWeight_LHE_1154;
   Double_t        GenWeight_LHE_1155;
   Double_t        GenWeight_LHE_1156;
   Double_t        GenWeight_LHE_1157;
   Double_t        GenWeight_LHE_1158;
   Double_t        GenWeight_LHE_1159;
   Double_t        GenWeight_LHE_1160;
   Double_t        GenWeight_LHE_1161;
   Double_t        GenWeight_LHE_1162;
   Double_t        GenWeight_LHE_1163;
   Double_t        GenWeight_LHE_1164;
   Double_t        GenWeight_LHE_1165;
   Double_t        GenWeight_LHE_1166;
   Double_t        GenWeight_LHE_1167;
   Double_t        GenWeight_LHE_1168;
   Double_t        GenWeight_LHE_1169;
   Double_t        GenWeight_LHE_1170;
   Double_t        GenWeight_LHE_1171;
   Double_t        GenWeight_LHE_1172;
   Double_t        GenWeight_LHE_1173;
   Double_t        GenWeight_LHE_1174;
   Double_t        GenWeight_LHE_1175;
   Double_t        GenWeight_LHE_1176;
   Double_t        GenWeight_LHE_1177;
   Double_t        GenWeight_LHE_1178;
   Double_t        GenWeight_LHE_1179;
   Double_t        EleWeight;
   Double_t        EleWeightId;
   Double_t        EleWeightIso;
   Double_t        EleWeightReco;
   Double_t        JetWeight;
   Double_t        JetWeightBTag;
   Double_t        JetWeightJVT;
   Double_t        MuoWeight;
   Double_t        MuoWeightIsol;
   Double_t        MuoWeightReco;
   Double_t        MuoWeightTTVA;
   Double_t        TauWeight;
   Double_t        TauWeightEleOREle;
   Double_t        TauWeightEleORHad;
   Double_t        TauWeightId;
   Double_t        TauWeightReco;
   Double_t        TauWeightTrigHLT_tau125_medium1_tracktwo;
   Double_t        TauWeightTrigHLT_tau160_medium1_tracktwo;
   Double_t        TauWeightTrigHLT_tau25_medium1_tracktwo;
   Double_t        TauWeightTrigHLT_tau35_medium1_tracktwo;
   Double_t        TauWeightTrigHLT_tau50_medium1_tracktwo_L1TAU12;
   Double_t        TauWeightTrigHLT_tau80_medium1_tracktwo;
   Double_t        TauWeightTrigHLT_tau80_medium1_tracktwo_L1TAU60;
   Double_t        muWeight;
   Float_t         Aplanarity;
   Float_t         Circularity;
   Float_t         CosChi1;
   Float_t         CosChi2;
   Float_t         Ht_Jet;
   Float_t         Ht_Lep;
   Float_t         Ht_Tau;
   Float_t         Lin_Aplanarity;
   Float_t         Lin_Circularity;
   Float_t         Lin_Planarity;
   Float_t         Lin_Sphericity;
   Float_t         Lin_Sphericity_C;
   Float_t         Lin_Sphericity_D;
   Float_t         MET_Centrality;
   Float_t         MET_CosMinDeltaPhi;
   Float_t         MET_LepTau_DeltaPhi;
   Float_t         MET_LepTau_VecSumPt;
   Float_t         MET_SumCosDeltaPhi;
   Float_t         MT2_max;
   Float_t         MT2_min;
   Float_t         Meff;
   Float_t         Meff_TauTau;
   Float_t         MetTST_OverSqrtHT;
   Float_t         MetTST_OverSqrtSumET;
   Float_t         MetTST_Significance;
   Float_t         MetTST_Significance_Rho;
   Float_t         MetTST_Significance_VarL;
   Float_t         MetTST_Significance_dataJER;
   Float_t         MetTST_Significance_dataJER_Rho;
   Float_t         MetTST_Significance_dataJER_VarL;
   Float_t         MetTST_Significance_dataJER_noPUJets;
   Float_t         MetTST_Significance_dataJER_noPUJets_Rho;
   Float_t         MetTST_Significance_dataJER_noPUJets_VarL;
   Float_t         MetTST_Significance_noPUJets;
   Float_t         MetTST_Significance_noPUJets_Rho;
   Float_t         MetTST_Significance_noPUJets_VarL;
   Float_t         MetTST_Significance_noPUJets_noSoftTerm;
   Float_t         MetTST_Significance_noPUJets_noSoftTerm_Rho;
   Float_t         MetTST_Significance_noPUJets_noSoftTerm_VarL;
   Float_t         MetTST_Significance_phireso_noPUJets;
   Float_t         MetTST_Significance_phireso_noPUJets_Rho;
   Float_t         MetTST_Significance_phireso_noPUJets_VarL;
   Float_t         Oblateness;
   Float_t         PTt;
   Float_t         Planarity;
   Float_t         Sphericity;
   Float_t         Spherocity;
   Float_t         Thrust;
   Float_t         ThrustMajor;
   Float_t         ThrustMinor;
   Float_t         VecSumPt_LepTau;
   Float_t         WolframMoment_0;
   Float_t         WolframMoment_1;
   Float_t         WolframMoment_10;
   Float_t         WolframMoment_11;
   Float_t         WolframMoment_2;
   Float_t         WolframMoment_3;
   Float_t         WolframMoment_4;
   Float_t         WolframMoment_5;
   Float_t         WolframMoment_6;
   Float_t         WolframMoment_7;
   Float_t         WolframMoment_8;
   Float_t         WolframMoment_9;
   Float_t         actualInteractionsPerCrossing;
   Float_t         averageInteractionsPerCrossing;
   Float_t         corr_avgIntPerX;
   Float_t         emt_MT2_max;
   Float_t         emt_MT2_min;
   Float_t         mu_density;
   Int_t           SUSYFinalState;
   Int_t           Vtx_n;
   Int_t           n_BJets;
   Int_t           n_BaseJets;
   Int_t           n_BaseTau;
   Int_t           n_SignalElec;
   Int_t           n_SignalJets;
   Int_t           n_SignalMuon;
   Int_t           n_SignalTau;
   Char_t          OS_BaseTauEle;
   Char_t          OS_BaseTauMuo;
   Char_t          OS_EleEle;
   Char_t          OS_MuoMuo;
   Char_t          OS_TauEle;
   Char_t          OS_TauMuo;
   Char_t          OS_TauTau;
   Char_t          TrigHLT_e120_lhloose;
   Char_t          TrigHLT_e140_lhloose_nod0;
   Char_t          TrigHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwo;
   Char_t          TrigHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwoEF;
   Char_t          TrigHLT_e17_lhmedium_nod0_ivarloose_tau25_mediumRNN_tracktwoMVA;
   Char_t          TrigHLT_e17_lhmedium_nod0_tau25_medium1_tracktwoEF_xe50;
   Char_t          TrigHLT_e17_lhmedium_nod0_tau25_medium1_tracktwo_xe50;
   Char_t          TrigHLT_e17_lhmedium_nod0_tau25_mediumRNN_tracktwoMVA_xe50;
   Char_t          TrigHLT_e24_lhmedium_L1EM20VH;
   Char_t          TrigHLT_e24_lhmedium_nod0_ivarloose_tau35_medium1_tracktwo;
   Char_t          TrigHLT_e26_lhtight_nod0_ivarloose;
   Char_t          TrigHLT_e60_lhmedium;
   Char_t          TrigHLT_e60_lhmedium_nod0;
   Char_t          TrigHLT_mu14_ivarloose_tau25_medium1_tracktwo;
   Char_t          TrigHLT_mu14_ivarloose_tau35_medium1_tracktwo;
   Char_t          TrigHLT_mu14_ivarloose_tau35_medium1_tracktwoEF;
   Char_t          TrigHLT_mu14_ivarloose_tau35_mediumRNN_tracktwoMVA;
   Char_t          TrigHLT_mu14_tau25_medium1_tracktwoEF_xe50;
   Char_t          TrigHLT_mu14_tau25_medium1_tracktwo_xe50;
   Char_t          TrigHLT_mu14_tau25_mediumRNN_tracktwoMVA_xe50;
   Char_t          TrigHLT_mu20_iloose_L1MU15;
   Char_t          TrigHLT_mu24_ivarmedium;
   Char_t          TrigHLT_mu26_imedium;
   Char_t          TrigHLT_mu26_ivarmedium;
   Char_t          TrigHLT_mu40;
   Char_t          TrigHLT_mu50;
   Char_t          TrigHLT_mu60_0eta105_msonly;
   Char_t          TrigHLT_tau125_medium1_tracktwo;
   Char_t          TrigHLT_tau160_medium1_tracktwo;
   Char_t          TrigHLT_tau25_medium1_tracktwo;
   Char_t          TrigHLT_tau25_medium1_tracktwoEF;
   Char_t          TrigHLT_tau25_mediumRNN_tracktwoMVA;
   Char_t          TrigHLT_tau35_medium1_tracktwo;
   Char_t          TrigHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo;
   Char_t          TrigHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_L1TAU20IM_2TAU12IM;
   Char_t          TrigHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_xe50;
   Char_t          TrigHLT_tau50_medium1_tracktwo_L1TAU12;
   Char_t          TrigHLT_tau60_medium1_tracktwo;
   Char_t          TrigHLT_tau60_medium1_tracktwoEF_tau25_medium1_tracktwoEF_xe50;
   Char_t          TrigHLT_tau60_medium1_tracktwo_tau25_medium1_tracktwo_xe50;
   Char_t          TrigHLT_tau60_mediumRNN_tracktwoMVA_tau25_mediumRNN_tracktwoMVA_xe50;
   Char_t          TrigHLT_tau80_medium1_tracktwo;
   Char_t          TrigHLT_tau80_medium1_tracktwoEF_L1TAU60_tau60_medium1_tracktwoEF_L1TAU40;
   Char_t          TrigHLT_tau80_medium1_tracktwo_L1TAU60;
   Char_t          TrigHLT_tau80_medium1_tracktwo_L1TAU60_tau25_medium1_tracktwo_L1TAU12;
   Char_t          TrigHLT_tau80_medium1_tracktwo_L1TAU60_tau50_medium1_tracktwo_L1TAU12;
   Char_t          TrigHLT_tau80_medium1_tracktwo_L1TAU60_tau60_medium1_tracktwo_L1TAU40;
   Char_t          TrigHLT_tau80_mediumRNN_tracktwoMVA_L1TAU60_tau60_mediumRNN_tracktwoMVA_L1TAU40;
   Char_t          TrigHLT_xe100_pufit_L1XE50;
   Char_t          TrigHLT_xe100_pufit_L1XE55;
   Char_t          TrigHLT_xe110_mht_L1XE50;
   Char_t          TrigHLT_xe110_pufit_L1XE50;
   Char_t          TrigHLT_xe110_pufit_L1XE55;
   Char_t          TrigHLT_xe110_pufit_xe65_L1XE50;
   Char_t          TrigHLT_xe110_pufit_xe70_L1XE50;
   Char_t          TrigHLT_xe120_pufit_L1XE50;
   Char_t          TrigHLT_xe70_mht;
   Char_t          TrigHLT_xe90_mht_L1XE50;
   Char_t          TrigHLT_xe90_pufit_L1XE50;
   Char_t          TrigMatchHLT_e120_lhloose;
   Char_t          TrigMatchHLT_e140_lhloose_nod0;
   Char_t          TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwo;
   Char_t          TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwoEF;
   Char_t          TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_mediumRNN_tracktwoMVA;
   Char_t          TrigMatchHLT_e17_lhmedium_nod0_tau25_medium1_tracktwoEF_xe50;
   Char_t          TrigMatchHLT_e17_lhmedium_nod0_tau25_medium1_tracktwo_xe50;
   Char_t          TrigMatchHLT_e17_lhmedium_nod0_tau25_mediumRNN_tracktwoMVA_xe50;
   Char_t          TrigMatchHLT_e24_lhmedium_L1EM20VH;
   Char_t          TrigMatchHLT_e24_lhmedium_nod0_ivarloose_tau35_medium1_tracktwo;
   Char_t          TrigMatchHLT_e26_lhtight_nod0_ivarloose;
   Char_t          TrigMatchHLT_e60_lhmedium;
   Char_t          TrigMatchHLT_e60_lhmedium_nod0;
   Char_t          TrigMatchHLT_mu14_ivarloose_tau25_medium1_tracktwo;
   Char_t          TrigMatchHLT_mu14_ivarloose_tau35_medium1_tracktwo;
   Char_t          TrigMatchHLT_mu14_ivarloose_tau35_medium1_tracktwoEF;
   Char_t          TrigMatchHLT_mu14_ivarloose_tau35_mediumRNN_tracktwoMVA;
   Char_t          TrigMatchHLT_mu14_tau25_medium1_tracktwoEF_xe50;
   Char_t          TrigMatchHLT_mu14_tau25_medium1_tracktwo_xe50;
   Char_t          TrigMatchHLT_mu14_tau25_mediumRNN_tracktwoMVA_xe50;
   Char_t          TrigMatchHLT_mu20_iloose_L1MU15;
   Char_t          TrigMatchHLT_mu24_ivarmedium;
   Char_t          TrigMatchHLT_mu26_imedium;
   Char_t          TrigMatchHLT_mu26_ivarmedium;
   Char_t          TrigMatchHLT_mu40;
   Char_t          TrigMatchHLT_mu50;
   Char_t          TrigMatchHLT_mu60_0eta105_msonly;
   Char_t          TrigMatchHLT_tau125_medium1_tracktwo;
   Char_t          TrigMatchHLT_tau160_medium1_tracktwo;
   Char_t          TrigMatchHLT_tau25_medium1_tracktwo;
   Char_t          TrigMatchHLT_tau25_medium1_tracktwoEF;
   Char_t          TrigMatchHLT_tau25_mediumRNN_tracktwoMVA;
   Char_t          TrigMatchHLT_tau35_medium1_tracktwo;
   Char_t          TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo;
   Char_t          TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_L1TAU20IM_2TAU12IM;
   Char_t          TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_xe50;
   Char_t          TrigMatchHLT_tau50_medium1_tracktwo_L1TAU12;
   Char_t          TrigMatchHLT_tau60_medium1_tracktwo;
   Char_t          TrigMatchHLT_tau60_medium1_tracktwoEF_tau25_medium1_tracktwoEF_xe50;
   Char_t          TrigMatchHLT_tau60_medium1_tracktwo_tau25_medium1_tracktwo_xe50;
   Char_t          TrigMatchHLT_tau60_mediumRNN_tracktwoMVA_tau25_mediumRNN_tracktwoMVA_xe50;
   Char_t          TrigMatchHLT_tau80_medium1_tracktwo;
   Char_t          TrigMatchHLT_tau80_medium1_tracktwoEF_L1TAU60_tau60_medium1_tracktwoEF_L1TAU40;
   Char_t          TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60;
   Char_t          TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau25_medium1_tracktwo_L1TAU12;
   Char_t          TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau50_medium1_tracktwo_L1TAU12;
   Char_t          TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau60_medium1_tracktwo_L1TAU40;
   Char_t          TrigMatchHLT_tau80_mediumRNN_tracktwoMVA_L1TAU60_tau60_mediumRNN_tracktwoMVA_L1TAU40;
   Char_t          TrigMatching;
   Char_t          Trigger;
   Bool_t          Lin_Sphericity_IsValid;
   Bool_t          MET_BiSect;
   Bool_t          Sphericity_IsValid;
   Bool_t          Spherocity_IsValid;
   Bool_t          Thrust_IsValid;
   Bool_t          WolframMoments_AreValid;
   UInt_t          RandomLumiBlockNumber;
   UInt_t          RandomRunNumber;
   Double_t        mergedRunNumber;
   UInt_t          backgroundFlags;
   UInt_t          bcid;
   UInt_t          coreFlags;
   UInt_t          forwardDetFlags;
   UInt_t          larFlags;
   UInt_t          lumiBlock;
   UInt_t          lumiFlags;
   UInt_t          muonFlags;
   UInt_t          pixelFlags;
   UInt_t          runNumber;
   UInt_t          sctFlags;
   UInt_t          tileFlags;
   UInt_t          trtFlags;
   ULong64_t       eventNumber;
   Float_t         MetCST_met;
   Float_t         MetCST_phi;
   Float_t         MetCST_sumet;
   Float_t         MetTST_met;
   Float_t         MetTST_phi;
   Float_t         MetTST_sumet;
   Float_t         TruthMET_met;
   Float_t         TruthMET_phi;
   Float_t         TruthMET_sumet;
   vector<float>   *RJW_JigSawCandidates_CosThetaStar;
   vector<float>   *RJW_JigSawCandidates_dPhiDecayPlane;
   vector<int>     *RJW_JigSawCandidates_pdgId;
   vector<float>   *RJW_JigSawCandidates_pt;
   vector<float>   *RJW_JigSawCandidates_eta;
   vector<float>   *RJW_JigSawCandidates_phi;
   vector<float>   *RJW_JigSawCandidates_m;
   vector<float>   *RJZ_JigSawCandidates_CosThetaStar;
   vector<float>   *RJZ_JigSawCandidates_dPhiDecayPlane;
   vector<int>     *RJZ_JigSawCandidates_pdgId;
   vector<float>   *RJZ_JigSawCandidates_pt;
   vector<float>   *RJZ_JigSawCandidates_eta;
   vector<float>   *RJZ_JigSawCandidates_phi;
   vector<float>   *RJZ_JigSawCandidates_m;
   vector<float>   *dilepton_charge;
   vector<int>     *dilepton_pdgId;
   vector<float>   *dilepton_pt;
   vector<float>   *dilepton_eta;
   vector<float>   *dilepton_phi;
   vector<float>   *dilepton_m;
   vector<float>   *electrons_MT;
   vector<char>    *electrons_TrigMatchHLT_e120_lhloose;
   vector<char>    *electrons_TrigMatchHLT_e140_lhloose_nod0;
   vector<char>    *electrons_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwo;
   vector<char>    *electrons_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwoEF;
   vector<char>    *electrons_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_mediumRNN_tracktwoMVA;
   vector<char>    *electrons_TrigMatchHLT_e17_lhmedium_nod0_tau25_medium1_tracktwoEF_xe50;
   vector<char>    *electrons_TrigMatchHLT_e17_lhmedium_nod0_tau25_medium1_tracktwo_xe50;
   vector<char>    *electrons_TrigMatchHLT_e17_lhmedium_nod0_tau25_mediumRNN_tracktwoMVA_xe50;
   vector<char>    *electrons_TrigMatchHLT_e24_lhmedium_L1EM20VH;
   vector<char>    *electrons_TrigMatchHLT_e24_lhmedium_nod0_ivarloose_tau35_medium1_tracktwo;
   vector<char>    *electrons_TrigMatchHLT_e26_lhtight_nod0_ivarloose;
   vector<char>    *electrons_TrigMatchHLT_e60_lhmedium;
   vector<char>    *electrons_TrigMatchHLT_e60_lhmedium_nod0;
   vector<float>   *electrons_charge;
   vector<float>   *electrons_d0sig;
   vector<char>    *electrons_isol;
   vector<char>    *electrons_signal;
   vector<int>     *electrons_truthOrigin;
   vector<int>     *electrons_truthType;
   vector<float>   *electrons_z0sinTheta;
   vector<float>   *electrons_pt;
   vector<float>   *electrons_eta;
   vector<float>   *electrons_phi;
   vector<float>   *electrons_e;
   vector<float>   *jets_Jvt;
   vector<double>  *jets_MV2c10;
   vector<int>     *jets_NTrks;
   vector<char>    *jets_bjet;
   vector<char>    *jets_signal;
   vector<float>   *jets_pt;
   vector<float>   *jets_eta;
   vector<float>   *jets_phi;
   vector<float>   *jets_m;
   vector<float>   *muons_MT;
   vector<char>    *muons_TrigMatchHLT_mu14_ivarloose_tau25_medium1_tracktwo;
   vector<char>    *muons_TrigMatchHLT_mu14_ivarloose_tau35_medium1_tracktwo;
   vector<char>    *muons_TrigMatchHLT_mu14_ivarloose_tau35_medium1_tracktwoEF;
   vector<char>    *muons_TrigMatchHLT_mu14_ivarloose_tau35_mediumRNN_tracktwoMVA;
   vector<char>    *muons_TrigMatchHLT_mu14_tau25_medium1_tracktwoEF_xe50;
   vector<char>    *muons_TrigMatchHLT_mu14_tau25_medium1_tracktwo_xe50;
   vector<char>    *muons_TrigMatchHLT_mu14_tau25_mediumRNN_tracktwoMVA_xe50;
   vector<char>    *muons_TrigMatchHLT_mu20_iloose_L1MU15;
   vector<char>    *muons_TrigMatchHLT_mu24_ivarmedium;
   vector<char>    *muons_TrigMatchHLT_mu26_imedium;
   vector<char>    *muons_TrigMatchHLT_mu26_ivarmedium;
   vector<char>    *muons_TrigMatchHLT_mu40;
   vector<char>    *muons_TrigMatchHLT_mu50;
   vector<char>    *muons_TrigMatchHLT_mu60_0eta105_msonly;
   vector<float>   *muons_charge;
   vector<float>   *muons_d0sig;
   vector<char>    *muons_isol;
   vector<char>    *muons_signal;
   vector<int>     *muons_truthOrigin;
   vector<int>     *muons_truthType;
   vector<float>   *muons_z0sinTheta;
   vector<float>   *muons_pt;
   vector<float>   *muons_eta;
   vector<float>   *muons_phi;
   vector<float>   *muons_e;
   vector<float>   *taus_BDTEleScore;
   vector<float>   *taus_BDTEleScoreSigTrans;
   vector<float>   *taus_BDTJetScore;
   vector<float>   *taus_BDTJetScoreSigTrans;
   vector<int>     *taus_ConeTruthLabelID;
   vector<float>   *taus_MT;
   vector<int>     *taus_NTrks;
   vector<int>     *taus_NTrksJet;
   vector<int>     *taus_PartonTruthLabelID;
   vector<int>     *taus_Quality;
   vector<char>    *taus_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwo;
   vector<char>    *taus_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwoEF;
   vector<char>    *taus_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_mediumRNN_tracktwoMVA;
   vector<char>    *taus_TrigMatchHLT_e17_lhmedium_nod0_tau25_medium1_tracktwoEF_xe50;
   vector<char>    *taus_TrigMatchHLT_e17_lhmedium_nod0_tau25_medium1_tracktwo_xe50;
   vector<char>    *taus_TrigMatchHLT_e17_lhmedium_nod0_tau25_mediumRNN_tracktwoMVA_xe50;
   vector<char>    *taus_TrigMatchHLT_e24_lhmedium_nod0_ivarloose_tau35_medium1_tracktwo;
   vector<char>    *taus_TrigMatchHLT_mu14_ivarloose_tau25_medium1_tracktwo;
   vector<char>    *taus_TrigMatchHLT_mu14_ivarloose_tau35_medium1_tracktwo;
   vector<char>    *taus_TrigMatchHLT_mu14_ivarloose_tau35_medium1_tracktwoEF;
   vector<char>    *taus_TrigMatchHLT_mu14_ivarloose_tau35_mediumRNN_tracktwoMVA;
   vector<char>    *taus_TrigMatchHLT_mu14_tau25_medium1_tracktwoEF_xe50;
   vector<char>    *taus_TrigMatchHLT_mu14_tau25_medium1_tracktwo_xe50;
   vector<char>    *taus_TrigMatchHLT_mu14_tau25_mediumRNN_tracktwoMVA_xe50;
   vector<char>    *taus_TrigMatchHLT_tau125_medium1_tracktwo;
   vector<char>    *taus_TrigMatchHLT_tau160_medium1_tracktwo;
   vector<char>    *taus_TrigMatchHLT_tau25_medium1_tracktwo;
   vector<char>    *taus_TrigMatchHLT_tau25_medium1_tracktwoEF;
   vector<char>    *taus_TrigMatchHLT_tau25_mediumRNN_tracktwoMVA;
   vector<char>    *taus_TrigMatchHLT_tau35_medium1_tracktwo;
   vector<char>    *taus_TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo;
   vector<char>    *taus_TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_L1TAU20IM_2TAU12IM;
   vector<char>    *taus_TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_xe50;
   vector<char>    *taus_TrigMatchHLT_tau50_medium1_tracktwo_L1TAU12;
   vector<char>    *taus_TrigMatchHLT_tau60_medium1_tracktwo;
   vector<char>    *taus_TrigMatchHLT_tau60_medium1_tracktwoEF_tau25_medium1_tracktwoEF_xe50;
   vector<char>    *taus_TrigMatchHLT_tau60_medium1_tracktwo_tau25_medium1_tracktwo_xe50;
   vector<char>    *taus_TrigMatchHLT_tau60_mediumRNN_tracktwoMVA_tau25_mediumRNN_tracktwoMVA_xe50;
   vector<char>    *taus_TrigMatchHLT_tau80_medium1_tracktwo;
   vector<char>    *taus_TrigMatchHLT_tau80_medium1_tracktwoEF_L1TAU60_tau60_medium1_tracktwoEF_L1TAU40;
   vector<char>    *taus_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60;
   vector<char>    *taus_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau25_medium1_tracktwo_L1TAU12;
   vector<char>    *taus_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau50_medium1_tracktwo_L1TAU12;
   vector<char>    *taus_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau60_medium1_tracktwo_L1TAU40;
   vector<char>    *taus_TrigMatchHLT_tau80_mediumRNN_tracktwoMVA_L1TAU60_tau60_mediumRNN_tracktwoMVA_L1TAU40;
   vector<float>   *taus_Width;
   vector<float>   *taus_charge;
   vector<char>    *taus_signalID;
   vector<int>     *taus_truthOrigin;
   vector<int>     *taus_truthType;
   vector<float>   *taus_pt;
   vector<float>   *taus_eta;
   vector<float>   *taus_phi;
   vector<float>   *taus_e;
   Double_t        tau1Pt;
   Double_t        tau2Pt;
   Double_t        tau1Mt;
   Double_t        tau2Mt;
   Double_t        dRtt;
   Double_t        dPhitt;
   Double_t        mt12tau;
   Double_t        Mll_tt;
   Double_t        meff_tt;
   Double_t        MT2;
   Double_t        tau1Pt_W;
   Double_t        mu1Pt;
   Double_t        tau1Mt_W;
   Double_t        mu1Mt;
   Double_t        dRtm;
   Double_t        dPhitm;
   Double_t        mt12tm;
   Double_t        Mll_tm;
   Double_t        meff_tm;
   Double_t        MT2_tm;
   Double_t        lJet_pt;
   Int_t           LJet_n;
   Double_t        Evt_MET;
   Double_t        Evt_METSIG;
   Double_t        Evt_METSIG_WithTauJet;
   Bool_t          isSR1;
   Bool_t          isSR2;
   Bool_t          isWCR;
   Bool_t          isWVR;
   Bool_t          isTVR;
   Bool_t          isZVR;
   Bool_t          isQCDCR_SR1;
   Bool_t          isQCDCR_SR2;
   Bool_t          isQCDCR_WCR;
   Bool_t          isQCDCR_WVR;

   // List of branches
   TBranch        *b_mcChannelNumber;   //!
   TBranch        *b_Weight_mc;   //!
   TBranch        *b_GenWeight;   //!
   TBranch        *b_GenWeight_LHE_1001;   //!
   TBranch        *b_GenWeight_LHE_1002;   //!
   TBranch        *b_GenWeight_LHE_1003;   //!
   TBranch        *b_GenWeight_LHE_1004;   //!
   TBranch        *b_GenWeight_LHE_1005;   //!
   TBranch        *b_GenWeight_LHE_1006;   //!
   TBranch        *b_GenWeight_LHE_1007;   //!
   TBranch        *b_GenWeight_LHE_1008;   //!
   TBranch        *b_GenWeight_LHE_1009;   //!
   TBranch        *b_GenWeight_LHE_1010;   //!
   TBranch        *b_GenWeight_LHE_1011;   //!
   TBranch        *b_GenWeight_LHE_1012;   //!
   TBranch        *b_GenWeight_LHE_1013;   //!
   TBranch        *b_GenWeight_LHE_1014;   //!
   TBranch        *b_GenWeight_LHE_1015;   //!
   TBranch        *b_GenWeight_LHE_1016;   //!
   TBranch        *b_GenWeight_LHE_1017;   //!
   TBranch        *b_GenWeight_LHE_1018;   //!
   TBranch        *b_GenWeight_LHE_1019;   //!
   TBranch        *b_GenWeight_LHE_1020;   //!
   TBranch        *b_GenWeight_LHE_1021;   //!
   TBranch        *b_GenWeight_LHE_1022;   //!
   TBranch        *b_GenWeight_LHE_1023;   //!
   TBranch        *b_GenWeight_LHE_1024;   //!
   TBranch        *b_GenWeight_LHE_1025;   //!
   TBranch        *b_GenWeight_LHE_1026;   //!
   TBranch        *b_GenWeight_LHE_1027;   //!
   TBranch        *b_GenWeight_LHE_1028;   //!
   TBranch        *b_GenWeight_LHE_1029;   //!
   TBranch        *b_GenWeight_LHE_1030;   //!
   TBranch        *b_GenWeight_LHE_1031;   //!
   TBranch        *b_GenWeight_LHE_1032;   //!
   TBranch        *b_GenWeight_LHE_1033;   //!
   TBranch        *b_GenWeight_LHE_1034;   //!
   TBranch        *b_GenWeight_LHE_1035;   //!
   TBranch        *b_GenWeight_LHE_1036;   //!
   TBranch        *b_GenWeight_LHE_1037;   //!
   TBranch        *b_GenWeight_LHE_1038;   //!
   TBranch        *b_GenWeight_LHE_1039;   //!
   TBranch        *b_GenWeight_LHE_1040;   //!
   TBranch        *b_GenWeight_LHE_1041;   //!
   TBranch        *b_GenWeight_LHE_1042;   //!
   TBranch        *b_GenWeight_LHE_1043;   //!
   TBranch        *b_GenWeight_LHE_1044;   //!
   TBranch        *b_GenWeight_LHE_1045;   //!
   TBranch        *b_GenWeight_LHE_1046;   //!
   TBranch        *b_GenWeight_LHE_1047;   //!
   TBranch        *b_GenWeight_LHE_1048;   //!
   TBranch        *b_GenWeight_LHE_1049;   //!
   TBranch        *b_GenWeight_LHE_1050;   //!
   TBranch        *b_GenWeight_LHE_1051;   //!
   TBranch        *b_GenWeight_LHE_1052;   //!
   TBranch        *b_GenWeight_LHE_1053;   //!
   TBranch        *b_GenWeight_LHE_1054;   //!
   TBranch        *b_GenWeight_LHE_1055;   //!
   TBranch        *b_GenWeight_LHE_1056;   //!
   TBranch        *b_GenWeight_LHE_1057;   //!
   TBranch        *b_GenWeight_LHE_1058;   //!
   TBranch        *b_GenWeight_LHE_1059;   //!
   TBranch        *b_GenWeight_LHE_1060;   //!
   TBranch        *b_GenWeight_LHE_1061;   //!
   TBranch        *b_GenWeight_LHE_1062;   //!
   TBranch        *b_GenWeight_LHE_1063;   //!
   TBranch        *b_GenWeight_LHE_1064;   //!
   TBranch        *b_GenWeight_LHE_1065;   //!
   TBranch        *b_GenWeight_LHE_1066;   //!
   TBranch        *b_GenWeight_LHE_1067;   //!
   TBranch        *b_GenWeight_LHE_1068;   //!
   TBranch        *b_GenWeight_LHE_1069;   //!
   TBranch        *b_GenWeight_LHE_1070;   //!
   TBranch        *b_GenWeight_LHE_1071;   //!
   TBranch        *b_GenWeight_LHE_1072;   //!
   TBranch        *b_GenWeight_LHE_1073;   //!
   TBranch        *b_GenWeight_LHE_1074;   //!
   TBranch        *b_GenWeight_LHE_1075;   //!
   TBranch        *b_GenWeight_LHE_1076;   //!
   TBranch        *b_GenWeight_LHE_1077;   //!
   TBranch        *b_GenWeight_LHE_1078;   //!
   TBranch        *b_GenWeight_LHE_1079;   //!
   TBranch        *b_GenWeight_LHE_1080;   //!
   TBranch        *b_GenWeight_LHE_1081;   //!
   TBranch        *b_GenWeight_LHE_1082;   //!
   TBranch        *b_GenWeight_LHE_1083;   //!
   TBranch        *b_GenWeight_LHE_1084;   //!
   TBranch        *b_GenWeight_LHE_1085;   //!
   TBranch        *b_GenWeight_LHE_1086;   //!
   TBranch        *b_GenWeight_LHE_1087;   //!
   TBranch        *b_GenWeight_LHE_1088;   //!
   TBranch        *b_GenWeight_LHE_1089;   //!
   TBranch        *b_GenWeight_LHE_1090;   //!
   TBranch        *b_GenWeight_LHE_1091;   //!
   TBranch        *b_GenWeight_LHE_1092;   //!
   TBranch        *b_GenWeight_LHE_1093;   //!
   TBranch        *b_GenWeight_LHE_1094;   //!
   TBranch        *b_GenWeight_LHE_1095;   //!
   TBranch        *b_GenWeight_LHE_1096;   //!
   TBranch        *b_GenWeight_LHE_1097;   //!
   TBranch        *b_GenWeight_LHE_1098;   //!
   TBranch        *b_GenWeight_LHE_1099;   //!
   TBranch        *b_GenWeight_LHE_1100;   //!
   TBranch        *b_GenWeight_LHE_1101;   //!
   TBranch        *b_GenWeight_LHE_1102;   //!
   TBranch        *b_GenWeight_LHE_1103;   //!
   TBranch        *b_GenWeight_LHE_1104;   //!
   TBranch        *b_GenWeight_LHE_1105;   //!
   TBranch        *b_GenWeight_LHE_1106;   //!
   TBranch        *b_GenWeight_LHE_1107;   //!
   TBranch        *b_GenWeight_LHE_1108;   //!
   TBranch        *b_GenWeight_LHE_1109;   //!
   TBranch        *b_GenWeight_LHE_1110;   //!
   TBranch        *b_GenWeight_LHE_1111;   //!
   TBranch        *b_GenWeight_LHE_1112;   //!
   TBranch        *b_GenWeight_LHE_1113;   //!
   TBranch        *b_GenWeight_LHE_1114;   //!
   TBranch        *b_GenWeight_LHE_1115;   //!
   TBranch        *b_GenWeight_LHE_1116;   //!
   TBranch        *b_GenWeight_LHE_1117;   //!
   TBranch        *b_GenWeight_LHE_1118;   //!
   TBranch        *b_GenWeight_LHE_1119;   //!
   TBranch        *b_GenWeight_LHE_1120;   //!
   TBranch        *b_GenWeight_LHE_1121;   //!
   TBranch        *b_GenWeight_LHE_1122;   //!
   TBranch        *b_GenWeight_LHE_1123;   //!
   TBranch        *b_GenWeight_LHE_1124;   //!
   TBranch        *b_GenWeight_LHE_1125;   //!
   TBranch        *b_GenWeight_LHE_1126;   //!
   TBranch        *b_GenWeight_LHE_1127;   //!
   TBranch        *b_GenWeight_LHE_1128;   //!
   TBranch        *b_GenWeight_LHE_1129;   //!
   TBranch        *b_GenWeight_LHE_1130;   //!
   TBranch        *b_GenWeight_LHE_1131;   //!
   TBranch        *b_GenWeight_LHE_1132;   //!
   TBranch        *b_GenWeight_LHE_1133;   //!
   TBranch        *b_GenWeight_LHE_1134;   //!
   TBranch        *b_GenWeight_LHE_1135;   //!
   TBranch        *b_GenWeight_LHE_1136;   //!
   TBranch        *b_GenWeight_LHE_1137;   //!
   TBranch        *b_GenWeight_LHE_1138;   //!
   TBranch        *b_GenWeight_LHE_1139;   //!
   TBranch        *b_GenWeight_LHE_1140;   //!
   TBranch        *b_GenWeight_LHE_1141;   //!
   TBranch        *b_GenWeight_LHE_1142;   //!
   TBranch        *b_GenWeight_LHE_1143;   //!
   TBranch        *b_GenWeight_LHE_1144;   //!
   TBranch        *b_GenWeight_LHE_1145;   //!
   TBranch        *b_GenWeight_LHE_1146;   //!
   TBranch        *b_GenWeight_LHE_1147;   //!
   TBranch        *b_GenWeight_LHE_1148;   //!
   TBranch        *b_GenWeight_LHE_1149;   //!
   TBranch        *b_GenWeight_LHE_1150;   //!
   TBranch        *b_GenWeight_LHE_1151;   //!
   TBranch        *b_GenWeight_LHE_1152;   //!
   TBranch        *b_GenWeight_LHE_1153;   //!
   TBranch        *b_GenWeight_LHE_1154;   //!
   TBranch        *b_GenWeight_LHE_1155;   //!
   TBranch        *b_GenWeight_LHE_1156;   //!
   TBranch        *b_GenWeight_LHE_1157;   //!
   TBranch        *b_GenWeight_LHE_1158;   //!
   TBranch        *b_GenWeight_LHE_1159;   //!
   TBranch        *b_GenWeight_LHE_1160;   //!
   TBranch        *b_GenWeight_LHE_1161;   //!
   TBranch        *b_GenWeight_LHE_1162;   //!
   TBranch        *b_GenWeight_LHE_1163;   //!
   TBranch        *b_GenWeight_LHE_1164;   //!
   TBranch        *b_GenWeight_LHE_1165;   //!
   TBranch        *b_GenWeight_LHE_1166;   //!
   TBranch        *b_GenWeight_LHE_1167;   //!
   TBranch        *b_GenWeight_LHE_1168;   //!
   TBranch        *b_GenWeight_LHE_1169;   //!
   TBranch        *b_GenWeight_LHE_1170;   //!
   TBranch        *b_GenWeight_LHE_1171;   //!
   TBranch        *b_GenWeight_LHE_1172;   //!
   TBranch        *b_GenWeight_LHE_1173;   //!
   TBranch        *b_GenWeight_LHE_1174;   //!
   TBranch        *b_GenWeight_LHE_1175;   //!
   TBranch        *b_GenWeight_LHE_1176;   //!
   TBranch        *b_GenWeight_LHE_1177;   //!
   TBranch        *b_GenWeight_LHE_1178;   //!
   TBranch        *b_GenWeight_LHE_1179;   //!
   TBranch        *b_EleWeight;   //!
   TBranch        *b_EleWeightId;   //!
   TBranch        *b_EleWeightIso;   //!
   TBranch        *b_EleWeightReco;   //!
   TBranch        *b_JetWeight;   //!
   TBranch        *b_JetWeightBTag;   //!
   TBranch        *b_JetWeightJVT;   //!
   TBranch        *b_MuoWeight;   //!
   TBranch        *b_MuoWeightIsol;   //!
   TBranch        *b_MuoWeightReco;   //!
   TBranch        *b_MuoWeightTTVA;   //!
   TBranch        *b_TauWeight;   //!
   TBranch        *b_TauWeightEleOREle;   //!
   TBranch        *b_TauWeightEleORHad;   //!
   TBranch        *b_TauWeightId;   //!
   TBranch        *b_TauWeightReco;   //!
   TBranch        *b_TauWeightTrigHLT_tau125_medium1_tracktwo;   //!
   TBranch        *b_TauWeightTrigHLT_tau160_medium1_tracktwo;   //!
   TBranch        *b_TauWeightTrigHLT_tau25_medium1_tracktwo;   //!
   TBranch        *b_TauWeightTrigHLT_tau35_medium1_tracktwo;   //!
   TBranch        *b_TauWeightTrigHLT_tau50_medium1_tracktwo_L1TAU12;   //!
   TBranch        *b_TauWeightTrigHLT_tau80_medium1_tracktwo;   //!
   TBranch        *b_TauWeightTrigHLT_tau80_medium1_tracktwo_L1TAU60;   //!
   TBranch        *b_muWeight;   //!
   TBranch        *b_Aplanarity;   //!
   TBranch        *b_Circularity;   //!
   TBranch        *b_CosChi1;   //!
   TBranch        *b_CosChi2;   //!
   TBranch        *b_Ht_Jet;   //!
   TBranch        *b_Ht_Lep;   //!
   TBranch        *b_Ht_Tau;   //!
   TBranch        *b_Lin_Aplanarity;   //!
   TBranch        *b_Lin_Circularity;   //!
   TBranch        *b_Lin_Planarity;   //!
   TBranch        *b_Lin_Sphericity;   //!
   TBranch        *b_Lin_Sphericity_C;   //!
   TBranch        *b_Lin_Sphericity_D;   //!
   TBranch        *b_MET_Centrality;   //!
   TBranch        *b_MET_CosMinDeltaPhi;   //!
   TBranch        *b_MET_LepTau_DeltaPhi;   //!
   TBranch        *b_MET_LepTau_VecSumPt;   //!
   TBranch        *b_MET_SumCosDeltaPhi;   //!
   TBranch        *b_MT2_max;   //!
   TBranch        *b_MT2_min;   //!
   TBranch        *b_Meff;   //!
   TBranch        *b_Meff_TauTau;   //!
   TBranch        *b_MetTST_OverSqrtHT;   //!
   TBranch        *b_MetTST_OverSqrtSumET;   //!
   TBranch        *b_MetTST_Significance;   //!
   TBranch        *b_MetTST_Significance_Rho;   //!
   TBranch        *b_MetTST_Significance_VarL;   //!
   TBranch        *b_MetTST_Significance_dataJER;   //!
   TBranch        *b_MetTST_Significance_dataJER_Rho;   //!
   TBranch        *b_MetTST_Significance_dataJER_VarL;   //!
   TBranch        *b_MetTST_Significance_dataJER_noPUJets;   //!
   TBranch        *b_MetTST_Significance_dataJER_noPUJets_Rho;   //!
   TBranch        *b_MetTST_Significance_dataJER_noPUJets_VarL;   //!
   TBranch        *b_MetTST_Significance_noPUJets;   //!
   TBranch        *b_MetTST_Significance_noPUJets_Rho;   //!
   TBranch        *b_MetTST_Significance_noPUJets_VarL;   //!
   TBranch        *b_MetTST_Significance_noPUJets_noSoftTerm;   //!
   TBranch        *b_MetTST_Significance_noPUJets_noSoftTerm_Rho;   //!
   TBranch        *b_MetTST_Significance_noPUJets_noSoftTerm_VarL;   //!
   TBranch        *b_MetTST_Significance_phireso_noPUJets;   //!
   TBranch        *b_MetTST_Significance_phireso_noPUJets_Rho;   //!
   TBranch        *b_MetTST_Significance_phireso_noPUJets_VarL;   //!
   TBranch        *b_Oblateness;   //!
   TBranch        *b_PTt;   //!
   TBranch        *b_Planarity;   //!
   TBranch        *b_Sphericity;   //!
   TBranch        *b_Spherocity;   //!
   TBranch        *b_Thrust;   //!
   TBranch        *b_ThrustMajor;   //!
   TBranch        *b_ThrustMinor;   //!
   TBranch        *b_VecSumPt_LepTau;   //!
   TBranch        *b_WolframMoment_0;   //!
   TBranch        *b_WolframMoment_1;   //!
   TBranch        *b_WolframMoment_10;   //!
   TBranch        *b_WolframMoment_11;   //!
   TBranch        *b_WolframMoment_2;   //!
   TBranch        *b_WolframMoment_3;   //!
   TBranch        *b_WolframMoment_4;   //!
   TBranch        *b_WolframMoment_5;   //!
   TBranch        *b_WolframMoment_6;   //!
   TBranch        *b_WolframMoment_7;   //!
   TBranch        *b_WolframMoment_8;   //!
   TBranch        *b_WolframMoment_9;   //!
   TBranch        *b_actualInteractionsPerCrossing;   //!
   TBranch        *b_averageInteractionsPerCrossing;   //!
   TBranch        *b_corr_avgIntPerX;   //!
   TBranch        *b_emt_MT2_max;   //!
   TBranch        *b_emt_MT2_min;   //!
   TBranch        *b_mu_density;   //!
   TBranch        *b_SUSYFinalState;   //!
   TBranch        *b_Vtx_n;   //!
   TBranch        *b_n_BJets;   //!
   TBranch        *b_n_BaseJets;   //!
   TBranch        *b_n_BaseTau;   //!
   TBranch        *b_n_SignalElec;   //!
   TBranch        *b_n_SignalJets;   //!
   TBranch        *b_n_SignalMuon;   //!
   TBranch        *b_n_SignalTau;   //!
   TBranch        *b_OS_BaseTauEle;   //!
   TBranch        *b_OS_BaseTauMuo;   //!
   TBranch        *b_OS_EleEle;   //!
   TBranch        *b_OS_MuoMuo;   //!
   TBranch        *b_OS_TauEle;   //!
   TBranch        *b_OS_TauMuo;   //!
   TBranch        *b_OS_TauTau;   //!
   TBranch        *b_TrigHLT_e120_lhloose;   //!
   TBranch        *b_TrigHLT_e140_lhloose_nod0;   //!
   TBranch        *b_TrigHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwo;   //!
   TBranch        *b_TrigHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwoEF;   //!
   TBranch        *b_TrigHLT_e17_lhmedium_nod0_ivarloose_tau25_mediumRNN_tracktwoMVA;   //!
   TBranch        *b_TrigHLT_e17_lhmedium_nod0_tau25_medium1_tracktwoEF_xe50;   //!
   TBranch        *b_TrigHLT_e17_lhmedium_nod0_tau25_medium1_tracktwo_xe50;   //!
   TBranch        *b_TrigHLT_e17_lhmedium_nod0_tau25_mediumRNN_tracktwoMVA_xe50;   //!
   TBranch        *b_TrigHLT_e24_lhmedium_L1EM20VH;   //!
   TBranch        *b_TrigHLT_e24_lhmedium_nod0_ivarloose_tau35_medium1_tracktwo;   //!
   TBranch        *b_TrigHLT_e26_lhtight_nod0_ivarloose;   //!
   TBranch        *b_TrigHLT_e60_lhmedium;   //!
   TBranch        *b_TrigHLT_e60_lhmedium_nod0;   //!
   TBranch        *b_TrigHLT_mu14_ivarloose_tau25_medium1_tracktwo;   //!
   TBranch        *b_TrigHLT_mu14_ivarloose_tau35_medium1_tracktwo;   //!
   TBranch        *b_TrigHLT_mu14_ivarloose_tau35_medium1_tracktwoEF;   //!
   TBranch        *b_TrigHLT_mu14_ivarloose_tau35_mediumRNN_tracktwoMVA;   //!
   TBranch        *b_TrigHLT_mu14_tau25_medium1_tracktwoEF_xe50;   //!
   TBranch        *b_TrigHLT_mu14_tau25_medium1_tracktwo_xe50;   //!
   TBranch        *b_TrigHLT_mu14_tau25_mediumRNN_tracktwoMVA_xe50;   //!
   TBranch        *b_TrigHLT_mu20_iloose_L1MU15;   //!
   TBranch        *b_TrigHLT_mu24_ivarmedium;   //!
   TBranch        *b_TrigHLT_mu26_imedium;   //!
   TBranch        *b_TrigHLT_mu26_ivarmedium;   //!
   TBranch        *b_TrigHLT_mu40;   //!
   TBranch        *b_TrigHLT_mu50;   //!
   TBranch        *b_TrigHLT_mu60_0eta105_msonly;   //!
   TBranch        *b_TrigHLT_tau125_medium1_tracktwo;   //!
   TBranch        *b_TrigHLT_tau160_medium1_tracktwo;   //!
   TBranch        *b_TrigHLT_tau25_medium1_tracktwo;   //!
   TBranch        *b_TrigHLT_tau25_medium1_tracktwoEF;   //!
   TBranch        *b_TrigHLT_tau25_mediumRNN_tracktwoMVA;   //!
   TBranch        *b_TrigHLT_tau35_medium1_tracktwo;   //!
   TBranch        *b_TrigHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo;   //!
   TBranch        *b_TrigHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_L1TAU20IM_2TAU12IM;   //!
   TBranch        *b_TrigHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_xe50;   //!
   TBranch        *b_TrigHLT_tau50_medium1_tracktwo_L1TAU12;   //!
   TBranch        *b_TrigHLT_tau60_medium1_tracktwo;   //!
   TBranch        *b_TrigHLT_tau60_medium1_tracktwoEF_tau25_medium1_tracktwoEF_xe50;   //!
   TBranch        *b_TrigHLT_tau60_medium1_tracktwo_tau25_medium1_tracktwo_xe50;   //!
   TBranch        *b_TrigHLT_tau60_mediumRNN_tracktwoMVA_tau25_mediumRNN_tracktwoMVA_xe50;   //!
   TBranch        *b_TrigHLT_tau80_medium1_tracktwo;   //!
   TBranch        *b_TrigHLT_tau80_medium1_tracktwoEF_L1TAU60_tau60_medium1_tracktwoEF_L1TAU40;   //!
   TBranch        *b_TrigHLT_tau80_medium1_tracktwo_L1TAU60;   //!
   TBranch        *b_TrigHLT_tau80_medium1_tracktwo_L1TAU60_tau25_medium1_tracktwo_L1TAU12;   //!
   TBranch        *b_TrigHLT_tau80_medium1_tracktwo_L1TAU60_tau50_medium1_tracktwo_L1TAU12;   //!
   TBranch        *b_TrigHLT_tau80_medium1_tracktwo_L1TAU60_tau60_medium1_tracktwo_L1TAU40;   //!
   TBranch        *b_TrigHLT_tau80_mediumRNN_tracktwoMVA_L1TAU60_tau60_mediumRNN_tracktwoMVA_L1TAU40;   //!
   TBranch        *b_TrigHLT_xe100_pufit_L1XE50;   //!
   TBranch        *b_TrigHLT_xe100_pufit_L1XE55;   //!
   TBranch        *b_TrigHLT_xe110_mht_L1XE50;   //!
   TBranch        *b_TrigHLT_xe110_pufit_L1XE50;   //!
   TBranch        *b_TrigHLT_xe110_pufit_L1XE55;   //!
   TBranch        *b_TrigHLT_xe110_pufit_xe65_L1XE50;   //!
   TBranch        *b_TrigHLT_xe110_pufit_xe70_L1XE50;   //!
   TBranch        *b_TrigHLT_xe120_pufit_L1XE50;   //!
   TBranch        *b_TrigHLT_xe70_mht;   //!
   TBranch        *b_TrigHLT_xe90_mht_L1XE50;   //!
   TBranch        *b_TrigHLT_xe90_pufit_L1XE50;   //!
   TBranch        *b_TrigMatchHLT_e120_lhloose;   //!
   TBranch        *b_TrigMatchHLT_e140_lhloose_nod0;   //!
   TBranch        *b_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwo;   //!
   TBranch        *b_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwoEF;   //!
   TBranch        *b_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_mediumRNN_tracktwoMVA;   //!
   TBranch        *b_TrigMatchHLT_e17_lhmedium_nod0_tau25_medium1_tracktwoEF_xe50;   //!
   TBranch        *b_TrigMatchHLT_e17_lhmedium_nod0_tau25_medium1_tracktwo_xe50;   //!
   TBranch        *b_TrigMatchHLT_e17_lhmedium_nod0_tau25_mediumRNN_tracktwoMVA_xe50;   //!
   TBranch        *b_TrigMatchHLT_e24_lhmedium_L1EM20VH;   //!
   TBranch        *b_TrigMatchHLT_e24_lhmedium_nod0_ivarloose_tau35_medium1_tracktwo;   //!
   TBranch        *b_TrigMatchHLT_e26_lhtight_nod0_ivarloose;   //!
   TBranch        *b_TrigMatchHLT_e60_lhmedium;   //!
   TBranch        *b_TrigMatchHLT_e60_lhmedium_nod0;   //!
   TBranch        *b_TrigMatchHLT_mu14_ivarloose_tau25_medium1_tracktwo;   //!
   TBranch        *b_TrigMatchHLT_mu14_ivarloose_tau35_medium1_tracktwo;   //!
   TBranch        *b_TrigMatchHLT_mu14_ivarloose_tau35_medium1_tracktwoEF;   //!
   TBranch        *b_TrigMatchHLT_mu14_ivarloose_tau35_mediumRNN_tracktwoMVA;   //!
   TBranch        *b_TrigMatchHLT_mu14_tau25_medium1_tracktwoEF_xe50;   //!
   TBranch        *b_TrigMatchHLT_mu14_tau25_medium1_tracktwo_xe50;   //!
   TBranch        *b_TrigMatchHLT_mu14_tau25_mediumRNN_tracktwoMVA_xe50;   //!
   TBranch        *b_TrigMatchHLT_mu20_iloose_L1MU15;   //!
   TBranch        *b_TrigMatchHLT_mu24_ivarmedium;   //!
   TBranch        *b_TrigMatchHLT_mu26_imedium;   //!
   TBranch        *b_TrigMatchHLT_mu26_ivarmedium;   //!
   TBranch        *b_TrigMatchHLT_mu40;   //!
   TBranch        *b_TrigMatchHLT_mu50;   //!
   TBranch        *b_TrigMatchHLT_mu60_0eta105_msonly;   //!
   TBranch        *b_TrigMatchHLT_tau125_medium1_tracktwo;   //!
   TBranch        *b_TrigMatchHLT_tau160_medium1_tracktwo;   //!
   TBranch        *b_TrigMatchHLT_tau25_medium1_tracktwo;   //!
   TBranch        *b_TrigMatchHLT_tau25_medium1_tracktwoEF;   //!
   TBranch        *b_TrigMatchHLT_tau25_mediumRNN_tracktwoMVA;   //!
   TBranch        *b_TrigMatchHLT_tau35_medium1_tracktwo;   //!
   TBranch        *b_TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo;   //!
   TBranch        *b_TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_L1TAU20IM_2TAU12IM;   //!
   TBranch        *b_TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_xe50;   //!
   TBranch        *b_TrigMatchHLT_tau50_medium1_tracktwo_L1TAU12;   //!
   TBranch        *b_TrigMatchHLT_tau60_medium1_tracktwo;   //!
   TBranch        *b_TrigMatchHLT_tau60_medium1_tracktwoEF_tau25_medium1_tracktwoEF_xe50;   //!
   TBranch        *b_TrigMatchHLT_tau60_medium1_tracktwo_tau25_medium1_tracktwo_xe50;   //!
   TBranch        *b_TrigMatchHLT_tau60_mediumRNN_tracktwoMVA_tau25_mediumRNN_tracktwoMVA_xe50;   //!
   TBranch        *b_TrigMatchHLT_tau80_medium1_tracktwo;   //!
   TBranch        *b_TrigMatchHLT_tau80_medium1_tracktwoEF_L1TAU60_tau60_medium1_tracktwoEF_L1TAU40;   //!
   TBranch        *b_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60;   //!
   TBranch        *b_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau25_medium1_tracktwo_L1TAU12;   //!
   TBranch        *b_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau50_medium1_tracktwo_L1TAU12;   //!
   TBranch        *b_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau60_medium1_tracktwo_L1TAU40;   //!
   TBranch        *b_TrigMatchHLT_tau80_mediumRNN_tracktwoMVA_L1TAU60_tau60_mediumRNN_tracktwoMVA_L1TAU40;   //!
   TBranch        *b_TrigMatching;   //!
   TBranch        *b_Trigger;   //!
   TBranch        *b_Lin_Sphericity_IsValid;   //!
   TBranch        *b_MET_BiSect;   //!
   TBranch        *b_Sphericity_IsValid;   //!
   TBranch        *b_Spherocity_IsValid;   //!
   TBranch        *b_Thrust_IsValid;   //!
   TBranch        *b_WolframMoments_AreValid;   //!
   TBranch        *b_RandomLumiBlockNumber;   //!
   TBranch        *b_RandomRunNumber;   //!
   TBranch        *b_mergedRunNumber;   //!
   TBranch        *b_backgroundFlags;   //!
   TBranch        *b_bcid;   //!
   TBranch        *b_coreFlags;   //!
   TBranch        *b_forwardDetFlags;   //!
   TBranch        *b_larFlags;   //!
   TBranch        *b_lumiBlock;   //!
   TBranch        *b_lumiFlags;   //!
   TBranch        *b_muonFlags;   //!
   TBranch        *b_pixelFlags;   //!
   TBranch        *b_runNumber;   //!
   TBranch        *b_sctFlags;   //!
   TBranch        *b_tileFlags;   //!
   TBranch        *b_trtFlags;   //!
   TBranch        *b_eventNumber;   //!
   TBranch        *b_MetCST_met;   //!
   TBranch        *b_MetCST_phi;   //!
   TBranch        *b_MetCST_sumet;   //!
   TBranch        *b_MetTST_met;   //!
   TBranch        *b_MetTST_phi;   //!
   TBranch        *b_MetTST_sumet;   //!
   TBranch        *b_TruthMET_met;   //!
   TBranch        *b_TruthMET_phi;   //!
   TBranch        *b_TruthMET_sumet;   //!
   TBranch        *b_RJW_JigSawCandidates_CosThetaStar;   //!
   TBranch        *b_RJW_JigSawCandidates_dPhiDecayPlane;   //!
   TBranch        *b_RJW_JigSawCandidates_pdgId;   //!
   TBranch        *b_RJW_JigSawCandidates_pt;   //!
   TBranch        *b_RJW_JigSawCandidates_eta;   //!
   TBranch        *b_RJW_JigSawCandidates_phi;   //!
   TBranch        *b_RJW_JigSawCandidates_m;   //!
   TBranch        *b_RJZ_JigSawCandidates_CosThetaStar;   //!
   TBranch        *b_RJZ_JigSawCandidates_dPhiDecayPlane;   //!
   TBranch        *b_RJZ_JigSawCandidates_pdgId;   //!
   TBranch        *b_RJZ_JigSawCandidates_pt;   //!
   TBranch        *b_RJZ_JigSawCandidates_eta;   //!
   TBranch        *b_RJZ_JigSawCandidates_phi;   //!
   TBranch        *b_RJZ_JigSawCandidates_m;   //!
   TBranch        *b_dilepton_charge;   //!
   TBranch        *b_dilepton_pdgId;   //!
   TBranch        *b_dilepton_pt;   //!
   TBranch        *b_dilepton_eta;   //!
   TBranch        *b_dilepton_phi;   //!
   TBranch        *b_dilepton_m;   //!
   TBranch        *b_electrons_MT;   //!
   TBranch        *b_electrons_TrigMatchHLT_e120_lhloose;   //!
   TBranch        *b_electrons_TrigMatchHLT_e140_lhloose_nod0;   //!
   TBranch        *b_electrons_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwo;   //!
   TBranch        *b_electrons_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwoEF;   //!
   TBranch        *b_electrons_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_mediumRNN_tracktwoMVA;   //!
   TBranch        *b_electrons_TrigMatchHLT_e17_lhmedium_nod0_tau25_medium1_tracktwoEF_xe50;   //!
   TBranch        *b_electrons_TrigMatchHLT_e17_lhmedium_nod0_tau25_medium1_tracktwo_xe50;   //!
   TBranch        *b_electrons_TrigMatchHLT_e17_lhmedium_nod0_tau25_mediumRNN_tracktwoMVA_xe50;   //!
   TBranch        *b_electrons_TrigMatchHLT_e24_lhmedium_L1EM20VH;   //!
   TBranch        *b_electrons_TrigMatchHLT_e24_lhmedium_nod0_ivarloose_tau35_medium1_tracktwo;   //!
   TBranch        *b_electrons_TrigMatchHLT_e26_lhtight_nod0_ivarloose;   //!
   TBranch        *b_electrons_TrigMatchHLT_e60_lhmedium;   //!
   TBranch        *b_electrons_TrigMatchHLT_e60_lhmedium_nod0;   //!
   TBranch        *b_electrons_charge;   //!
   TBranch        *b_electrons_d0sig;   //!
   TBranch        *b_electrons_isol;   //!
   TBranch        *b_electrons_signal;   //!
   TBranch        *b_electrons_truthOrigin;   //!
   TBranch        *b_electrons_truthType;   //!
   TBranch        *b_electrons_z0sinTheta;   //!
   TBranch        *b_electrons_pt;   //!
   TBranch        *b_electrons_eta;   //!
   TBranch        *b_electrons_phi;   //!
   TBranch        *b_electrons_e;   //!
   TBranch        *b_jets_Jvt;   //!
   TBranch        *b_jets_MV2c10;   //!
   TBranch        *b_jets_NTrks;   //!
   TBranch        *b_jets_bjet;   //!
   TBranch        *b_jets_signal;   //!
   TBranch        *b_jets_pt;   //!
   TBranch        *b_jets_eta;   //!
   TBranch        *b_jets_phi;   //!
   TBranch        *b_jets_m;   //!
   TBranch        *b_muons_MT;   //!
   TBranch        *b_muons_TrigMatchHLT_mu14_ivarloose_tau25_medium1_tracktwo;   //!
   TBranch        *b_muons_TrigMatchHLT_mu14_ivarloose_tau35_medium1_tracktwo;   //!
   TBranch        *b_muons_TrigMatchHLT_mu14_ivarloose_tau35_medium1_tracktwoEF;   //!
   TBranch        *b_muons_TrigMatchHLT_mu14_ivarloose_tau35_mediumRNN_tracktwoMVA;   //!
   TBranch        *b_muons_TrigMatchHLT_mu14_tau25_medium1_tracktwoEF_xe50;   //!
   TBranch        *b_muons_TrigMatchHLT_mu14_tau25_medium1_tracktwo_xe50;   //!
   TBranch        *b_muons_TrigMatchHLT_mu14_tau25_mediumRNN_tracktwoMVA_xe50;   //!
   TBranch        *b_muons_TrigMatchHLT_mu20_iloose_L1MU15;   //!
   TBranch        *b_muons_TrigMatchHLT_mu24_ivarmedium;   //!
   TBranch        *b_muons_TrigMatchHLT_mu26_imedium;   //!
   TBranch        *b_muons_TrigMatchHLT_mu26_ivarmedium;   //!
   TBranch        *b_muons_TrigMatchHLT_mu40;   //!
   TBranch        *b_muons_TrigMatchHLT_mu50;   //!
   TBranch        *b_muons_TrigMatchHLT_mu60_0eta105_msonly;   //!
   TBranch        *b_muons_charge;   //!
   TBranch        *b_muons_d0sig;   //!
   TBranch        *b_muons_isol;   //!
   TBranch        *b_muons_signal;   //!
   TBranch        *b_muons_truthOrigin;   //!
   TBranch        *b_muons_truthType;   //!
   TBranch        *b_muons_z0sinTheta;   //!
   TBranch        *b_muons_pt;   //!
   TBranch        *b_muons_eta;   //!
   TBranch        *b_muons_phi;   //!
   TBranch        *b_muons_e;   //!
   TBranch        *b_taus_BDTEleScore;   //!
   TBranch        *b_taus_BDTEleScoreSigTrans;   //!
   TBranch        *b_taus_BDTJetScore;   //!
   TBranch        *b_taus_BDTJetScoreSigTrans;   //!
   TBranch        *b_taus_ConeTruthLabelID;   //!
   TBranch        *b_taus_MT;   //!
   TBranch        *b_taus_NTrks;   //!
   TBranch        *b_taus_NTrksJet;   //!
   TBranch        *b_taus_PartonTruthLabelID;   //!
   TBranch        *b_taus_Quality;   //!
   TBranch        *b_taus_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwo;   //!
   TBranch        *b_taus_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwoEF;   //!
   TBranch        *b_taus_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_mediumRNN_tracktwoMVA;   //!
   TBranch        *b_taus_TrigMatchHLT_e17_lhmedium_nod0_tau25_medium1_tracktwoEF_xe50;   //!
   TBranch        *b_taus_TrigMatchHLT_e17_lhmedium_nod0_tau25_medium1_tracktwo_xe50;   //!
   TBranch        *b_taus_TrigMatchHLT_e17_lhmedium_nod0_tau25_mediumRNN_tracktwoMVA_xe50;   //!
   TBranch        *b_taus_TrigMatchHLT_e24_lhmedium_nod0_ivarloose_tau35_medium1_tracktwo;   //!
   TBranch        *b_taus_TrigMatchHLT_mu14_ivarloose_tau25_medium1_tracktwo;   //!
   TBranch        *b_taus_TrigMatchHLT_mu14_ivarloose_tau35_medium1_tracktwo;   //!
   TBranch        *b_taus_TrigMatchHLT_mu14_ivarloose_tau35_medium1_tracktwoEF;   //!
   TBranch        *b_taus_TrigMatchHLT_mu14_ivarloose_tau35_mediumRNN_tracktwoMVA;   //!
   TBranch        *b_taus_TrigMatchHLT_mu14_tau25_medium1_tracktwoEF_xe50;   //!
   TBranch        *b_taus_TrigMatchHLT_mu14_tau25_medium1_tracktwo_xe50;   //!
   TBranch        *b_taus_TrigMatchHLT_mu14_tau25_mediumRNN_tracktwoMVA_xe50;   //!
   TBranch        *b_taus_TrigMatchHLT_tau125_medium1_tracktwo;   //!
   TBranch        *b_taus_TrigMatchHLT_tau160_medium1_tracktwo;   //!
   TBranch        *b_taus_TrigMatchHLT_tau25_medium1_tracktwo;   //!
   TBranch        *b_taus_TrigMatchHLT_tau25_medium1_tracktwoEF;   //!
   TBranch        *b_taus_TrigMatchHLT_tau25_mediumRNN_tracktwoMVA;   //!
   TBranch        *b_taus_TrigMatchHLT_tau35_medium1_tracktwo;   //!
   TBranch        *b_taus_TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo;   //!
   TBranch        *b_taus_TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_L1TAU20IM_2TAU12IM;   //!
   TBranch        *b_taus_TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_xe50;   //!
   TBranch        *b_taus_TrigMatchHLT_tau50_medium1_tracktwo_L1TAU12;   //!
   TBranch        *b_taus_TrigMatchHLT_tau60_medium1_tracktwo;   //!
   TBranch        *b_taus_TrigMatchHLT_tau60_medium1_tracktwoEF_tau25_medium1_tracktwoEF_xe50;   //!
   TBranch        *b_taus_TrigMatchHLT_tau60_medium1_tracktwo_tau25_medium1_tracktwo_xe50;   //!
   TBranch        *b_taus_TrigMatchHLT_tau60_mediumRNN_tracktwoMVA_tau25_mediumRNN_tracktwoMVA_xe50;   //!
   TBranch        *b_taus_TrigMatchHLT_tau80_medium1_tracktwo;   //!
   TBranch        *b_taus_TrigMatchHLT_tau80_medium1_tracktwoEF_L1TAU60_tau60_medium1_tracktwoEF_L1TAU40;   //!
   TBranch        *b_taus_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60;   //!
   TBranch        *b_taus_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau25_medium1_tracktwo_L1TAU12;   //!
   TBranch        *b_taus_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau50_medium1_tracktwo_L1TAU12;   //!
   TBranch        *b_taus_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau60_medium1_tracktwo_L1TAU40;   //!
   TBranch        *b_taus_TrigMatchHLT_tau80_mediumRNN_tracktwoMVA_L1TAU60_tau60_mediumRNN_tracktwoMVA_L1TAU40;   //!
   TBranch        *b_taus_Width;   //!
   TBranch        *b_taus_charge;   //!
   TBranch        *b_taus_signalID;   //!
   TBranch        *b_taus_truthOrigin;   //!
   TBranch        *b_taus_truthType;   //!
   TBranch        *b_taus_pt;   //!
   TBranch        *b_taus_eta;   //!
   TBranch        *b_taus_phi;   //!
   TBranch        *b_taus_e;   //!
   TBranch        *b_tau1Pt;   //!
   TBranch        *b_tau2Pt;   //!
   TBranch        *b_tau1Mt;   //!
   TBranch        *b_tau2Mt;   //!
   TBranch        *b_dRtt;   //!
   TBranch        *b_dPhitt;   //!
   TBranch        *b_mt12tau;   //!
   TBranch        *b_Mll_tt;   //!
   TBranch        *b_meff_tt;   //!
   TBranch        *b_MT2;   //!
   TBranch        *b_tau1Pt_W;   //!
   TBranch        *b_mu1Pt;   //!
   TBranch        *b_tau1Mt_W;   //!
   TBranch        *b_mu1Mt;   //!
   TBranch        *b_dRtm;   //!
   TBranch        *b_dPhitm;   //!
   TBranch        *b_mt12tm;   //!
   TBranch        *b_Mll_tm;   //!
   TBranch        *b_meff_tm;   //!
   TBranch        *b_MT2_tm;   //!
   TBranch        *b_lJet_pt;   //!
   TBranch        *b_LJet_n;   //!
   TBranch        *b_Evt_MET;   //!
   TBranch        *b_Evt_METSIG;   //!
   TBranch        *b_Evt_METSIG_WithTauJet;   //!
   TBranch        *b_isSR1;   //!
   TBranch        *b_isSR2;   //!
   TBranch        *b_isWCR;   //!
   TBranch        *b_isWVR;   //!
   TBranch        *b_isTVR;   //!
   TBranch        *b_isZVR;   //!
   TBranch        *b_isQCDCR_SR1;   //!
   TBranch        *b_isQCDCR_SR2;   //!
   TBranch        *b_isQCDCR_WCR;   //!
   TBranch        *b_isQCDCR_WVR;   //!

   selector(TTree *tree=0);
   virtual ~selector();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef selector_cxx
selector::selector(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("Bkg/Higgs.mc16e.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("Bkg/Higgs.mc16e.root");
      }
      f->GetObject("Staus_Nominal",tree);

   }
   Init(tree);
}

selector::~selector()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t selector::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t selector::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void selector::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   RJW_JigSawCandidates_CosThetaStar = 0;
   RJW_JigSawCandidates_dPhiDecayPlane = 0;
   RJW_JigSawCandidates_pdgId = 0;
   RJW_JigSawCandidates_pt = 0;
   RJW_JigSawCandidates_eta = 0;
   RJW_JigSawCandidates_phi = 0;
   RJW_JigSawCandidates_m = 0;
   RJZ_JigSawCandidates_CosThetaStar = 0;
   RJZ_JigSawCandidates_dPhiDecayPlane = 0;
   RJZ_JigSawCandidates_pdgId = 0;
   RJZ_JigSawCandidates_pt = 0;
   RJZ_JigSawCandidates_eta = 0;
   RJZ_JigSawCandidates_phi = 0;
   RJZ_JigSawCandidates_m = 0;
   dilepton_charge = 0;
   dilepton_pdgId = 0;
   dilepton_pt = 0;
   dilepton_eta = 0;
   dilepton_phi = 0;
   dilepton_m = 0;
   electrons_MT = 0;
   electrons_TrigMatchHLT_e120_lhloose = 0;
   electrons_TrigMatchHLT_e140_lhloose_nod0 = 0;
   electrons_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwo = 0;
   electrons_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwoEF = 0;
   electrons_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_mediumRNN_tracktwoMVA = 0;
   electrons_TrigMatchHLT_e17_lhmedium_nod0_tau25_medium1_tracktwoEF_xe50 = 0;
   electrons_TrigMatchHLT_e17_lhmedium_nod0_tau25_medium1_tracktwo_xe50 = 0;
   electrons_TrigMatchHLT_e17_lhmedium_nod0_tau25_mediumRNN_tracktwoMVA_xe50 = 0;
   electrons_TrigMatchHLT_e24_lhmedium_L1EM20VH = 0;
   electrons_TrigMatchHLT_e24_lhmedium_nod0_ivarloose_tau35_medium1_tracktwo = 0;
   electrons_TrigMatchHLT_e26_lhtight_nod0_ivarloose = 0;
   electrons_TrigMatchHLT_e60_lhmedium = 0;
   electrons_TrigMatchHLT_e60_lhmedium_nod0 = 0;
   electrons_charge = 0;
   electrons_d0sig = 0;
   electrons_isol = 0;
   electrons_signal = 0;
   electrons_truthOrigin = 0;
   electrons_truthType = 0;
   electrons_z0sinTheta = 0;
   electrons_pt = 0;
   electrons_eta = 0;
   electrons_phi = 0;
   electrons_e = 0;
   jets_Jvt = 0;
   jets_MV2c10 = 0;
   jets_NTrks = 0;
   jets_bjet = 0;
   jets_signal = 0;
   jets_pt = 0;
   jets_eta = 0;
   jets_phi = 0;
   jets_m = 0;
   muons_MT = 0;
   muons_TrigMatchHLT_mu14_ivarloose_tau25_medium1_tracktwo = 0;
   muons_TrigMatchHLT_mu14_ivarloose_tau35_medium1_tracktwo = 0;
   muons_TrigMatchHLT_mu14_ivarloose_tau35_medium1_tracktwoEF = 0;
   muons_TrigMatchHLT_mu14_ivarloose_tau35_mediumRNN_tracktwoMVA = 0;
   muons_TrigMatchHLT_mu14_tau25_medium1_tracktwoEF_xe50 = 0;
   muons_TrigMatchHLT_mu14_tau25_medium1_tracktwo_xe50 = 0;
   muons_TrigMatchHLT_mu14_tau25_mediumRNN_tracktwoMVA_xe50 = 0;
   muons_TrigMatchHLT_mu20_iloose_L1MU15 = 0;
   muons_TrigMatchHLT_mu24_ivarmedium = 0;
   muons_TrigMatchHLT_mu26_imedium = 0;
   muons_TrigMatchHLT_mu26_ivarmedium = 0;
   muons_TrigMatchHLT_mu40 = 0;
   muons_TrigMatchHLT_mu50 = 0;
   muons_TrigMatchHLT_mu60_0eta105_msonly = 0;
   muons_charge = 0;
   muons_d0sig = 0;
   muons_isol = 0;
   muons_signal = 0;
   muons_truthOrigin = 0;
   muons_truthType = 0;
   muons_z0sinTheta = 0;
   muons_pt = 0;
   muons_eta = 0;
   muons_phi = 0;
   muons_e = 0;
   taus_BDTEleScore = 0;
   taus_BDTEleScoreSigTrans = 0;
   taus_BDTJetScore = 0;
   taus_BDTJetScoreSigTrans = 0;
   taus_ConeTruthLabelID = 0;
   taus_MT = 0;
   taus_NTrks = 0;
   taus_NTrksJet = 0;
   taus_PartonTruthLabelID = 0;
   taus_Quality = 0;
   taus_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwo = 0;
   taus_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwoEF = 0;
   taus_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_mediumRNN_tracktwoMVA = 0;
   taus_TrigMatchHLT_e17_lhmedium_nod0_tau25_medium1_tracktwoEF_xe50 = 0;
   taus_TrigMatchHLT_e17_lhmedium_nod0_tau25_medium1_tracktwo_xe50 = 0;
   taus_TrigMatchHLT_e17_lhmedium_nod0_tau25_mediumRNN_tracktwoMVA_xe50 = 0;
   taus_TrigMatchHLT_e24_lhmedium_nod0_ivarloose_tau35_medium1_tracktwo = 0;
   taus_TrigMatchHLT_mu14_ivarloose_tau25_medium1_tracktwo = 0;
   taus_TrigMatchHLT_mu14_ivarloose_tau35_medium1_tracktwo = 0;
   taus_TrigMatchHLT_mu14_ivarloose_tau35_medium1_tracktwoEF = 0;
   taus_TrigMatchHLT_mu14_ivarloose_tau35_mediumRNN_tracktwoMVA = 0;
   taus_TrigMatchHLT_mu14_tau25_medium1_tracktwoEF_xe50 = 0;
   taus_TrigMatchHLT_mu14_tau25_medium1_tracktwo_xe50 = 0;
   taus_TrigMatchHLT_mu14_tau25_mediumRNN_tracktwoMVA_xe50 = 0;
   taus_TrigMatchHLT_tau125_medium1_tracktwo = 0;
   taus_TrigMatchHLT_tau160_medium1_tracktwo = 0;
   taus_TrigMatchHLT_tau25_medium1_tracktwo = 0;
   taus_TrigMatchHLT_tau25_medium1_tracktwoEF = 0;
   taus_TrigMatchHLT_tau25_mediumRNN_tracktwoMVA = 0;
   taus_TrigMatchHLT_tau35_medium1_tracktwo = 0;
   taus_TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo = 0;
   taus_TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_L1TAU20IM_2TAU12IM = 0;
   taus_TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_xe50 = 0;
   taus_TrigMatchHLT_tau50_medium1_tracktwo_L1TAU12 = 0;
   taus_TrigMatchHLT_tau60_medium1_tracktwo = 0;
   taus_TrigMatchHLT_tau60_medium1_tracktwoEF_tau25_medium1_tracktwoEF_xe50 = 0;
   taus_TrigMatchHLT_tau60_medium1_tracktwo_tau25_medium1_tracktwo_xe50 = 0;
   taus_TrigMatchHLT_tau60_mediumRNN_tracktwoMVA_tau25_mediumRNN_tracktwoMVA_xe50 = 0;
   taus_TrigMatchHLT_tau80_medium1_tracktwo = 0;
   taus_TrigMatchHLT_tau80_medium1_tracktwoEF_L1TAU60_tau60_medium1_tracktwoEF_L1TAU40 = 0;
   taus_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60 = 0;
   taus_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau25_medium1_tracktwo_L1TAU12 = 0;
   taus_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau50_medium1_tracktwo_L1TAU12 = 0;
   taus_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau60_medium1_tracktwo_L1TAU40 = 0;
   taus_TrigMatchHLT_tau80_mediumRNN_tracktwoMVA_L1TAU60_tau60_mediumRNN_tracktwoMVA_L1TAU40 = 0;
   taus_Width = 0;
   taus_charge = 0;
   taus_signalID = 0;
   taus_truthOrigin = 0;
   taus_truthType = 0;
   taus_pt = 0;
   taus_eta = 0;
   taus_phi = 0;
   taus_e = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("mcChannelNumber", &mcChannelNumber, &b_mcChannelNumber);
   fChain->SetBranchAddress("Weight_mc", &Weight_mc, &b_Weight_mc); 
   fChain->SetBranchAddress("GenWeight", &GenWeight, &b_GenWeight);
   fChain->SetBranchAddress("GenWeight_LHE_1001", &GenWeight_LHE_1001, &b_GenWeight_LHE_1001);
   fChain->SetBranchAddress("GenWeight_LHE_1002", &GenWeight_LHE_1002, &b_GenWeight_LHE_1002);
   fChain->SetBranchAddress("GenWeight_LHE_1003", &GenWeight_LHE_1003, &b_GenWeight_LHE_1003);
   fChain->SetBranchAddress("GenWeight_LHE_1004", &GenWeight_LHE_1004, &b_GenWeight_LHE_1004);
   fChain->SetBranchAddress("GenWeight_LHE_1005", &GenWeight_LHE_1005, &b_GenWeight_LHE_1005);
   fChain->SetBranchAddress("GenWeight_LHE_1006", &GenWeight_LHE_1006, &b_GenWeight_LHE_1006);
   fChain->SetBranchAddress("GenWeight_LHE_1007", &GenWeight_LHE_1007, &b_GenWeight_LHE_1007);
   fChain->SetBranchAddress("GenWeight_LHE_1008", &GenWeight_LHE_1008, &b_GenWeight_LHE_1008);
   fChain->SetBranchAddress("GenWeight_LHE_1009", &GenWeight_LHE_1009, &b_GenWeight_LHE_1009);
   fChain->SetBranchAddress("GenWeight_LHE_1010", &GenWeight_LHE_1010, &b_GenWeight_LHE_1010);
   fChain->SetBranchAddress("GenWeight_LHE_1011", &GenWeight_LHE_1011, &b_GenWeight_LHE_1011);
   fChain->SetBranchAddress("GenWeight_LHE_1012", &GenWeight_LHE_1012, &b_GenWeight_LHE_1012);
   fChain->SetBranchAddress("GenWeight_LHE_1013", &GenWeight_LHE_1013, &b_GenWeight_LHE_1013);
   fChain->SetBranchAddress("GenWeight_LHE_1014", &GenWeight_LHE_1014, &b_GenWeight_LHE_1014);
   fChain->SetBranchAddress("GenWeight_LHE_1015", &GenWeight_LHE_1015, &b_GenWeight_LHE_1015);
   fChain->SetBranchAddress("GenWeight_LHE_1016", &GenWeight_LHE_1016, &b_GenWeight_LHE_1016);
   fChain->SetBranchAddress("GenWeight_LHE_1017", &GenWeight_LHE_1017, &b_GenWeight_LHE_1017);
   fChain->SetBranchAddress("GenWeight_LHE_1018", &GenWeight_LHE_1018, &b_GenWeight_LHE_1018);
   fChain->SetBranchAddress("GenWeight_LHE_1019", &GenWeight_LHE_1019, &b_GenWeight_LHE_1019);
   fChain->SetBranchAddress("GenWeight_LHE_1020", &GenWeight_LHE_1020, &b_GenWeight_LHE_1020);
   fChain->SetBranchAddress("GenWeight_LHE_1021", &GenWeight_LHE_1021, &b_GenWeight_LHE_1021);
   fChain->SetBranchAddress("GenWeight_LHE_1022", &GenWeight_LHE_1022, &b_GenWeight_LHE_1022);
   fChain->SetBranchAddress("GenWeight_LHE_1023", &GenWeight_LHE_1023, &b_GenWeight_LHE_1023);
   fChain->SetBranchAddress("GenWeight_LHE_1024", &GenWeight_LHE_1024, &b_GenWeight_LHE_1024);
   fChain->SetBranchAddress("GenWeight_LHE_1025", &GenWeight_LHE_1025, &b_GenWeight_LHE_1025);
   fChain->SetBranchAddress("GenWeight_LHE_1026", &GenWeight_LHE_1026, &b_GenWeight_LHE_1026);
   fChain->SetBranchAddress("GenWeight_LHE_1027", &GenWeight_LHE_1027, &b_GenWeight_LHE_1027);
   fChain->SetBranchAddress("GenWeight_LHE_1028", &GenWeight_LHE_1028, &b_GenWeight_LHE_1028);
   fChain->SetBranchAddress("GenWeight_LHE_1029", &GenWeight_LHE_1029, &b_GenWeight_LHE_1029);
   fChain->SetBranchAddress("GenWeight_LHE_1030", &GenWeight_LHE_1030, &b_GenWeight_LHE_1030);
   fChain->SetBranchAddress("GenWeight_LHE_1031", &GenWeight_LHE_1031, &b_GenWeight_LHE_1031);
   fChain->SetBranchAddress("GenWeight_LHE_1032", &GenWeight_LHE_1032, &b_GenWeight_LHE_1032);
   fChain->SetBranchAddress("GenWeight_LHE_1033", &GenWeight_LHE_1033, &b_GenWeight_LHE_1033);
   fChain->SetBranchAddress("GenWeight_LHE_1034", &GenWeight_LHE_1034, &b_GenWeight_LHE_1034);
   fChain->SetBranchAddress("GenWeight_LHE_1035", &GenWeight_LHE_1035, &b_GenWeight_LHE_1035);
   fChain->SetBranchAddress("GenWeight_LHE_1036", &GenWeight_LHE_1036, &b_GenWeight_LHE_1036);
   fChain->SetBranchAddress("GenWeight_LHE_1037", &GenWeight_LHE_1037, &b_GenWeight_LHE_1037);
   fChain->SetBranchAddress("GenWeight_LHE_1038", &GenWeight_LHE_1038, &b_GenWeight_LHE_1038);
   fChain->SetBranchAddress("GenWeight_LHE_1039", &GenWeight_LHE_1039, &b_GenWeight_LHE_1039);
   fChain->SetBranchAddress("GenWeight_LHE_1040", &GenWeight_LHE_1040, &b_GenWeight_LHE_1040);
   fChain->SetBranchAddress("GenWeight_LHE_1041", &GenWeight_LHE_1041, &b_GenWeight_LHE_1041);
   fChain->SetBranchAddress("GenWeight_LHE_1042", &GenWeight_LHE_1042, &b_GenWeight_LHE_1042);
   fChain->SetBranchAddress("GenWeight_LHE_1043", &GenWeight_LHE_1043, &b_GenWeight_LHE_1043);
   fChain->SetBranchAddress("GenWeight_LHE_1044", &GenWeight_LHE_1044, &b_GenWeight_LHE_1044);
   fChain->SetBranchAddress("GenWeight_LHE_1045", &GenWeight_LHE_1045, &b_GenWeight_LHE_1045);
   fChain->SetBranchAddress("GenWeight_LHE_1046", &GenWeight_LHE_1046, &b_GenWeight_LHE_1046);
   fChain->SetBranchAddress("GenWeight_LHE_1047", &GenWeight_LHE_1047, &b_GenWeight_LHE_1047);
   fChain->SetBranchAddress("GenWeight_LHE_1048", &GenWeight_LHE_1048, &b_GenWeight_LHE_1048);
   fChain->SetBranchAddress("GenWeight_LHE_1049", &GenWeight_LHE_1049, &b_GenWeight_LHE_1049);
   fChain->SetBranchAddress("GenWeight_LHE_1050", &GenWeight_LHE_1050, &b_GenWeight_LHE_1050);
   fChain->SetBranchAddress("GenWeight_LHE_1051", &GenWeight_LHE_1051, &b_GenWeight_LHE_1051);
   fChain->SetBranchAddress("GenWeight_LHE_1052", &GenWeight_LHE_1052, &b_GenWeight_LHE_1052);
   fChain->SetBranchAddress("GenWeight_LHE_1053", &GenWeight_LHE_1053, &b_GenWeight_LHE_1053);
   fChain->SetBranchAddress("GenWeight_LHE_1054", &GenWeight_LHE_1054, &b_GenWeight_LHE_1054);
   fChain->SetBranchAddress("GenWeight_LHE_1055", &GenWeight_LHE_1055, &b_GenWeight_LHE_1055);
   fChain->SetBranchAddress("GenWeight_LHE_1056", &GenWeight_LHE_1056, &b_GenWeight_LHE_1056);
   fChain->SetBranchAddress("GenWeight_LHE_1057", &GenWeight_LHE_1057, &b_GenWeight_LHE_1057);
   fChain->SetBranchAddress("GenWeight_LHE_1058", &GenWeight_LHE_1058, &b_GenWeight_LHE_1058);
   fChain->SetBranchAddress("GenWeight_LHE_1059", &GenWeight_LHE_1059, &b_GenWeight_LHE_1059);
   fChain->SetBranchAddress("GenWeight_LHE_1060", &GenWeight_LHE_1060, &b_GenWeight_LHE_1060);
   fChain->SetBranchAddress("GenWeight_LHE_1061", &GenWeight_LHE_1061, &b_GenWeight_LHE_1061);
   fChain->SetBranchAddress("GenWeight_LHE_1062", &GenWeight_LHE_1062, &b_GenWeight_LHE_1062);
   fChain->SetBranchAddress("GenWeight_LHE_1063", &GenWeight_LHE_1063, &b_GenWeight_LHE_1063);
   fChain->SetBranchAddress("GenWeight_LHE_1064", &GenWeight_LHE_1064, &b_GenWeight_LHE_1064);
   fChain->SetBranchAddress("GenWeight_LHE_1065", &GenWeight_LHE_1065, &b_GenWeight_LHE_1065);
   fChain->SetBranchAddress("GenWeight_LHE_1066", &GenWeight_LHE_1066, &b_GenWeight_LHE_1066);
   fChain->SetBranchAddress("GenWeight_LHE_1067", &GenWeight_LHE_1067, &b_GenWeight_LHE_1067);
   fChain->SetBranchAddress("GenWeight_LHE_1068", &GenWeight_LHE_1068, &b_GenWeight_LHE_1068);
   fChain->SetBranchAddress("GenWeight_LHE_1069", &GenWeight_LHE_1069, &b_GenWeight_LHE_1069);
   fChain->SetBranchAddress("GenWeight_LHE_1070", &GenWeight_LHE_1070, &b_GenWeight_LHE_1070);
   fChain->SetBranchAddress("GenWeight_LHE_1071", &GenWeight_LHE_1071, &b_GenWeight_LHE_1071);
   fChain->SetBranchAddress("GenWeight_LHE_1072", &GenWeight_LHE_1072, &b_GenWeight_LHE_1072);
   fChain->SetBranchAddress("GenWeight_LHE_1073", &GenWeight_LHE_1073, &b_GenWeight_LHE_1073);
   fChain->SetBranchAddress("GenWeight_LHE_1074", &GenWeight_LHE_1074, &b_GenWeight_LHE_1074);
   fChain->SetBranchAddress("GenWeight_LHE_1075", &GenWeight_LHE_1075, &b_GenWeight_LHE_1075);
   fChain->SetBranchAddress("GenWeight_LHE_1076", &GenWeight_LHE_1076, &b_GenWeight_LHE_1076);
   fChain->SetBranchAddress("GenWeight_LHE_1077", &GenWeight_LHE_1077, &b_GenWeight_LHE_1077);
   fChain->SetBranchAddress("GenWeight_LHE_1078", &GenWeight_LHE_1078, &b_GenWeight_LHE_1078);
   fChain->SetBranchAddress("GenWeight_LHE_1079", &GenWeight_LHE_1079, &b_GenWeight_LHE_1079);
   fChain->SetBranchAddress("GenWeight_LHE_1080", &GenWeight_LHE_1080, &b_GenWeight_LHE_1080);
   fChain->SetBranchAddress("GenWeight_LHE_1081", &GenWeight_LHE_1081, &b_GenWeight_LHE_1081);
   fChain->SetBranchAddress("GenWeight_LHE_1082", &GenWeight_LHE_1082, &b_GenWeight_LHE_1082);
   fChain->SetBranchAddress("GenWeight_LHE_1083", &GenWeight_LHE_1083, &b_GenWeight_LHE_1083);
   fChain->SetBranchAddress("GenWeight_LHE_1084", &GenWeight_LHE_1084, &b_GenWeight_LHE_1084);
   fChain->SetBranchAddress("GenWeight_LHE_1085", &GenWeight_LHE_1085, &b_GenWeight_LHE_1085);
   fChain->SetBranchAddress("GenWeight_LHE_1086", &GenWeight_LHE_1086, &b_GenWeight_LHE_1086);
   fChain->SetBranchAddress("GenWeight_LHE_1087", &GenWeight_LHE_1087, &b_GenWeight_LHE_1087);
   fChain->SetBranchAddress("GenWeight_LHE_1088", &GenWeight_LHE_1088, &b_GenWeight_LHE_1088);
   fChain->SetBranchAddress("GenWeight_LHE_1089", &GenWeight_LHE_1089, &b_GenWeight_LHE_1089);
   fChain->SetBranchAddress("GenWeight_LHE_1090", &GenWeight_LHE_1090, &b_GenWeight_LHE_1090);
   fChain->SetBranchAddress("GenWeight_LHE_1091", &GenWeight_LHE_1091, &b_GenWeight_LHE_1091);
   fChain->SetBranchAddress("GenWeight_LHE_1092", &GenWeight_LHE_1092, &b_GenWeight_LHE_1092);
   fChain->SetBranchAddress("GenWeight_LHE_1093", &GenWeight_LHE_1093, &b_GenWeight_LHE_1093);
   fChain->SetBranchAddress("GenWeight_LHE_1094", &GenWeight_LHE_1094, &b_GenWeight_LHE_1094);
   fChain->SetBranchAddress("GenWeight_LHE_1095", &GenWeight_LHE_1095, &b_GenWeight_LHE_1095);
   fChain->SetBranchAddress("GenWeight_LHE_1096", &GenWeight_LHE_1096, &b_GenWeight_LHE_1096);
   fChain->SetBranchAddress("GenWeight_LHE_1097", &GenWeight_LHE_1097, &b_GenWeight_LHE_1097);
   fChain->SetBranchAddress("GenWeight_LHE_1098", &GenWeight_LHE_1098, &b_GenWeight_LHE_1098);
   fChain->SetBranchAddress("GenWeight_LHE_1099", &GenWeight_LHE_1099, &b_GenWeight_LHE_1099);
   fChain->SetBranchAddress("GenWeight_LHE_1100", &GenWeight_LHE_1100, &b_GenWeight_LHE_1100);
   fChain->SetBranchAddress("GenWeight_LHE_1101", &GenWeight_LHE_1101, &b_GenWeight_LHE_1101);
   fChain->SetBranchAddress("GenWeight_LHE_1102", &GenWeight_LHE_1102, &b_GenWeight_LHE_1102);
   fChain->SetBranchAddress("GenWeight_LHE_1103", &GenWeight_LHE_1103, &b_GenWeight_LHE_1103);
   fChain->SetBranchAddress("GenWeight_LHE_1104", &GenWeight_LHE_1104, &b_GenWeight_LHE_1104);
   fChain->SetBranchAddress("GenWeight_LHE_1105", &GenWeight_LHE_1105, &b_GenWeight_LHE_1105);
   fChain->SetBranchAddress("GenWeight_LHE_1106", &GenWeight_LHE_1106, &b_GenWeight_LHE_1106);
   fChain->SetBranchAddress("GenWeight_LHE_1107", &GenWeight_LHE_1107, &b_GenWeight_LHE_1107);
   fChain->SetBranchAddress("GenWeight_LHE_1108", &GenWeight_LHE_1108, &b_GenWeight_LHE_1108);
   fChain->SetBranchAddress("GenWeight_LHE_1109", &GenWeight_LHE_1109, &b_GenWeight_LHE_1109);
   fChain->SetBranchAddress("GenWeight_LHE_1110", &GenWeight_LHE_1110, &b_GenWeight_LHE_1110);
   fChain->SetBranchAddress("GenWeight_LHE_1111", &GenWeight_LHE_1111, &b_GenWeight_LHE_1111);
   fChain->SetBranchAddress("GenWeight_LHE_1112", &GenWeight_LHE_1112, &b_GenWeight_LHE_1112);
   fChain->SetBranchAddress("GenWeight_LHE_1113", &GenWeight_LHE_1113, &b_GenWeight_LHE_1113);
   fChain->SetBranchAddress("GenWeight_LHE_1114", &GenWeight_LHE_1114, &b_GenWeight_LHE_1114);
   fChain->SetBranchAddress("GenWeight_LHE_1115", &GenWeight_LHE_1115, &b_GenWeight_LHE_1115);
   fChain->SetBranchAddress("GenWeight_LHE_1116", &GenWeight_LHE_1116, &b_GenWeight_LHE_1116);
   fChain->SetBranchAddress("GenWeight_LHE_1117", &GenWeight_LHE_1117, &b_GenWeight_LHE_1117);
   fChain->SetBranchAddress("GenWeight_LHE_1118", &GenWeight_LHE_1118, &b_GenWeight_LHE_1118);
   fChain->SetBranchAddress("GenWeight_LHE_1119", &GenWeight_LHE_1119, &b_GenWeight_LHE_1119);
   fChain->SetBranchAddress("GenWeight_LHE_1120", &GenWeight_LHE_1120, &b_GenWeight_LHE_1120);
   fChain->SetBranchAddress("GenWeight_LHE_1121", &GenWeight_LHE_1121, &b_GenWeight_LHE_1121);
   fChain->SetBranchAddress("GenWeight_LHE_1122", &GenWeight_LHE_1122, &b_GenWeight_LHE_1122);
   fChain->SetBranchAddress("GenWeight_LHE_1123", &GenWeight_LHE_1123, &b_GenWeight_LHE_1123);
   fChain->SetBranchAddress("GenWeight_LHE_1124", &GenWeight_LHE_1124, &b_GenWeight_LHE_1124);
   fChain->SetBranchAddress("GenWeight_LHE_1125", &GenWeight_LHE_1125, &b_GenWeight_LHE_1125);
   fChain->SetBranchAddress("GenWeight_LHE_1126", &GenWeight_LHE_1126, &b_GenWeight_LHE_1126);
   fChain->SetBranchAddress("GenWeight_LHE_1127", &GenWeight_LHE_1127, &b_GenWeight_LHE_1127);
   fChain->SetBranchAddress("GenWeight_LHE_1128", &GenWeight_LHE_1128, &b_GenWeight_LHE_1128);
   fChain->SetBranchAddress("GenWeight_LHE_1129", &GenWeight_LHE_1129, &b_GenWeight_LHE_1129);
   fChain->SetBranchAddress("GenWeight_LHE_1130", &GenWeight_LHE_1130, &b_GenWeight_LHE_1130);
   fChain->SetBranchAddress("GenWeight_LHE_1131", &GenWeight_LHE_1131, &b_GenWeight_LHE_1131);
   fChain->SetBranchAddress("GenWeight_LHE_1132", &GenWeight_LHE_1132, &b_GenWeight_LHE_1132);
   fChain->SetBranchAddress("GenWeight_LHE_1133", &GenWeight_LHE_1133, &b_GenWeight_LHE_1133);
   fChain->SetBranchAddress("GenWeight_LHE_1134", &GenWeight_LHE_1134, &b_GenWeight_LHE_1134);
   fChain->SetBranchAddress("GenWeight_LHE_1135", &GenWeight_LHE_1135, &b_GenWeight_LHE_1135);
   fChain->SetBranchAddress("GenWeight_LHE_1136", &GenWeight_LHE_1136, &b_GenWeight_LHE_1136);
   fChain->SetBranchAddress("GenWeight_LHE_1137", &GenWeight_LHE_1137, &b_GenWeight_LHE_1137);
   fChain->SetBranchAddress("GenWeight_LHE_1138", &GenWeight_LHE_1138, &b_GenWeight_LHE_1138);
   fChain->SetBranchAddress("GenWeight_LHE_1139", &GenWeight_LHE_1139, &b_GenWeight_LHE_1139);
   fChain->SetBranchAddress("GenWeight_LHE_1140", &GenWeight_LHE_1140, &b_GenWeight_LHE_1140);
   fChain->SetBranchAddress("GenWeight_LHE_1141", &GenWeight_LHE_1141, &b_GenWeight_LHE_1141);
   fChain->SetBranchAddress("GenWeight_LHE_1142", &GenWeight_LHE_1142, &b_GenWeight_LHE_1142);
   fChain->SetBranchAddress("GenWeight_LHE_1143", &GenWeight_LHE_1143, &b_GenWeight_LHE_1143);
   fChain->SetBranchAddress("GenWeight_LHE_1144", &GenWeight_LHE_1144, &b_GenWeight_LHE_1144);
   fChain->SetBranchAddress("GenWeight_LHE_1145", &GenWeight_LHE_1145, &b_GenWeight_LHE_1145);
   fChain->SetBranchAddress("GenWeight_LHE_1146", &GenWeight_LHE_1146, &b_GenWeight_LHE_1146);
   fChain->SetBranchAddress("GenWeight_LHE_1147", &GenWeight_LHE_1147, &b_GenWeight_LHE_1147);
   fChain->SetBranchAddress("GenWeight_LHE_1148", &GenWeight_LHE_1148, &b_GenWeight_LHE_1148);
   fChain->SetBranchAddress("GenWeight_LHE_1149", &GenWeight_LHE_1149, &b_GenWeight_LHE_1149);
   fChain->SetBranchAddress("GenWeight_LHE_1150", &GenWeight_LHE_1150, &b_GenWeight_LHE_1150);
   fChain->SetBranchAddress("GenWeight_LHE_1151", &GenWeight_LHE_1151, &b_GenWeight_LHE_1151);
   fChain->SetBranchAddress("GenWeight_LHE_1152", &GenWeight_LHE_1152, &b_GenWeight_LHE_1152);
   fChain->SetBranchAddress("GenWeight_LHE_1153", &GenWeight_LHE_1153, &b_GenWeight_LHE_1153);
   fChain->SetBranchAddress("GenWeight_LHE_1154", &GenWeight_LHE_1154, &b_GenWeight_LHE_1154);
   fChain->SetBranchAddress("GenWeight_LHE_1155", &GenWeight_LHE_1155, &b_GenWeight_LHE_1155);
   fChain->SetBranchAddress("GenWeight_LHE_1156", &GenWeight_LHE_1156, &b_GenWeight_LHE_1156);
   fChain->SetBranchAddress("GenWeight_LHE_1157", &GenWeight_LHE_1157, &b_GenWeight_LHE_1157);
   fChain->SetBranchAddress("GenWeight_LHE_1158", &GenWeight_LHE_1158, &b_GenWeight_LHE_1158);
   fChain->SetBranchAddress("GenWeight_LHE_1159", &GenWeight_LHE_1159, &b_GenWeight_LHE_1159);
   fChain->SetBranchAddress("GenWeight_LHE_1160", &GenWeight_LHE_1160, &b_GenWeight_LHE_1160);
   fChain->SetBranchAddress("GenWeight_LHE_1161", &GenWeight_LHE_1161, &b_GenWeight_LHE_1161);
   fChain->SetBranchAddress("GenWeight_LHE_1162", &GenWeight_LHE_1162, &b_GenWeight_LHE_1162);
   fChain->SetBranchAddress("GenWeight_LHE_1163", &GenWeight_LHE_1163, &b_GenWeight_LHE_1163);
   fChain->SetBranchAddress("GenWeight_LHE_1164", &GenWeight_LHE_1164, &b_GenWeight_LHE_1164);
   fChain->SetBranchAddress("GenWeight_LHE_1165", &GenWeight_LHE_1165, &b_GenWeight_LHE_1165);
   fChain->SetBranchAddress("GenWeight_LHE_1166", &GenWeight_LHE_1166, &b_GenWeight_LHE_1166);
   fChain->SetBranchAddress("GenWeight_LHE_1167", &GenWeight_LHE_1167, &b_GenWeight_LHE_1167);
   fChain->SetBranchAddress("GenWeight_LHE_1168", &GenWeight_LHE_1168, &b_GenWeight_LHE_1168);
   fChain->SetBranchAddress("GenWeight_LHE_1169", &GenWeight_LHE_1169, &b_GenWeight_LHE_1169);
   fChain->SetBranchAddress("GenWeight_LHE_1170", &GenWeight_LHE_1170, &b_GenWeight_LHE_1170);
   fChain->SetBranchAddress("GenWeight_LHE_1171", &GenWeight_LHE_1171, &b_GenWeight_LHE_1171);
   fChain->SetBranchAddress("GenWeight_LHE_1172", &GenWeight_LHE_1172, &b_GenWeight_LHE_1172);
   fChain->SetBranchAddress("GenWeight_LHE_1173", &GenWeight_LHE_1173, &b_GenWeight_LHE_1173);
   fChain->SetBranchAddress("GenWeight_LHE_1174", &GenWeight_LHE_1174, &b_GenWeight_LHE_1174);
   fChain->SetBranchAddress("GenWeight_LHE_1175", &GenWeight_LHE_1175, &b_GenWeight_LHE_1175);
   fChain->SetBranchAddress("GenWeight_LHE_1176", &GenWeight_LHE_1176, &b_GenWeight_LHE_1176);
   fChain->SetBranchAddress("GenWeight_LHE_1177", &GenWeight_LHE_1177, &b_GenWeight_LHE_1177);
   fChain->SetBranchAddress("GenWeight_LHE_1178", &GenWeight_LHE_1178, &b_GenWeight_LHE_1178);
   fChain->SetBranchAddress("GenWeight_LHE_1179", &GenWeight_LHE_1179, &b_GenWeight_LHE_1179);
   fChain->SetBranchAddress("EleWeight", &EleWeight, &b_EleWeight);
   fChain->SetBranchAddress("EleWeightId", &EleWeightId, &b_EleWeightId);
   fChain->SetBranchAddress("EleWeightIso", &EleWeightIso, &b_EleWeightIso);
   fChain->SetBranchAddress("EleWeightReco", &EleWeightReco, &b_EleWeightReco);
   fChain->SetBranchAddress("JetWeight", &JetWeight, &b_JetWeight);
   fChain->SetBranchAddress("JetWeightBTag", &JetWeightBTag, &b_JetWeightBTag);
   fChain->SetBranchAddress("JetWeightJVT", &JetWeightJVT, &b_JetWeightJVT);
   fChain->SetBranchAddress("MuoWeight", &MuoWeight, &b_MuoWeight);
   fChain->SetBranchAddress("MuoWeightIsol", &MuoWeightIsol, &b_MuoWeightIsol);
   fChain->SetBranchAddress("MuoWeightReco", &MuoWeightReco, &b_MuoWeightReco);
   fChain->SetBranchAddress("MuoWeightTTVA", &MuoWeightTTVA, &b_MuoWeightTTVA);
   fChain->SetBranchAddress("TauWeight", &TauWeight, &b_TauWeight);
   fChain->SetBranchAddress("TauWeightEleOREle", &TauWeightEleOREle, &b_TauWeightEleOREle);
   fChain->SetBranchAddress("TauWeightEleORHad", &TauWeightEleORHad, &b_TauWeightEleORHad);
   fChain->SetBranchAddress("TauWeightId", &TauWeightId, &b_TauWeightId);
   fChain->SetBranchAddress("TauWeightReco", &TauWeightReco, &b_TauWeightReco);
   fChain->SetBranchAddress("TauWeightTrigHLT_tau125_medium1_tracktwo", &TauWeightTrigHLT_tau125_medium1_tracktwo, &b_TauWeightTrigHLT_tau125_medium1_tracktwo);
   fChain->SetBranchAddress("TauWeightTrigHLT_tau160_medium1_tracktwo", &TauWeightTrigHLT_tau160_medium1_tracktwo, &b_TauWeightTrigHLT_tau160_medium1_tracktwo);
   fChain->SetBranchAddress("TauWeightTrigHLT_tau25_medium1_tracktwo", &TauWeightTrigHLT_tau25_medium1_tracktwo, &b_TauWeightTrigHLT_tau25_medium1_tracktwo);
   fChain->SetBranchAddress("TauWeightTrigHLT_tau35_medium1_tracktwo", &TauWeightTrigHLT_tau35_medium1_tracktwo, &b_TauWeightTrigHLT_tau35_medium1_tracktwo);
   fChain->SetBranchAddress("TauWeightTrigHLT_tau50_medium1_tracktwo_L1TAU12", &TauWeightTrigHLT_tau50_medium1_tracktwo_L1TAU12, &b_TauWeightTrigHLT_tau50_medium1_tracktwo_L1TAU12);
   fChain->SetBranchAddress("TauWeightTrigHLT_tau80_medium1_tracktwo", &TauWeightTrigHLT_tau80_medium1_tracktwo, &b_TauWeightTrigHLT_tau80_medium1_tracktwo);
   fChain->SetBranchAddress("TauWeightTrigHLT_tau80_medium1_tracktwo_L1TAU60", &TauWeightTrigHLT_tau80_medium1_tracktwo_L1TAU60, &b_TauWeightTrigHLT_tau80_medium1_tracktwo_L1TAU60);
   fChain->SetBranchAddress("muWeight", &muWeight, &b_muWeight);
   fChain->SetBranchAddress("Aplanarity", &Aplanarity, &b_Aplanarity);
   fChain->SetBranchAddress("Circularity", &Circularity, &b_Circularity);
   fChain->SetBranchAddress("CosChi1", &CosChi1, &b_CosChi1);
   fChain->SetBranchAddress("CosChi2", &CosChi2, &b_CosChi2);
   fChain->SetBranchAddress("Ht_Jet", &Ht_Jet, &b_Ht_Jet);
   fChain->SetBranchAddress("Ht_Lep", &Ht_Lep, &b_Ht_Lep);
   fChain->SetBranchAddress("Ht_Tau", &Ht_Tau, &b_Ht_Tau);
   fChain->SetBranchAddress("Lin_Aplanarity", &Lin_Aplanarity, &b_Lin_Aplanarity);
   fChain->SetBranchAddress("Lin_Circularity", &Lin_Circularity, &b_Lin_Circularity);
   fChain->SetBranchAddress("Lin_Planarity", &Lin_Planarity, &b_Lin_Planarity);
   fChain->SetBranchAddress("Lin_Sphericity", &Lin_Sphericity, &b_Lin_Sphericity);
   fChain->SetBranchAddress("Lin_Sphericity_C", &Lin_Sphericity_C, &b_Lin_Sphericity_C);
   fChain->SetBranchAddress("Lin_Sphericity_D", &Lin_Sphericity_D, &b_Lin_Sphericity_D);
   fChain->SetBranchAddress("MET_Centrality", &MET_Centrality, &b_MET_Centrality);
   fChain->SetBranchAddress("MET_CosMinDeltaPhi", &MET_CosMinDeltaPhi, &b_MET_CosMinDeltaPhi);
   fChain->SetBranchAddress("MET_LepTau_DeltaPhi", &MET_LepTau_DeltaPhi, &b_MET_LepTau_DeltaPhi);
   fChain->SetBranchAddress("MET_LepTau_VecSumPt", &MET_LepTau_VecSumPt, &b_MET_LepTau_VecSumPt);
   fChain->SetBranchAddress("MET_SumCosDeltaPhi", &MET_SumCosDeltaPhi, &b_MET_SumCosDeltaPhi);
   fChain->SetBranchAddress("MT2_max", &MT2_max, &b_MT2_max);
   fChain->SetBranchAddress("MT2_min", &MT2_min, &b_MT2_min);
   fChain->SetBranchAddress("Meff", &Meff, &b_Meff);
   fChain->SetBranchAddress("Meff_TauTau", &Meff_TauTau, &b_Meff_TauTau);
   fChain->SetBranchAddress("MetTST_OverSqrtHT", &MetTST_OverSqrtHT, &b_MetTST_OverSqrtHT);
   fChain->SetBranchAddress("MetTST_OverSqrtSumET", &MetTST_OverSqrtSumET, &b_MetTST_OverSqrtSumET);
   fChain->SetBranchAddress("MetTST_Significance", &MetTST_Significance, &b_MetTST_Significance);
   fChain->SetBranchAddress("MetTST_Significance_Rho", &MetTST_Significance_Rho, &b_MetTST_Significance_Rho);
   fChain->SetBranchAddress("MetTST_Significance_VarL", &MetTST_Significance_VarL, &b_MetTST_Significance_VarL);
   fChain->SetBranchAddress("MetTST_Significance_dataJER", &MetTST_Significance_dataJER, &b_MetTST_Significance_dataJER);
   fChain->SetBranchAddress("MetTST_Significance_dataJER_Rho", &MetTST_Significance_dataJER_Rho, &b_MetTST_Significance_dataJER_Rho);
   fChain->SetBranchAddress("MetTST_Significance_dataJER_VarL", &MetTST_Significance_dataJER_VarL, &b_MetTST_Significance_dataJER_VarL);
   fChain->SetBranchAddress("MetTST_Significance_dataJER_noPUJets", &MetTST_Significance_dataJER_noPUJets, &b_MetTST_Significance_dataJER_noPUJets);
   fChain->SetBranchAddress("MetTST_Significance_dataJER_noPUJets_Rho", &MetTST_Significance_dataJER_noPUJets_Rho, &b_MetTST_Significance_dataJER_noPUJets_Rho);
   fChain->SetBranchAddress("MetTST_Significance_dataJER_noPUJets_VarL", &MetTST_Significance_dataJER_noPUJets_VarL, &b_MetTST_Significance_dataJER_noPUJets_VarL);
   fChain->SetBranchAddress("MetTST_Significance_noPUJets", &MetTST_Significance_noPUJets, &b_MetTST_Significance_noPUJets);
   fChain->SetBranchAddress("MetTST_Significance_noPUJets_Rho", &MetTST_Significance_noPUJets_Rho, &b_MetTST_Significance_noPUJets_Rho);
   fChain->SetBranchAddress("MetTST_Significance_noPUJets_VarL", &MetTST_Significance_noPUJets_VarL, &b_MetTST_Significance_noPUJets_VarL);
   fChain->SetBranchAddress("MetTST_Significance_noPUJets_noSoftTerm", &MetTST_Significance_noPUJets_noSoftTerm, &b_MetTST_Significance_noPUJets_noSoftTerm);
   fChain->SetBranchAddress("MetTST_Significance_noPUJets_noSoftTerm_Rho", &MetTST_Significance_noPUJets_noSoftTerm_Rho, &b_MetTST_Significance_noPUJets_noSoftTerm_Rho);
   fChain->SetBranchAddress("MetTST_Significance_noPUJets_noSoftTerm_VarL", &MetTST_Significance_noPUJets_noSoftTerm_VarL, &b_MetTST_Significance_noPUJets_noSoftTerm_VarL);
   fChain->SetBranchAddress("MetTST_Significance_phireso_noPUJets", &MetTST_Significance_phireso_noPUJets, &b_MetTST_Significance_phireso_noPUJets);
   fChain->SetBranchAddress("MetTST_Significance_phireso_noPUJets_Rho", &MetTST_Significance_phireso_noPUJets_Rho, &b_MetTST_Significance_phireso_noPUJets_Rho);
   fChain->SetBranchAddress("MetTST_Significance_phireso_noPUJets_VarL", &MetTST_Significance_phireso_noPUJets_VarL, &b_MetTST_Significance_phireso_noPUJets_VarL);
   fChain->SetBranchAddress("Oblateness", &Oblateness, &b_Oblateness);
   fChain->SetBranchAddress("PTt", &PTt, &b_PTt);
   fChain->SetBranchAddress("Planarity", &Planarity, &b_Planarity);
   fChain->SetBranchAddress("Sphericity", &Sphericity, &b_Sphericity);
   fChain->SetBranchAddress("Spherocity", &Spherocity, &b_Spherocity);
   fChain->SetBranchAddress("Thrust", &Thrust, &b_Thrust);
   fChain->SetBranchAddress("ThrustMajor", &ThrustMajor, &b_ThrustMajor);
   fChain->SetBranchAddress("ThrustMinor", &ThrustMinor, &b_ThrustMinor);
   fChain->SetBranchAddress("VecSumPt_LepTau", &VecSumPt_LepTau, &b_VecSumPt_LepTau);
   fChain->SetBranchAddress("WolframMoment_0", &WolframMoment_0, &b_WolframMoment_0);
   fChain->SetBranchAddress("WolframMoment_1", &WolframMoment_1, &b_WolframMoment_1);
   fChain->SetBranchAddress("WolframMoment_10", &WolframMoment_10, &b_WolframMoment_10);
   fChain->SetBranchAddress("WolframMoment_11", &WolframMoment_11, &b_WolframMoment_11);
   fChain->SetBranchAddress("WolframMoment_2", &WolframMoment_2, &b_WolframMoment_2);
   fChain->SetBranchAddress("WolframMoment_3", &WolframMoment_3, &b_WolframMoment_3);
   fChain->SetBranchAddress("WolframMoment_4", &WolframMoment_4, &b_WolframMoment_4);
   fChain->SetBranchAddress("WolframMoment_5", &WolframMoment_5, &b_WolframMoment_5);
   fChain->SetBranchAddress("WolframMoment_6", &WolframMoment_6, &b_WolframMoment_6);
   fChain->SetBranchAddress("WolframMoment_7", &WolframMoment_7, &b_WolframMoment_7);
   fChain->SetBranchAddress("WolframMoment_8", &WolframMoment_8, &b_WolframMoment_8);
   fChain->SetBranchAddress("WolframMoment_9", &WolframMoment_9, &b_WolframMoment_9);
   fChain->SetBranchAddress("actualInteractionsPerCrossing", &actualInteractionsPerCrossing, &b_actualInteractionsPerCrossing);
   fChain->SetBranchAddress("averageInteractionsPerCrossing", &averageInteractionsPerCrossing, &b_averageInteractionsPerCrossing);
   fChain->SetBranchAddress("corr_avgIntPerX", &corr_avgIntPerX, &b_corr_avgIntPerX);
   fChain->SetBranchAddress("emt_MT2_max", &emt_MT2_max, &b_emt_MT2_max);
   fChain->SetBranchAddress("emt_MT2_min", &emt_MT2_min, &b_emt_MT2_min);
   fChain->SetBranchAddress("mu_density", &mu_density, &b_mu_density);
   fChain->SetBranchAddress("SUSYFinalState", &SUSYFinalState, &b_SUSYFinalState);
   fChain->SetBranchAddress("Vtx_n", &Vtx_n, &b_Vtx_n);
   fChain->SetBranchAddress("n_BJets", &n_BJets, &b_n_BJets);
   fChain->SetBranchAddress("n_BaseJets", &n_BaseJets, &b_n_BaseJets);
   fChain->SetBranchAddress("n_BaseTau", &n_BaseTau, &b_n_BaseTau);
   fChain->SetBranchAddress("n_SignalElec", &n_SignalElec, &b_n_SignalElec);
   fChain->SetBranchAddress("n_SignalJets", &n_SignalJets, &b_n_SignalJets);
   fChain->SetBranchAddress("n_SignalMuon", &n_SignalMuon, &b_n_SignalMuon);
   fChain->SetBranchAddress("n_SignalTau", &n_SignalTau, &b_n_SignalTau);
   fChain->SetBranchAddress("OS_BaseTauEle", &OS_BaseTauEle, &b_OS_BaseTauEle);
   fChain->SetBranchAddress("OS_BaseTauMuo", &OS_BaseTauMuo, &b_OS_BaseTauMuo);
   fChain->SetBranchAddress("OS_EleEle", &OS_EleEle, &b_OS_EleEle);
   fChain->SetBranchAddress("OS_MuoMuo", &OS_MuoMuo, &b_OS_MuoMuo);
   fChain->SetBranchAddress("OS_TauEle", &OS_TauEle, &b_OS_TauEle);
   fChain->SetBranchAddress("OS_TauMuo", &OS_TauMuo, &b_OS_TauMuo);
   fChain->SetBranchAddress("OS_TauTau", &OS_TauTau, &b_OS_TauTau);
   fChain->SetBranchAddress("TrigHLT_e120_lhloose", &TrigHLT_e120_lhloose, &b_TrigHLT_e120_lhloose);
   fChain->SetBranchAddress("TrigHLT_e140_lhloose_nod0", &TrigHLT_e140_lhloose_nod0, &b_TrigHLT_e140_lhloose_nod0);
   fChain->SetBranchAddress("TrigHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwo", &TrigHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwo, &b_TrigHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwo);
   fChain->SetBranchAddress("TrigHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwoEF", &TrigHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwoEF, &b_TrigHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwoEF);
   fChain->SetBranchAddress("TrigHLT_e17_lhmedium_nod0_ivarloose_tau25_mediumRNN_tracktwoMVA", &TrigHLT_e17_lhmedium_nod0_ivarloose_tau25_mediumRNN_tracktwoMVA, &b_TrigHLT_e17_lhmedium_nod0_ivarloose_tau25_mediumRNN_tracktwoMVA);
   fChain->SetBranchAddress("TrigHLT_e17_lhmedium_nod0_tau25_medium1_tracktwoEF_xe50", &TrigHLT_e17_lhmedium_nod0_tau25_medium1_tracktwoEF_xe50, &b_TrigHLT_e17_lhmedium_nod0_tau25_medium1_tracktwoEF_xe50);
   fChain->SetBranchAddress("TrigHLT_e17_lhmedium_nod0_tau25_medium1_tracktwo_xe50", &TrigHLT_e17_lhmedium_nod0_tau25_medium1_tracktwo_xe50, &b_TrigHLT_e17_lhmedium_nod0_tau25_medium1_tracktwo_xe50);
   fChain->SetBranchAddress("TrigHLT_e17_lhmedium_nod0_tau25_mediumRNN_tracktwoMVA_xe50", &TrigHLT_e17_lhmedium_nod0_tau25_mediumRNN_tracktwoMVA_xe50, &b_TrigHLT_e17_lhmedium_nod0_tau25_mediumRNN_tracktwoMVA_xe50);
   fChain->SetBranchAddress("TrigHLT_e24_lhmedium_L1EM20VH", &TrigHLT_e24_lhmedium_L1EM20VH, &b_TrigHLT_e24_lhmedium_L1EM20VH);
   fChain->SetBranchAddress("TrigHLT_e24_lhmedium_nod0_ivarloose_tau35_medium1_tracktwo", &TrigHLT_e24_lhmedium_nod0_ivarloose_tau35_medium1_tracktwo, &b_TrigHLT_e24_lhmedium_nod0_ivarloose_tau35_medium1_tracktwo);
   fChain->SetBranchAddress("TrigHLT_e26_lhtight_nod0_ivarloose", &TrigHLT_e26_lhtight_nod0_ivarloose, &b_TrigHLT_e26_lhtight_nod0_ivarloose);
   fChain->SetBranchAddress("TrigHLT_e60_lhmedium", &TrigHLT_e60_lhmedium, &b_TrigHLT_e60_lhmedium);
   fChain->SetBranchAddress("TrigHLT_e60_lhmedium_nod0", &TrigHLT_e60_lhmedium_nod0, &b_TrigHLT_e60_lhmedium_nod0);
   fChain->SetBranchAddress("TrigHLT_mu14_ivarloose_tau25_medium1_tracktwo", &TrigHLT_mu14_ivarloose_tau25_medium1_tracktwo, &b_TrigHLT_mu14_ivarloose_tau25_medium1_tracktwo);
   fChain->SetBranchAddress("TrigHLT_mu14_ivarloose_tau35_medium1_tracktwo", &TrigHLT_mu14_ivarloose_tau35_medium1_tracktwo, &b_TrigHLT_mu14_ivarloose_tau35_medium1_tracktwo);
   fChain->SetBranchAddress("TrigHLT_mu14_ivarloose_tau35_medium1_tracktwoEF", &TrigHLT_mu14_ivarloose_tau35_medium1_tracktwoEF, &b_TrigHLT_mu14_ivarloose_tau35_medium1_tracktwoEF);
   fChain->SetBranchAddress("TrigHLT_mu14_ivarloose_tau35_mediumRNN_tracktwoMVA", &TrigHLT_mu14_ivarloose_tau35_mediumRNN_tracktwoMVA, &b_TrigHLT_mu14_ivarloose_tau35_mediumRNN_tracktwoMVA);
   fChain->SetBranchAddress("TrigHLT_mu14_tau25_medium1_tracktwoEF_xe50", &TrigHLT_mu14_tau25_medium1_tracktwoEF_xe50, &b_TrigHLT_mu14_tau25_medium1_tracktwoEF_xe50);
   fChain->SetBranchAddress("TrigHLT_mu14_tau25_medium1_tracktwo_xe50", &TrigHLT_mu14_tau25_medium1_tracktwo_xe50, &b_TrigHLT_mu14_tau25_medium1_tracktwo_xe50);
   fChain->SetBranchAddress("TrigHLT_mu14_tau25_mediumRNN_tracktwoMVA_xe50", &TrigHLT_mu14_tau25_mediumRNN_tracktwoMVA_xe50, &b_TrigHLT_mu14_tau25_mediumRNN_tracktwoMVA_xe50);
   fChain->SetBranchAddress("TrigHLT_mu20_iloose_L1MU15", &TrigHLT_mu20_iloose_L1MU15, &b_TrigHLT_mu20_iloose_L1MU15);
   fChain->SetBranchAddress("TrigHLT_mu24_ivarmedium", &TrigHLT_mu24_ivarmedium, &b_TrigHLT_mu24_ivarmedium);
   fChain->SetBranchAddress("TrigHLT_mu26_imedium", &TrigHLT_mu26_imedium, &b_TrigHLT_mu26_imedium);
   fChain->SetBranchAddress("TrigHLT_mu26_ivarmedium", &TrigHLT_mu26_ivarmedium, &b_TrigHLT_mu26_ivarmedium);
   fChain->SetBranchAddress("TrigHLT_mu40", &TrigHLT_mu40, &b_TrigHLT_mu40);
   fChain->SetBranchAddress("TrigHLT_mu50", &TrigHLT_mu50, &b_TrigHLT_mu50);
   fChain->SetBranchAddress("TrigHLT_mu60_0eta105_msonly", &TrigHLT_mu60_0eta105_msonly, &b_TrigHLT_mu60_0eta105_msonly);
   fChain->SetBranchAddress("TrigHLT_tau125_medium1_tracktwo", &TrigHLT_tau125_medium1_tracktwo, &b_TrigHLT_tau125_medium1_tracktwo);
   fChain->SetBranchAddress("TrigHLT_tau160_medium1_tracktwo", &TrigHLT_tau160_medium1_tracktwo, &b_TrigHLT_tau160_medium1_tracktwo);
   fChain->SetBranchAddress("TrigHLT_tau25_medium1_tracktwo", &TrigHLT_tau25_medium1_tracktwo, &b_TrigHLT_tau25_medium1_tracktwo);
   fChain->SetBranchAddress("TrigHLT_tau25_medium1_tracktwoEF", &TrigHLT_tau25_medium1_tracktwoEF, &b_TrigHLT_tau25_medium1_tracktwoEF);
   fChain->SetBranchAddress("TrigHLT_tau25_mediumRNN_tracktwoMVA", &TrigHLT_tau25_mediumRNN_tracktwoMVA, &b_TrigHLT_tau25_mediumRNN_tracktwoMVA);
   fChain->SetBranchAddress("TrigHLT_tau35_medium1_tracktwo", &TrigHLT_tau35_medium1_tracktwo, &b_TrigHLT_tau35_medium1_tracktwo);
   fChain->SetBranchAddress("TrigHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo", &TrigHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo, &b_TrigHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo);
   fChain->SetBranchAddress("TrigHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_L1TAU20IM_2TAU12IM", &TrigHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_L1TAU20IM_2TAU12IM, &b_TrigHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_L1TAU20IM_2TAU12IM);
   fChain->SetBranchAddress("TrigHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_xe50", &TrigHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_xe50, &b_TrigHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_xe50);
   fChain->SetBranchAddress("TrigHLT_tau50_medium1_tracktwo_L1TAU12", &TrigHLT_tau50_medium1_tracktwo_L1TAU12, &b_TrigHLT_tau50_medium1_tracktwo_L1TAU12);
   fChain->SetBranchAddress("TrigHLT_tau60_medium1_tracktwo", &TrigHLT_tau60_medium1_tracktwo, &b_TrigHLT_tau60_medium1_tracktwo);
   fChain->SetBranchAddress("TrigHLT_tau60_medium1_tracktwoEF_tau25_medium1_tracktwoEF_xe50", &TrigHLT_tau60_medium1_tracktwoEF_tau25_medium1_tracktwoEF_xe50, &b_TrigHLT_tau60_medium1_tracktwoEF_tau25_medium1_tracktwoEF_xe50);
   fChain->SetBranchAddress("TrigHLT_tau60_medium1_tracktwo_tau25_medium1_tracktwo_xe50", &TrigHLT_tau60_medium1_tracktwo_tau25_medium1_tracktwo_xe50, &b_TrigHLT_tau60_medium1_tracktwo_tau25_medium1_tracktwo_xe50);
   fChain->SetBranchAddress("TrigHLT_tau60_mediumRNN_tracktwoMVA_tau25_mediumRNN_tracktwoMVA_xe50", &TrigHLT_tau60_mediumRNN_tracktwoMVA_tau25_mediumRNN_tracktwoMVA_xe50, &b_TrigHLT_tau60_mediumRNN_tracktwoMVA_tau25_mediumRNN_tracktwoMVA_xe50);
   fChain->SetBranchAddress("TrigHLT_tau80_medium1_tracktwo", &TrigHLT_tau80_medium1_tracktwo, &b_TrigHLT_tau80_medium1_tracktwo);
   fChain->SetBranchAddress("TrigHLT_tau80_medium1_tracktwoEF_L1TAU60_tau60_medium1_tracktwoEF_L1TAU40", &TrigHLT_tau80_medium1_tracktwoEF_L1TAU60_tau60_medium1_tracktwoEF_L1TAU40, &b_TrigHLT_tau80_medium1_tracktwoEF_L1TAU60_tau60_medium1_tracktwoEF_L1TAU40);
   fChain->SetBranchAddress("TrigHLT_tau80_medium1_tracktwo_L1TAU60", &TrigHLT_tau80_medium1_tracktwo_L1TAU60, &b_TrigHLT_tau80_medium1_tracktwo_L1TAU60);
   fChain->SetBranchAddress("TrigHLT_tau80_medium1_tracktwo_L1TAU60_tau25_medium1_tracktwo_L1TAU12", &TrigHLT_tau80_medium1_tracktwo_L1TAU60_tau25_medium1_tracktwo_L1TAU12, &b_TrigHLT_tau80_medium1_tracktwo_L1TAU60_tau25_medium1_tracktwo_L1TAU12);
   fChain->SetBranchAddress("TrigHLT_tau80_medium1_tracktwo_L1TAU60_tau50_medium1_tracktwo_L1TAU12", &TrigHLT_tau80_medium1_tracktwo_L1TAU60_tau50_medium1_tracktwo_L1TAU12, &b_TrigHLT_tau80_medium1_tracktwo_L1TAU60_tau50_medium1_tracktwo_L1TAU12);
   fChain->SetBranchAddress("TrigHLT_tau80_medium1_tracktwo_L1TAU60_tau60_medium1_tracktwo_L1TAU40", &TrigHLT_tau80_medium1_tracktwo_L1TAU60_tau60_medium1_tracktwo_L1TAU40, &b_TrigHLT_tau80_medium1_tracktwo_L1TAU60_tau60_medium1_tracktwo_L1TAU40);
   fChain->SetBranchAddress("TrigHLT_tau80_mediumRNN_tracktwoMVA_L1TAU60_tau60_mediumRNN_tracktwoMVA_L1TAU40", &TrigHLT_tau80_mediumRNN_tracktwoMVA_L1TAU60_tau60_mediumRNN_tracktwoMVA_L1TAU40, &b_TrigHLT_tau80_mediumRNN_tracktwoMVA_L1TAU60_tau60_mediumRNN_tracktwoMVA_L1TAU40);
   fChain->SetBranchAddress("TrigHLT_xe100_pufit_L1XE50", &TrigHLT_xe100_pufit_L1XE50, &b_TrigHLT_xe100_pufit_L1XE50);
   fChain->SetBranchAddress("TrigHLT_xe100_pufit_L1XE55", &TrigHLT_xe100_pufit_L1XE55, &b_TrigHLT_xe100_pufit_L1XE55);
   fChain->SetBranchAddress("TrigHLT_xe110_mht_L1XE50", &TrigHLT_xe110_mht_L1XE50, &b_TrigHLT_xe110_mht_L1XE50);
   fChain->SetBranchAddress("TrigHLT_xe110_pufit_L1XE50", &TrigHLT_xe110_pufit_L1XE50, &b_TrigHLT_xe110_pufit_L1XE50);
   fChain->SetBranchAddress("TrigHLT_xe110_pufit_L1XE55", &TrigHLT_xe110_pufit_L1XE55, &b_TrigHLT_xe110_pufit_L1XE55);
   fChain->SetBranchAddress("TrigHLT_xe110_pufit_xe65_L1XE50", &TrigHLT_xe110_pufit_xe65_L1XE50, &b_TrigHLT_xe110_pufit_xe65_L1XE50);
   fChain->SetBranchAddress("TrigHLT_xe110_pufit_xe70_L1XE50", &TrigHLT_xe110_pufit_xe70_L1XE50, &b_TrigHLT_xe110_pufit_xe70_L1XE50);
   fChain->SetBranchAddress("TrigHLT_xe120_pufit_L1XE50", &TrigHLT_xe120_pufit_L1XE50, &b_TrigHLT_xe120_pufit_L1XE50);
   fChain->SetBranchAddress("TrigHLT_xe70_mht", &TrigHLT_xe70_mht, &b_TrigHLT_xe70_mht);
   fChain->SetBranchAddress("TrigHLT_xe90_mht_L1XE50", &TrigHLT_xe90_mht_L1XE50, &b_TrigHLT_xe90_mht_L1XE50);
   fChain->SetBranchAddress("TrigHLT_xe90_pufit_L1XE50", &TrigHLT_xe90_pufit_L1XE50, &b_TrigHLT_xe90_pufit_L1XE50);
   fChain->SetBranchAddress("TrigMatchHLT_e120_lhloose", &TrigMatchHLT_e120_lhloose, &b_TrigMatchHLT_e120_lhloose);
   fChain->SetBranchAddress("TrigMatchHLT_e140_lhloose_nod0", &TrigMatchHLT_e140_lhloose_nod0, &b_TrigMatchHLT_e140_lhloose_nod0);
   fChain->SetBranchAddress("TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwo", &TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwo, &b_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwo);
   fChain->SetBranchAddress("TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwoEF", &TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwoEF, &b_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwoEF);
   fChain->SetBranchAddress("TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_mediumRNN_tracktwoMVA", &TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_mediumRNN_tracktwoMVA, &b_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_mediumRNN_tracktwoMVA);
   fChain->SetBranchAddress("TrigMatchHLT_e17_lhmedium_nod0_tau25_medium1_tracktwoEF_xe50", &TrigMatchHLT_e17_lhmedium_nod0_tau25_medium1_tracktwoEF_xe50, &b_TrigMatchHLT_e17_lhmedium_nod0_tau25_medium1_tracktwoEF_xe50);
   fChain->SetBranchAddress("TrigMatchHLT_e17_lhmedium_nod0_tau25_medium1_tracktwo_xe50", &TrigMatchHLT_e17_lhmedium_nod0_tau25_medium1_tracktwo_xe50, &b_TrigMatchHLT_e17_lhmedium_nod0_tau25_medium1_tracktwo_xe50);
   fChain->SetBranchAddress("TrigMatchHLT_e17_lhmedium_nod0_tau25_mediumRNN_tracktwoMVA_xe50", &TrigMatchHLT_e17_lhmedium_nod0_tau25_mediumRNN_tracktwoMVA_xe50, &b_TrigMatchHLT_e17_lhmedium_nod0_tau25_mediumRNN_tracktwoMVA_xe50);
   fChain->SetBranchAddress("TrigMatchHLT_e24_lhmedium_L1EM20VH", &TrigMatchHLT_e24_lhmedium_L1EM20VH, &b_TrigMatchHLT_e24_lhmedium_L1EM20VH);
   fChain->SetBranchAddress("TrigMatchHLT_e24_lhmedium_nod0_ivarloose_tau35_medium1_tracktwo", &TrigMatchHLT_e24_lhmedium_nod0_ivarloose_tau35_medium1_tracktwo, &b_TrigMatchHLT_e24_lhmedium_nod0_ivarloose_tau35_medium1_tracktwo);
   fChain->SetBranchAddress("TrigMatchHLT_e26_lhtight_nod0_ivarloose", &TrigMatchHLT_e26_lhtight_nod0_ivarloose, &b_TrigMatchHLT_e26_lhtight_nod0_ivarloose);
   fChain->SetBranchAddress("TrigMatchHLT_e60_lhmedium", &TrigMatchHLT_e60_lhmedium, &b_TrigMatchHLT_e60_lhmedium);
   fChain->SetBranchAddress("TrigMatchHLT_e60_lhmedium_nod0", &TrigMatchHLT_e60_lhmedium_nod0, &b_TrigMatchHLT_e60_lhmedium_nod0);
   fChain->SetBranchAddress("TrigMatchHLT_mu14_ivarloose_tau25_medium1_tracktwo", &TrigMatchHLT_mu14_ivarloose_tau25_medium1_tracktwo, &b_TrigMatchHLT_mu14_ivarloose_tau25_medium1_tracktwo);
   fChain->SetBranchAddress("TrigMatchHLT_mu14_ivarloose_tau35_medium1_tracktwo", &TrigMatchHLT_mu14_ivarloose_tau35_medium1_tracktwo, &b_TrigMatchHLT_mu14_ivarloose_tau35_medium1_tracktwo);
   fChain->SetBranchAddress("TrigMatchHLT_mu14_ivarloose_tau35_medium1_tracktwoEF", &TrigMatchHLT_mu14_ivarloose_tau35_medium1_tracktwoEF, &b_TrigMatchHLT_mu14_ivarloose_tau35_medium1_tracktwoEF);
   fChain->SetBranchAddress("TrigMatchHLT_mu14_ivarloose_tau35_mediumRNN_tracktwoMVA", &TrigMatchHLT_mu14_ivarloose_tau35_mediumRNN_tracktwoMVA, &b_TrigMatchHLT_mu14_ivarloose_tau35_mediumRNN_tracktwoMVA);
   fChain->SetBranchAddress("TrigMatchHLT_mu14_tau25_medium1_tracktwoEF_xe50", &TrigMatchHLT_mu14_tau25_medium1_tracktwoEF_xe50, &b_TrigMatchHLT_mu14_tau25_medium1_tracktwoEF_xe50);
   fChain->SetBranchAddress("TrigMatchHLT_mu14_tau25_medium1_tracktwo_xe50", &TrigMatchHLT_mu14_tau25_medium1_tracktwo_xe50, &b_TrigMatchHLT_mu14_tau25_medium1_tracktwo_xe50);
   fChain->SetBranchAddress("TrigMatchHLT_mu14_tau25_mediumRNN_tracktwoMVA_xe50", &TrigMatchHLT_mu14_tau25_mediumRNN_tracktwoMVA_xe50, &b_TrigMatchHLT_mu14_tau25_mediumRNN_tracktwoMVA_xe50);
   fChain->SetBranchAddress("TrigMatchHLT_mu20_iloose_L1MU15", &TrigMatchHLT_mu20_iloose_L1MU15, &b_TrigMatchHLT_mu20_iloose_L1MU15);
   fChain->SetBranchAddress("TrigMatchHLT_mu24_ivarmedium", &TrigMatchHLT_mu24_ivarmedium, &b_TrigMatchHLT_mu24_ivarmedium);
   fChain->SetBranchAddress("TrigMatchHLT_mu26_imedium", &TrigMatchHLT_mu26_imedium, &b_TrigMatchHLT_mu26_imedium);
   fChain->SetBranchAddress("TrigMatchHLT_mu26_ivarmedium", &TrigMatchHLT_mu26_ivarmedium, &b_TrigMatchHLT_mu26_ivarmedium);
   fChain->SetBranchAddress("TrigMatchHLT_mu40", &TrigMatchHLT_mu40, &b_TrigMatchHLT_mu40);
   fChain->SetBranchAddress("TrigMatchHLT_mu50", &TrigMatchHLT_mu50, &b_TrigMatchHLT_mu50);
   fChain->SetBranchAddress("TrigMatchHLT_mu60_0eta105_msonly", &TrigMatchHLT_mu60_0eta105_msonly, &b_TrigMatchHLT_mu60_0eta105_msonly);
   fChain->SetBranchAddress("TrigMatchHLT_tau125_medium1_tracktwo", &TrigMatchHLT_tau125_medium1_tracktwo, &b_TrigMatchHLT_tau125_medium1_tracktwo);
   fChain->SetBranchAddress("TrigMatchHLT_tau160_medium1_tracktwo", &TrigMatchHLT_tau160_medium1_tracktwo, &b_TrigMatchHLT_tau160_medium1_tracktwo);
   fChain->SetBranchAddress("TrigMatchHLT_tau25_medium1_tracktwo", &TrigMatchHLT_tau25_medium1_tracktwo, &b_TrigMatchHLT_tau25_medium1_tracktwo);
   fChain->SetBranchAddress("TrigMatchHLT_tau25_medium1_tracktwoEF", &TrigMatchHLT_tau25_medium1_tracktwoEF, &b_TrigMatchHLT_tau25_medium1_tracktwoEF);
   fChain->SetBranchAddress("TrigMatchHLT_tau25_mediumRNN_tracktwoMVA", &TrigMatchHLT_tau25_mediumRNN_tracktwoMVA, &b_TrigMatchHLT_tau25_mediumRNN_tracktwoMVA);
   fChain->SetBranchAddress("TrigMatchHLT_tau35_medium1_tracktwo", &TrigMatchHLT_tau35_medium1_tracktwo, &b_TrigMatchHLT_tau35_medium1_tracktwo);
   fChain->SetBranchAddress("TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo", &TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo, &b_TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo);
   fChain->SetBranchAddress("TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_L1TAU20IM_2TAU12IM", &TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_L1TAU20IM_2TAU12IM, &b_TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_L1TAU20IM_2TAU12IM);
   fChain->SetBranchAddress("TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_xe50", &TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_xe50, &b_TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_xe50);
   fChain->SetBranchAddress("TrigMatchHLT_tau50_medium1_tracktwo_L1TAU12", &TrigMatchHLT_tau50_medium1_tracktwo_L1TAU12, &b_TrigMatchHLT_tau50_medium1_tracktwo_L1TAU12);
   fChain->SetBranchAddress("TrigMatchHLT_tau60_medium1_tracktwo", &TrigMatchHLT_tau60_medium1_tracktwo, &b_TrigMatchHLT_tau60_medium1_tracktwo);
   fChain->SetBranchAddress("TrigMatchHLT_tau60_medium1_tracktwoEF_tau25_medium1_tracktwoEF_xe50", &TrigMatchHLT_tau60_medium1_tracktwoEF_tau25_medium1_tracktwoEF_xe50, &b_TrigMatchHLT_tau60_medium1_tracktwoEF_tau25_medium1_tracktwoEF_xe50);
   fChain->SetBranchAddress("TrigMatchHLT_tau60_medium1_tracktwo_tau25_medium1_tracktwo_xe50", &TrigMatchHLT_tau60_medium1_tracktwo_tau25_medium1_tracktwo_xe50, &b_TrigMatchHLT_tau60_medium1_tracktwo_tau25_medium1_tracktwo_xe50);
   fChain->SetBranchAddress("TrigMatchHLT_tau60_mediumRNN_tracktwoMVA_tau25_mediumRNN_tracktwoMVA_xe50", &TrigMatchHLT_tau60_mediumRNN_tracktwoMVA_tau25_mediumRNN_tracktwoMVA_xe50, &b_TrigMatchHLT_tau60_mediumRNN_tracktwoMVA_tau25_mediumRNN_tracktwoMVA_xe50);
   fChain->SetBranchAddress("TrigMatchHLT_tau80_medium1_tracktwo", &TrigMatchHLT_tau80_medium1_tracktwo, &b_TrigMatchHLT_tau80_medium1_tracktwo);
   fChain->SetBranchAddress("TrigMatchHLT_tau80_medium1_tracktwoEF_L1TAU60_tau60_medium1_tracktwoEF_L1TAU40", &TrigMatchHLT_tau80_medium1_tracktwoEF_L1TAU60_tau60_medium1_tracktwoEF_L1TAU40, &b_TrigMatchHLT_tau80_medium1_tracktwoEF_L1TAU60_tau60_medium1_tracktwoEF_L1TAU40);
   fChain->SetBranchAddress("TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60", &TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60, &b_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60);
   fChain->SetBranchAddress("TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau25_medium1_tracktwo_L1TAU12", &TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau25_medium1_tracktwo_L1TAU12, &b_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau25_medium1_tracktwo_L1TAU12);
   fChain->SetBranchAddress("TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau50_medium1_tracktwo_L1TAU12", &TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau50_medium1_tracktwo_L1TAU12, &b_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau50_medium1_tracktwo_L1TAU12);
   fChain->SetBranchAddress("TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau60_medium1_tracktwo_L1TAU40", &TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau60_medium1_tracktwo_L1TAU40, &b_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau60_medium1_tracktwo_L1TAU40);
   fChain->SetBranchAddress("TrigMatchHLT_tau80_mediumRNN_tracktwoMVA_L1TAU60_tau60_mediumRNN_tracktwoMVA_L1TAU40", &TrigMatchHLT_tau80_mediumRNN_tracktwoMVA_L1TAU60_tau60_mediumRNN_tracktwoMVA_L1TAU40, &b_TrigMatchHLT_tau80_mediumRNN_tracktwoMVA_L1TAU60_tau60_mediumRNN_tracktwoMVA_L1TAU40);
   fChain->SetBranchAddress("TrigMatching", &TrigMatching, &b_TrigMatching);
   fChain->SetBranchAddress("Trigger", &Trigger, &b_Trigger);
   fChain->SetBranchAddress("Lin_Sphericity_IsValid", &Lin_Sphericity_IsValid, &b_Lin_Sphericity_IsValid);
   fChain->SetBranchAddress("MET_BiSect", &MET_BiSect, &b_MET_BiSect);
   fChain->SetBranchAddress("Sphericity_IsValid", &Sphericity_IsValid, &b_Sphericity_IsValid);
   fChain->SetBranchAddress("Spherocity_IsValid", &Spherocity_IsValid, &b_Spherocity_IsValid);
   fChain->SetBranchAddress("Thrust_IsValid", &Thrust_IsValid, &b_Thrust_IsValid);
   fChain->SetBranchAddress("WolframMoments_AreValid", &WolframMoments_AreValid, &b_WolframMoments_AreValid);
   fChain->SetBranchAddress("RandomLumiBlockNumber", &RandomLumiBlockNumber, &b_RandomLumiBlockNumber);
   fChain->SetBranchAddress("RandomRunNumber", &RandomRunNumber, &b_RandomRunNumber);
   fChain->SetBranchAddress("mergedRunNumber", &mergedRunNumber, &b_mergedRunNumber);
   fChain->SetBranchAddress("backgroundFlags", &backgroundFlags, &b_backgroundFlags);
   fChain->SetBranchAddress("bcid", &bcid, &b_bcid);
   fChain->SetBranchAddress("coreFlags", &coreFlags, &b_coreFlags);
   fChain->SetBranchAddress("forwardDetFlags", &forwardDetFlags, &b_forwardDetFlags);
   fChain->SetBranchAddress("larFlags", &larFlags, &b_larFlags);
   fChain->SetBranchAddress("lumiBlock", &lumiBlock, &b_lumiBlock);
   fChain->SetBranchAddress("lumiFlags", &lumiFlags, &b_lumiFlags);
   fChain->SetBranchAddress("muonFlags", &muonFlags, &b_muonFlags);
   fChain->SetBranchAddress("pixelFlags", &pixelFlags, &b_pixelFlags);
   fChain->SetBranchAddress("runNumber", &runNumber, &b_runNumber);
   fChain->SetBranchAddress("sctFlags", &sctFlags, &b_sctFlags);
   fChain->SetBranchAddress("tileFlags", &tileFlags, &b_tileFlags);
   fChain->SetBranchAddress("trtFlags", &trtFlags, &b_trtFlags);
   fChain->SetBranchAddress("eventNumber", &eventNumber, &b_eventNumber);
   fChain->SetBranchAddress("MetCST_met", &MetCST_met, &b_MetCST_met);
   fChain->SetBranchAddress("MetCST_phi", &MetCST_phi, &b_MetCST_phi);
   fChain->SetBranchAddress("MetCST_sumet", &MetCST_sumet, &b_MetCST_sumet);
   fChain->SetBranchAddress("MetTST_met", &MetTST_met, &b_MetTST_met);
   fChain->SetBranchAddress("MetTST_phi", &MetTST_phi, &b_MetTST_phi);
   fChain->SetBranchAddress("MetTST_sumet", &MetTST_sumet, &b_MetTST_sumet);
   fChain->SetBranchAddress("TruthMET_met", &TruthMET_met, &b_TruthMET_met);
   fChain->SetBranchAddress("TruthMET_phi", &TruthMET_phi, &b_TruthMET_phi);
   fChain->SetBranchAddress("TruthMET_sumet", &TruthMET_sumet, &b_TruthMET_sumet);
   fChain->SetBranchAddress("RJW_JigSawCandidates_CosThetaStar", &RJW_JigSawCandidates_CosThetaStar, &b_RJW_JigSawCandidates_CosThetaStar);
   fChain->SetBranchAddress("RJW_JigSawCandidates_dPhiDecayPlane", &RJW_JigSawCandidates_dPhiDecayPlane, &b_RJW_JigSawCandidates_dPhiDecayPlane);
   fChain->SetBranchAddress("RJW_JigSawCandidates_pdgId", &RJW_JigSawCandidates_pdgId, &b_RJW_JigSawCandidates_pdgId);
   fChain->SetBranchAddress("RJW_JigSawCandidates_pt", &RJW_JigSawCandidates_pt, &b_RJW_JigSawCandidates_pt);
   fChain->SetBranchAddress("RJW_JigSawCandidates_eta", &RJW_JigSawCandidates_eta, &b_RJW_JigSawCandidates_eta);
   fChain->SetBranchAddress("RJW_JigSawCandidates_phi", &RJW_JigSawCandidates_phi, &b_RJW_JigSawCandidates_phi);
   fChain->SetBranchAddress("RJW_JigSawCandidates_m", &RJW_JigSawCandidates_m, &b_RJW_JigSawCandidates_m);
   fChain->SetBranchAddress("RJZ_JigSawCandidates_CosThetaStar", &RJZ_JigSawCandidates_CosThetaStar, &b_RJZ_JigSawCandidates_CosThetaStar);
   fChain->SetBranchAddress("RJZ_JigSawCandidates_dPhiDecayPlane", &RJZ_JigSawCandidates_dPhiDecayPlane, &b_RJZ_JigSawCandidates_dPhiDecayPlane);
   fChain->SetBranchAddress("RJZ_JigSawCandidates_pdgId", &RJZ_JigSawCandidates_pdgId, &b_RJZ_JigSawCandidates_pdgId);
   fChain->SetBranchAddress("RJZ_JigSawCandidates_pt", &RJZ_JigSawCandidates_pt, &b_RJZ_JigSawCandidates_pt);
   fChain->SetBranchAddress("RJZ_JigSawCandidates_eta", &RJZ_JigSawCandidates_eta, &b_RJZ_JigSawCandidates_eta);
   fChain->SetBranchAddress("RJZ_JigSawCandidates_phi", &RJZ_JigSawCandidates_phi, &b_RJZ_JigSawCandidates_phi);
   fChain->SetBranchAddress("RJZ_JigSawCandidates_m", &RJZ_JigSawCandidates_m, &b_RJZ_JigSawCandidates_m);
   fChain->SetBranchAddress("dilepton_charge", &dilepton_charge, &b_dilepton_charge);
   fChain->SetBranchAddress("dilepton_pdgId", &dilepton_pdgId, &b_dilepton_pdgId);
   fChain->SetBranchAddress("dilepton_pt", &dilepton_pt, &b_dilepton_pt);
   fChain->SetBranchAddress("dilepton_eta", &dilepton_eta, &b_dilepton_eta);
   fChain->SetBranchAddress("dilepton_phi", &dilepton_phi, &b_dilepton_phi);
   fChain->SetBranchAddress("dilepton_m", &dilepton_m, &b_dilepton_m);
   fChain->SetBranchAddress("electrons_MT", &electrons_MT, &b_electrons_MT);
   fChain->SetBranchAddress("electrons_TrigMatchHLT_e120_lhloose", &electrons_TrigMatchHLT_e120_lhloose, &b_electrons_TrigMatchHLT_e120_lhloose);
   fChain->SetBranchAddress("electrons_TrigMatchHLT_e140_lhloose_nod0", &electrons_TrigMatchHLT_e140_lhloose_nod0, &b_electrons_TrigMatchHLT_e140_lhloose_nod0);
   fChain->SetBranchAddress("electrons_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwo", &electrons_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwo, &b_electrons_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwo);
   fChain->SetBranchAddress("electrons_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwoEF", &electrons_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwoEF, &b_electrons_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwoEF);
   fChain->SetBranchAddress("electrons_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_mediumRNN_tracktwoMVA", &electrons_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_mediumRNN_tracktwoMVA, &b_electrons_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_mediumRNN_tracktwoMVA);
   fChain->SetBranchAddress("electrons_TrigMatchHLT_e17_lhmedium_nod0_tau25_medium1_tracktwoEF_xe50", &electrons_TrigMatchHLT_e17_lhmedium_nod0_tau25_medium1_tracktwoEF_xe50, &b_electrons_TrigMatchHLT_e17_lhmedium_nod0_tau25_medium1_tracktwoEF_xe50);
   fChain->SetBranchAddress("electrons_TrigMatchHLT_e17_lhmedium_nod0_tau25_medium1_tracktwo_xe50", &electrons_TrigMatchHLT_e17_lhmedium_nod0_tau25_medium1_tracktwo_xe50, &b_electrons_TrigMatchHLT_e17_lhmedium_nod0_tau25_medium1_tracktwo_xe50);
   fChain->SetBranchAddress("electrons_TrigMatchHLT_e17_lhmedium_nod0_tau25_mediumRNN_tracktwoMVA_xe50", &electrons_TrigMatchHLT_e17_lhmedium_nod0_tau25_mediumRNN_tracktwoMVA_xe50, &b_electrons_TrigMatchHLT_e17_lhmedium_nod0_tau25_mediumRNN_tracktwoMVA_xe50);
   fChain->SetBranchAddress("electrons_TrigMatchHLT_e24_lhmedium_L1EM20VH", &electrons_TrigMatchHLT_e24_lhmedium_L1EM20VH, &b_electrons_TrigMatchHLT_e24_lhmedium_L1EM20VH);
   fChain->SetBranchAddress("electrons_TrigMatchHLT_e24_lhmedium_nod0_ivarloose_tau35_medium1_tracktwo", &electrons_TrigMatchHLT_e24_lhmedium_nod0_ivarloose_tau35_medium1_tracktwo, &b_electrons_TrigMatchHLT_e24_lhmedium_nod0_ivarloose_tau35_medium1_tracktwo);
   fChain->SetBranchAddress("electrons_TrigMatchHLT_e26_lhtight_nod0_ivarloose", &electrons_TrigMatchHLT_e26_lhtight_nod0_ivarloose, &b_electrons_TrigMatchHLT_e26_lhtight_nod0_ivarloose);
   fChain->SetBranchAddress("electrons_TrigMatchHLT_e60_lhmedium", &electrons_TrigMatchHLT_e60_lhmedium, &b_electrons_TrigMatchHLT_e60_lhmedium);
   fChain->SetBranchAddress("electrons_TrigMatchHLT_e60_lhmedium_nod0", &electrons_TrigMatchHLT_e60_lhmedium_nod0, &b_electrons_TrigMatchHLT_e60_lhmedium_nod0);
   fChain->SetBranchAddress("electrons_charge", &electrons_charge, &b_electrons_charge);
   fChain->SetBranchAddress("electrons_d0sig", &electrons_d0sig, &b_electrons_d0sig);
   fChain->SetBranchAddress("electrons_isol", &electrons_isol, &b_electrons_isol);
   fChain->SetBranchAddress("electrons_signal", &electrons_signal, &b_electrons_signal);
   fChain->SetBranchAddress("electrons_truthOrigin", &electrons_truthOrigin, &b_electrons_truthOrigin);
   fChain->SetBranchAddress("electrons_truthType", &electrons_truthType, &b_electrons_truthType);
   fChain->SetBranchAddress("electrons_z0sinTheta", &electrons_z0sinTheta, &b_electrons_z0sinTheta);
   fChain->SetBranchAddress("electrons_pt", &electrons_pt, &b_electrons_pt);
   fChain->SetBranchAddress("electrons_eta", &electrons_eta, &b_electrons_eta);
   fChain->SetBranchAddress("electrons_phi", &electrons_phi, &b_electrons_phi);
   fChain->SetBranchAddress("electrons_e", &electrons_e, &b_electrons_e);
   fChain->SetBranchAddress("jets_Jvt", &jets_Jvt, &b_jets_Jvt);
   fChain->SetBranchAddress("jets_MV2c10", &jets_MV2c10, &b_jets_MV2c10);
   fChain->SetBranchAddress("jets_NTrks", &jets_NTrks, &b_jets_NTrks);
   fChain->SetBranchAddress("jets_bjet", &jets_bjet, &b_jets_bjet);
   fChain->SetBranchAddress("jets_signal", &jets_signal, &b_jets_signal);
   fChain->SetBranchAddress("jets_pt", &jets_pt, &b_jets_pt);
   fChain->SetBranchAddress("jets_eta", &jets_eta, &b_jets_eta);
   fChain->SetBranchAddress("jets_phi", &jets_phi, &b_jets_phi);
   fChain->SetBranchAddress("jets_m", &jets_m, &b_jets_m);
   fChain->SetBranchAddress("muons_MT", &muons_MT, &b_muons_MT);
   fChain->SetBranchAddress("muons_TrigMatchHLT_mu14_ivarloose_tau25_medium1_tracktwo", &muons_TrigMatchHLT_mu14_ivarloose_tau25_medium1_tracktwo, &b_muons_TrigMatchHLT_mu14_ivarloose_tau25_medium1_tracktwo);
   fChain->SetBranchAddress("muons_TrigMatchHLT_mu14_ivarloose_tau35_medium1_tracktwo", &muons_TrigMatchHLT_mu14_ivarloose_tau35_medium1_tracktwo, &b_muons_TrigMatchHLT_mu14_ivarloose_tau35_medium1_tracktwo);
   fChain->SetBranchAddress("muons_TrigMatchHLT_mu14_ivarloose_tau35_medium1_tracktwoEF", &muons_TrigMatchHLT_mu14_ivarloose_tau35_medium1_tracktwoEF, &b_muons_TrigMatchHLT_mu14_ivarloose_tau35_medium1_tracktwoEF);
   fChain->SetBranchAddress("muons_TrigMatchHLT_mu14_ivarloose_tau35_mediumRNN_tracktwoMVA", &muons_TrigMatchHLT_mu14_ivarloose_tau35_mediumRNN_tracktwoMVA, &b_muons_TrigMatchHLT_mu14_ivarloose_tau35_mediumRNN_tracktwoMVA);
   fChain->SetBranchAddress("muons_TrigMatchHLT_mu14_tau25_medium1_tracktwoEF_xe50", &muons_TrigMatchHLT_mu14_tau25_medium1_tracktwoEF_xe50, &b_muons_TrigMatchHLT_mu14_tau25_medium1_tracktwoEF_xe50);
   fChain->SetBranchAddress("muons_TrigMatchHLT_mu14_tau25_medium1_tracktwo_xe50", &muons_TrigMatchHLT_mu14_tau25_medium1_tracktwo_xe50, &b_muons_TrigMatchHLT_mu14_tau25_medium1_tracktwo_xe50);
   fChain->SetBranchAddress("muons_TrigMatchHLT_mu14_tau25_mediumRNN_tracktwoMVA_xe50", &muons_TrigMatchHLT_mu14_tau25_mediumRNN_tracktwoMVA_xe50, &b_muons_TrigMatchHLT_mu14_tau25_mediumRNN_tracktwoMVA_xe50);
   fChain->SetBranchAddress("muons_TrigMatchHLT_mu20_iloose_L1MU15", &muons_TrigMatchHLT_mu20_iloose_L1MU15, &b_muons_TrigMatchHLT_mu20_iloose_L1MU15);
   fChain->SetBranchAddress("muons_TrigMatchHLT_mu24_ivarmedium", &muons_TrigMatchHLT_mu24_ivarmedium, &b_muons_TrigMatchHLT_mu24_ivarmedium);
   fChain->SetBranchAddress("muons_TrigMatchHLT_mu26_imedium", &muons_TrigMatchHLT_mu26_imedium, &b_muons_TrigMatchHLT_mu26_imedium);
   fChain->SetBranchAddress("muons_TrigMatchHLT_mu26_ivarmedium", &muons_TrigMatchHLT_mu26_ivarmedium, &b_muons_TrigMatchHLT_mu26_ivarmedium);
   fChain->SetBranchAddress("muons_TrigMatchHLT_mu40", &muons_TrigMatchHLT_mu40, &b_muons_TrigMatchHLT_mu40);
   fChain->SetBranchAddress("muons_TrigMatchHLT_mu50", &muons_TrigMatchHLT_mu50, &b_muons_TrigMatchHLT_mu50);
   fChain->SetBranchAddress("muons_TrigMatchHLT_mu60_0eta105_msonly", &muons_TrigMatchHLT_mu60_0eta105_msonly, &b_muons_TrigMatchHLT_mu60_0eta105_msonly);
   fChain->SetBranchAddress("muons_charge", &muons_charge, &b_muons_charge);
   fChain->SetBranchAddress("muons_d0sig", &muons_d0sig, &b_muons_d0sig);
   fChain->SetBranchAddress("muons_isol", &muons_isol, &b_muons_isol);
   fChain->SetBranchAddress("muons_signal", &muons_signal, &b_muons_signal);
   fChain->SetBranchAddress("muons_truthOrigin", &muons_truthOrigin, &b_muons_truthOrigin);
   fChain->SetBranchAddress("muons_truthType", &muons_truthType, &b_muons_truthType);
   fChain->SetBranchAddress("muons_z0sinTheta", &muons_z0sinTheta, &b_muons_z0sinTheta);
   fChain->SetBranchAddress("muons_pt", &muons_pt, &b_muons_pt);
   fChain->SetBranchAddress("muons_eta", &muons_eta, &b_muons_eta);
   fChain->SetBranchAddress("muons_phi", &muons_phi, &b_muons_phi);
   fChain->SetBranchAddress("muons_e", &muons_e, &b_muons_e);
   fChain->SetBranchAddress("taus_BDTEleScore", &taus_BDTEleScore, &b_taus_BDTEleScore);
   fChain->SetBranchAddress("taus_BDTEleScoreSigTrans", &taus_BDTEleScoreSigTrans, &b_taus_BDTEleScoreSigTrans);
   fChain->SetBranchAddress("taus_BDTJetScore", &taus_BDTJetScore, &b_taus_BDTJetScore);
   fChain->SetBranchAddress("taus_BDTJetScoreSigTrans", &taus_BDTJetScoreSigTrans, &b_taus_BDTJetScoreSigTrans);
   fChain->SetBranchAddress("taus_ConeTruthLabelID", &taus_ConeTruthLabelID, &b_taus_ConeTruthLabelID);
   fChain->SetBranchAddress("taus_MT", &taus_MT, &b_taus_MT);
   fChain->SetBranchAddress("taus_NTrks", &taus_NTrks, &b_taus_NTrks);
   fChain->SetBranchAddress("taus_NTrksJet", &taus_NTrksJet, &b_taus_NTrksJet);
   fChain->SetBranchAddress("taus_PartonTruthLabelID", &taus_PartonTruthLabelID, &b_taus_PartonTruthLabelID);
   fChain->SetBranchAddress("taus_Quality", &taus_Quality, &b_taus_Quality);
   fChain->SetBranchAddress("taus_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwo", &taus_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwo, &b_taus_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwo);
   fChain->SetBranchAddress("taus_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwoEF", &taus_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwoEF, &b_taus_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwoEF);
   fChain->SetBranchAddress("taus_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_mediumRNN_tracktwoMVA", &taus_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_mediumRNN_tracktwoMVA, &b_taus_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_mediumRNN_tracktwoMVA);
   fChain->SetBranchAddress("taus_TrigMatchHLT_e17_lhmedium_nod0_tau25_medium1_tracktwoEF_xe50", &taus_TrigMatchHLT_e17_lhmedium_nod0_tau25_medium1_tracktwoEF_xe50, &b_taus_TrigMatchHLT_e17_lhmedium_nod0_tau25_medium1_tracktwoEF_xe50);
   fChain->SetBranchAddress("taus_TrigMatchHLT_e17_lhmedium_nod0_tau25_medium1_tracktwo_xe50", &taus_TrigMatchHLT_e17_lhmedium_nod0_tau25_medium1_tracktwo_xe50, &b_taus_TrigMatchHLT_e17_lhmedium_nod0_tau25_medium1_tracktwo_xe50);
   fChain->SetBranchAddress("taus_TrigMatchHLT_e17_lhmedium_nod0_tau25_mediumRNN_tracktwoMVA_xe50", &taus_TrigMatchHLT_e17_lhmedium_nod0_tau25_mediumRNN_tracktwoMVA_xe50, &b_taus_TrigMatchHLT_e17_lhmedium_nod0_tau25_mediumRNN_tracktwoMVA_xe50);
   fChain->SetBranchAddress("taus_TrigMatchHLT_e24_lhmedium_nod0_ivarloose_tau35_medium1_tracktwo", &taus_TrigMatchHLT_e24_lhmedium_nod0_ivarloose_tau35_medium1_tracktwo, &b_taus_TrigMatchHLT_e24_lhmedium_nod0_ivarloose_tau35_medium1_tracktwo);
   fChain->SetBranchAddress("taus_TrigMatchHLT_mu14_ivarloose_tau25_medium1_tracktwo", &taus_TrigMatchHLT_mu14_ivarloose_tau25_medium1_tracktwo, &b_taus_TrigMatchHLT_mu14_ivarloose_tau25_medium1_tracktwo);
   fChain->SetBranchAddress("taus_TrigMatchHLT_mu14_ivarloose_tau35_medium1_tracktwo", &taus_TrigMatchHLT_mu14_ivarloose_tau35_medium1_tracktwo, &b_taus_TrigMatchHLT_mu14_ivarloose_tau35_medium1_tracktwo);
   fChain->SetBranchAddress("taus_TrigMatchHLT_mu14_ivarloose_tau35_medium1_tracktwoEF", &taus_TrigMatchHLT_mu14_ivarloose_tau35_medium1_tracktwoEF, &b_taus_TrigMatchHLT_mu14_ivarloose_tau35_medium1_tracktwoEF);
   fChain->SetBranchAddress("taus_TrigMatchHLT_mu14_ivarloose_tau35_mediumRNN_tracktwoMVA", &taus_TrigMatchHLT_mu14_ivarloose_tau35_mediumRNN_tracktwoMVA, &b_taus_TrigMatchHLT_mu14_ivarloose_tau35_mediumRNN_tracktwoMVA);
   fChain->SetBranchAddress("taus_TrigMatchHLT_mu14_tau25_medium1_tracktwoEF_xe50", &taus_TrigMatchHLT_mu14_tau25_medium1_tracktwoEF_xe50, &b_taus_TrigMatchHLT_mu14_tau25_medium1_tracktwoEF_xe50);
   fChain->SetBranchAddress("taus_TrigMatchHLT_mu14_tau25_medium1_tracktwo_xe50", &taus_TrigMatchHLT_mu14_tau25_medium1_tracktwo_xe50, &b_taus_TrigMatchHLT_mu14_tau25_medium1_tracktwo_xe50);
   fChain->SetBranchAddress("taus_TrigMatchHLT_mu14_tau25_mediumRNN_tracktwoMVA_xe50", &taus_TrigMatchHLT_mu14_tau25_mediumRNN_tracktwoMVA_xe50, &b_taus_TrigMatchHLT_mu14_tau25_mediumRNN_tracktwoMVA_xe50);
   fChain->SetBranchAddress("taus_TrigMatchHLT_tau125_medium1_tracktwo", &taus_TrigMatchHLT_tau125_medium1_tracktwo, &b_taus_TrigMatchHLT_tau125_medium1_tracktwo);
   fChain->SetBranchAddress("taus_TrigMatchHLT_tau160_medium1_tracktwo", &taus_TrigMatchHLT_tau160_medium1_tracktwo, &b_taus_TrigMatchHLT_tau160_medium1_tracktwo);
   fChain->SetBranchAddress("taus_TrigMatchHLT_tau25_medium1_tracktwo", &taus_TrigMatchHLT_tau25_medium1_tracktwo, &b_taus_TrigMatchHLT_tau25_medium1_tracktwo);
   fChain->SetBranchAddress("taus_TrigMatchHLT_tau25_medium1_tracktwoEF", &taus_TrigMatchHLT_tau25_medium1_tracktwoEF, &b_taus_TrigMatchHLT_tau25_medium1_tracktwoEF);
   fChain->SetBranchAddress("taus_TrigMatchHLT_tau25_mediumRNN_tracktwoMVA", &taus_TrigMatchHLT_tau25_mediumRNN_tracktwoMVA, &b_taus_TrigMatchHLT_tau25_mediumRNN_tracktwoMVA);
   fChain->SetBranchAddress("taus_TrigMatchHLT_tau35_medium1_tracktwo", &taus_TrigMatchHLT_tau35_medium1_tracktwo, &b_taus_TrigMatchHLT_tau35_medium1_tracktwo);
   fChain->SetBranchAddress("taus_TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo", &taus_TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo, &b_taus_TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo);
   fChain->SetBranchAddress("taus_TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_L1TAU20IM_2TAU12IM", &taus_TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_L1TAU20IM_2TAU12IM, &b_taus_TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_L1TAU20IM_2TAU12IM);
   fChain->SetBranchAddress("taus_TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_xe50", &taus_TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_xe50, &b_taus_TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_xe50);
   fChain->SetBranchAddress("taus_TrigMatchHLT_tau50_medium1_tracktwo_L1TAU12", &taus_TrigMatchHLT_tau50_medium1_tracktwo_L1TAU12, &b_taus_TrigMatchHLT_tau50_medium1_tracktwo_L1TAU12);
   fChain->SetBranchAddress("taus_TrigMatchHLT_tau60_medium1_tracktwo", &taus_TrigMatchHLT_tau60_medium1_tracktwo, &b_taus_TrigMatchHLT_tau60_medium1_tracktwo);
   fChain->SetBranchAddress("taus_TrigMatchHLT_tau60_medium1_tracktwoEF_tau25_medium1_tracktwoEF_xe50", &taus_TrigMatchHLT_tau60_medium1_tracktwoEF_tau25_medium1_tracktwoEF_xe50, &b_taus_TrigMatchHLT_tau60_medium1_tracktwoEF_tau25_medium1_tracktwoEF_xe50);
   fChain->SetBranchAddress("taus_TrigMatchHLT_tau60_medium1_tracktwo_tau25_medium1_tracktwo_xe50", &taus_TrigMatchHLT_tau60_medium1_tracktwo_tau25_medium1_tracktwo_xe50, &b_taus_TrigMatchHLT_tau60_medium1_tracktwo_tau25_medium1_tracktwo_xe50);
   fChain->SetBranchAddress("taus_TrigMatchHLT_tau60_mediumRNN_tracktwoMVA_tau25_mediumRNN_tracktwoMVA_xe50", &taus_TrigMatchHLT_tau60_mediumRNN_tracktwoMVA_tau25_mediumRNN_tracktwoMVA_xe50, &b_taus_TrigMatchHLT_tau60_mediumRNN_tracktwoMVA_tau25_mediumRNN_tracktwoMVA_xe50);
   fChain->SetBranchAddress("taus_TrigMatchHLT_tau80_medium1_tracktwo", &taus_TrigMatchHLT_tau80_medium1_tracktwo, &b_taus_TrigMatchHLT_tau80_medium1_tracktwo);
   fChain->SetBranchAddress("taus_TrigMatchHLT_tau80_medium1_tracktwoEF_L1TAU60_tau60_medium1_tracktwoEF_L1TAU40", &taus_TrigMatchHLT_tau80_medium1_tracktwoEF_L1TAU60_tau60_medium1_tracktwoEF_L1TAU40, &b_taus_TrigMatchHLT_tau80_medium1_tracktwoEF_L1TAU60_tau60_medium1_tracktwoEF_L1TAU40);
   fChain->SetBranchAddress("taus_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60", &taus_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60, &b_taus_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60);
   fChain->SetBranchAddress("taus_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau25_medium1_tracktwo_L1TAU12", &taus_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau25_medium1_tracktwo_L1TAU12, &b_taus_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau25_medium1_tracktwo_L1TAU12);
   fChain->SetBranchAddress("taus_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau50_medium1_tracktwo_L1TAU12", &taus_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau50_medium1_tracktwo_L1TAU12, &b_taus_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau50_medium1_tracktwo_L1TAU12);
   fChain->SetBranchAddress("taus_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau60_medium1_tracktwo_L1TAU40", &taus_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau60_medium1_tracktwo_L1TAU40, &b_taus_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau60_medium1_tracktwo_L1TAU40);
   fChain->SetBranchAddress("taus_TrigMatchHLT_tau80_mediumRNN_tracktwoMVA_L1TAU60_tau60_mediumRNN_tracktwoMVA_L1TAU40", &taus_TrigMatchHLT_tau80_mediumRNN_tracktwoMVA_L1TAU60_tau60_mediumRNN_tracktwoMVA_L1TAU40, &b_taus_TrigMatchHLT_tau80_mediumRNN_tracktwoMVA_L1TAU60_tau60_mediumRNN_tracktwoMVA_L1TAU40);
   fChain->SetBranchAddress("taus_Width", &taus_Width, &b_taus_Width);
   fChain->SetBranchAddress("taus_charge", &taus_charge, &b_taus_charge);
   fChain->SetBranchAddress("taus_signalID", &taus_signalID, &b_taus_signalID);
   fChain->SetBranchAddress("taus_truthOrigin", &taus_truthOrigin, &b_taus_truthOrigin);
   fChain->SetBranchAddress("taus_truthType", &taus_truthType, &b_taus_truthType);
   fChain->SetBranchAddress("taus_pt", &taus_pt, &b_taus_pt);
   fChain->SetBranchAddress("taus_eta", &taus_eta, &b_taus_eta);
   fChain->SetBranchAddress("taus_phi", &taus_phi, &b_taus_phi);
   fChain->SetBranchAddress("taus_e", &taus_e, &b_taus_e);
   fChain->SetBranchAddress("tau1Pt", &tau1Pt, &b_tau1Pt);
   fChain->SetBranchAddress("tau2Pt", &tau2Pt, &b_tau2Pt);
   fChain->SetBranchAddress("tau1Mt", &tau1Mt, &b_tau1Mt);
   fChain->SetBranchAddress("tau2Mt", &tau2Mt, &b_tau2Mt);
   fChain->SetBranchAddress("dRtt", &dRtt, &b_dRtt);
   fChain->SetBranchAddress("dPhitt", &dPhitt, &b_dPhitt);
   fChain->SetBranchAddress("mt12tau", &mt12tau, &b_mt12tau);
   fChain->SetBranchAddress("Mll_tt", &Mll_tt, &b_Mll_tt);
   fChain->SetBranchAddress("meff_tt", &meff_tt, &b_meff_tt);
   fChain->SetBranchAddress("MT2", &MT2, &b_MT2);
   fChain->SetBranchAddress("tau1Pt_W", &tau1Pt_W, &b_tau1Pt_W);
   fChain->SetBranchAddress("mu1Pt", &mu1Pt, &b_mu1Pt);
   fChain->SetBranchAddress("tau1Mt_W", &tau1Mt_W, &b_tau1Mt_W);
   fChain->SetBranchAddress("mu1Mt", &mu1Mt, &b_mu1Mt);
   fChain->SetBranchAddress("dRtm", &dRtm, &b_dRtm);
   fChain->SetBranchAddress("dPhitm", &dPhitm, &b_dPhitm);
   fChain->SetBranchAddress("mt12tm", &mt12tm, &b_mt12tm);
   fChain->SetBranchAddress("Mll_tm", &Mll_tm, &b_Mll_tm);
   fChain->SetBranchAddress("meff_tm", &meff_tm, &b_meff_tm);
   fChain->SetBranchAddress("MT2_tm", &MT2_tm, &b_MT2_tm);
   fChain->SetBranchAddress("lJet_pt", &lJet_pt, &b_lJet_pt);
   fChain->SetBranchAddress("LJet_n", &LJet_n, &b_LJet_n);
   fChain->SetBranchAddress("Evt_MET", &Evt_MET, &b_Evt_MET);
   fChain->SetBranchAddress("Evt_METSIG", &Evt_METSIG, &b_Evt_METSIG);
   fChain->SetBranchAddress("Evt_METSIG_WithTauJet", &Evt_METSIG_WithTauJet, &b_Evt_METSIG_WithTauJet);
   fChain->SetBranchAddress("isSR1", &isSR1, &b_isSR1);
   fChain->SetBranchAddress("isSR2", &isSR2, &b_isSR2);
   fChain->SetBranchAddress("isWCR", &isWCR, &b_isWCR);
   fChain->SetBranchAddress("isWVR", &isWVR, &b_isWVR);
   fChain->SetBranchAddress("isTVR", &isTVR, &b_isTVR);
   fChain->SetBranchAddress("isZVR", &isZVR, &b_isZVR);
   fChain->SetBranchAddress("isQCDCR_SR1", &isQCDCR_SR1, &b_isQCDCR_SR1);
   fChain->SetBranchAddress("isQCDCR_SR2", &isQCDCR_SR2, &b_isQCDCR_SR2);
   fChain->SetBranchAddress("isQCDCR_WCR", &isQCDCR_WCR, &b_isQCDCR_WCR);
   fChain->SetBranchAddress("isQCDCR_WVR", &isQCDCR_WVR, &b_isQCDCR_WVR);

//   newTree = fChain->CloneTree(0);
//   newTree->SetBranchStatus("*",0);
//   newTree->SetBranchStatus("*up*",1);
//   newTree->SetBranchStatus("*down*",1);
//   newTree->SetBranchStatus("*Weight*",1);
//   newTree->SetBranchStatus("*weight*",1);
   Notify();
}

Bool_t selector::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void selector::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t selector::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef selector_cxx

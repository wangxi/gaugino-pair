#pragma once
#include <string>
#include <fstream>
#include <iostream>
#include "json.hpp"

class MetaDB
{
using json = nlohmann::json;

public:
	static MetaDB * getInstance() {
		if (mMetaDB == NULL)
		{
			mMetaDB = new MetaDB();
		}
		return mMetaDB;
	}
	~MetaDB();
	void loadMetaDBFile(std::string dbFile);
	void resetMetaDBFile(std::string dbFile);
	double getLumi();
	double getXsection(int mcChannel, int SUSYFinalState);
	double getNnorm(int mcChannel, int SUSYFinalState);
	double getkFactor(int mcChannel);
	double getFilterEff(int mcChannel);
private:
	MetaDB() {
		std::cout << "Metadata database class constructed. Warnning! This class currently only support constructing in one thread."
			<< "If you see this message twice or more. Please check you code." << std::endl;
	};
	static MetaDB* mMetaDB;
	json metadata;
};


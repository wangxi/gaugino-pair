#pragma once
#include "Hist.h"
#include "Utils.h"
#include <fstream>
#include <vector>
#include <functional>

//A region is defined by cuts. We can also register some histograms into it
class Region {
public:

	std::function<bool()> cut;
	std::vector<Hist*> hists;

	Region(std::function<bool()> cut) {
		this->cut = cut;
		sum = 0;
		err = 0;
	};
	Region() {}
	void setCut(std::function<bool()> cut) {
		this->cut = cut;
	}
	void addHist(std::string name, int Nbins, double LowValue, double UpValue, std::function<double()> value, bool useOverflow) {
		hists.emplace_back(new Hist(name, Nbins, LowValue, UpValue, value, useOverflow));
	}
	void addHist(Hist* hist) {
		hists.emplace_back(hist);
	}
	void addHist(HistTemplate histtemplate, std::string name) {
		hists.emplace_back(histtemplate.createHist(name));
	}
	void fill(double weight) {
		if (cut())
		{
			sum += weight;
			err = sqrt(err * err + weight * weight);
			for (auto hist : hists) {
				hist->Fill(weight);
			}
		}
	}
	void printOut(std::string output = "", std::string marker = "") {
		std::string name;
		if (output == ""){
			name = "yield.csv";
		}else {
			name = output + ".csv";
		}
		std::ofstream yield(name, std::ios::app);
		yield << marker << ", ";
		Utils::roundCSV(yield, sum, err);
		yield << std::endl;
	}
	//define a new region based on previous region with an addition cut -- Error: The func will let the program crash. Should study more
/*	Region* addCut(std::function<bool()> additionCut){
		std::function<bool()> newCut = [&] {return (cut() && additionCut());};
		return new Region(newCut);
	}*/
private:
	double sum, err;
};

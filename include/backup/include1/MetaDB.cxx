#include "MetaDB.h"

MetaDB* MetaDB::mMetaDB = NULL;

void MetaDB::loadMetaDBFile(std::string dbFile)
{
	std::ifstream cFile(dbFile);
	if(!cFile){
		std::cerr << "File " << dbFile << " not Found! Loading terminated" << std::endl;
		return;
	}
	if(metadata.empty()){
		cFile >> metadata;
	} else{
		json tmp;
		cFile >> tmp;
		metadata.merge_patch(tmp);
	}
}

void MetaDB::resetMetaDBFile(std::string dbFile)
{
	std::ifstream cFile(dbFile);
	if(!cFile){
		std::cerr << "File " << dbFile << " not Found! Reseting terminated" << std::endl;
		return;
	}
	cFile >> metadata;
}

double MetaDB::getLumi() {
	double lumi(1);
	if (metadata["lumi"].empty()) {
		std::cerr << "Warnning! No lumi is set! Set to 1." << std::endl;
	}
	else {
		lumi = metadata["lumi"];
	}
	return lumi;
}

double MetaDB::getXsection(int mcChannel, int SUSYFinalState) {
	std::string channel = std::to_string(mcChannel);
	std::string finalState = std::to_string(SUSYFinalState);
	double xSec(0);
	if (metadata["xSection"][finalState.c_str()][channel.c_str()].empty()) {
		std::cerr << "Warnning! No xSection is found for mcChannel " << mcChannel << " and SUSYFinalState " << SUSYFinalState << "! Set to 0." << std::endl;
	}else{
		xSec = metadata["xSection"][finalState.c_str()][channel.c_str()];
	}
	return xSec;
}

double MetaDB::getNnorm(int mcChannel, int SUSYFinalState) {
	std::string channel = std::to_string(mcChannel);
	std::string finalState = std::to_string(SUSYFinalState);
	float nNorm(0);
	if (metadata["Nnorm"][finalState.c_str()][channel.c_str()].empty()) {
		std::cerr << "Warnning! No nNorm is found for mcChannel " << mcChannel << " and SUSYFinalState " << SUSYFinalState << "! Set to 0." << std::endl;
	}else {
		nNorm = metadata["Nnorm"][finalState.c_str()][channel.c_str()];
	}
	return nNorm;
}

double MetaDB::getkFactor(int mcChannel) {
	std::string channel = std::to_string(mcChannel);
	float kFactor(1);
	if (!metadata["kFactor"][channel.c_str()].empty()) {
		kFactor = metadata["kFactor"][channel.c_str()];
	}
	return kFactor;
}

double MetaDB::getFilterEff(int mcChannel) {
	std::string channel = std::to_string(mcChannel);
	float filterEff(1);
	if (!metadata["filterEff"][channel.c_str()].empty()) {
		filterEff = metadata["filterEff"][channel.c_str()];
	}
	return filterEff;
}

#pragma once
//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Mon Jan 14 17:13:36 2019 by ROOT version 6.10/06
// from TTree Staus_Nominal/SmallTree for fast analysis
// found on file: MC16a_StauStau_320_160.root
//////////////////////////////////////////////////////////

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include "vector"

using namespace std;

class selector {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain
   TTree          *newTree;

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   Int_t           N_AllFakeTaus;
   Int_t           N_FakeTaus;
   Int_t           N_antiFakeTaus;
   Char_t          EventIsPromoted;
   vector<char>    *taus_isPromoted;
   vector<double>  *PreAllFakeTau_pt;
   vector<double>  *PreFakeTau_pt;
   vector<double>  *PreAllFakeTau_eta;
   vector<double>  *PreFakeTau_eta;
   vector<double>  *PreAllFakeTau_NTrks;
   vector<double>  *PreFakeTau_NTrks;
   Double_t        PromotionWeight;
   Int_t           mcChannelNumber;
   Double_t        mergedRunNumber;
   Int_t           mergedlumiBlock;
   Double_t        tau1Pt;
   Double_t        tau2Pt;
   Double_t        tau1Mt;
   Double_t        tau2Mt;
   Double_t        dRtt;
   Double_t        dPhitt;
   Double_t        mt12tau;
   Double_t        Mll_tt;
   Double_t        meff_tt;
   Double_t        MT2;
   Double_t        tau1Pt_W;
   Double_t        mu1Pt;
   Double_t        tau1Mt_W;
   Double_t        mu1Mt;
   Double_t        dRtm;
   Double_t        dPhitm;
   Double_t        mt12tm;
   Double_t        Mll_tm; 
   Double_t        meff_tm;
   Double_t        MT2_tm;
   Double_t        lJet_pt;
   Int_t           LJet_n;
   Double_t        JetWeight;
   Double_t        Evt_MET;
   Double_t        Evt_METSIG;
   Double_t        Evt_METSIG_WithTauJet;
   Bool_t          isSR1;
   Bool_t          isSR2;
   Bool_t          isWCR;
   Bool_t          isWVR;
   Bool_t          isTVR;
   Bool_t          isZVR;
   Bool_t          isBosonVR;
   Bool_t          isQCDCR_SR1;
   Bool_t          isQCDCR_SR2;
   Bool_t          isQCDCR_WCR;
   Bool_t          isQCDCR_WVR;
   Double_t        Weight_mc;
   Double_t        ff_weight;
   Double_t        ff_weight_sysUP;
   Double_t        ff_weight_sysDown;
   Double_t        GenWeight;
   Double_t        EleWeight;
   Double_t        EleWeightId;
   Double_t        EleWeightId_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down;
   Double_t        EleWeightId_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up;
   Double_t        EleWeightIso;
   Double_t        EleWeightIso_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down;
   Double_t        EleWeightIso_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up;
   Double_t        EleWeightReco;
   Double_t        EleWeightReco_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down;
   Double_t        EleWeightReco_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up;
   Double_t        EleWeight_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down;
   Double_t        EleWeight_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up;
   Double_t        EleWeight_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down;
   Double_t        EleWeight_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up;
   Double_t        EleWeight_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down;
   Double_t        EleWeight_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up;
   Double_t        MuoWeight;
   Double_t        MuoWeightIsol;
   Double_t        MuoWeightIsol_MUON_EFF_ISO_STAT__1down;
   Double_t        MuoWeightIsol_MUON_EFF_ISO_STAT__1up;
   Double_t        MuoWeightIsol_MUON_EFF_ISO_SYS__1down;
   Double_t        MuoWeightIsol_MUON_EFF_ISO_SYS__1up;
   Double_t        MuoWeightReco;
   Double_t        MuoWeightReco_MUON_EFF_RECO_STAT_LOWPT__1down;
   Double_t        MuoWeightReco_MUON_EFF_RECO_STAT_LOWPT__1up;
   Double_t        MuoWeightReco_MUON_EFF_RECO_STAT__1down;
   Double_t        MuoWeightReco_MUON_EFF_RECO_STAT__1up;
   Double_t        MuoWeightReco_MUON_EFF_RECO_SYS_LOWPT__1down;
   Double_t        MuoWeightReco_MUON_EFF_RECO_SYS_LOWPT__1up;
   Double_t        MuoWeightReco_MUON_EFF_RECO_SYS__1down;
   Double_t        MuoWeightReco_MUON_EFF_RECO_SYS__1up;
   Double_t        MuoWeightTTVA;
   Double_t        MuoWeightTTVA_MUON_EFF_TTVA_STAT__1down;
   Double_t        MuoWeightTTVA_MUON_EFF_TTVA_STAT__1up;
   Double_t        MuoWeightTTVA_MUON_EFF_TTVA_SYS__1down;
   Double_t        MuoWeightTTVA_MUON_EFF_TTVA_SYS__1up;
   Double_t        MuoWeightTrigHLT_mu20_iloose_L1MU15_OR_HLT_mu40;
   Double_t        MuoWeightTrigHLT_mu20_iloose_L1MU15_OR_HLT_mu40_MUON_EFF_TrigStatUncertainty__1down;
   Double_t        MuoWeightTrigHLT_mu20_iloose_L1MU15_OR_HLT_mu40_MUON_EFF_TrigStatUncertainty__1up;
   Double_t        MuoWeightTrigHLT_mu20_iloose_L1MU15_OR_HLT_mu40_MUON_EFF_TrigSystUncertainty__1down;
   Double_t        MuoWeightTrigHLT_mu20_iloose_L1MU15_OR_HLT_mu40_MUON_EFF_TrigSystUncertainty__1up;
   Double_t        MuoWeightTrigHLT_mu24_ivarmedium_OR_HLT_mu40;
   Double_t        MuoWeightTrigHLT_mu24_ivarmedium_OR_HLT_mu40_MUON_EFF_TrigStatUncertainty__1down;
   Double_t        MuoWeightTrigHLT_mu24_ivarmedium_OR_HLT_mu40_MUON_EFF_TrigStatUncertainty__1up;
   Double_t        MuoWeightTrigHLT_mu24_ivarmedium_OR_HLT_mu40_MUON_EFF_TrigSystUncertainty__1down;
   Double_t        MuoWeightTrigHLT_mu24_ivarmedium_OR_HLT_mu40_MUON_EFF_TrigSystUncertainty__1up;
   Double_t        MuoWeightTrigHLT_mu24_ivarmedium_OR_HLT_mu50;
   Double_t        MuoWeightTrigHLT_mu24_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1down;
   Double_t        MuoWeightTrigHLT_mu24_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1up;
   Double_t        MuoWeightTrigHLT_mu24_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1down;
   Double_t        MuoWeightTrigHLT_mu24_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1up;
   Double_t        MuoWeightTrigHLT_mu26_ivarmediumORHLT_mu50;
   Double_t        MuoWeightTrigHLT_mu26_ivarmediumORHLT_mu50_MUON_EFF_TrigStatUncertainty__1down;
   Double_t        MuoWeightTrigHLT_mu26_ivarmediumORHLT_mu50_MUON_EFF_TrigStatUncertainty__1up;
   Double_t        MuoWeightTrigHLT_mu26_ivarmediumORHLT_mu50_MUON_EFF_TrigSystUncertainty__1down;
   Double_t        MuoWeightTrigHLT_mu26_ivarmediumORHLT_mu50_MUON_EFF_TrigSystUncertainty__1up;
   Double_t        MuoWeightTrigHLT_mu26_ivarmedium_OR_HLT_mu50;
   Double_t        MuoWeightTrigHLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1down;
   Double_t        MuoWeightTrigHLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1up;
   Double_t        MuoWeightTrigHLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1down;
   Double_t        MuoWeightTrigHLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1up;
   Double_t        MuoWeight_MUON_EFF_ISO_STAT__1down;
   Double_t        MuoWeight_MUON_EFF_ISO_STAT__1up;
   Double_t        MuoWeight_MUON_EFF_ISO_SYS__1down;
   Double_t        MuoWeight_MUON_EFF_ISO_SYS__1up;
   Double_t        MuoWeight_MUON_EFF_RECO_STAT_LOWPT__1down;
   Double_t        MuoWeight_MUON_EFF_RECO_STAT_LOWPT__1up;
   Double_t        MuoWeight_MUON_EFF_RECO_STAT__1down;
   Double_t        MuoWeight_MUON_EFF_RECO_STAT__1up;
   Double_t        MuoWeight_MUON_EFF_RECO_SYS_LOWPT__1down;
   Double_t        MuoWeight_MUON_EFF_RECO_SYS_LOWPT__1up;
   Double_t        MuoWeight_MUON_EFF_RECO_SYS__1down;
   Double_t        MuoWeight_MUON_EFF_RECO_SYS__1up;
   Double_t        MuoWeight_MUON_EFF_TTVA_STAT__1down;
   Double_t        MuoWeight_MUON_EFF_TTVA_STAT__1up;
   Double_t        MuoWeight_MUON_EFF_TTVA_SYS__1down;
   Double_t        MuoWeight_MUON_EFF_TTVA_SYS__1up;
   Double_t        TauWeight;
   Double_t        TauWeightEleOREle;
   Double_t        TauWeightEleOREle_TAUS_TRUEELECTRON_EFF_ELEOLR_STATHIGHMU__1down;
   Double_t        TauWeightEleOREle_TAUS_TRUEELECTRON_EFF_ELEOLR_STATHIGHMU__1up;
   Double_t        TauWeightEleOREle_TAUS_TRUEELECTRON_EFF_ELEOLR_STATLOWMU__1down;
   Double_t        TauWeightEleOREle_TAUS_TRUEELECTRON_EFF_ELEOLR_STATLOWMU__1up;
   Double_t        TauWeightEleOREle_TAUS_TRUEELECTRON_EFF_ELEOLR_SYST__1down;
   Double_t        TauWeightEleOREle_TAUS_TRUEELECTRON_EFF_ELEOLR_SYST__1up;
   Double_t        TauWeightEleORHad;
   Double_t        TauWeightEleORHad_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL__1down;
   Double_t        TauWeightEleORHad_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL__1up;
   Double_t        TauWeightId;
   Double_t        TauWeightId_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTUNCORR2025__1down;
   Double_t        TauWeightId_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTUNCORR2025__1up;
   Double_t        TauWeightId_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTUNCORR2530__1down;
   Double_t        TauWeightId_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTUNCORR2530__1up;
   Double_t        TauWeightId_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTUNCORR3040__1down;
   Double_t        TauWeightId_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTUNCORR3040__1up;
   Double_t        TauWeightId_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTUNCORRGE40__1down;
   Double_t        TauWeightId_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTUNCORRGE40__1up;
   Double_t        TauWeightId_TAUS_TRUEHADTAU_EFF_JETID_3PRONGSTATSYSTUNCORR2030__1down;
   Double_t        TauWeightId_TAUS_TRUEHADTAU_EFF_JETID_3PRONGSTATSYSTUNCORR2030__1up;
   Double_t        TauWeightId_TAUS_TRUEHADTAU_EFF_JETID_3PRONGSTATSYSTUNCORRGE30__1down;
   Double_t        TauWeightId_TAUS_TRUEHADTAU_EFF_JETID_3PRONGSTATSYSTUNCORRGE30__1up;
   Double_t        TauWeightId_TAUS_TRUEHADTAU_EFF_JETID_HIGHPT__1down;
   Double_t        TauWeightId_TAUS_TRUEHADTAU_EFF_JETID_HIGHPT__1up;
   Double_t        TauWeightId_TAUS_TRUEHADTAU_EFF_JETID_SYST__1down;
   Double_t        TauWeightId_TAUS_TRUEHADTAU_EFF_JETID_SYST__1up;
   Double_t        TauWeightReco;
   Double_t        TauWeightReco_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT__1down;
   Double_t        TauWeightReco_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT__1up;
   Double_t        TauWeightReco_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1down;
   Double_t        TauWeightReco_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1up;
   Double_t        TauWeightTrigHLT_tau125_medium1_tracktwo;
   Double_t        TauWeightTrigHLT_tau125_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2015__1down;
   Double_t        TauWeightTrigHLT_tau125_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2015__1up;
   Double_t        TauWeightTrigHLT_tau125_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2016__1down;
   Double_t        TauWeightTrigHLT_tau125_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2016__1up;
   Double_t        TauWeightTrigHLT_tau125_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2017__1down;
   Double_t        TauWeightTrigHLT_tau125_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2017__1up;
   Double_t        TauWeightTrigHLT_tau125_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2015__1down;
   Double_t        TauWeightTrigHLT_tau125_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2015__1up;
   Double_t        TauWeightTrigHLT_tau125_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2016__1down;
   Double_t        TauWeightTrigHLT_tau125_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2016__1up;
   Double_t        TauWeightTrigHLT_tau125_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2017__1down;
   Double_t        TauWeightTrigHLT_tau125_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2017__1up;
   Double_t        TauWeightTrigHLT_tau125_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2015__1down;
   Double_t        TauWeightTrigHLT_tau125_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2015__1up;
   Double_t        TauWeightTrigHLT_tau125_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2016__1down;
   Double_t        TauWeightTrigHLT_tau125_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2016__1up;
   Double_t        TauWeightTrigHLT_tau125_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2017__1down;
   Double_t        TauWeightTrigHLT_tau125_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2017__1up;
   Double_t        TauWeightTrigHLT_tau160_medium1_tracktwo;
   Double_t        TauWeightTrigHLT_tau160_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2016__1down;
   Double_t        TauWeightTrigHLT_tau160_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2016__1up;
   Double_t        TauWeightTrigHLT_tau160_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2017__1down;
   Double_t        TauWeightTrigHLT_tau160_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2017__1up;
   Double_t        TauWeightTrigHLT_tau160_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2016__1down;
   Double_t        TauWeightTrigHLT_tau160_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2016__1up;
   Double_t        TauWeightTrigHLT_tau160_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2017__1down;
   Double_t        TauWeightTrigHLT_tau160_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2017__1up;
   Double_t        TauWeightTrigHLT_tau160_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2016__1down;
   Double_t        TauWeightTrigHLT_tau160_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2016__1up;
   Double_t        TauWeightTrigHLT_tau160_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2017__1down;
   Double_t        TauWeightTrigHLT_tau160_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2017__1up;
   Double_t        TauWeightTrigHLT_tau25_medium1_tracktwo;
   Double_t        TauWeightTrigHLT_tau25_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2016__1down;
   Double_t        TauWeightTrigHLT_tau25_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2016__1up;
   Double_t        TauWeightTrigHLT_tau25_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2017__1down;
   Double_t        TauWeightTrigHLT_tau25_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2017__1up;
   Double_t        TauWeightTrigHLT_tau25_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2016__1down;
   Double_t        TauWeightTrigHLT_tau25_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2016__1up;
   Double_t        TauWeightTrigHLT_tau25_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2017__1down;
   Double_t        TauWeightTrigHLT_tau25_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2017__1up;
   Double_t        TauWeightTrigHLT_tau25_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2016__1down;
   Double_t        TauWeightTrigHLT_tau25_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2016__1up;
   Double_t        TauWeightTrigHLT_tau25_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2017__1down;
   Double_t        TauWeightTrigHLT_tau25_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2017__1up;
   Double_t        TauWeightTrigHLT_tau35_medium1_tracktwo;
   Double_t        TauWeightTrigHLT_tau35_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2016__1down;
   Double_t        TauWeightTrigHLT_tau35_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2016__1up;
   Double_t        TauWeightTrigHLT_tau35_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2017__1down;
   Double_t        TauWeightTrigHLT_tau35_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2017__1up;
   Double_t        TauWeightTrigHLT_tau35_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2016__1down;
   Double_t        TauWeightTrigHLT_tau35_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2016__1up;
   Double_t        TauWeightTrigHLT_tau35_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2017__1down;
   Double_t        TauWeightTrigHLT_tau35_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2017__1up;
   Double_t        TauWeightTrigHLT_tau35_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2016__1down;
   Double_t        TauWeightTrigHLT_tau35_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2016__1up;
   Double_t        TauWeightTrigHLT_tau35_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2017__1down;
   Double_t        TauWeightTrigHLT_tau35_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2017__1up;
   Double_t        TauWeightTrigHLT_tau50_medium1_tracktwo_L1TAU12;
   Double_t        TauWeightTrigHLT_tau50_medium1_tracktwo_L1TAU12_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2016__1down;
   Double_t        TauWeightTrigHLT_tau50_medium1_tracktwo_L1TAU12_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2016__1up;
   Double_t        TauWeightTrigHLT_tau50_medium1_tracktwo_L1TAU12_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2017__1down;
   Double_t        TauWeightTrigHLT_tau50_medium1_tracktwo_L1TAU12_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2017__1up;
   Double_t        TauWeightTrigHLT_tau50_medium1_tracktwo_L1TAU12_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2016__1down;
   Double_t        TauWeightTrigHLT_tau50_medium1_tracktwo_L1TAU12_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2016__1up;
   Double_t        TauWeightTrigHLT_tau50_medium1_tracktwo_L1TAU12_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2017__1down;
   Double_t        TauWeightTrigHLT_tau50_medium1_tracktwo_L1TAU12_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2017__1up;
   Double_t        TauWeightTrigHLT_tau50_medium1_tracktwo_L1TAU12_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2016__1down;
   Double_t        TauWeightTrigHLT_tau50_medium1_tracktwo_L1TAU12_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2016__1up;
   Double_t        TauWeightTrigHLT_tau50_medium1_tracktwo_L1TAU12_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2017__1down;
   Double_t        TauWeightTrigHLT_tau50_medium1_tracktwo_L1TAU12_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2017__1up;
   Double_t        TauWeightTrigHLT_tau80_medium1_tracktwo;
   Double_t        TauWeightTrigHLT_tau80_medium1_tracktwo_L1TAU60;
   Double_t        TauWeightTrigHLT_tau80_medium1_tracktwo_L1TAU60_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2016__1down;
   Double_t        TauWeightTrigHLT_tau80_medium1_tracktwo_L1TAU60_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2016__1up;
   Double_t        TauWeightTrigHLT_tau80_medium1_tracktwo_L1TAU60_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2017__1down;
   Double_t        TauWeightTrigHLT_tau80_medium1_tracktwo_L1TAU60_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2017__1up;
   Double_t        TauWeightTrigHLT_tau80_medium1_tracktwo_L1TAU60_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2016__1down;
   Double_t        TauWeightTrigHLT_tau80_medium1_tracktwo_L1TAU60_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2016__1up;
   Double_t        TauWeightTrigHLT_tau80_medium1_tracktwo_L1TAU60_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2017__1down;
   Double_t        TauWeightTrigHLT_tau80_medium1_tracktwo_L1TAU60_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2017__1up;
   Double_t        TauWeightTrigHLT_tau80_medium1_tracktwo_L1TAU60_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2016__1down;
   Double_t        TauWeightTrigHLT_tau80_medium1_tracktwo_L1TAU60_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2016__1up;
   Double_t        TauWeightTrigHLT_tau80_medium1_tracktwo_L1TAU60_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2017__1down;
   Double_t        TauWeightTrigHLT_tau80_medium1_tracktwo_L1TAU60_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2017__1up;
   Double_t        TauWeightTrigHLT_tau80_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2016__1down;
   Double_t        TauWeightTrigHLT_tau80_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2016__1up;
   Double_t        TauWeightTrigHLT_tau80_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2017__1down;
   Double_t        TauWeightTrigHLT_tau80_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2017__1up;
   Double_t        TauWeightTrigHLT_tau80_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2016__1down;
   Double_t        TauWeightTrigHLT_tau80_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2016__1up;
   Double_t        TauWeightTrigHLT_tau80_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2017__1down;
   Double_t        TauWeightTrigHLT_tau80_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2017__1up;
   Double_t        TauWeightTrigHLT_tau80_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2016__1down;
   Double_t        TauWeightTrigHLT_tau80_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2016__1up;
   Double_t        TauWeightTrigHLT_tau80_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2017__1down;
   Double_t        TauWeightTrigHLT_tau80_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2017__1up;
   Double_t        TauWeight_TAUS_TRUEELECTRON_EFF_ELEOLR_STATHIGHMU__1down;
   Double_t        TauWeight_TAUS_TRUEELECTRON_EFF_ELEOLR_STATHIGHMU__1up;
   Double_t        TauWeight_TAUS_TRUEELECTRON_EFF_ELEOLR_STATLOWMU__1down;
   Double_t        TauWeight_TAUS_TRUEELECTRON_EFF_ELEOLR_STATLOWMU__1up;
   Double_t        TauWeight_TAUS_TRUEELECTRON_EFF_ELEOLR_SYST__1down;
   Double_t        TauWeight_TAUS_TRUEELECTRON_EFF_ELEOLR_SYST__1up;
   Double_t        TauWeight_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL__1down;
   Double_t        TauWeight_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL__1up;
   Double_t        TauWeight_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTUNCORR2025__1down;
   Double_t        TauWeight_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTUNCORR2025__1up;
   Double_t        TauWeight_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTUNCORR2530__1down;
   Double_t        TauWeight_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTUNCORR2530__1up;
   Double_t        TauWeight_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTUNCORR3040__1down;
   Double_t        TauWeight_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTUNCORR3040__1up;
   Double_t        TauWeight_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTUNCORRGE40__1down;
   Double_t        TauWeight_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTUNCORRGE40__1up;
   Double_t        TauWeight_TAUS_TRUEHADTAU_EFF_JETID_3PRONGSTATSYSTUNCORR2030__1down;
   Double_t        TauWeight_TAUS_TRUEHADTAU_EFF_JETID_3PRONGSTATSYSTUNCORR2030__1up;
   Double_t        TauWeight_TAUS_TRUEHADTAU_EFF_JETID_3PRONGSTATSYSTUNCORRGE30__1down;
   Double_t        TauWeight_TAUS_TRUEHADTAU_EFF_JETID_3PRONGSTATSYSTUNCORRGE30__1up;
   Double_t        TauWeight_TAUS_TRUEHADTAU_EFF_JETID_HIGHPT__1down;
   Double_t        TauWeight_TAUS_TRUEHADTAU_EFF_JETID_HIGHPT__1up;
   Double_t        TauWeight_TAUS_TRUEHADTAU_EFF_JETID_SYST__1down;
   Double_t        TauWeight_TAUS_TRUEHADTAU_EFF_JETID_SYST__1up;
   Double_t        TauWeight_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT__1down;
   Double_t        TauWeight_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT__1up;
   Double_t        TauWeight_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1down;
   Double_t        TauWeight_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1up;
   Double_t        muWeight;
   Double_t        muWeight_PRW_DATASF__1down;
   Double_t        muWeight_PRW_DATASF__1up;
   Float_t         Aplanarity;
   Float_t         Circularity;
   Float_t         CosChi1;
   Float_t         CosChi2;
   Float_t         Ht_Jet;
   Float_t         Ht_Lep;
   Float_t         Ht_Tau;
   Float_t         Lin_Aplanarity;
   Float_t         Lin_Circularity;
   Float_t         Lin_Planarity;
   Float_t         Lin_Sphericity;
   Float_t         Lin_Sphericity_C;
   Float_t         Lin_Sphericity_D;
   Float_t         MET_Centrality;
   Float_t         MET_CosMinDeltaPhi;
   Float_t         MET_LepTau_DeltaPhi;
   Float_t         MET_LepTau_VecSumPt;
   Float_t         MET_SumCosDeltaPhi;
   Float_t         MT2_max;
   Float_t         MT2_min;
   Float_t         Meff;
   Float_t         Meff_TauTau;
   Float_t         MetTST_OverSqrtHT;
   Float_t         MetTST_OverSqrtSumET;
   Float_t         MetTST_Significance;
   Float_t         MetTST_Significance_Rho;
   Float_t         MetTST_Significance_VarL;
   Float_t         MetTST_Significance_dataJER;
   Float_t         MetTST_Significance_dataJER_Rho;
   Float_t         MetTST_Significance_dataJER_VarL;
   Float_t         MetTST_Significance_dataJER_noPUJets;
   Float_t         MetTST_Significance_dataJER_noPUJets_Rho;
   Float_t         MetTST_Significance_dataJER_noPUJets_VarL;
   Float_t         MetTST_Significance_noPUJets;
   Float_t         MetTST_Significance_noPUJets_Rho;
   Float_t         MetTST_Significance_noPUJets_VarL;
   Float_t         MetTST_Significance_noPUJets_noSoftTerm;
   Float_t         MetTST_Significance_noPUJets_noSoftTerm_Rho;
   Float_t         MetTST_Significance_noPUJets_noSoftTerm_VarL;
   Float_t         MetTST_Significance_phireso_noPUJets;
   Float_t         MetTST_Significance_phireso_noPUJets_Rho;
   Float_t         MetTST_Significance_phireso_noPUJets_VarL;
   Float_t         Oblateness;
   Float_t         PTt;
   Float_t         Planarity;
   Float_t         RJW_CosThetaStarW;
   Float_t         RJW_GammaBetaW;
   Float_t         RJW_dPhiDecayPlaneW;
   Float_t         RJZ_BalMetObj_Z;
   Float_t         RJZ_CMS_Mass;
   Float_t         RJZ_H01_Tau1;
   Float_t         RJZ_H01_Z;
   Float_t         RJZ_H11_Tau1;
   Float_t         RJZ_H11_Z;
   Float_t         RJZ_H21_Tau1;
   Float_t         RJZ_H21_Z;
   Float_t         RJZ_Ht01_Tau1;
   Float_t         RJZ_Ht01_Z;
   Float_t         RJZ_Ht11_Tau1;
   Float_t         RJZ_Ht11_Z;
   Float_t         RJZ_Ht21_Tau1;
   Float_t         RJZ_Ht21_Z;
   Float_t         RJZ_R_BoostZ;
   Float_t         RJZ_cosTheta_Tau1;
   Float_t         RJZ_cosTheta_Tau2;
   Float_t         RJZ_cosTheta_Z;
   Float_t         RJZ_dPhiDecayPlane_Tau1;
   Float_t         RJZ_dPhiDecayPlane_Tau2;
   Float_t         RJZ_dPhiDecayPlane_Z;
   Float_t         Sphericity;
   Float_t         Spherocity;
   Float_t         Thrust;
   Float_t         ThrustMajor;
   Float_t         ThrustMinor;
   Float_t         VecSumPt_LepTau;
   Float_t         WolframMoment_0;
   Float_t         WolframMoment_1;
   Float_t         WolframMoment_10;
   Float_t         WolframMoment_11;
   Float_t         WolframMoment_2;
   Float_t         WolframMoment_3;
   Float_t         WolframMoment_4;
   Float_t         WolframMoment_5;
   Float_t         WolframMoment_6;
   Float_t         WolframMoment_7;
   Float_t         WolframMoment_8;
   Float_t         WolframMoment_9;
   Float_t         actualInteractionsPerCrossing;
   Float_t         averageInteractionsPerCrossing;
   Float_t         corr_avgIntPerX;
   Float_t         corr_avgIntPerX_PRW_DATASF__1down;
   Float_t         corr_avgIntPerX_PRW_DATASF__1up;
   Float_t         emt_MT2_max;
   Float_t         emt_MT2_min;
   Float_t         mu_density;
   Int_t           SUSYFinalState;
   Int_t           Vtx_n;
   Int_t           n_BJets;
   Int_t           n_BaseJets;
   Int_t           n_BaseTau;
   Int_t           n_SignalElec;
   Int_t           n_SignalJets;
   Int_t           n_SignalMuon;
   Int_t           n_SignalTau;
   Char_t          OS_BaseTauEle;
   Char_t          OS_BaseTauMuo;
   Char_t          OS_EleEle;
   Char_t          OS_MuoMuo;
   Char_t          OS_TauEle;
   Char_t          OS_TauMuo;
   Char_t          OS_TauTau;
   Char_t          TrigHLT_e120_lhloose;
   Char_t          TrigHLT_e140_lhloose_nod0;
   Char_t          TrigHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwo;
   Char_t          TrigHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwoEF;
   Char_t          TrigHLT_e17_lhmedium_nod0_ivarloose_tau25_mediumRNN_tracktwoMVA;
   Char_t          TrigHLT_e17_lhmedium_nod0_tau25_medium1_tracktwoEF_xe50;
   Char_t          TrigHLT_e17_lhmedium_nod0_tau25_medium1_tracktwo_xe50;
   Char_t          TrigHLT_e17_lhmedium_nod0_tau25_mediumRNN_tracktwoMVA_xe50;
   Char_t          TrigHLT_e24_lhmedium_L1EM20VH;
   Char_t          TrigHLT_e24_lhmedium_nod0_ivarloose_tau35_medium1_tracktwo;
   Char_t          TrigHLT_e26_lhtight_nod0_ivarloose;
   Char_t          TrigHLT_e60_lhmedium;
   Char_t          TrigHLT_e60_lhmedium_nod0;
   Char_t          TrigHLT_mu14_ivarloose_tau25_medium1_tracktwo;
   Char_t          TrigHLT_mu14_ivarloose_tau35_medium1_tracktwo;
   Char_t          TrigHLT_mu14_ivarloose_tau35_medium1_tracktwoEF;
   Char_t          TrigHLT_mu14_ivarloose_tau35_mediumRNN_tracktwoMVA;
   Char_t          TrigHLT_mu14_tau25_medium1_tracktwoEF_xe50;
   Char_t          TrigHLT_mu14_tau25_medium1_tracktwo_xe50;
   Char_t          TrigHLT_mu14_tau25_mediumRNN_tracktwoMVA_xe50;
   Char_t          TrigHLT_mu20_iloose_L1MU15;
   Char_t          TrigHLT_mu24_ivarmedium;
   Char_t          TrigHLT_mu26_imedium;
   Char_t          TrigHLT_mu26_ivarmedium;
   Char_t          TrigHLT_mu40;
   Char_t          TrigHLT_mu50;
   Char_t          TrigHLT_mu60_0eta105_msonly;
   Char_t          TrigHLT_tau125_medium1_tracktwo;
   Char_t          TrigHLT_tau125_medium1_tracktwoEF;
   Char_t          TrigHLT_tau160_medium1_tracktwo;
   Char_t          TrigHLT_tau160_medium1_tracktwoEF;
   Char_t          TrigHLT_tau160_medium1_tracktwo_L1TAU100;
   Char_t          TrigHLT_tau25_medium1_tracktwo;
   Char_t          TrigHLT_tau25_medium1_tracktwoEF;
   Char_t          TrigHLT_tau25_mediumRNN_tracktwoMVA;
   Char_t          TrigHLT_tau35_medium1_tracktwo;
   Char_t          TrigHLT_tau35_medium1_tracktwoEF;
   Char_t          TrigHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo;
   Char_t          TrigHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_L1TAU20IM_2TAU12IM;
   Char_t          TrigHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_xe50;
   Char_t          TrigHLT_tau50_medium1_tracktwo_L1TAU12;
   Char_t          TrigHLT_tau60_medium1_tracktwo;
   Char_t          TrigHLT_tau60_medium1_tracktwoEF;
   Char_t          TrigHLT_tau60_medium1_tracktwoEF_tau25_medium1_tracktwoEF_xe50;
   Char_t          TrigHLT_tau60_medium1_tracktwo_tau25_medium1_tracktwo_xe50;
   Char_t          TrigHLT_tau60_mediumRNN_tracktwoMVA_tau25_mediumRNN_tracktwoMVA_xe50;
   Char_t          TrigHLT_tau80_medium1_tracktwo;
   Char_t          TrigHLT_tau80_medium1_tracktwoEF;
   Char_t          TrigHLT_tau80_medium1_tracktwoEF_L1TAU60;
   Char_t          TrigHLT_tau80_medium1_tracktwoEF_L1TAU60_tau60_medium1_tracktwoEF_L1TAU40;
   Char_t          TrigHLT_tau80_medium1_tracktwo_L1TAU60;
   Char_t          TrigHLT_tau80_medium1_tracktwo_L1TAU60_tau25_medium1_tracktwo_L1TAU12;
   Char_t          TrigHLT_tau80_medium1_tracktwo_L1TAU60_tau50_medium1_tracktwo_L1TAU12;
   Char_t          TrigHLT_tau80_medium1_tracktwo_L1TAU60_tau60_medium1_tracktwo_L1TAU40;
   Char_t          TrigHLT_tau80_mediumRNN_tracktwoMVA_L1TAU60_tau60_mediumRNN_tracktwoMVA_L1TAU40;
   Char_t          TrigHLT_xe100_pufit_L1XE50;
   Char_t          TrigHLT_xe100_pufit_L1XE55;
   Char_t          TrigHLT_xe110_mht_L1XE50;
   Char_t          TrigHLT_xe110_pufit_L1XE50;
   Char_t          TrigHLT_xe110_pufit_L1XE55;
   Char_t          TrigHLT_xe110_pufit_xe65_L1XE50;
   Char_t          TrigHLT_xe110_pufit_xe70_L1XE50;
   Char_t          TrigHLT_xe120_pufit_L1XE50;
   Char_t          TrigHLT_xe50;
   Char_t          TrigHLT_xe70_mht;
   Char_t          TrigHLT_xe90_mht_L1XE50;
   Char_t          TrigHLT_xe90_pufit_L1XE50;
   Char_t          TrigMatchHLT_e120_lhloose;
   Char_t          TrigMatchHLT_e140_lhloose_nod0;
   Char_t          TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwo;
   Char_t          TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwoEF;
   Char_t          TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_mediumRNN_tracktwoMVA;
   Char_t          TrigMatchHLT_e17_lhmedium_nod0_tau25_medium1_tracktwoEF_xe50;
   Char_t          TrigMatchHLT_e17_lhmedium_nod0_tau25_medium1_tracktwo_xe50;
   Char_t          TrigMatchHLT_e17_lhmedium_nod0_tau25_mediumRNN_tracktwoMVA_xe50;
   Char_t          TrigMatchHLT_e24_lhmedium_L1EM20VH;
   Char_t          TrigMatchHLT_e24_lhmedium_nod0_ivarloose_tau35_medium1_tracktwo;
   Char_t          TrigMatchHLT_e26_lhtight_nod0_ivarloose;
   Char_t          TrigMatchHLT_e60_lhmedium;
   Char_t          TrigMatchHLT_e60_lhmedium_nod0;
   Char_t          TrigMatchHLT_mu14_ivarloose_tau25_medium1_tracktwo;
   Char_t          TrigMatchHLT_mu14_ivarloose_tau35_medium1_tracktwo;
   Char_t          TrigMatchHLT_mu14_ivarloose_tau35_medium1_tracktwoEF;
   Char_t          TrigMatchHLT_mu14_ivarloose_tau35_mediumRNN_tracktwoMVA;
   Char_t          TrigMatchHLT_mu14_tau25_medium1_tracktwoEF_xe50;
   Char_t          TrigMatchHLT_mu14_tau25_medium1_tracktwo_xe50;
   Char_t          TrigMatchHLT_mu14_tau25_mediumRNN_tracktwoMVA_xe50;
   Char_t          TrigMatchHLT_mu20_iloose_L1MU15;
   Char_t          TrigMatchHLT_mu24_ivarmedium;
   Char_t          TrigMatchHLT_mu26_imedium;
   Char_t          TrigMatchHLT_mu26_ivarmedium;
   Char_t          TrigMatchHLT_mu40;
   Char_t          TrigMatchHLT_mu50;
   Char_t          TrigMatchHLT_mu60_0eta105_msonly;
   Char_t          TrigMatchHLT_tau125_medium1_tracktwo;
   Char_t          TrigMatchHLT_tau125_medium1_tracktwoEF;
   Char_t          TrigMatchHLT_tau160_medium1_tracktwo;
   Char_t          TrigMatchHLT_tau160_medium1_tracktwoEF;
   Char_t          TrigMatchHLT_tau160_medium1_tracktwo_L1TAU100;
   Char_t          TrigMatchHLT_tau25_medium1_tracktwo;
   Char_t          TrigMatchHLT_tau25_medium1_tracktwoEF;
   Char_t          TrigMatchHLT_tau25_mediumRNN_tracktwoMVA;
   Char_t          TrigMatchHLT_tau35_medium1_tracktwo;
   Char_t          TrigMatchHLT_tau35_medium1_tracktwoEF;
   Char_t          TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo;
   Char_t          TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_L1TAU20IM_2TAU12IM;
   Char_t          TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_xe50;
   Char_t          TrigMatchHLT_tau50_medium1_tracktwo_L1TAU12;
   Char_t          TrigMatchHLT_tau60_medium1_tracktwo;
   Char_t          TrigMatchHLT_tau60_medium1_tracktwoEF;
   Char_t          TrigMatchHLT_tau60_medium1_tracktwoEF_tau25_medium1_tracktwoEF_xe50;
   Char_t          TrigMatchHLT_tau60_medium1_tracktwo_tau25_medium1_tracktwo_xe50;
   Char_t          TrigMatchHLT_tau60_mediumRNN_tracktwoMVA_tau25_mediumRNN_tracktwoMVA_xe50;
   Char_t          TrigMatchHLT_tau80_medium1_tracktwo;
   Char_t          TrigMatchHLT_tau80_medium1_tracktwoEF;
   Char_t          TrigMatchHLT_tau80_medium1_tracktwoEF_L1TAU60;
   Char_t          TrigMatchHLT_tau80_medium1_tracktwoEF_L1TAU60_tau60_medium1_tracktwoEF_L1TAU40;
   Char_t          TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60;
   Char_t          TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau25_medium1_tracktwo_L1TAU12;
   Char_t          TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau50_medium1_tracktwo_L1TAU12;
   Char_t          TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau60_medium1_tracktwo_L1TAU40;
   Char_t          TrigMatchHLT_tau80_mediumRNN_tracktwoMVA_L1TAU60_tau60_mediumRNN_tracktwoMVA_L1TAU40;
   Char_t          TrigMatching;
   Bool_t          Lin_Sphericity_IsValid;
   Bool_t          MET_BiSect;
   Bool_t          Sphericity_IsValid;
   Bool_t          Spherocity_IsValid;
   Bool_t          Thrust_IsValid;
   Bool_t          WolframMoments_AreValid;
   UInt_t          RandomLumiBlockNumber;
   UInt_t          RandomRunNumber;
   UInt_t          backgroundFlags;
   UInt_t          bcid;
   UInt_t          coreFlags;
   UInt_t          forwardDetFlags;
   UInt_t          larFlags;
   UInt_t          lumiBlock;
   UInt_t          lumiFlags;
   UInt_t          muonFlags;
   UInt_t          pixelFlags;
   UInt_t          runNumber;
   UInt_t          sctFlags;
   UInt_t          tileFlags;
   UInt_t          trtFlags;
   ULong64_t       eventNumber;
   Float_t         MetCST_met;
   Float_t         MetCST_phi;
   Float_t         MetCST_sumet;
   Float_t         MetTST_met;
   Float_t         MetTST_phi;
   Float_t         MetTST_sumet;
   Float_t         TruthMET_met;
   Float_t         TruthMET_phi;
   Float_t         TruthMET_sumet;
   vector<unsigned int> *RJW_JigSawCandidates_frame;
   vector<int>     *RJW_JigSawCandidates_pdgId;
   vector<float>   *RJW_JigSawCandidates_pt;
   vector<float>   *RJW_JigSawCandidates_eta;
   vector<float>   *RJW_JigSawCandidates_phi;
   vector<float>   *RJW_JigSawCandidates_m;
   vector<float>   *RJZ_JigSawCandidates_CosThetaStar;
   vector<float>   *RJZ_JigSawCandidates_dPhiDecayPlane;
   vector<unsigned int> *RJZ_JigSawCandidates_frame;
   vector<int>     *RJZ_JigSawCandidates_pdgId;
   vector<float>   *RJZ_JigSawCandidates_pt;
   vector<float>   *RJZ_JigSawCandidates_eta;
   vector<float>   *RJZ_JigSawCandidates_phi;
   vector<float>   *RJZ_JigSawCandidates_m;
   vector<float>   *dilepton_charge;
   vector<int>     *dilepton_pdgId;
   vector<float>   *dilepton_pt;
   vector<float>   *dilepton_eta;
   vector<float>   *dilepton_phi;
   vector<float>   *dilepton_m;
   vector<float>   *electrons_MT;
   vector<float>   *electrons_charge;
   vector<float>   *electrons_d0sig;
   vector<char>    *electrons_isol;
   vector<char>    *electrons_signal;
   vector<int>     *electrons_truthOrigin;
   vector<int>     *electrons_truthType;
   vector<float>   *electrons_z0sinTheta;
   vector<float>   *electrons_pt;
   vector<float>   *electrons_eta;
   vector<float>   *electrons_phi;
   vector<float>   *electrons_e;
   vector<float>   *jets_Jvt;
   vector<double>  *jets_MV2c10;
   vector<int>     *jets_NTrks;
   vector<char>    *jets_bjet;
   vector<char>    *jets_signal;
   vector<float>   *jets_pt;
   vector<float>   *jets_eta;
   vector<float>   *jets_phi;
   vector<float>   *jets_m;
   vector<float>   *muons_MT;
   vector<float>   *muons_charge;
   vector<float>   *muons_d0sig;
   vector<char>    *muons_isol;
   vector<char>    *muons_signal;
   vector<int>     *muons_truthOrigin;
   vector<int>     *muons_truthType;
   vector<float>   *muons_z0sinTheta;
   vector<float>   *muons_pt;
   vector<float>   *muons_eta;
   vector<float>   *muons_phi;
   vector<float>   *muons_e;
   vector<float>   *taus_BDTEleScore;
   vector<float>   *taus_BDTEleScoreSigTrans;
   vector<float>   *taus_BDTJetScore;
   vector<float>   *taus_BDTJetScoreSigTrans;
   vector<int>     *taus_ConeTruthLabelID;
   vector<float>   *taus_MT;
   vector<int>     *taus_NTrks;
   vector<int>     *taus_NTrksJet;
   vector<int>     *taus_PartonTruthLabelID;
   vector<int>     *taus_Quality;
   vector<char>    *taus_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwo;
   vector<char>    *taus_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwoEF;
   vector<char>    *taus_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_mediumRNN_tracktwoMVA;
   vector<char>    *taus_TrigMatchHLT_e17_lhmedium_nod0_tau25_medium1_tracktwoEF_xe50;
   vector<char>    *taus_TrigMatchHLT_e17_lhmedium_nod0_tau25_medium1_tracktwo_xe50;
   vector<char>    *taus_TrigMatchHLT_e17_lhmedium_nod0_tau25_mediumRNN_tracktwoMVA_xe50;
   vector<char>    *taus_TrigMatchHLT_e24_lhmedium_nod0_ivarloose_tau35_medium1_tracktwo;
   vector<char>    *taus_TrigMatchHLT_mu14_ivarloose_tau25_medium1_tracktwo;
   vector<char>    *taus_TrigMatchHLT_mu14_ivarloose_tau35_medium1_tracktwo;
   vector<char>    *taus_TrigMatchHLT_mu14_ivarloose_tau35_medium1_tracktwoEF;
   vector<char>    *taus_TrigMatchHLT_mu14_ivarloose_tau35_mediumRNN_tracktwoMVA;
   vector<char>    *taus_TrigMatchHLT_mu14_tau25_medium1_tracktwoEF_xe50;
   vector<char>    *taus_TrigMatchHLT_mu14_tau25_medium1_tracktwo_xe50;
   vector<char>    *taus_TrigMatchHLT_mu14_tau25_mediumRNN_tracktwoMVA_xe50;
   vector<char>    *taus_TrigMatchHLT_tau125_medium1_tracktwo;
   vector<char>    *taus_TrigMatchHLT_tau125_medium1_tracktwoEF;
   vector<char>    *taus_TrigMatchHLT_tau160_medium1_tracktwo;
   vector<char>    *taus_TrigMatchHLT_tau160_medium1_tracktwoEF;
   vector<char>    *taus_TrigMatchHLT_tau160_medium1_tracktwo_L1TAU100;
   vector<char>    *taus_TrigMatchHLT_tau25_medium1_tracktwo;
   vector<char>    *taus_TrigMatchHLT_tau25_medium1_tracktwoEF;
   vector<char>    *taus_TrigMatchHLT_tau25_mediumRNN_tracktwoMVA;
   vector<char>    *taus_TrigMatchHLT_tau35_medium1_tracktwo;
   vector<char>    *taus_TrigMatchHLT_tau35_medium1_tracktwoEF;
   vector<char>    *taus_TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo;
   vector<char>    *taus_TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_L1TAU20IM_2TAU12IM;
   vector<char>    *taus_TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_xe50;
   vector<char>    *taus_TrigMatchHLT_tau50_medium1_tracktwo_L1TAU12;
   vector<char>    *taus_TrigMatchHLT_tau60_medium1_tracktwo;
   vector<char>    *taus_TrigMatchHLT_tau60_medium1_tracktwoEF;
   vector<char>    *taus_TrigMatchHLT_tau60_medium1_tracktwoEF_tau25_medium1_tracktwoEF_xe50;
   vector<char>    *taus_TrigMatchHLT_tau60_medium1_tracktwo_tau25_medium1_tracktwo_xe50;
   vector<char>    *taus_TrigMatchHLT_tau60_mediumRNN_tracktwoMVA_tau25_mediumRNN_tracktwoMVA_xe50;
   vector<char>    *taus_TrigMatchHLT_tau80_medium1_tracktwo;
   vector<char>    *taus_TrigMatchHLT_tau80_medium1_tracktwoEF;
   vector<char>    *taus_TrigMatchHLT_tau80_medium1_tracktwoEF_L1TAU60;
   vector<char>    *taus_TrigMatchHLT_tau80_medium1_tracktwoEF_L1TAU60_tau60_medium1_tracktwoEF_L1TAU40;
   vector<char>    *taus_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60;
   vector<char>    *taus_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau25_medium1_tracktwo_L1TAU12;
   vector<char>    *taus_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau50_medium1_tracktwo_L1TAU12;
   vector<char>    *taus_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau60_medium1_tracktwo_L1TAU40;
   vector<char>    *taus_TrigMatchHLT_tau80_mediumRNN_tracktwoMVA_L1TAU60_tau60_mediumRNN_tracktwoMVA_L1TAU40;
   vector<float>   *taus_Width;
   vector<float>   *taus_charge;
   vector<char>    *taus_signalID;
   vector<int>     *taus_truthOrigin;
   vector<int>     *taus_truthType;
   vector<float>   *taus_pt;
   vector<float>   *taus_eta;
   vector<float>   *taus_phi;
   vector<float>   *taus_e;

   // List of branches
   TBranch        *b_JetWeight;   //!
   TBranch        *b_N_AllFakeTaus;   //!
   TBranch        *b_N_FakeTaus;   //!
   TBranch        *b_N_antiFakeTaus;   //!
   TBranch        *b_EventIsPromoted;   //!
   TBranch        *b_taus_isPromoted;   //!
   TBranch        *b_PreAllFakeTau_pt;   //!
   TBranch        *b_PreFakeTau_pt;   //!
   TBranch        *b_PreAllFakeTau_eta;   //!
   TBranch        *b_PreFakeTau_eta;   //!
   TBranch        *b_PreAllFakeTau_NTrks;   //!
   TBranch        *b_PreFakeTau_NTrks;   //!
   TBranch        *b_PromotionWeight;   //!
   TBranch        *b_mcChannelNumber;   //!
   TBranch        *b_Weight_mc;   //!
   TBranch        *b_ff_weight;   //!
   TBranch        *b_ff_weight_sysUP;   //!
   TBranch        *b_ff_weight_sysDown;   //!
   TBranch        *b_mergedRunNumber;   //!
   TBranch        *b_mergedlumiBlock;   //!
   TBranch        *b_tau1Pt;   //!
   TBranch        *b_tau2Pt;   //!
   TBranch        *b_tau1Mt;   //!
   TBranch        *b_tau2Mt;   //!
   TBranch        *b_dRtt;   //!
   TBranch        *b_dPhitt;   //!
   TBranch        *b_mt12tau;   //!
   TBranch        *b_Mll_tt;   //!
   TBranch        *b_meff_tt;   //!
   TBranch        *b_MT2;   //!
   TBranch        *b_tau1Pt_W;   //!
   TBranch        *b_mu1Pt;   //!
   TBranch        *b_tau1Mt_W;   //!
   TBranch        *b_mu1Mt;   //!
   TBranch        *b_dRtm;   //!
   TBranch        *b_dPhitm;   //!
   TBranch        *b_mt12tm;   //!
   TBranch        *b_Mll_tm;   //!
   TBranch        *b_meff_tm;   //!
   TBranch        *b_MT2_tm;   //!
   TBranch        *b_lJet_pt;   //!
   TBranch        *b_LJet_n;   //!
   TBranch        *b_Evt_MET;   //!
   TBranch        *b_Evt_METSIG;   //!
   TBranch        *b_Evt_METSIG_WithTauJet;   //!
   TBranch        *b_isSR1;   //!
   TBranch        *b_isSR2;   //!
   TBranch        *b_isWCR;   //!
   TBranch        *b_isWVR;   //!
   TBranch        *b_isTVR;   //!
   TBranch        *b_isZVR;   //!
   TBranch        *b_isBosonVR;   //!
   TBranch        *b_isQCDCR_SR1;   //!
   TBranch        *b_isQCDCR_SR2;   //!
   TBranch        *b_isQCDCR_WCR;   //!
   TBranch        *b_isQCDCR_WVR;   //!
   TBranch        *b_GenWeight;   //!
   TBranch        *b_EleWeight;   //!
   TBranch        *b_EleWeightId;   //!
   TBranch        *b_EleWeightId_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down;   //!
   TBranch        *b_EleWeightId_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up;   //!
   TBranch        *b_EleWeightIso;   //!
   TBranch        *b_EleWeightIso_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down;   //!
   TBranch        *b_EleWeightIso_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up;   //!
   TBranch        *b_EleWeightReco;   //!
   TBranch        *b_EleWeightReco_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down;   //!
   TBranch        *b_EleWeightReco_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up;   //!
   TBranch        *b_EleWeight_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down;   //!
   TBranch        *b_EleWeight_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up;   //!
   TBranch        *b_EleWeight_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down;   //!
   TBranch        *b_EleWeight_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up;   //!
   TBranch        *b_EleWeight_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down;   //!
   TBranch        *b_EleWeight_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up;   //!
   TBranch        *b_MuoWeight;   //!
   TBranch        *b_MuoWeightIsol;   //!
   TBranch        *b_MuoWeightIsol_MUON_EFF_ISO_STAT__1down;   //!
   TBranch        *b_MuoWeightIsol_MUON_EFF_ISO_STAT__1up;   //!
   TBranch        *b_MuoWeightIsol_MUON_EFF_ISO_SYS__1down;   //!
   TBranch        *b_MuoWeightIsol_MUON_EFF_ISO_SYS__1up;   //!
   TBranch        *b_MuoWeightReco;   //!
   TBranch        *b_MuoWeightReco_MUON_EFF_RECO_STAT_LOWPT__1down;   //!
   TBranch        *b_MuoWeightReco_MUON_EFF_RECO_STAT_LOWPT__1up;   //!
   TBranch        *b_MuoWeightReco_MUON_EFF_RECO_STAT__1down;   //!
   TBranch        *b_MuoWeightReco_MUON_EFF_RECO_STAT__1up;   //!
   TBranch        *b_MuoWeightReco_MUON_EFF_RECO_SYS_LOWPT__1down;   //!
   TBranch        *b_MuoWeightReco_MUON_EFF_RECO_SYS_LOWPT__1up;   //!
   TBranch        *b_MuoWeightReco_MUON_EFF_RECO_SYS__1down;   //!
   TBranch        *b_MuoWeightReco_MUON_EFF_RECO_SYS__1up;   //!
   TBranch        *b_MuoWeightTTVA;   //!
   TBranch        *b_MuoWeightTTVA_MUON_EFF_TTVA_STAT__1down;   //!
   TBranch        *b_MuoWeightTTVA_MUON_EFF_TTVA_STAT__1up;   //!
   TBranch        *b_MuoWeightTTVA_MUON_EFF_TTVA_SYS__1down;   //!
   TBranch        *b_MuoWeightTTVA_MUON_EFF_TTVA_SYS__1up;   //!
   TBranch        *b_MuoWeightTrigHLT_mu20_iloose_L1MU15_OR_HLT_mu40;   //!
   TBranch        *b_MuoWeightTrigHLT_mu20_iloose_L1MU15_OR_HLT_mu40_MUON_EFF_TrigStatUncertainty__1down;   //!
   TBranch        *b_MuoWeightTrigHLT_mu20_iloose_L1MU15_OR_HLT_mu40_MUON_EFF_TrigStatUncertainty__1up;   //!
   TBranch        *b_MuoWeightTrigHLT_mu20_iloose_L1MU15_OR_HLT_mu40_MUON_EFF_TrigSystUncertainty__1down;   //!
   TBranch        *b_MuoWeightTrigHLT_mu20_iloose_L1MU15_OR_HLT_mu40_MUON_EFF_TrigSystUncertainty__1up;   //!
   TBranch        *b_MuoWeightTrigHLT_mu24_ivarmedium_OR_HLT_mu40;   //!
   TBranch        *b_MuoWeightTrigHLT_mu24_ivarmedium_OR_HLT_mu40_MUON_EFF_TrigStatUncertainty__1down;   //!
   TBranch        *b_MuoWeightTrigHLT_mu24_ivarmedium_OR_HLT_mu40_MUON_EFF_TrigStatUncertainty__1up;   //!
   TBranch        *b_MuoWeightTrigHLT_mu24_ivarmedium_OR_HLT_mu40_MUON_EFF_TrigSystUncertainty__1down;   //!
   TBranch        *b_MuoWeightTrigHLT_mu24_ivarmedium_OR_HLT_mu40_MUON_EFF_TrigSystUncertainty__1up;   //!
   TBranch        *b_MuoWeightTrigHLT_mu24_ivarmedium_OR_HLT_mu50;   //!
   TBranch        *b_MuoWeightTrigHLT_mu24_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1down;   //!
   TBranch        *b_MuoWeightTrigHLT_mu24_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1up;   //!
   TBranch        *b_MuoWeightTrigHLT_mu24_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1down;   //!
   TBranch        *b_MuoWeightTrigHLT_mu24_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1up;   //!
   TBranch        *b_MuoWeightTrigHLT_mu26_ivarmediumORHLT_mu50;   //!
   TBranch        *b_MuoWeightTrigHLT_mu26_ivarmediumORHLT_mu50_MUON_EFF_TrigStatUncertainty__1down;   //!
   TBranch        *b_MuoWeightTrigHLT_mu26_ivarmediumORHLT_mu50_MUON_EFF_TrigStatUncertainty__1up;   //!
   TBranch        *b_MuoWeightTrigHLT_mu26_ivarmediumORHLT_mu50_MUON_EFF_TrigSystUncertainty__1down;   //!
   TBranch        *b_MuoWeightTrigHLT_mu26_ivarmediumORHLT_mu50_MUON_EFF_TrigSystUncertainty__1up;   //!
   TBranch        *b_MuoWeightTrigHLT_mu26_ivarmedium_OR_HLT_mu50;   //!
   TBranch        *b_MuoWeightTrigHLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1down;   //!
   TBranch        *b_MuoWeightTrigHLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1up;   //!
   TBranch        *b_MuoWeightTrigHLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1down;   //!
   TBranch        *b_MuoWeightTrigHLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1up;   //!
   TBranch        *b_MuoWeight_MUON_EFF_ISO_STAT__1down;   //!
   TBranch        *b_MuoWeight_MUON_EFF_ISO_STAT__1up;   //!
   TBranch        *b_MuoWeight_MUON_EFF_ISO_SYS__1down;   //!
   TBranch        *b_MuoWeight_MUON_EFF_ISO_SYS__1up;   //!
   TBranch        *b_MuoWeight_MUON_EFF_RECO_STAT_LOWPT__1down;   //!
   TBranch        *b_MuoWeight_MUON_EFF_RECO_STAT_LOWPT__1up;   //!
   TBranch        *b_MuoWeight_MUON_EFF_RECO_STAT__1down;   //!
   TBranch        *b_MuoWeight_MUON_EFF_RECO_STAT__1up;   //!
   TBranch        *b_MuoWeight_MUON_EFF_RECO_SYS_LOWPT__1down;   //!
   TBranch        *b_MuoWeight_MUON_EFF_RECO_SYS_LOWPT__1up;   //!
   TBranch        *b_MuoWeight_MUON_EFF_RECO_SYS__1down;   //!
   TBranch        *b_MuoWeight_MUON_EFF_RECO_SYS__1up;   //!
   TBranch        *b_MuoWeight_MUON_EFF_TTVA_STAT__1down;   //!
   TBranch        *b_MuoWeight_MUON_EFF_TTVA_STAT__1up;   //!
   TBranch        *b_MuoWeight_MUON_EFF_TTVA_SYS__1down;   //!
   TBranch        *b_MuoWeight_MUON_EFF_TTVA_SYS__1up;   //!
   TBranch        *b_TauWeight;   //!
   TBranch        *b_TauWeightEleOREle;   //!
   TBranch        *b_TauWeightEleOREle_TAUS_TRUEELECTRON_EFF_ELEOLR_STATHIGHMU__1down;   //!
   TBranch        *b_TauWeightEleOREle_TAUS_TRUEELECTRON_EFF_ELEOLR_STATHIGHMU__1up;   //!
   TBranch        *b_TauWeightEleOREle_TAUS_TRUEELECTRON_EFF_ELEOLR_STATLOWMU__1down;   //!
   TBranch        *b_TauWeightEleOREle_TAUS_TRUEELECTRON_EFF_ELEOLR_STATLOWMU__1up;   //!
   TBranch        *b_TauWeightEleOREle_TAUS_TRUEELECTRON_EFF_ELEOLR_SYST__1down;   //!
   TBranch        *b_TauWeightEleOREle_TAUS_TRUEELECTRON_EFF_ELEOLR_SYST__1up;   //!
   TBranch        *b_TauWeightEleORHad;   //!
   TBranch        *b_TauWeightEleORHad_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL__1down;   //!
   TBranch        *b_TauWeightEleORHad_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL__1up;   //!
   TBranch        *b_TauWeightId;   //!
   TBranch        *b_TauWeightId_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTUNCORR2025__1down;   //!
   TBranch        *b_TauWeightId_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTUNCORR2025__1up;   //!
   TBranch        *b_TauWeightId_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTUNCORR2530__1down;   //!
   TBranch        *b_TauWeightId_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTUNCORR2530__1up;   //!
   TBranch        *b_TauWeightId_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTUNCORR3040__1down;   //!
   TBranch        *b_TauWeightId_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTUNCORR3040__1up;   //!
   TBranch        *b_TauWeightId_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTUNCORRGE40__1down;   //!
   TBranch        *b_TauWeightId_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTUNCORRGE40__1up;   //!
   TBranch        *b_TauWeightId_TAUS_TRUEHADTAU_EFF_JETID_3PRONGSTATSYSTUNCORR2030__1down;   //!
   TBranch        *b_TauWeightId_TAUS_TRUEHADTAU_EFF_JETID_3PRONGSTATSYSTUNCORR2030__1up;   //!
   TBranch        *b_TauWeightId_TAUS_TRUEHADTAU_EFF_JETID_3PRONGSTATSYSTUNCORRGE30__1down;   //!
   TBranch        *b_TauWeightId_TAUS_TRUEHADTAU_EFF_JETID_3PRONGSTATSYSTUNCORRGE30__1up;   //!
   TBranch        *b_TauWeightId_TAUS_TRUEHADTAU_EFF_JETID_HIGHPT__1down;   //!
   TBranch        *b_TauWeightId_TAUS_TRUEHADTAU_EFF_JETID_HIGHPT__1up;   //!
   TBranch        *b_TauWeightId_TAUS_TRUEHADTAU_EFF_JETID_SYST__1down;   //!
   TBranch        *b_TauWeightId_TAUS_TRUEHADTAU_EFF_JETID_SYST__1up;   //!
   TBranch        *b_TauWeightReco;   //!
   TBranch        *b_TauWeightReco_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT__1down;   //!
   TBranch        *b_TauWeightReco_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT__1up;   //!
   TBranch        *b_TauWeightReco_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1down;   //!
   TBranch        *b_TauWeightReco_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1up;   //!
   TBranch        *b_TauWeightTrigHLT_tau125_medium1_tracktwo;   //!
   TBranch        *b_TauWeightTrigHLT_tau125_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2015__1down;   //!
   TBranch        *b_TauWeightTrigHLT_tau125_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2015__1up;   //!
   TBranch        *b_TauWeightTrigHLT_tau125_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2016__1down;   //!
   TBranch        *b_TauWeightTrigHLT_tau125_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2016__1up;   //!
   TBranch        *b_TauWeightTrigHLT_tau125_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2017__1down;   //!
   TBranch        *b_TauWeightTrigHLT_tau125_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2017__1up;   //!
   TBranch        *b_TauWeightTrigHLT_tau125_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2015__1down;   //!
   TBranch        *b_TauWeightTrigHLT_tau125_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2015__1up;   //!
   TBranch        *b_TauWeightTrigHLT_tau125_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2016__1down;   //!
   TBranch        *b_TauWeightTrigHLT_tau125_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2016__1up;   //!
   TBranch        *b_TauWeightTrigHLT_tau125_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2017__1down;   //!
   TBranch        *b_TauWeightTrigHLT_tau125_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2017__1up;   //!
   TBranch        *b_TauWeightTrigHLT_tau125_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2015__1down;   //!
   TBranch        *b_TauWeightTrigHLT_tau125_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2015__1up;   //!
   TBranch        *b_TauWeightTrigHLT_tau125_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2016__1down;   //!
   TBranch        *b_TauWeightTrigHLT_tau125_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2016__1up;   //!
   TBranch        *b_TauWeightTrigHLT_tau125_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2017__1down;   //!
   TBranch        *b_TauWeightTrigHLT_tau125_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2017__1up;   //!
   TBranch        *b_TauWeightTrigHLT_tau160_medium1_tracktwo;   //!
   TBranch        *b_TauWeightTrigHLT_tau160_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2016__1down;   //!
   TBranch        *b_TauWeightTrigHLT_tau160_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2016__1up;   //!
   TBranch        *b_TauWeightTrigHLT_tau160_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2017__1down;   //!
   TBranch        *b_TauWeightTrigHLT_tau160_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2017__1up;   //!
   TBranch        *b_TauWeightTrigHLT_tau160_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2016__1down;   //!
   TBranch        *b_TauWeightTrigHLT_tau160_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2016__1up;   //!
   TBranch        *b_TauWeightTrigHLT_tau160_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2017__1down;   //!
   TBranch        *b_TauWeightTrigHLT_tau160_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2017__1up;   //!
   TBranch        *b_TauWeightTrigHLT_tau160_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2016__1down;   //!
   TBranch        *b_TauWeightTrigHLT_tau160_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2016__1up;   //!
   TBranch        *b_TauWeightTrigHLT_tau160_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2017__1down;   //!
   TBranch        *b_TauWeightTrigHLT_tau160_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2017__1up;   //!
   TBranch        *b_TauWeightTrigHLT_tau25_medium1_tracktwo;   //!
   TBranch        *b_TauWeightTrigHLT_tau25_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2016__1down;   //!
   TBranch        *b_TauWeightTrigHLT_tau25_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2016__1up;   //!
   TBranch        *b_TauWeightTrigHLT_tau25_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2017__1down;   //!
   TBranch        *b_TauWeightTrigHLT_tau25_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2017__1up;   //!
   TBranch        *b_TauWeightTrigHLT_tau25_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2016__1down;   //!
   TBranch        *b_TauWeightTrigHLT_tau25_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2016__1up;   //!
   TBranch        *b_TauWeightTrigHLT_tau25_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2017__1down;   //!
   TBranch        *b_TauWeightTrigHLT_tau25_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2017__1up;   //!
   TBranch        *b_TauWeightTrigHLT_tau25_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2016__1down;   //!
   TBranch        *b_TauWeightTrigHLT_tau25_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2016__1up;   //!
   TBranch        *b_TauWeightTrigHLT_tau25_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2017__1down;   //!
   TBranch        *b_TauWeightTrigHLT_tau25_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2017__1up;   //!
   TBranch        *b_TauWeightTrigHLT_tau35_medium1_tracktwo;   //!
   TBranch        *b_TauWeightTrigHLT_tau35_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2016__1down;   //!
   TBranch        *b_TauWeightTrigHLT_tau35_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2016__1up;   //!
   TBranch        *b_TauWeightTrigHLT_tau35_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2017__1down;   //!
   TBranch        *b_TauWeightTrigHLT_tau35_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2017__1up;   //!
   TBranch        *b_TauWeightTrigHLT_tau35_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2016__1down;   //!
   TBranch        *b_TauWeightTrigHLT_tau35_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2016__1up;   //!
   TBranch        *b_TauWeightTrigHLT_tau35_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2017__1down;   //!
   TBranch        *b_TauWeightTrigHLT_tau35_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2017__1up;   //!
   TBranch        *b_TauWeightTrigHLT_tau35_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2016__1down;   //!
   TBranch        *b_TauWeightTrigHLT_tau35_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2016__1up;   //!
   TBranch        *b_TauWeightTrigHLT_tau35_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2017__1down;   //!
   TBranch        *b_TauWeightTrigHLT_tau35_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2017__1up;   //!
   TBranch        *b_TauWeightTrigHLT_tau50_medium1_tracktwo_L1TAU12;   //!
   TBranch        *b_TauWeightTrigHLT_tau50_medium1_tracktwo_L1TAU12_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2016__1down;   //!
   TBranch        *b_TauWeightTrigHLT_tau50_medium1_tracktwo_L1TAU12_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2016__1up;   //!
   TBranch        *b_TauWeightTrigHLT_tau50_medium1_tracktwo_L1TAU12_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2017__1down;   //!
   TBranch        *b_TauWeightTrigHLT_tau50_medium1_tracktwo_L1TAU12_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2017__1up;   //!
   TBranch        *b_TauWeightTrigHLT_tau50_medium1_tracktwo_L1TAU12_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2016__1down;   //!
   TBranch        *b_TauWeightTrigHLT_tau50_medium1_tracktwo_L1TAU12_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2016__1up;   //!
   TBranch        *b_TauWeightTrigHLT_tau50_medium1_tracktwo_L1TAU12_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2017__1down;   //!
   TBranch        *b_TauWeightTrigHLT_tau50_medium1_tracktwo_L1TAU12_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2017__1up;   //!
   TBranch        *b_TauWeightTrigHLT_tau50_medium1_tracktwo_L1TAU12_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2016__1down;   //!
   TBranch        *b_TauWeightTrigHLT_tau50_medium1_tracktwo_L1TAU12_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2016__1up;   //!
   TBranch        *b_TauWeightTrigHLT_tau50_medium1_tracktwo_L1TAU12_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2017__1down;   //!
   TBranch        *b_TauWeightTrigHLT_tau50_medium1_tracktwo_L1TAU12_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2017__1up;   //!
   TBranch        *b_TauWeightTrigHLT_tau80_medium1_tracktwo;   //!
   TBranch        *b_TauWeightTrigHLT_tau80_medium1_tracktwo_L1TAU60;   //!
   TBranch        *b_TauWeightTrigHLT_tau80_medium1_tracktwo_L1TAU60_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2016__1down;   //!
   TBranch        *b_TauWeightTrigHLT_tau80_medium1_tracktwo_L1TAU60_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2016__1up;   //!
   TBranch        *b_TauWeightTrigHLT_tau80_medium1_tracktwo_L1TAU60_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2017__1down;   //!
   TBranch        *b_TauWeightTrigHLT_tau80_medium1_tracktwo_L1TAU60_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2017__1up;   //!
   TBranch        *b_TauWeightTrigHLT_tau80_medium1_tracktwo_L1TAU60_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2016__1down;   //!
   TBranch        *b_TauWeightTrigHLT_tau80_medium1_tracktwo_L1TAU60_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2016__1up;   //!
   TBranch        *b_TauWeightTrigHLT_tau80_medium1_tracktwo_L1TAU60_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2017__1down;   //!
   TBranch        *b_TauWeightTrigHLT_tau80_medium1_tracktwo_L1TAU60_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2017__1up;   //!
   TBranch        *b_TauWeightTrigHLT_tau80_medium1_tracktwo_L1TAU60_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2016__1down;   //!
   TBranch        *b_TauWeightTrigHLT_tau80_medium1_tracktwo_L1TAU60_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2016__1up;   //!
   TBranch        *b_TauWeightTrigHLT_tau80_medium1_tracktwo_L1TAU60_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2017__1down;   //!
   TBranch        *b_TauWeightTrigHLT_tau80_medium1_tracktwo_L1TAU60_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2017__1up;   //!
   TBranch        *b_TauWeightTrigHLT_tau80_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2016__1down;   //!
   TBranch        *b_TauWeightTrigHLT_tau80_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2016__1up;   //!
   TBranch        *b_TauWeightTrigHLT_tau80_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2017__1down;   //!
   TBranch        *b_TauWeightTrigHLT_tau80_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2017__1up;   //!
   TBranch        *b_TauWeightTrigHLT_tau80_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2016__1down;   //!
   TBranch        *b_TauWeightTrigHLT_tau80_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2016__1up;   //!
   TBranch        *b_TauWeightTrigHLT_tau80_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2017__1down;   //!
   TBranch        *b_TauWeightTrigHLT_tau80_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2017__1up;   //!
   TBranch        *b_TauWeightTrigHLT_tau80_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2016__1down;   //!
   TBranch        *b_TauWeightTrigHLT_tau80_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2016__1up;   //!
   TBranch        *b_TauWeightTrigHLT_tau80_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2017__1down;   //!
   TBranch        *b_TauWeightTrigHLT_tau80_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2017__1up;   //!
   TBranch        *b_TauWeight_TAUS_TRUEELECTRON_EFF_ELEOLR_STATHIGHMU__1down;   //!
   TBranch        *b_TauWeight_TAUS_TRUEELECTRON_EFF_ELEOLR_STATHIGHMU__1up;   //!
   TBranch        *b_TauWeight_TAUS_TRUEELECTRON_EFF_ELEOLR_STATLOWMU__1down;   //!
   TBranch        *b_TauWeight_TAUS_TRUEELECTRON_EFF_ELEOLR_STATLOWMU__1up;   //!
   TBranch        *b_TauWeight_TAUS_TRUEELECTRON_EFF_ELEOLR_SYST__1down;   //!
   TBranch        *b_TauWeight_TAUS_TRUEELECTRON_EFF_ELEOLR_SYST__1up;   //!
   TBranch        *b_TauWeight_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL__1down;   //!
   TBranch        *b_TauWeight_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL__1up;   //!
   TBranch        *b_TauWeight_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTUNCORR2025__1down;   //!
   TBranch        *b_TauWeight_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTUNCORR2025__1up;   //!
   TBranch        *b_TauWeight_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTUNCORR2530__1down;   //!
   TBranch        *b_TauWeight_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTUNCORR2530__1up;   //!
   TBranch        *b_TauWeight_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTUNCORR3040__1down;   //!
   TBranch        *b_TauWeight_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTUNCORR3040__1up;   //!
   TBranch        *b_TauWeight_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTUNCORRGE40__1down;   //!
   TBranch        *b_TauWeight_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTUNCORRGE40__1up;   //!
   TBranch        *b_TauWeight_TAUS_TRUEHADTAU_EFF_JETID_3PRONGSTATSYSTUNCORR2030__1down;   //!
   TBranch        *b_TauWeight_TAUS_TRUEHADTAU_EFF_JETID_3PRONGSTATSYSTUNCORR2030__1up;   //!
   TBranch        *b_TauWeight_TAUS_TRUEHADTAU_EFF_JETID_3PRONGSTATSYSTUNCORRGE30__1down;   //!
   TBranch        *b_TauWeight_TAUS_TRUEHADTAU_EFF_JETID_3PRONGSTATSYSTUNCORRGE30__1up;   //!
   TBranch        *b_TauWeight_TAUS_TRUEHADTAU_EFF_JETID_HIGHPT__1down;   //!
   TBranch        *b_TauWeight_TAUS_TRUEHADTAU_EFF_JETID_HIGHPT__1up;   //!
   TBranch        *b_TauWeight_TAUS_TRUEHADTAU_EFF_JETID_SYST__1down;   //!
   TBranch        *b_TauWeight_TAUS_TRUEHADTAU_EFF_JETID_SYST__1up;   //!
   TBranch        *b_TauWeight_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT__1down;   //!
   TBranch        *b_TauWeight_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT__1up;   //!
   TBranch        *b_TauWeight_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1down;   //!
   TBranch        *b_TauWeight_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1up;   //!
   TBranch        *b_muWeight;   //!
   TBranch        *b_muWeight_PRW_DATASF__1down;   //!
   TBranch        *b_muWeight_PRW_DATASF__1up;   //!
   TBranch        *b_Aplanarity;   //!
   TBranch        *b_Circularity;   //!
   TBranch        *b_CosChi1;   //!
   TBranch        *b_CosChi2;   //!
   TBranch        *b_Ht_Jet;   //!
   TBranch        *b_Ht_Lep;   //!
   TBranch        *b_Ht_Tau;   //!
   TBranch        *b_Lin_Aplanarity;   //!
   TBranch        *b_Lin_Circularity;   //!
   TBranch        *b_Lin_Planarity;   //!
   TBranch        *b_Lin_Sphericity;   //!
   TBranch        *b_Lin_Sphericity_C;   //!
   TBranch        *b_Lin_Sphericity_D;   //!
   TBranch        *b_MET_Centrality;   //!
   TBranch        *b_MET_CosMinDeltaPhi;   //!
   TBranch        *b_MET_LepTau_DeltaPhi;   //!
   TBranch        *b_MET_LepTau_VecSumPt;   //!
   TBranch        *b_MET_SumCosDeltaPhi;   //!
   TBranch        *b_MT2_max;   //!
   TBranch        *b_MT2_min;   //!
   TBranch        *b_Meff;   //!
   TBranch        *b_Meff_TauTau;   //!
   TBranch        *b_MetTST_OverSqrtHT;   //!
   TBranch        *b_MetTST_OverSqrtSumET;   //!
   TBranch        *b_MetTST_Significance;   //!
   TBranch        *b_MetTST_Significance_Rho;   //!
   TBranch        *b_MetTST_Significance_VarL;   //!
   TBranch        *b_MetTST_Significance_dataJER;   //!
   TBranch        *b_MetTST_Significance_dataJER_Rho;   //!
   TBranch        *b_MetTST_Significance_dataJER_VarL;   //!
   TBranch        *b_MetTST_Significance_dataJER_noPUJets;   //!
   TBranch        *b_MetTST_Significance_dataJER_noPUJets_Rho;   //!
   TBranch        *b_MetTST_Significance_dataJER_noPUJets_VarL;   //!
   TBranch        *b_MetTST_Significance_noPUJets;   //!
   TBranch        *b_MetTST_Significance_noPUJets_Rho;   //!
   TBranch        *b_MetTST_Significance_noPUJets_VarL;   //!
   TBranch        *b_MetTST_Significance_noPUJets_noSoftTerm;   //!
   TBranch        *b_MetTST_Significance_noPUJets_noSoftTerm_Rho;   //!
   TBranch        *b_MetTST_Significance_noPUJets_noSoftTerm_VarL;   //!
   TBranch        *b_MetTST_Significance_phireso_noPUJets;   //!
   TBranch        *b_MetTST_Significance_phireso_noPUJets_Rho;   //!
   TBranch        *b_MetTST_Significance_phireso_noPUJets_VarL;   //!
   TBranch        *b_Oblateness;   //!
   TBranch        *b_PTt;   //!
   TBranch        *b_Planarity;   //!
   TBranch        *b_RJW_CosThetaStarW;   //!
   TBranch        *b_RJW_GammaBetaW;   //!
   TBranch        *b_RJW_dPhiDecayPlaneW;   //!
   TBranch        *b_RJZ_BalMetObj_Z;   //!
   TBranch        *b_RJZ_CMS_Mass;   //!
   TBranch        *b_RJZ_H01_Tau1;   //!
   TBranch        *b_RJZ_H01_Z;   //!
   TBranch        *b_RJZ_H11_Tau1;   //!
   TBranch        *b_RJZ_H11_Z;   //!
   TBranch        *b_RJZ_H21_Tau1;   //!
   TBranch        *b_RJZ_H21_Z;   //!
   TBranch        *b_RJZ_Ht01_Tau1;   //!
   TBranch        *b_RJZ_Ht01_Z;   //!
   TBranch        *b_RJZ_Ht11_Tau1;   //!
   TBranch        *b_RJZ_Ht11_Z;   //!
   TBranch        *b_RJZ_Ht21_Tau1;   //!
   TBranch        *b_RJZ_Ht21_Z;   //!
   TBranch        *b_RJZ_R_BoostZ;   //!
   TBranch        *b_RJZ_cosTheta_Tau1;   //!
   TBranch        *b_RJZ_cosTheta_Tau2;   //!
   TBranch        *b_RJZ_cosTheta_Z;   //!
   TBranch        *b_RJZ_dPhiDecayPlane_Tau1;   //!
   TBranch        *b_RJZ_dPhiDecayPlane_Tau2;   //!
   TBranch        *b_RJZ_dPhiDecayPlane_Z;   //!
   TBranch        *b_Sphericity;   //!
   TBranch        *b_Spherocity;   //!
   TBranch        *b_Thrust;   //!
   TBranch        *b_ThrustMajor;   //!
   TBranch        *b_ThrustMinor;   //!
   TBranch        *b_VecSumPt_LepTau;   //!
   TBranch        *b_WolframMoment_0;   //!
   TBranch        *b_WolframMoment_1;   //!
   TBranch        *b_WolframMoment_10;   //!
   TBranch        *b_WolframMoment_11;   //!
   TBranch        *b_WolframMoment_2;   //!
   TBranch        *b_WolframMoment_3;   //!
   TBranch        *b_WolframMoment_4;   //!
   TBranch        *b_WolframMoment_5;   //!
   TBranch        *b_WolframMoment_6;   //!
   TBranch        *b_WolframMoment_7;   //!
   TBranch        *b_WolframMoment_8;   //!
   TBranch        *b_WolframMoment_9;   //!
   TBranch        *b_actualInteractionsPerCrossing;   //!
   TBranch        *b_averageInteractionsPerCrossing;   //!
   TBranch        *b_corr_avgIntPerX;   //!
   TBranch        *b_corr_avgIntPerX_PRW_DATASF__1down;   //!
   TBranch        *b_corr_avgIntPerX_PRW_DATASF__1up;   //!
   TBranch        *b_emt_MT2_max;   //!
   TBranch        *b_emt_MT2_min;   //!
   TBranch        *b_mu_density;   //!
   TBranch        *b_SUSYFinalState;   //!
   TBranch        *b_Vtx_n;   //!
   TBranch        *b_n_BJets;   //!
   TBranch        *b_n_BaseJets;   //!
   TBranch        *b_n_BaseTau;   //!
   TBranch        *b_n_SignalElec;   //!
   TBranch        *b_n_SignalJets;   //!
   TBranch        *b_n_SignalMuon;   //!
   TBranch        *b_n_SignalTau;   //!
   TBranch        *b_OS_BaseTauEle;   //!
   TBranch        *b_OS_BaseTauMuo;   //!
   TBranch        *b_OS_EleEle;   //!
   TBranch        *b_OS_MuoMuo;   //!
   TBranch        *b_OS_TauEle;   //!
   TBranch        *b_OS_TauMuo;   //!
   TBranch        *b_OS_TauTau;   //!
   TBranch        *b_TrigHLT_e120_lhloose;   //!
   TBranch        *b_TrigHLT_e140_lhloose_nod0;   //!
   TBranch        *b_TrigHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwo;   //!
   TBranch        *b_TrigHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwoEF;   //!
   TBranch        *b_TrigHLT_e17_lhmedium_nod0_ivarloose_tau25_mediumRNN_tracktwoMVA;   //!
   TBranch        *b_TrigHLT_e17_lhmedium_nod0_tau25_medium1_tracktwoEF_xe50;   //!
   TBranch        *b_TrigHLT_e17_lhmedium_nod0_tau25_medium1_tracktwo_xe50;   //!
   TBranch        *b_TrigHLT_e17_lhmedium_nod0_tau25_mediumRNN_tracktwoMVA_xe50;   //!
   TBranch        *b_TrigHLT_e24_lhmedium_L1EM20VH;   //!
   TBranch        *b_TrigHLT_e24_lhmedium_nod0_ivarloose_tau35_medium1_tracktwo;   //!
   TBranch        *b_TrigHLT_e26_lhtight_nod0_ivarloose;   //!
   TBranch        *b_TrigHLT_e60_lhmedium;   //!
   TBranch        *b_TrigHLT_e60_lhmedium_nod0;   //!
   TBranch        *b_TrigHLT_mu14_ivarloose_tau25_medium1_tracktwo;   //!
   TBranch        *b_TrigHLT_mu14_ivarloose_tau35_medium1_tracktwo;   //!
   TBranch        *b_TrigHLT_mu14_ivarloose_tau35_medium1_tracktwoEF;   //!
   TBranch        *b_TrigHLT_mu14_ivarloose_tau35_mediumRNN_tracktwoMVA;   //!
   TBranch        *b_TrigHLT_mu14_tau25_medium1_tracktwoEF_xe50;   //!
   TBranch        *b_TrigHLT_mu14_tau25_medium1_tracktwo_xe50;   //!
   TBranch        *b_TrigHLT_mu14_tau25_mediumRNN_tracktwoMVA_xe50;   //!
   TBranch        *b_TrigHLT_mu20_iloose_L1MU15;   //!
   TBranch        *b_TrigHLT_mu24_ivarmedium;   //!
   TBranch        *b_TrigHLT_mu26_imedium;   //!
   TBranch        *b_TrigHLT_mu26_ivarmedium;   //!
   TBranch        *b_TrigHLT_mu40;   //!
   TBranch        *b_TrigHLT_mu50;   //!
   TBranch        *b_TrigHLT_mu60_0eta105_msonly;   //!
   TBranch        *b_TrigHLT_tau125_medium1_tracktwo;   //!
   TBranch        *b_TrigHLT_tau125_medium1_tracktwoEF;   //!
   TBranch        *b_TrigHLT_tau160_medium1_tracktwo;   //!
   TBranch        *b_TrigHLT_tau160_medium1_tracktwoEF;   //!
   TBranch        *b_TrigHLT_tau160_medium1_tracktwo_L1TAU100;   //!
   TBranch        *b_TrigHLT_tau25_medium1_tracktwo;   //!
   TBranch        *b_TrigHLT_tau25_medium1_tracktwoEF;   //!
   TBranch        *b_TrigHLT_tau25_mediumRNN_tracktwoMVA;   //!
   TBranch        *b_TrigHLT_tau35_medium1_tracktwo;   //!
   TBranch        *b_TrigHLT_tau35_medium1_tracktwoEF;   //!
   TBranch        *b_TrigHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo;   //!
   TBranch        *b_TrigHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_L1TAU20IM_2TAU12IM;   //!
   TBranch        *b_TrigHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_xe50;   //!
   TBranch        *b_TrigHLT_tau50_medium1_tracktwo_L1TAU12;   //!
   TBranch        *b_TrigHLT_tau60_medium1_tracktwo;   //!
   TBranch        *b_TrigHLT_tau60_medium1_tracktwoEF;   //!
   TBranch        *b_TrigHLT_tau60_medium1_tracktwoEF_tau25_medium1_tracktwoEF_xe50;   //!
   TBranch        *b_TrigHLT_tau60_medium1_tracktwo_tau25_medium1_tracktwo_xe50;   //!
   TBranch        *b_TrigHLT_tau60_mediumRNN_tracktwoMVA_tau25_mediumRNN_tracktwoMVA_xe50;   //!
   TBranch        *b_TrigHLT_tau80_medium1_tracktwo;   //!
   TBranch        *b_TrigHLT_tau80_medium1_tracktwoEF;   //!
   TBranch        *b_TrigHLT_tau80_medium1_tracktwoEF_L1TAU60;   //!
   TBranch        *b_TrigHLT_tau80_medium1_tracktwoEF_L1TAU60_tau60_medium1_tracktwoEF_L1TAU40;   //!
   TBranch        *b_TrigHLT_tau80_medium1_tracktwo_L1TAU60;   //!
   TBranch        *b_TrigHLT_tau80_medium1_tracktwo_L1TAU60_tau25_medium1_tracktwo_L1TAU12;   //!
   TBranch        *b_TrigHLT_tau80_medium1_tracktwo_L1TAU60_tau50_medium1_tracktwo_L1TAU12;   //!
   TBranch        *b_TrigHLT_tau80_medium1_tracktwo_L1TAU60_tau60_medium1_tracktwo_L1TAU40;   //!
   TBranch        *b_TrigHLT_tau80_mediumRNN_tracktwoMVA_L1TAU60_tau60_mediumRNN_tracktwoMVA_L1TAU40;   //!
   TBranch        *b_TrigHLT_xe100_pufit_L1XE50;   //!
   TBranch        *b_TrigHLT_xe100_pufit_L1XE55;   //!
   TBranch        *b_TrigHLT_xe110_mht_L1XE50;   //!
   TBranch        *b_TrigHLT_xe110_pufit_L1XE50;   //!
   TBranch        *b_TrigHLT_xe110_pufit_L1XE55;   //!
   TBranch        *b_TrigHLT_xe110_pufit_xe65_L1XE50;   //!
   TBranch        *b_TrigHLT_xe110_pufit_xe70_L1XE50;   //!
   TBranch        *b_TrigHLT_xe120_pufit_L1XE50;   //!
   TBranch        *b_TrigHLT_xe50;   //!
   TBranch        *b_TrigHLT_xe70_mht;   //!
   TBranch        *b_TrigHLT_xe90_mht_L1XE50;   //!
   TBranch        *b_TrigHLT_xe90_pufit_L1XE50;   //!
   TBranch        *b_TrigMatchHLT_e120_lhloose;   //!
   TBranch        *b_TrigMatchHLT_e140_lhloose_nod0;   //!
   TBranch        *b_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwo;   //!
   TBranch        *b_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwoEF;   //!
   TBranch        *b_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_mediumRNN_tracktwoMVA;   //!
   TBranch        *b_TrigMatchHLT_e17_lhmedium_nod0_tau25_medium1_tracktwoEF_xe50;   //!
   TBranch        *b_TrigMatchHLT_e17_lhmedium_nod0_tau25_medium1_tracktwo_xe50;   //!
   TBranch        *b_TrigMatchHLT_e17_lhmedium_nod0_tau25_mediumRNN_tracktwoMVA_xe50;   //!
   TBranch        *b_TrigMatchHLT_e24_lhmedium_L1EM20VH;   //!
   TBranch        *b_TrigMatchHLT_e24_lhmedium_nod0_ivarloose_tau35_medium1_tracktwo;   //!
   TBranch        *b_TrigMatchHLT_e26_lhtight_nod0_ivarloose;   //!
   TBranch        *b_TrigMatchHLT_e60_lhmedium;   //!
   TBranch        *b_TrigMatchHLT_e60_lhmedium_nod0;   //!
   TBranch        *b_TrigMatchHLT_mu14_ivarloose_tau25_medium1_tracktwo;   //!
   TBranch        *b_TrigMatchHLT_mu14_ivarloose_tau35_medium1_tracktwo;   //!
   TBranch        *b_TrigMatchHLT_mu14_ivarloose_tau35_medium1_tracktwoEF;   //!
   TBranch        *b_TrigMatchHLT_mu14_ivarloose_tau35_mediumRNN_tracktwoMVA;   //!
   TBranch        *b_TrigMatchHLT_mu14_tau25_medium1_tracktwoEF_xe50;   //!
   TBranch        *b_TrigMatchHLT_mu14_tau25_medium1_tracktwo_xe50;   //!
   TBranch        *b_TrigMatchHLT_mu14_tau25_mediumRNN_tracktwoMVA_xe50;   //!
   TBranch        *b_TrigMatchHLT_mu20_iloose_L1MU15;   //!
   TBranch        *b_TrigMatchHLT_mu24_ivarmedium;   //!
   TBranch        *b_TrigMatchHLT_mu26_imedium;   //!
   TBranch        *b_TrigMatchHLT_mu26_ivarmedium;   //!
   TBranch        *b_TrigMatchHLT_mu40;   //!
   TBranch        *b_TrigMatchHLT_mu50;   //!
   TBranch        *b_TrigMatchHLT_mu60_0eta105_msonly;   //!
   TBranch        *b_TrigMatchHLT_tau125_medium1_tracktwo;   //!
   TBranch        *b_TrigMatchHLT_tau125_medium1_tracktwoEF;   //!
   TBranch        *b_TrigMatchHLT_tau160_medium1_tracktwo;   //!
   TBranch        *b_TrigMatchHLT_tau160_medium1_tracktwoEF;   //!
   TBranch        *b_TrigMatchHLT_tau160_medium1_tracktwo_L1TAU100;   //!
   TBranch        *b_TrigMatchHLT_tau25_medium1_tracktwo;   //!
   TBranch        *b_TrigMatchHLT_tau25_medium1_tracktwoEF;   //!
   TBranch        *b_TrigMatchHLT_tau25_mediumRNN_tracktwoMVA;   //!
   TBranch        *b_TrigMatchHLT_tau35_medium1_tracktwo;   //!
   TBranch        *b_TrigMatchHLT_tau35_medium1_tracktwoEF;   //!
   TBranch        *b_TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo;   //!
   TBranch        *b_TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_L1TAU20IM_2TAU12IM;   //!
   TBranch        *b_TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_xe50;   //!
   TBranch        *b_TrigMatchHLT_tau50_medium1_tracktwo_L1TAU12;   //!
   TBranch        *b_TrigMatchHLT_tau60_medium1_tracktwo;   //!
   TBranch        *b_TrigMatchHLT_tau60_medium1_tracktwoEF;   //!
   TBranch        *b_TrigMatchHLT_tau60_medium1_tracktwoEF_tau25_medium1_tracktwoEF_xe50;   //!
   TBranch        *b_TrigMatchHLT_tau60_medium1_tracktwo_tau25_medium1_tracktwo_xe50;   //!
   TBranch        *b_TrigMatchHLT_tau60_mediumRNN_tracktwoMVA_tau25_mediumRNN_tracktwoMVA_xe50;   //!
   TBranch        *b_TrigMatchHLT_tau80_medium1_tracktwo;   //!
   TBranch        *b_TrigMatchHLT_tau80_medium1_tracktwoEF;   //!
   TBranch        *b_TrigMatchHLT_tau80_medium1_tracktwoEF_L1TAU60;   //!
   TBranch        *b_TrigMatchHLT_tau80_medium1_tracktwoEF_L1TAU60_tau60_medium1_tracktwoEF_L1TAU40;   //!
   TBranch        *b_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60;   //!
   TBranch        *b_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau25_medium1_tracktwo_L1TAU12;   //!
   TBranch        *b_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau50_medium1_tracktwo_L1TAU12;   //!
   TBranch        *b_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau60_medium1_tracktwo_L1TAU40;   //!
   TBranch        *b_TrigMatchHLT_tau80_mediumRNN_tracktwoMVA_L1TAU60_tau60_mediumRNN_tracktwoMVA_L1TAU40;   //!
   TBranch        *b_TrigMatching;   //!
   TBranch        *b_Lin_Sphericity_IsValid;   //!
   TBranch        *b_MET_BiSect;   //!
   TBranch        *b_Sphericity_IsValid;   //!
   TBranch        *b_Spherocity_IsValid;   //!
   TBranch        *b_Thrust_IsValid;   //!
   TBranch        *b_WolframMoments_AreValid;   //!
   TBranch        *b_RandomLumiBlockNumber;   //!
   TBranch        *b_RandomRunNumber;   //!
   TBranch        *b_backgroundFlags;   //!
   TBranch        *b_bcid;   //!
   TBranch        *b_coreFlags;   //!
   TBranch        *b_forwardDetFlags;   //!
   TBranch        *b_larFlags;   //!
   TBranch        *b_lumiBlock;   //!
   TBranch        *b_lumiFlags;   //!
   TBranch        *b_muonFlags;   //!
   TBranch        *b_pixelFlags;   //!
   TBranch        *b_runNumber;   //!
   TBranch        *b_sctFlags;   //!
   TBranch        *b_tileFlags;   //!
   TBranch        *b_trtFlags;   //!
   TBranch        *b_eventNumber;   //!
   TBranch        *b_MetCST_met;   //!
   TBranch        *b_MetCST_phi;   //!
   TBranch        *b_MetCST_sumet;   //!
   TBranch        *b_MetTST_met;   //!
   TBranch        *b_MetTST_phi;   //!
   TBranch        *b_MetTST_sumet;   //!
   TBranch        *b_TruthMET_met;   //!
   TBranch        *b_TruthMET_phi;   //!
   TBranch        *b_TruthMET_sumet;   //!
   TBranch        *b_RJW_JigSawCandidates_frame;   //!
   TBranch        *b_RJW_JigSawCandidates_pdgId;   //!
   TBranch        *b_RJW_JigSawCandidates_pt;   //!
   TBranch        *b_RJW_JigSawCandidates_eta;   //!
   TBranch        *b_RJW_JigSawCandidates_phi;   //!
   TBranch        *b_RJW_JigSawCandidates_m;   //!
   TBranch        *b_RJZ_JigSawCandidates_CosThetaStar;   //!
   TBranch        *b_RJZ_JigSawCandidates_dPhiDecayPlane;   //!
   TBranch        *b_RJZ_JigSawCandidates_frame;   //!
   TBranch        *b_RJZ_JigSawCandidates_pdgId;   //!
   TBranch        *b_RJZ_JigSawCandidates_pt;   //!
   TBranch        *b_RJZ_JigSawCandidates_eta;   //!
   TBranch        *b_RJZ_JigSawCandidates_phi;   //!
   TBranch        *b_RJZ_JigSawCandidates_m;   //!
   TBranch        *b_dilepton_charge;   //!
   TBranch        *b_dilepton_pdgId;   //!
   TBranch        *b_dilepton_pt;   //!
   TBranch        *b_dilepton_eta;   //!
   TBranch        *b_dilepton_phi;   //!
   TBranch        *b_dilepton_m;   //!
   TBranch        *b_electrons_MT;   //!
   TBranch        *b_electrons_charge;   //!
   TBranch        *b_electrons_d0sig;   //!
   TBranch        *b_electrons_isol;   //!
   TBranch        *b_electrons_signal;   //!
   TBranch        *b_electrons_truthOrigin;   //!
   TBranch        *b_electrons_truthType;   //!
   TBranch        *b_electrons_z0sinTheta;   //!
   TBranch        *b_electrons_pt;   //!
   TBranch        *b_electrons_eta;   //!
   TBranch        *b_electrons_phi;   //!
   TBranch        *b_electrons_e;   //!
   TBranch        *b_jets_Jvt;   //!
   TBranch        *b_jets_MV2c10;   //!
   TBranch        *b_jets_NTrks;   //!
   TBranch        *b_jets_bjet;   //!
   TBranch        *b_jets_signal;   //!
   TBranch        *b_jets_pt;   //!
   TBranch        *b_jets_eta;   //!
   TBranch        *b_jets_phi;   //!
   TBranch        *b_jets_m;   //!
   TBranch        *b_muons_MT;   //!
   TBranch        *b_muons_charge;   //!
   TBranch        *b_muons_d0sig;   //!
   TBranch        *b_muons_isol;   //!
   TBranch        *b_muons_signal;   //!
   TBranch        *b_muons_truthOrigin;   //!
   TBranch        *b_muons_truthType;   //!
   TBranch        *b_muons_z0sinTheta;   //!
   TBranch        *b_muons_pt;   //!
   TBranch        *b_muons_eta;   //!
   TBranch        *b_muons_phi;   //!
   TBranch        *b_muons_e;   //!
   TBranch        *b_taus_BDTEleScore;   //!
   TBranch        *b_taus_BDTEleScoreSigTrans;   //!
   TBranch        *b_taus_BDTJetScore;   //!
   TBranch        *b_taus_BDTJetScoreSigTrans;   //!
   TBranch        *b_taus_ConeTruthLabelID;   //!
   TBranch        *b_taus_MT;   //!
   TBranch        *b_taus_NTrks;   //!
   TBranch        *b_taus_NTrksJet;   //!
   TBranch        *b_taus_PartonTruthLabelID;   //!
   TBranch        *b_taus_Quality;   //!
   TBranch        *b_taus_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwo;   //!
   TBranch        *b_taus_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwoEF;   //!
   TBranch        *b_taus_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_mediumRNN_tracktwoMVA;   //!
   TBranch        *b_taus_TrigMatchHLT_e17_lhmedium_nod0_tau25_medium1_tracktwoEF_xe50;   //!
   TBranch        *b_taus_TrigMatchHLT_e17_lhmedium_nod0_tau25_medium1_tracktwo_xe50;   //!
   TBranch        *b_taus_TrigMatchHLT_e17_lhmedium_nod0_tau25_mediumRNN_tracktwoMVA_xe50;   //!
   TBranch        *b_taus_TrigMatchHLT_e24_lhmedium_nod0_ivarloose_tau35_medium1_tracktwo;   //!
   TBranch        *b_taus_TrigMatchHLT_mu14_ivarloose_tau25_medium1_tracktwo;   //!
   TBranch        *b_taus_TrigMatchHLT_mu14_ivarloose_tau35_medium1_tracktwo;   //!
   TBranch        *b_taus_TrigMatchHLT_mu14_ivarloose_tau35_medium1_tracktwoEF;   //!
   TBranch        *b_taus_TrigMatchHLT_mu14_ivarloose_tau35_mediumRNN_tracktwoMVA;   //!
   TBranch        *b_taus_TrigMatchHLT_mu14_tau25_medium1_tracktwoEF_xe50;   //!
   TBranch        *b_taus_TrigMatchHLT_mu14_tau25_medium1_tracktwo_xe50;   //!
   TBranch        *b_taus_TrigMatchHLT_mu14_tau25_mediumRNN_tracktwoMVA_xe50;   //!
   TBranch        *b_taus_TrigMatchHLT_tau125_medium1_tracktwo;   //!
   TBranch        *b_taus_TrigMatchHLT_tau125_medium1_tracktwoEF;   //!
   TBranch        *b_taus_TrigMatchHLT_tau160_medium1_tracktwo;   //!
   TBranch        *b_taus_TrigMatchHLT_tau160_medium1_tracktwoEF;   //!
   TBranch        *b_taus_TrigMatchHLT_tau160_medium1_tracktwo_L1TAU100;   //!
   TBranch        *b_taus_TrigMatchHLT_tau25_medium1_tracktwo;   //!
   TBranch        *b_taus_TrigMatchHLT_tau25_medium1_tracktwoEF;   //!
   TBranch        *b_taus_TrigMatchHLT_tau25_mediumRNN_tracktwoMVA;   //!
   TBranch        *b_taus_TrigMatchHLT_tau35_medium1_tracktwo;   //!
   TBranch        *b_taus_TrigMatchHLT_tau35_medium1_tracktwoEF;   //!
   TBranch        *b_taus_TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo;   //!
   TBranch        *b_taus_TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_L1TAU20IM_2TAU12IM;   //!
   TBranch        *b_taus_TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_xe50;   //!
   TBranch        *b_taus_TrigMatchHLT_tau50_medium1_tracktwo_L1TAU12;   //!
   TBranch        *b_taus_TrigMatchHLT_tau60_medium1_tracktwo;   //!
   TBranch        *b_taus_TrigMatchHLT_tau60_medium1_tracktwoEF;   //!
   TBranch        *b_taus_TrigMatchHLT_tau60_medium1_tracktwoEF_tau25_medium1_tracktwoEF_xe50;   //!
   TBranch        *b_taus_TrigMatchHLT_tau60_medium1_tracktwo_tau25_medium1_tracktwo_xe50;   //!
   TBranch        *b_taus_TrigMatchHLT_tau60_mediumRNN_tracktwoMVA_tau25_mediumRNN_tracktwoMVA_xe50;   //!
   TBranch        *b_taus_TrigMatchHLT_tau80_medium1_tracktwo;   //!
   TBranch        *b_taus_TrigMatchHLT_tau80_medium1_tracktwoEF;   //!
   TBranch        *b_taus_TrigMatchHLT_tau80_medium1_tracktwoEF_L1TAU60;   //!
   TBranch        *b_taus_TrigMatchHLT_tau80_medium1_tracktwoEF_L1TAU60_tau60_medium1_tracktwoEF_L1TAU40;   //!
   TBranch        *b_taus_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60;   //!
   TBranch        *b_taus_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau25_medium1_tracktwo_L1TAU12;   //!
   TBranch        *b_taus_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau50_medium1_tracktwo_L1TAU12;   //!
   TBranch        *b_taus_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau60_medium1_tracktwo_L1TAU40;   //!
   TBranch        *b_taus_TrigMatchHLT_tau80_mediumRNN_tracktwoMVA_L1TAU60_tau60_mediumRNN_tracktwoMVA_L1TAU40;   //!
   TBranch        *b_taus_Width;   //!
   TBranch        *b_taus_charge;   //!
   TBranch        *b_taus_signalID;   //!
   TBranch        *b_taus_truthOrigin;   //!
   TBranch        *b_taus_truthType;   //!
   TBranch        *b_taus_pt;   //!
   TBranch        *b_taus_eta;   //!
   TBranch        *b_taus_phi;   //!
   TBranch        *b_taus_e;   //!

   selector(TTree *tree=0);
   virtual ~selector();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

selector::selector(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("MC16a_StauStau_320_160.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("MC16a_StauStau_320_160.root");
      }
      f->GetObject("Staus_Nominal",tree);

   }
   Init(tree);
}

selector::~selector()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t selector::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t selector::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void selector::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   taus_isPromoted = 0;
   PreAllFakeTau_pt = 0;
   PreFakeTau_pt = 0;
   PreAllFakeTau_eta = 0;
   PreFakeTau_eta = 0;
   PreAllFakeTau_NTrks = 0;
   PreFakeTau_NTrks = 0;
   RJW_JigSawCandidates_frame = 0;
   RJW_JigSawCandidates_pdgId = 0;
   RJW_JigSawCandidates_pt = 0;
   RJW_JigSawCandidates_eta = 0;
   RJW_JigSawCandidates_phi = 0;
   RJW_JigSawCandidates_m = 0;
   RJZ_JigSawCandidates_CosThetaStar = 0;
   RJZ_JigSawCandidates_dPhiDecayPlane = 0;
   RJZ_JigSawCandidates_frame = 0;
   RJZ_JigSawCandidates_pdgId = 0;
   RJZ_JigSawCandidates_pt = 0;
   RJZ_JigSawCandidates_eta = 0;
   RJZ_JigSawCandidates_phi = 0;
   RJZ_JigSawCandidates_m = 0;
   dilepton_charge = 0;
   dilepton_pdgId = 0;
   dilepton_pt = 0;
   dilepton_eta = 0;
   dilepton_phi = 0;
   dilepton_m = 0;
   electrons_MT = 0;
   electrons_charge = 0;
   electrons_d0sig = 0;
   electrons_isol = 0;
   electrons_signal = 0;
   electrons_truthOrigin = 0;
   electrons_truthType = 0;
   electrons_z0sinTheta = 0;
   electrons_pt = 0;
   electrons_eta = 0;
   electrons_phi = 0;
   electrons_e = 0;
   jets_Jvt = 0;
   jets_MV2c10 = 0;
   jets_NTrks = 0;
   jets_bjet = 0;
   jets_signal = 0;
   jets_pt = 0;
   jets_eta = 0;
   jets_phi = 0;
   jets_m = 0;
   muons_MT = 0;
   muons_charge = 0;
   muons_d0sig = 0;
   muons_isol = 0;
   muons_signal = 0;
   muons_truthOrigin = 0;
   muons_truthType = 0;
   muons_z0sinTheta = 0;
   muons_pt = 0;
   muons_eta = 0;
   muons_phi = 0;
   muons_e = 0;
   taus_BDTEleScore = 0;
   taus_BDTEleScoreSigTrans = 0;
   taus_BDTJetScore = 0;
   taus_BDTJetScoreSigTrans = 0;
   taus_ConeTruthLabelID = 0;
   taus_MT = 0;
   taus_NTrks = 0;
   taus_NTrksJet = 0;
   taus_PartonTruthLabelID = 0;
   taus_Quality = 0;
   taus_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwo = 0;
   taus_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwoEF = 0;
   taus_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_mediumRNN_tracktwoMVA = 0;
   taus_TrigMatchHLT_e17_lhmedium_nod0_tau25_medium1_tracktwoEF_xe50 = 0;
   taus_TrigMatchHLT_e17_lhmedium_nod0_tau25_medium1_tracktwo_xe50 = 0;
   taus_TrigMatchHLT_e17_lhmedium_nod0_tau25_mediumRNN_tracktwoMVA_xe50 = 0;
   taus_TrigMatchHLT_e24_lhmedium_nod0_ivarloose_tau35_medium1_tracktwo = 0;
   taus_TrigMatchHLT_mu14_ivarloose_tau25_medium1_tracktwo = 0;
   taus_TrigMatchHLT_mu14_ivarloose_tau35_medium1_tracktwo = 0;
   taus_TrigMatchHLT_mu14_ivarloose_tau35_medium1_tracktwoEF = 0;
   taus_TrigMatchHLT_mu14_ivarloose_tau35_mediumRNN_tracktwoMVA = 0;
   taus_TrigMatchHLT_mu14_tau25_medium1_tracktwoEF_xe50 = 0;
   taus_TrigMatchHLT_mu14_tau25_medium1_tracktwo_xe50 = 0;
   taus_TrigMatchHLT_mu14_tau25_mediumRNN_tracktwoMVA_xe50 = 0;
   taus_TrigMatchHLT_tau125_medium1_tracktwo = 0;
   taus_TrigMatchHLT_tau125_medium1_tracktwoEF = 0;
   taus_TrigMatchHLT_tau160_medium1_tracktwo = 0;
   taus_TrigMatchHLT_tau160_medium1_tracktwoEF = 0;
   taus_TrigMatchHLT_tau160_medium1_tracktwo_L1TAU100 = 0;
   taus_TrigMatchHLT_tau25_medium1_tracktwo = 0;
   taus_TrigMatchHLT_tau25_medium1_tracktwoEF = 0;
   taus_TrigMatchHLT_tau25_mediumRNN_tracktwoMVA = 0;
   taus_TrigMatchHLT_tau35_medium1_tracktwo = 0;
   taus_TrigMatchHLT_tau35_medium1_tracktwoEF = 0;
   taus_TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo = 0;
   taus_TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_L1TAU20IM_2TAU12IM = 0;
   taus_TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_xe50 = 0;
   taus_TrigMatchHLT_tau50_medium1_tracktwo_L1TAU12 = 0;
   taus_TrigMatchHLT_tau60_medium1_tracktwo = 0;
   taus_TrigMatchHLT_tau60_medium1_tracktwoEF = 0;
   taus_TrigMatchHLT_tau60_medium1_tracktwoEF_tau25_medium1_tracktwoEF_xe50 = 0;
   taus_TrigMatchHLT_tau60_medium1_tracktwo_tau25_medium1_tracktwo_xe50 = 0;
   taus_TrigMatchHLT_tau60_mediumRNN_tracktwoMVA_tau25_mediumRNN_tracktwoMVA_xe50 = 0;
   taus_TrigMatchHLT_tau80_medium1_tracktwo = 0;
   taus_TrigMatchHLT_tau80_medium1_tracktwoEF = 0;
   taus_TrigMatchHLT_tau80_medium1_tracktwoEF_L1TAU60 = 0;
   taus_TrigMatchHLT_tau80_medium1_tracktwoEF_L1TAU60_tau60_medium1_tracktwoEF_L1TAU40 = 0;
   taus_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60 = 0;
   taus_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau25_medium1_tracktwo_L1TAU12 = 0;
   taus_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau50_medium1_tracktwo_L1TAU12 = 0;
   taus_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau60_medium1_tracktwo_L1TAU40 = 0;
   taus_TrigMatchHLT_tau80_mediumRNN_tracktwoMVA_L1TAU60_tau60_mediumRNN_tracktwoMVA_L1TAU40 = 0;
   taus_Width = 0;
   taus_charge = 0;
   taus_signalID = 0;
   taus_truthOrigin = 0;
   taus_truthType = 0;
   taus_pt = 0;
   taus_eta = 0;
   taus_phi = 0;
   taus_e = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("JetWeight", &JetWeight, &b_JetWeight);
   fChain->SetBranchAddress("N_AllFakeTaus", &N_AllFakeTaus, &b_N_AllFakeTaus);
   fChain->SetBranchAddress("N_FakeTaus", &N_FakeTaus, &b_N_FakeTaus);
   fChain->SetBranchAddress("N_antiFakeTaus", &N_antiFakeTaus, &b_N_antiFakeTaus);
   fChain->SetBranchAddress("EventIsPromoted", &EventIsPromoted, &b_EventIsPromoted);
   fChain->SetBranchAddress("taus_isPromoted", &taus_isPromoted, &b_taus_isPromoted);
   fChain->SetBranchAddress("PreAllFakeTau_pt", &PreAllFakeTau_pt, &b_PreAllFakeTau_pt);
   fChain->SetBranchAddress("PreFakeTau_pt", &PreFakeTau_pt, &b_PreFakeTau_pt);
   fChain->SetBranchAddress("PreAllFakeTau_eta", &PreAllFakeTau_eta, &b_PreAllFakeTau_eta);
   fChain->SetBranchAddress("PreFakeTau_eta", &PreFakeTau_eta, &b_PreFakeTau_eta);
   fChain->SetBranchAddress("PreAllFakeTau_NTrks", &PreAllFakeTau_NTrks, &b_PreAllFakeTau_NTrks);
   fChain->SetBranchAddress("PreFakeTau_NTrks", &PreFakeTau_NTrks, &b_PreFakeTau_NTrks);
   fChain->SetBranchAddress("PromotionWeight", &PromotionWeight, &b_PromotionWeight);

   fChain->SetBranchAddress("mcChannelNumber", &mcChannelNumber, &b_mcChannelNumber);
   fChain->SetBranchAddress("mergedRunNumber", &mergedRunNumber, &b_mergedRunNumber);
   fChain->SetBranchAddress("mergedlumiBlock", &mergedlumiBlock, &b_mergedlumiBlock);
   fChain->SetBranchAddress("Weight_mc", &Weight_mc, &b_Weight_mc); 
   fChain->SetBranchAddress("ff_weight", &ff_weight, &b_ff_weight); 
   fChain->SetBranchAddress("ff_weight_sysUP", &ff_weight_sysUP, &b_ff_weight_sysUP); 
   fChain->SetBranchAddress("ff_weight_sysDown", &ff_weight_sysDown, &b_ff_weight_sysDown); 
   fChain->SetBranchAddress("tau1Pt", &tau1Pt, &b_tau1Pt);
   fChain->SetBranchAddress("tau2Pt", &tau2Pt, &b_tau2Pt);
   fChain->SetBranchAddress("tau1Mt", &tau1Mt, &b_tau1Mt);
   fChain->SetBranchAddress("tau2Mt", &tau2Mt, &b_tau2Mt);
   fChain->SetBranchAddress("dRtt", &dRtt, &b_dRtt);
   fChain->SetBranchAddress("dPhitt", &dPhitt, &b_dPhitt);
   fChain->SetBranchAddress("mt12tau", &mt12tau, &b_mt12tau);
   fChain->SetBranchAddress("Mll_tt", &Mll_tt, &b_Mll_tt);
   fChain->SetBranchAddress("meff_tt", &meff_tt, &b_meff_tt);
   fChain->SetBranchAddress("MT2", &MT2, &b_MT2);
   fChain->SetBranchAddress("tau1Pt_W", &tau1Pt_W, &b_tau1Pt_W);
   fChain->SetBranchAddress("mu1Pt", &mu1Pt, &b_mu1Pt);
   fChain->SetBranchAddress("tau1Mt_W", &tau1Mt_W, &b_tau1Mt_W);
   fChain->SetBranchAddress("mu1Mt", &mu1Mt, &b_mu1Mt);
   fChain->SetBranchAddress("dRtm", &dRtm, &b_dRtm);
   fChain->SetBranchAddress("dPhitm", &dPhitm, &b_dPhitm);
   fChain->SetBranchAddress("mt12tm", &mt12tm, &b_mt12tm);
   fChain->SetBranchAddress("Mll_tm", &Mll_tm, &b_Mll_tm);
   fChain->SetBranchAddress("meff_tm", &meff_tm, &b_meff_tm);
   fChain->SetBranchAddress("MT2_tm", &MT2_tm, &b_MT2_tm);
   fChain->SetBranchAddress("lJet_pt", &lJet_pt, &b_lJet_pt);
   fChain->SetBranchAddress("LJet_n", &LJet_n, &b_LJet_n);
   fChain->SetBranchAddress("Evt_MET", &Evt_MET, &b_Evt_MET);
   fChain->SetBranchAddress("Evt_METSIG", &Evt_METSIG, &b_Evt_METSIG);
   fChain->SetBranchAddress("Evt_METSIG_WithTauJet", &Evt_METSIG_WithTauJet, &b_Evt_METSIG_WithTauJet);
   fChain->SetBranchAddress("isSR1", &isSR1, &b_isSR1);
   fChain->SetBranchAddress("isSR2", &isSR2, &b_isSR2);
   fChain->SetBranchAddress("isWCR", &isWCR, &b_isWCR);
   fChain->SetBranchAddress("isWVR", &isWVR, &b_isWVR);
   fChain->SetBranchAddress("isTVR", &isTVR, &b_isTVR);
   fChain->SetBranchAddress("isZVR", &isZVR, &b_isZVR);
   fChain->SetBranchAddress("isBosonVR", &isBosonVR, &b_isBosonVR);
   fChain->SetBranchAddress("isQCDCR_SR1", &isQCDCR_SR1, &b_isQCDCR_SR1);
   fChain->SetBranchAddress("isQCDCR_SR2", &isQCDCR_SR2, &b_isQCDCR_SR2);
   fChain->SetBranchAddress("isQCDCR_WCR", &isQCDCR_WCR, &b_isQCDCR_WCR);
   fChain->SetBranchAddress("isQCDCR_WVR", &isQCDCR_WVR, &b_isQCDCR_WVR);
   fChain->SetBranchAddress("GenWeight", &GenWeight, &b_GenWeight);
   fChain->SetBranchAddress("EleWeight", &EleWeight, &b_EleWeight);
   fChain->SetBranchAddress("EleWeightId", &EleWeightId, &b_EleWeightId);
   fChain->SetBranchAddress("EleWeightId_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down", &EleWeightId_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down, &b_EleWeightId_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down);
   fChain->SetBranchAddress("EleWeightId_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up", &EleWeightId_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up, &b_EleWeightId_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up);
   fChain->SetBranchAddress("EleWeightIso", &EleWeightIso, &b_EleWeightIso);
   fChain->SetBranchAddress("EleWeightIso_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down", &EleWeightIso_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down, &b_EleWeightIso_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down);
   fChain->SetBranchAddress("EleWeightIso_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up", &EleWeightIso_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up, &b_EleWeightIso_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up);
   fChain->SetBranchAddress("EleWeightReco", &EleWeightReco, &b_EleWeightReco);
   fChain->SetBranchAddress("EleWeightReco_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down", &EleWeightReco_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down, &b_EleWeightReco_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down);
   fChain->SetBranchAddress("EleWeightReco_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up", &EleWeightReco_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up, &b_EleWeightReco_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up);
   fChain->SetBranchAddress("EleWeight_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down", &EleWeight_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down, &b_EleWeight_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down);
   fChain->SetBranchAddress("EleWeight_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up", &EleWeight_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up, &b_EleWeight_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up);
   fChain->SetBranchAddress("EleWeight_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down", &EleWeight_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down, &b_EleWeight_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down);
   fChain->SetBranchAddress("EleWeight_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up", &EleWeight_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up, &b_EleWeight_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up);
   fChain->SetBranchAddress("EleWeight_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down", &EleWeight_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down, &b_EleWeight_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down);
   fChain->SetBranchAddress("EleWeight_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up", &EleWeight_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up, &b_EleWeight_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up);
   fChain->SetBranchAddress("MuoWeight", &MuoWeight, &b_MuoWeight);
   fChain->SetBranchAddress("MuoWeightIsol", &MuoWeightIsol, &b_MuoWeightIsol);
   fChain->SetBranchAddress("MuoWeightIsol_MUON_EFF_ISO_STAT__1down", &MuoWeightIsol_MUON_EFF_ISO_STAT__1down, &b_MuoWeightIsol_MUON_EFF_ISO_STAT__1down);
   fChain->SetBranchAddress("MuoWeightIsol_MUON_EFF_ISO_STAT__1up", &MuoWeightIsol_MUON_EFF_ISO_STAT__1up, &b_MuoWeightIsol_MUON_EFF_ISO_STAT__1up);
   fChain->SetBranchAddress("MuoWeightIsol_MUON_EFF_ISO_SYS__1down", &MuoWeightIsol_MUON_EFF_ISO_SYS__1down, &b_MuoWeightIsol_MUON_EFF_ISO_SYS__1down);
   fChain->SetBranchAddress("MuoWeightIsol_MUON_EFF_ISO_SYS__1up", &MuoWeightIsol_MUON_EFF_ISO_SYS__1up, &b_MuoWeightIsol_MUON_EFF_ISO_SYS__1up);
   fChain->SetBranchAddress("MuoWeightReco", &MuoWeightReco, &b_MuoWeightReco);
   fChain->SetBranchAddress("MuoWeightReco_MUON_EFF_RECO_STAT_LOWPT__1down", &MuoWeightReco_MUON_EFF_RECO_STAT_LOWPT__1down, &b_MuoWeightReco_MUON_EFF_RECO_STAT_LOWPT__1down);
   fChain->SetBranchAddress("MuoWeightReco_MUON_EFF_RECO_STAT_LOWPT__1up", &MuoWeightReco_MUON_EFF_RECO_STAT_LOWPT__1up, &b_MuoWeightReco_MUON_EFF_RECO_STAT_LOWPT__1up);
   fChain->SetBranchAddress("MuoWeightReco_MUON_EFF_RECO_STAT__1down", &MuoWeightReco_MUON_EFF_RECO_STAT__1down, &b_MuoWeightReco_MUON_EFF_RECO_STAT__1down);
   fChain->SetBranchAddress("MuoWeightReco_MUON_EFF_RECO_STAT__1up", &MuoWeightReco_MUON_EFF_RECO_STAT__1up, &b_MuoWeightReco_MUON_EFF_RECO_STAT__1up);
   fChain->SetBranchAddress("MuoWeightReco_MUON_EFF_RECO_SYS_LOWPT__1down", &MuoWeightReco_MUON_EFF_RECO_SYS_LOWPT__1down, &b_MuoWeightReco_MUON_EFF_RECO_SYS_LOWPT__1down);
   fChain->SetBranchAddress("MuoWeightReco_MUON_EFF_RECO_SYS_LOWPT__1up", &MuoWeightReco_MUON_EFF_RECO_SYS_LOWPT__1up, &b_MuoWeightReco_MUON_EFF_RECO_SYS_LOWPT__1up);
   fChain->SetBranchAddress("MuoWeightReco_MUON_EFF_RECO_SYS__1down", &MuoWeightReco_MUON_EFF_RECO_SYS__1down, &b_MuoWeightReco_MUON_EFF_RECO_SYS__1down);
   fChain->SetBranchAddress("MuoWeightReco_MUON_EFF_RECO_SYS__1up", &MuoWeightReco_MUON_EFF_RECO_SYS__1up, &b_MuoWeightReco_MUON_EFF_RECO_SYS__1up);
   fChain->SetBranchAddress("MuoWeightTTVA", &MuoWeightTTVA, &b_MuoWeightTTVA);
   fChain->SetBranchAddress("MuoWeightTTVA_MUON_EFF_TTVA_STAT__1down", &MuoWeightTTVA_MUON_EFF_TTVA_STAT__1down, &b_MuoWeightTTVA_MUON_EFF_TTVA_STAT__1down);
   fChain->SetBranchAddress("MuoWeightTTVA_MUON_EFF_TTVA_STAT__1up", &MuoWeightTTVA_MUON_EFF_TTVA_STAT__1up, &b_MuoWeightTTVA_MUON_EFF_TTVA_STAT__1up);
   fChain->SetBranchAddress("MuoWeightTTVA_MUON_EFF_TTVA_SYS__1down", &MuoWeightTTVA_MUON_EFF_TTVA_SYS__1down, &b_MuoWeightTTVA_MUON_EFF_TTVA_SYS__1down);
   fChain->SetBranchAddress("MuoWeightTTVA_MUON_EFF_TTVA_SYS__1up", &MuoWeightTTVA_MUON_EFF_TTVA_SYS__1up, &b_MuoWeightTTVA_MUON_EFF_TTVA_SYS__1up);
   fChain->SetBranchAddress("MuoWeightTrigHLT_mu20_iloose_L1MU15_OR_HLT_mu40", &MuoWeightTrigHLT_mu20_iloose_L1MU15_OR_HLT_mu40, &b_MuoWeightTrigHLT_mu20_iloose_L1MU15_OR_HLT_mu40);
   fChain->SetBranchAddress("MuoWeightTrigHLT_mu20_iloose_L1MU15_OR_HLT_mu40_MUON_EFF_TrigStatUncertainty__1down", &MuoWeightTrigHLT_mu20_iloose_L1MU15_OR_HLT_mu40_MUON_EFF_TrigStatUncertainty__1down, &b_MuoWeightTrigHLT_mu20_iloose_L1MU15_OR_HLT_mu40_MUON_EFF_TrigStatUncertainty__1down);
   fChain->SetBranchAddress("MuoWeightTrigHLT_mu20_iloose_L1MU15_OR_HLT_mu40_MUON_EFF_TrigStatUncertainty__1up", &MuoWeightTrigHLT_mu20_iloose_L1MU15_OR_HLT_mu40_MUON_EFF_TrigStatUncertainty__1up, &b_MuoWeightTrigHLT_mu20_iloose_L1MU15_OR_HLT_mu40_MUON_EFF_TrigStatUncertainty__1up);
   fChain->SetBranchAddress("MuoWeightTrigHLT_mu20_iloose_L1MU15_OR_HLT_mu40_MUON_EFF_TrigSystUncertainty__1down", &MuoWeightTrigHLT_mu20_iloose_L1MU15_OR_HLT_mu40_MUON_EFF_TrigSystUncertainty__1down, &b_MuoWeightTrigHLT_mu20_iloose_L1MU15_OR_HLT_mu40_MUON_EFF_TrigSystUncertainty__1down);
   fChain->SetBranchAddress("MuoWeightTrigHLT_mu20_iloose_L1MU15_OR_HLT_mu40_MUON_EFF_TrigSystUncertainty__1up", &MuoWeightTrigHLT_mu20_iloose_L1MU15_OR_HLT_mu40_MUON_EFF_TrigSystUncertainty__1up, &b_MuoWeightTrigHLT_mu20_iloose_L1MU15_OR_HLT_mu40_MUON_EFF_TrigSystUncertainty__1up);
   fChain->SetBranchAddress("MuoWeightTrigHLT_mu24_ivarmedium_OR_HLT_mu40", &MuoWeightTrigHLT_mu24_ivarmedium_OR_HLT_mu40, &b_MuoWeightTrigHLT_mu24_ivarmedium_OR_HLT_mu40);
   fChain->SetBranchAddress("MuoWeightTrigHLT_mu24_ivarmedium_OR_HLT_mu40_MUON_EFF_TrigStatUncertainty__1down", &MuoWeightTrigHLT_mu24_ivarmedium_OR_HLT_mu40_MUON_EFF_TrigStatUncertainty__1down, &b_MuoWeightTrigHLT_mu24_ivarmedium_OR_HLT_mu40_MUON_EFF_TrigStatUncertainty__1down);
   fChain->SetBranchAddress("MuoWeightTrigHLT_mu24_ivarmedium_OR_HLT_mu40_MUON_EFF_TrigStatUncertainty__1up", &MuoWeightTrigHLT_mu24_ivarmedium_OR_HLT_mu40_MUON_EFF_TrigStatUncertainty__1up, &b_MuoWeightTrigHLT_mu24_ivarmedium_OR_HLT_mu40_MUON_EFF_TrigStatUncertainty__1up);
   fChain->SetBranchAddress("MuoWeightTrigHLT_mu24_ivarmedium_OR_HLT_mu40_MUON_EFF_TrigSystUncertainty__1down", &MuoWeightTrigHLT_mu24_ivarmedium_OR_HLT_mu40_MUON_EFF_TrigSystUncertainty__1down, &b_MuoWeightTrigHLT_mu24_ivarmedium_OR_HLT_mu40_MUON_EFF_TrigSystUncertainty__1down);
   fChain->SetBranchAddress("MuoWeightTrigHLT_mu24_ivarmedium_OR_HLT_mu40_MUON_EFF_TrigSystUncertainty__1up", &MuoWeightTrigHLT_mu24_ivarmedium_OR_HLT_mu40_MUON_EFF_TrigSystUncertainty__1up, &b_MuoWeightTrigHLT_mu24_ivarmedium_OR_HLT_mu40_MUON_EFF_TrigSystUncertainty__1up);
   fChain->SetBranchAddress("MuoWeightTrigHLT_mu24_ivarmedium_OR_HLT_mu50", &MuoWeightTrigHLT_mu24_ivarmedium_OR_HLT_mu50, &b_MuoWeightTrigHLT_mu24_ivarmedium_OR_HLT_mu50);
   fChain->SetBranchAddress("MuoWeightTrigHLT_mu24_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1down", &MuoWeightTrigHLT_mu24_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1down, &b_MuoWeightTrigHLT_mu24_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1down);
   fChain->SetBranchAddress("MuoWeightTrigHLT_mu24_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1up", &MuoWeightTrigHLT_mu24_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1up, &b_MuoWeightTrigHLT_mu24_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1up);
   fChain->SetBranchAddress("MuoWeightTrigHLT_mu24_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1down", &MuoWeightTrigHLT_mu24_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1down, &b_MuoWeightTrigHLT_mu24_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1down);
   fChain->SetBranchAddress("MuoWeightTrigHLT_mu24_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1up", &MuoWeightTrigHLT_mu24_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1up, &b_MuoWeightTrigHLT_mu24_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1up);
   fChain->SetBranchAddress("MuoWeightTrigHLT_mu26_ivarmediumORHLT_mu50", &MuoWeightTrigHLT_mu26_ivarmediumORHLT_mu50, &b_MuoWeightTrigHLT_mu26_ivarmediumORHLT_mu50);
   fChain->SetBranchAddress("MuoWeightTrigHLT_mu26_ivarmediumORHLT_mu50_MUON_EFF_TrigStatUncertainty__1down", &MuoWeightTrigHLT_mu26_ivarmediumORHLT_mu50_MUON_EFF_TrigStatUncertainty__1down, &b_MuoWeightTrigHLT_mu26_ivarmediumORHLT_mu50_MUON_EFF_TrigStatUncertainty__1down);
   fChain->SetBranchAddress("MuoWeightTrigHLT_mu26_ivarmediumORHLT_mu50_MUON_EFF_TrigStatUncertainty__1up", &MuoWeightTrigHLT_mu26_ivarmediumORHLT_mu50_MUON_EFF_TrigStatUncertainty__1up, &b_MuoWeightTrigHLT_mu26_ivarmediumORHLT_mu50_MUON_EFF_TrigStatUncertainty__1up);
   fChain->SetBranchAddress("MuoWeightTrigHLT_mu26_ivarmediumORHLT_mu50_MUON_EFF_TrigSystUncertainty__1down", &MuoWeightTrigHLT_mu26_ivarmediumORHLT_mu50_MUON_EFF_TrigSystUncertainty__1down, &b_MuoWeightTrigHLT_mu26_ivarmediumORHLT_mu50_MUON_EFF_TrigSystUncertainty__1down);
   fChain->SetBranchAddress("MuoWeightTrigHLT_mu26_ivarmediumORHLT_mu50_MUON_EFF_TrigSystUncertainty__1up", &MuoWeightTrigHLT_mu26_ivarmediumORHLT_mu50_MUON_EFF_TrigSystUncertainty__1up, &b_MuoWeightTrigHLT_mu26_ivarmediumORHLT_mu50_MUON_EFF_TrigSystUncertainty__1up);
   fChain->SetBranchAddress("MuoWeightTrigHLT_mu26_ivarmedium_OR_HLT_mu50", &MuoWeightTrigHLT_mu26_ivarmedium_OR_HLT_mu50, &b_MuoWeightTrigHLT_mu26_ivarmedium_OR_HLT_mu50);
   fChain->SetBranchAddress("MuoWeightTrigHLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1down", &MuoWeightTrigHLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1down, &b_MuoWeightTrigHLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1down);
   fChain->SetBranchAddress("MuoWeightTrigHLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1up", &MuoWeightTrigHLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1up, &b_MuoWeightTrigHLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigStatUncertainty__1up);
   fChain->SetBranchAddress("MuoWeightTrigHLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1down", &MuoWeightTrigHLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1down, &b_MuoWeightTrigHLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1down);
   fChain->SetBranchAddress("MuoWeightTrigHLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1up", &MuoWeightTrigHLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1up, &b_MuoWeightTrigHLT_mu26_ivarmedium_OR_HLT_mu50_MUON_EFF_TrigSystUncertainty__1up);
   fChain->SetBranchAddress("MuoWeight_MUON_EFF_ISO_STAT__1down", &MuoWeight_MUON_EFF_ISO_STAT__1down, &b_MuoWeight_MUON_EFF_ISO_STAT__1down);
   fChain->SetBranchAddress("MuoWeight_MUON_EFF_ISO_STAT__1up", &MuoWeight_MUON_EFF_ISO_STAT__1up, &b_MuoWeight_MUON_EFF_ISO_STAT__1up);
   fChain->SetBranchAddress("MuoWeight_MUON_EFF_ISO_SYS__1down", &MuoWeight_MUON_EFF_ISO_SYS__1down, &b_MuoWeight_MUON_EFF_ISO_SYS__1down);
   fChain->SetBranchAddress("MuoWeight_MUON_EFF_ISO_SYS__1up", &MuoWeight_MUON_EFF_ISO_SYS__1up, &b_MuoWeight_MUON_EFF_ISO_SYS__1up);
   fChain->SetBranchAddress("MuoWeight_MUON_EFF_RECO_STAT_LOWPT__1down", &MuoWeight_MUON_EFF_RECO_STAT_LOWPT__1down, &b_MuoWeight_MUON_EFF_RECO_STAT_LOWPT__1down);
   fChain->SetBranchAddress("MuoWeight_MUON_EFF_RECO_STAT_LOWPT__1up", &MuoWeight_MUON_EFF_RECO_STAT_LOWPT__1up, &b_MuoWeight_MUON_EFF_RECO_STAT_LOWPT__1up);
   fChain->SetBranchAddress("MuoWeight_MUON_EFF_RECO_STAT__1down", &MuoWeight_MUON_EFF_RECO_STAT__1down, &b_MuoWeight_MUON_EFF_RECO_STAT__1down);
   fChain->SetBranchAddress("MuoWeight_MUON_EFF_RECO_STAT__1up", &MuoWeight_MUON_EFF_RECO_STAT__1up, &b_MuoWeight_MUON_EFF_RECO_STAT__1up);
   fChain->SetBranchAddress("MuoWeight_MUON_EFF_RECO_SYS_LOWPT__1down", &MuoWeight_MUON_EFF_RECO_SYS_LOWPT__1down, &b_MuoWeight_MUON_EFF_RECO_SYS_LOWPT__1down);
   fChain->SetBranchAddress("MuoWeight_MUON_EFF_RECO_SYS_LOWPT__1up", &MuoWeight_MUON_EFF_RECO_SYS_LOWPT__1up, &b_MuoWeight_MUON_EFF_RECO_SYS_LOWPT__1up);
   fChain->SetBranchAddress("MuoWeight_MUON_EFF_RECO_SYS__1down", &MuoWeight_MUON_EFF_RECO_SYS__1down, &b_MuoWeight_MUON_EFF_RECO_SYS__1down);
   fChain->SetBranchAddress("MuoWeight_MUON_EFF_RECO_SYS__1up", &MuoWeight_MUON_EFF_RECO_SYS__1up, &b_MuoWeight_MUON_EFF_RECO_SYS__1up);
   fChain->SetBranchAddress("MuoWeight_MUON_EFF_TTVA_STAT__1down", &MuoWeight_MUON_EFF_TTVA_STAT__1down, &b_MuoWeight_MUON_EFF_TTVA_STAT__1down);
   fChain->SetBranchAddress("MuoWeight_MUON_EFF_TTVA_STAT__1up", &MuoWeight_MUON_EFF_TTVA_STAT__1up, &b_MuoWeight_MUON_EFF_TTVA_STAT__1up);
   fChain->SetBranchAddress("MuoWeight_MUON_EFF_TTVA_SYS__1down", &MuoWeight_MUON_EFF_TTVA_SYS__1down, &b_MuoWeight_MUON_EFF_TTVA_SYS__1down);
   fChain->SetBranchAddress("MuoWeight_MUON_EFF_TTVA_SYS__1up", &MuoWeight_MUON_EFF_TTVA_SYS__1up, &b_MuoWeight_MUON_EFF_TTVA_SYS__1up);
   fChain->SetBranchAddress("TauWeight", &TauWeight, &b_TauWeight);
   fChain->SetBranchAddress("TauWeightEleOREle", &TauWeightEleOREle, &b_TauWeightEleOREle);
   fChain->SetBranchAddress("TauWeightEleOREle_TAUS_TRUEELECTRON_EFF_ELEOLR_STATHIGHMU__1down", &TauWeightEleOREle_TAUS_TRUEELECTRON_EFF_ELEOLR_STATHIGHMU__1down, &b_TauWeightEleOREle_TAUS_TRUEELECTRON_EFF_ELEOLR_STATHIGHMU__1down);
   fChain->SetBranchAddress("TauWeightEleOREle_TAUS_TRUEELECTRON_EFF_ELEOLR_STATHIGHMU__1up", &TauWeightEleOREle_TAUS_TRUEELECTRON_EFF_ELEOLR_STATHIGHMU__1up, &b_TauWeightEleOREle_TAUS_TRUEELECTRON_EFF_ELEOLR_STATHIGHMU__1up);
   fChain->SetBranchAddress("TauWeightEleOREle_TAUS_TRUEELECTRON_EFF_ELEOLR_STATLOWMU__1down", &TauWeightEleOREle_TAUS_TRUEELECTRON_EFF_ELEOLR_STATLOWMU__1down, &b_TauWeightEleOREle_TAUS_TRUEELECTRON_EFF_ELEOLR_STATLOWMU__1down);
   fChain->SetBranchAddress("TauWeightEleOREle_TAUS_TRUEELECTRON_EFF_ELEOLR_STATLOWMU__1up", &TauWeightEleOREle_TAUS_TRUEELECTRON_EFF_ELEOLR_STATLOWMU__1up, &b_TauWeightEleOREle_TAUS_TRUEELECTRON_EFF_ELEOLR_STATLOWMU__1up);
   fChain->SetBranchAddress("TauWeightEleOREle_TAUS_TRUEELECTRON_EFF_ELEOLR_SYST__1down", &TauWeightEleOREle_TAUS_TRUEELECTRON_EFF_ELEOLR_SYST__1down, &b_TauWeightEleOREle_TAUS_TRUEELECTRON_EFF_ELEOLR_SYST__1down);
   fChain->SetBranchAddress("TauWeightEleOREle_TAUS_TRUEELECTRON_EFF_ELEOLR_SYST__1up", &TauWeightEleOREle_TAUS_TRUEELECTRON_EFF_ELEOLR_SYST__1up, &b_TauWeightEleOREle_TAUS_TRUEELECTRON_EFF_ELEOLR_SYST__1up);
   fChain->SetBranchAddress("TauWeightEleORHad", &TauWeightEleORHad, &b_TauWeightEleORHad);
   fChain->SetBranchAddress("TauWeightEleORHad_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL__1down", &TauWeightEleORHad_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL__1down, &b_TauWeightEleORHad_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL__1down);
   fChain->SetBranchAddress("TauWeightEleORHad_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL__1up", &TauWeightEleORHad_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL__1up, &b_TauWeightEleORHad_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL__1up);
   fChain->SetBranchAddress("TauWeightId", &TauWeightId, &b_TauWeightId);
   fChain->SetBranchAddress("TauWeightId_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTUNCORR2025__1down", &TauWeightId_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTUNCORR2025__1down, &b_TauWeightId_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTUNCORR2025__1down);
   fChain->SetBranchAddress("TauWeightId_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTUNCORR2025__1up", &TauWeightId_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTUNCORR2025__1up, &b_TauWeightId_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTUNCORR2025__1up);
   fChain->SetBranchAddress("TauWeightId_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTUNCORR2530__1down", &TauWeightId_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTUNCORR2530__1down, &b_TauWeightId_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTUNCORR2530__1down);
   fChain->SetBranchAddress("TauWeightId_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTUNCORR2530__1up", &TauWeightId_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTUNCORR2530__1up, &b_TauWeightId_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTUNCORR2530__1up);
   fChain->SetBranchAddress("TauWeightId_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTUNCORR3040__1down", &TauWeightId_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTUNCORR3040__1down, &b_TauWeightId_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTUNCORR3040__1down);
   fChain->SetBranchAddress("TauWeightId_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTUNCORR3040__1up", &TauWeightId_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTUNCORR3040__1up, &b_TauWeightId_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTUNCORR3040__1up);
   fChain->SetBranchAddress("TauWeightId_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTUNCORRGE40__1down", &TauWeightId_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTUNCORRGE40__1down, &b_TauWeightId_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTUNCORRGE40__1down);
   fChain->SetBranchAddress("TauWeightId_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTUNCORRGE40__1up", &TauWeightId_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTUNCORRGE40__1up, &b_TauWeightId_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTUNCORRGE40__1up);
   fChain->SetBranchAddress("TauWeightId_TAUS_TRUEHADTAU_EFF_JETID_3PRONGSTATSYSTUNCORR2030__1down", &TauWeightId_TAUS_TRUEHADTAU_EFF_JETID_3PRONGSTATSYSTUNCORR2030__1down, &b_TauWeightId_TAUS_TRUEHADTAU_EFF_JETID_3PRONGSTATSYSTUNCORR2030__1down);
   fChain->SetBranchAddress("TauWeightId_TAUS_TRUEHADTAU_EFF_JETID_3PRONGSTATSYSTUNCORR2030__1up", &TauWeightId_TAUS_TRUEHADTAU_EFF_JETID_3PRONGSTATSYSTUNCORR2030__1up, &b_TauWeightId_TAUS_TRUEHADTAU_EFF_JETID_3PRONGSTATSYSTUNCORR2030__1up);
   fChain->SetBranchAddress("TauWeightId_TAUS_TRUEHADTAU_EFF_JETID_3PRONGSTATSYSTUNCORRGE30__1down", &TauWeightId_TAUS_TRUEHADTAU_EFF_JETID_3PRONGSTATSYSTUNCORRGE30__1down, &b_TauWeightId_TAUS_TRUEHADTAU_EFF_JETID_3PRONGSTATSYSTUNCORRGE30__1down);
   fChain->SetBranchAddress("TauWeightId_TAUS_TRUEHADTAU_EFF_JETID_3PRONGSTATSYSTUNCORRGE30__1up", &TauWeightId_TAUS_TRUEHADTAU_EFF_JETID_3PRONGSTATSYSTUNCORRGE30__1up, &b_TauWeightId_TAUS_TRUEHADTAU_EFF_JETID_3PRONGSTATSYSTUNCORRGE30__1up);
   fChain->SetBranchAddress("TauWeightId_TAUS_TRUEHADTAU_EFF_JETID_HIGHPT__1down", &TauWeightId_TAUS_TRUEHADTAU_EFF_JETID_HIGHPT__1down, &b_TauWeightId_TAUS_TRUEHADTAU_EFF_JETID_HIGHPT__1down);
   fChain->SetBranchAddress("TauWeightId_TAUS_TRUEHADTAU_EFF_JETID_HIGHPT__1up", &TauWeightId_TAUS_TRUEHADTAU_EFF_JETID_HIGHPT__1up, &b_TauWeightId_TAUS_TRUEHADTAU_EFF_JETID_HIGHPT__1up);
   fChain->SetBranchAddress("TauWeightId_TAUS_TRUEHADTAU_EFF_JETID_SYST__1down", &TauWeightId_TAUS_TRUEHADTAU_EFF_JETID_SYST__1down, &b_TauWeightId_TAUS_TRUEHADTAU_EFF_JETID_SYST__1down);
   fChain->SetBranchAddress("TauWeightId_TAUS_TRUEHADTAU_EFF_JETID_SYST__1up", &TauWeightId_TAUS_TRUEHADTAU_EFF_JETID_SYST__1up, &b_TauWeightId_TAUS_TRUEHADTAU_EFF_JETID_SYST__1up);
   fChain->SetBranchAddress("TauWeightReco", &TauWeightReco, &b_TauWeightReco);
   fChain->SetBranchAddress("TauWeightReco_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT__1down", &TauWeightReco_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT__1down, &b_TauWeightReco_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT__1down);
   fChain->SetBranchAddress("TauWeightReco_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT__1up", &TauWeightReco_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT__1up, &b_TauWeightReco_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT__1up);
   fChain->SetBranchAddress("TauWeightReco_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1down", &TauWeightReco_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1down, &b_TauWeightReco_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1down);
   fChain->SetBranchAddress("TauWeightReco_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1up", &TauWeightReco_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1up, &b_TauWeightReco_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1up);
   fChain->SetBranchAddress("TauWeightTrigHLT_tau125_medium1_tracktwo", &TauWeightTrigHLT_tau125_medium1_tracktwo, &b_TauWeightTrigHLT_tau125_medium1_tracktwo);
   fChain->SetBranchAddress("TauWeightTrigHLT_tau125_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2015__1down", &TauWeightTrigHLT_tau125_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2015__1down, &b_TauWeightTrigHLT_tau125_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2015__1down);
   fChain->SetBranchAddress("TauWeightTrigHLT_tau125_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2015__1up", &TauWeightTrigHLT_tau125_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2015__1up, &b_TauWeightTrigHLT_tau125_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2015__1up);
   fChain->SetBranchAddress("TauWeightTrigHLT_tau125_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2016__1down", &TauWeightTrigHLT_tau125_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2016__1down, &b_TauWeightTrigHLT_tau125_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2016__1down);
   fChain->SetBranchAddress("TauWeightTrigHLT_tau125_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2016__1up", &TauWeightTrigHLT_tau125_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2016__1up, &b_TauWeightTrigHLT_tau125_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2016__1up);
   fChain->SetBranchAddress("TauWeightTrigHLT_tau125_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2017__1down", &TauWeightTrigHLT_tau125_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2017__1down, &b_TauWeightTrigHLT_tau125_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2017__1down);
   fChain->SetBranchAddress("TauWeightTrigHLT_tau125_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2017__1up", &TauWeightTrigHLT_tau125_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2017__1up, &b_TauWeightTrigHLT_tau125_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2017__1up);
   fChain->SetBranchAddress("TauWeightTrigHLT_tau125_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2015__1down", &TauWeightTrigHLT_tau125_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2015__1down, &b_TauWeightTrigHLT_tau125_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2015__1down);
   fChain->SetBranchAddress("TauWeightTrigHLT_tau125_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2015__1up", &TauWeightTrigHLT_tau125_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2015__1up, &b_TauWeightTrigHLT_tau125_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2015__1up);
   fChain->SetBranchAddress("TauWeightTrigHLT_tau125_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2016__1down", &TauWeightTrigHLT_tau125_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2016__1down, &b_TauWeightTrigHLT_tau125_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2016__1down);
   fChain->SetBranchAddress("TauWeightTrigHLT_tau125_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2016__1up", &TauWeightTrigHLT_tau125_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2016__1up, &b_TauWeightTrigHLT_tau125_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2016__1up);
   fChain->SetBranchAddress("TauWeightTrigHLT_tau125_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2017__1down", &TauWeightTrigHLT_tau125_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2017__1down, &b_TauWeightTrigHLT_tau125_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2017__1down);
   fChain->SetBranchAddress("TauWeightTrigHLT_tau125_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2017__1up", &TauWeightTrigHLT_tau125_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2017__1up, &b_TauWeightTrigHLT_tau125_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2017__1up);
   fChain->SetBranchAddress("TauWeightTrigHLT_tau125_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2015__1down", &TauWeightTrigHLT_tau125_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2015__1down, &b_TauWeightTrigHLT_tau125_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2015__1down);
   fChain->SetBranchAddress("TauWeightTrigHLT_tau125_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2015__1up", &TauWeightTrigHLT_tau125_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2015__1up, &b_TauWeightTrigHLT_tau125_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2015__1up);
   fChain->SetBranchAddress("TauWeightTrigHLT_tau125_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2016__1down", &TauWeightTrigHLT_tau125_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2016__1down, &b_TauWeightTrigHLT_tau125_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2016__1down);
   fChain->SetBranchAddress("TauWeightTrigHLT_tau125_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2016__1up", &TauWeightTrigHLT_tau125_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2016__1up, &b_TauWeightTrigHLT_tau125_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2016__1up);
   fChain->SetBranchAddress("TauWeightTrigHLT_tau125_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2017__1down", &TauWeightTrigHLT_tau125_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2017__1down, &b_TauWeightTrigHLT_tau125_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2017__1down);
   fChain->SetBranchAddress("TauWeightTrigHLT_tau125_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2017__1up", &TauWeightTrigHLT_tau125_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2017__1up, &b_TauWeightTrigHLT_tau125_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2017__1up);
   fChain->SetBranchAddress("TauWeightTrigHLT_tau160_medium1_tracktwo", &TauWeightTrigHLT_tau160_medium1_tracktwo, &b_TauWeightTrigHLT_tau160_medium1_tracktwo);
   fChain->SetBranchAddress("TauWeightTrigHLT_tau160_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2016__1down", &TauWeightTrigHLT_tau160_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2016__1down, &b_TauWeightTrigHLT_tau160_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2016__1down);
   fChain->SetBranchAddress("TauWeightTrigHLT_tau160_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2016__1up", &TauWeightTrigHLT_tau160_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2016__1up, &b_TauWeightTrigHLT_tau160_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2016__1up);
   fChain->SetBranchAddress("TauWeightTrigHLT_tau160_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2017__1down", &TauWeightTrigHLT_tau160_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2017__1down, &b_TauWeightTrigHLT_tau160_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2017__1down);
   fChain->SetBranchAddress("TauWeightTrigHLT_tau160_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2017__1up", &TauWeightTrigHLT_tau160_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2017__1up, &b_TauWeightTrigHLT_tau160_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2017__1up);
   fChain->SetBranchAddress("TauWeightTrigHLT_tau160_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2016__1down", &TauWeightTrigHLT_tau160_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2016__1down, &b_TauWeightTrigHLT_tau160_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2016__1down);
   fChain->SetBranchAddress("TauWeightTrigHLT_tau160_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2016__1up", &TauWeightTrigHLT_tau160_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2016__1up, &b_TauWeightTrigHLT_tau160_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2016__1up);
   fChain->SetBranchAddress("TauWeightTrigHLT_tau160_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2017__1down", &TauWeightTrigHLT_tau160_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2017__1down, &b_TauWeightTrigHLT_tau160_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2017__1down);
   fChain->SetBranchAddress("TauWeightTrigHLT_tau160_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2017__1up", &TauWeightTrigHLT_tau160_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2017__1up, &b_TauWeightTrigHLT_tau160_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2017__1up);
   fChain->SetBranchAddress("TauWeightTrigHLT_tau160_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2016__1down", &TauWeightTrigHLT_tau160_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2016__1down, &b_TauWeightTrigHLT_tau160_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2016__1down);
   fChain->SetBranchAddress("TauWeightTrigHLT_tau160_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2016__1up", &TauWeightTrigHLT_tau160_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2016__1up, &b_TauWeightTrigHLT_tau160_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2016__1up);
   fChain->SetBranchAddress("TauWeightTrigHLT_tau160_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2017__1down", &TauWeightTrigHLT_tau160_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2017__1down, &b_TauWeightTrigHLT_tau160_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2017__1down);
   fChain->SetBranchAddress("TauWeightTrigHLT_tau160_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2017__1up", &TauWeightTrigHLT_tau160_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2017__1up, &b_TauWeightTrigHLT_tau160_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2017__1up);
   fChain->SetBranchAddress("TauWeightTrigHLT_tau25_medium1_tracktwo", &TauWeightTrigHLT_tau25_medium1_tracktwo, &b_TauWeightTrigHLT_tau25_medium1_tracktwo);
   fChain->SetBranchAddress("TauWeightTrigHLT_tau25_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2016__1down", &TauWeightTrigHLT_tau25_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2016__1down, &b_TauWeightTrigHLT_tau25_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2016__1down);
   fChain->SetBranchAddress("TauWeightTrigHLT_tau25_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2016__1up", &TauWeightTrigHLT_tau25_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2016__1up, &b_TauWeightTrigHLT_tau25_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2016__1up);
   fChain->SetBranchAddress("TauWeightTrigHLT_tau25_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2017__1down", &TauWeightTrigHLT_tau25_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2017__1down, &b_TauWeightTrigHLT_tau25_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2017__1down);
   fChain->SetBranchAddress("TauWeightTrigHLT_tau25_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2017__1up", &TauWeightTrigHLT_tau25_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2017__1up, &b_TauWeightTrigHLT_tau25_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2017__1up);
   fChain->SetBranchAddress("TauWeightTrigHLT_tau25_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2016__1down", &TauWeightTrigHLT_tau25_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2016__1down, &b_TauWeightTrigHLT_tau25_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2016__1down);
   fChain->SetBranchAddress("TauWeightTrigHLT_tau25_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2016__1up", &TauWeightTrigHLT_tau25_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2016__1up, &b_TauWeightTrigHLT_tau25_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2016__1up);
   fChain->SetBranchAddress("TauWeightTrigHLT_tau25_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2017__1down", &TauWeightTrigHLT_tau25_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2017__1down, &b_TauWeightTrigHLT_tau25_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2017__1down);
   fChain->SetBranchAddress("TauWeightTrigHLT_tau25_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2017__1up", &TauWeightTrigHLT_tau25_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2017__1up, &b_TauWeightTrigHLT_tau25_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2017__1up);
   fChain->SetBranchAddress("TauWeightTrigHLT_tau25_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2016__1down", &TauWeightTrigHLT_tau25_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2016__1down, &b_TauWeightTrigHLT_tau25_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2016__1down);
   fChain->SetBranchAddress("TauWeightTrigHLT_tau25_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2016__1up", &TauWeightTrigHLT_tau25_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2016__1up, &b_TauWeightTrigHLT_tau25_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2016__1up);
   fChain->SetBranchAddress("TauWeightTrigHLT_tau25_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2017__1down", &TauWeightTrigHLT_tau25_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2017__1down, &b_TauWeightTrigHLT_tau25_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2017__1down);
   fChain->SetBranchAddress("TauWeightTrigHLT_tau25_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2017__1up", &TauWeightTrigHLT_tau25_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2017__1up, &b_TauWeightTrigHLT_tau25_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2017__1up);
   fChain->SetBranchAddress("TauWeightTrigHLT_tau35_medium1_tracktwo", &TauWeightTrigHLT_tau35_medium1_tracktwo, &b_TauWeightTrigHLT_tau35_medium1_tracktwo);
   fChain->SetBranchAddress("TauWeightTrigHLT_tau35_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2016__1down", &TauWeightTrigHLT_tau35_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2016__1down, &b_TauWeightTrigHLT_tau35_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2016__1down);
   fChain->SetBranchAddress("TauWeightTrigHLT_tau35_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2016__1up", &TauWeightTrigHLT_tau35_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2016__1up, &b_TauWeightTrigHLT_tau35_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2016__1up);
   fChain->SetBranchAddress("TauWeightTrigHLT_tau35_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2017__1down", &TauWeightTrigHLT_tau35_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2017__1down, &b_TauWeightTrigHLT_tau35_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2017__1down);
   fChain->SetBranchAddress("TauWeightTrigHLT_tau35_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2017__1up", &TauWeightTrigHLT_tau35_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2017__1up, &b_TauWeightTrigHLT_tau35_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2017__1up);
   fChain->SetBranchAddress("TauWeightTrigHLT_tau35_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2016__1down", &TauWeightTrigHLT_tau35_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2016__1down, &b_TauWeightTrigHLT_tau35_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2016__1down);
   fChain->SetBranchAddress("TauWeightTrigHLT_tau35_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2016__1up", &TauWeightTrigHLT_tau35_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2016__1up, &b_TauWeightTrigHLT_tau35_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2016__1up);
   fChain->SetBranchAddress("TauWeightTrigHLT_tau35_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2017__1down", &TauWeightTrigHLT_tau35_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2017__1down, &b_TauWeightTrigHLT_tau35_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2017__1down);
   fChain->SetBranchAddress("TauWeightTrigHLT_tau35_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2017__1up", &TauWeightTrigHLT_tau35_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2017__1up, &b_TauWeightTrigHLT_tau35_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2017__1up);
   fChain->SetBranchAddress("TauWeightTrigHLT_tau35_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2016__1down", &TauWeightTrigHLT_tau35_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2016__1down, &b_TauWeightTrigHLT_tau35_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2016__1down);
   fChain->SetBranchAddress("TauWeightTrigHLT_tau35_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2016__1up", &TauWeightTrigHLT_tau35_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2016__1up, &b_TauWeightTrigHLT_tau35_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2016__1up);
   fChain->SetBranchAddress("TauWeightTrigHLT_tau35_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2017__1down", &TauWeightTrigHLT_tau35_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2017__1down, &b_TauWeightTrigHLT_tau35_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2017__1down);
   fChain->SetBranchAddress("TauWeightTrigHLT_tau35_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2017__1up", &TauWeightTrigHLT_tau35_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2017__1up, &b_TauWeightTrigHLT_tau35_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2017__1up);
   fChain->SetBranchAddress("TauWeightTrigHLT_tau50_medium1_tracktwo_L1TAU12", &TauWeightTrigHLT_tau50_medium1_tracktwo_L1TAU12, &b_TauWeightTrigHLT_tau50_medium1_tracktwo_L1TAU12);
   fChain->SetBranchAddress("TauWeightTrigHLT_tau50_medium1_tracktwo_L1TAU12_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2016__1down", &TauWeightTrigHLT_tau50_medium1_tracktwo_L1TAU12_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2016__1down, &b_TauWeightTrigHLT_tau50_medium1_tracktwo_L1TAU12_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2016__1down);
   fChain->SetBranchAddress("TauWeightTrigHLT_tau50_medium1_tracktwo_L1TAU12_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2016__1up", &TauWeightTrigHLT_tau50_medium1_tracktwo_L1TAU12_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2016__1up, &b_TauWeightTrigHLT_tau50_medium1_tracktwo_L1TAU12_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2016__1up);
   fChain->SetBranchAddress("TauWeightTrigHLT_tau50_medium1_tracktwo_L1TAU12_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2017__1down", &TauWeightTrigHLT_tau50_medium1_tracktwo_L1TAU12_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2017__1down, &b_TauWeightTrigHLT_tau50_medium1_tracktwo_L1TAU12_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2017__1down);
   fChain->SetBranchAddress("TauWeightTrigHLT_tau50_medium1_tracktwo_L1TAU12_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2017__1up", &TauWeightTrigHLT_tau50_medium1_tracktwo_L1TAU12_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2017__1up, &b_TauWeightTrigHLT_tau50_medium1_tracktwo_L1TAU12_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2017__1up);
   fChain->SetBranchAddress("TauWeightTrigHLT_tau50_medium1_tracktwo_L1TAU12_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2016__1down", &TauWeightTrigHLT_tau50_medium1_tracktwo_L1TAU12_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2016__1down, &b_TauWeightTrigHLT_tau50_medium1_tracktwo_L1TAU12_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2016__1down);
   fChain->SetBranchAddress("TauWeightTrigHLT_tau50_medium1_tracktwo_L1TAU12_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2016__1up", &TauWeightTrigHLT_tau50_medium1_tracktwo_L1TAU12_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2016__1up, &b_TauWeightTrigHLT_tau50_medium1_tracktwo_L1TAU12_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2016__1up);
   fChain->SetBranchAddress("TauWeightTrigHLT_tau50_medium1_tracktwo_L1TAU12_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2017__1down", &TauWeightTrigHLT_tau50_medium1_tracktwo_L1TAU12_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2017__1down, &b_TauWeightTrigHLT_tau50_medium1_tracktwo_L1TAU12_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2017__1down);
   fChain->SetBranchAddress("TauWeightTrigHLT_tau50_medium1_tracktwo_L1TAU12_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2017__1up", &TauWeightTrigHLT_tau50_medium1_tracktwo_L1TAU12_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2017__1up, &b_TauWeightTrigHLT_tau50_medium1_tracktwo_L1TAU12_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2017__1up);
   fChain->SetBranchAddress("TauWeightTrigHLT_tau50_medium1_tracktwo_L1TAU12_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2016__1down", &TauWeightTrigHLT_tau50_medium1_tracktwo_L1TAU12_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2016__1down, &b_TauWeightTrigHLT_tau50_medium1_tracktwo_L1TAU12_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2016__1down);
   fChain->SetBranchAddress("TauWeightTrigHLT_tau50_medium1_tracktwo_L1TAU12_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2016__1up", &TauWeightTrigHLT_tau50_medium1_tracktwo_L1TAU12_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2016__1up, &b_TauWeightTrigHLT_tau50_medium1_tracktwo_L1TAU12_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2016__1up);
   fChain->SetBranchAddress("TauWeightTrigHLT_tau50_medium1_tracktwo_L1TAU12_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2017__1down", &TauWeightTrigHLT_tau50_medium1_tracktwo_L1TAU12_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2017__1down, &b_TauWeightTrigHLT_tau50_medium1_tracktwo_L1TAU12_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2017__1down);
   fChain->SetBranchAddress("TauWeightTrigHLT_tau50_medium1_tracktwo_L1TAU12_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2017__1up", &TauWeightTrigHLT_tau50_medium1_tracktwo_L1TAU12_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2017__1up, &b_TauWeightTrigHLT_tau50_medium1_tracktwo_L1TAU12_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2017__1up);
   fChain->SetBranchAddress("TauWeightTrigHLT_tau80_medium1_tracktwo", &TauWeightTrigHLT_tau80_medium1_tracktwo, &b_TauWeightTrigHLT_tau80_medium1_tracktwo);
   fChain->SetBranchAddress("TauWeightTrigHLT_tau80_medium1_tracktwo_L1TAU60", &TauWeightTrigHLT_tau80_medium1_tracktwo_L1TAU60, &b_TauWeightTrigHLT_tau80_medium1_tracktwo_L1TAU60);
   fChain->SetBranchAddress("TauWeightTrigHLT_tau80_medium1_tracktwo_L1TAU60_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2016__1down", &TauWeightTrigHLT_tau80_medium1_tracktwo_L1TAU60_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2016__1down, &b_TauWeightTrigHLT_tau80_medium1_tracktwo_L1TAU60_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2016__1down);
   fChain->SetBranchAddress("TauWeightTrigHLT_tau80_medium1_tracktwo_L1TAU60_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2016__1up", &TauWeightTrigHLT_tau80_medium1_tracktwo_L1TAU60_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2016__1up, &b_TauWeightTrigHLT_tau80_medium1_tracktwo_L1TAU60_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2016__1up);
   fChain->SetBranchAddress("TauWeightTrigHLT_tau80_medium1_tracktwo_L1TAU60_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2017__1down", &TauWeightTrigHLT_tau80_medium1_tracktwo_L1TAU60_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2017__1down, &b_TauWeightTrigHLT_tau80_medium1_tracktwo_L1TAU60_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2017__1down);
   fChain->SetBranchAddress("TauWeightTrigHLT_tau80_medium1_tracktwo_L1TAU60_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2017__1up", &TauWeightTrigHLT_tau80_medium1_tracktwo_L1TAU60_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2017__1up, &b_TauWeightTrigHLT_tau80_medium1_tracktwo_L1TAU60_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2017__1up);
   fChain->SetBranchAddress("TauWeightTrigHLT_tau80_medium1_tracktwo_L1TAU60_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2016__1down", &TauWeightTrigHLT_tau80_medium1_tracktwo_L1TAU60_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2016__1down, &b_TauWeightTrigHLT_tau80_medium1_tracktwo_L1TAU60_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2016__1down);
   fChain->SetBranchAddress("TauWeightTrigHLT_tau80_medium1_tracktwo_L1TAU60_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2016__1up", &TauWeightTrigHLT_tau80_medium1_tracktwo_L1TAU60_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2016__1up, &b_TauWeightTrigHLT_tau80_medium1_tracktwo_L1TAU60_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2016__1up);
   fChain->SetBranchAddress("TauWeightTrigHLT_tau80_medium1_tracktwo_L1TAU60_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2017__1down", &TauWeightTrigHLT_tau80_medium1_tracktwo_L1TAU60_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2017__1down, &b_TauWeightTrigHLT_tau80_medium1_tracktwo_L1TAU60_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2017__1down);
   fChain->SetBranchAddress("TauWeightTrigHLT_tau80_medium1_tracktwo_L1TAU60_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2017__1up", &TauWeightTrigHLT_tau80_medium1_tracktwo_L1TAU60_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2017__1up, &b_TauWeightTrigHLT_tau80_medium1_tracktwo_L1TAU60_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2017__1up);
   fChain->SetBranchAddress("TauWeightTrigHLT_tau80_medium1_tracktwo_L1TAU60_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2016__1down", &TauWeightTrigHLT_tau80_medium1_tracktwo_L1TAU60_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2016__1down, &b_TauWeightTrigHLT_tau80_medium1_tracktwo_L1TAU60_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2016__1down);
   fChain->SetBranchAddress("TauWeightTrigHLT_tau80_medium1_tracktwo_L1TAU60_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2016__1up", &TauWeightTrigHLT_tau80_medium1_tracktwo_L1TAU60_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2016__1up, &b_TauWeightTrigHLT_tau80_medium1_tracktwo_L1TAU60_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2016__1up);
   fChain->SetBranchAddress("TauWeightTrigHLT_tau80_medium1_tracktwo_L1TAU60_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2017__1down", &TauWeightTrigHLT_tau80_medium1_tracktwo_L1TAU60_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2017__1down, &b_TauWeightTrigHLT_tau80_medium1_tracktwo_L1TAU60_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2017__1down);
   fChain->SetBranchAddress("TauWeightTrigHLT_tau80_medium1_tracktwo_L1TAU60_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2017__1up", &TauWeightTrigHLT_tau80_medium1_tracktwo_L1TAU60_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2017__1up, &b_TauWeightTrigHLT_tau80_medium1_tracktwo_L1TAU60_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2017__1up);
   fChain->SetBranchAddress("TauWeightTrigHLT_tau80_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2016__1down", &TauWeightTrigHLT_tau80_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2016__1down, &b_TauWeightTrigHLT_tau80_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2016__1down);
   fChain->SetBranchAddress("TauWeightTrigHLT_tau80_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2016__1up", &TauWeightTrigHLT_tau80_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2016__1up, &b_TauWeightTrigHLT_tau80_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2016__1up);
   fChain->SetBranchAddress("TauWeightTrigHLT_tau80_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2017__1down", &TauWeightTrigHLT_tau80_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2017__1down, &b_TauWeightTrigHLT_tau80_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2017__1down);
   fChain->SetBranchAddress("TauWeightTrigHLT_tau80_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2017__1up", &TauWeightTrigHLT_tau80_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2017__1up, &b_TauWeightTrigHLT_tau80_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATDATA2017__1up);
   fChain->SetBranchAddress("TauWeightTrigHLT_tau80_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2016__1down", &TauWeightTrigHLT_tau80_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2016__1down, &b_TauWeightTrigHLT_tau80_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2016__1down);
   fChain->SetBranchAddress("TauWeightTrigHLT_tau80_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2016__1up", &TauWeightTrigHLT_tau80_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2016__1up, &b_TauWeightTrigHLT_tau80_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2016__1up);
   fChain->SetBranchAddress("TauWeightTrigHLT_tau80_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2017__1down", &TauWeightTrigHLT_tau80_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2017__1down, &b_TauWeightTrigHLT_tau80_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2017__1down);
   fChain->SetBranchAddress("TauWeightTrigHLT_tau80_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2017__1up", &TauWeightTrigHLT_tau80_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2017__1up, &b_TauWeightTrigHLT_tau80_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_STATMC2017__1up);
   fChain->SetBranchAddress("TauWeightTrigHLT_tau80_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2016__1down", &TauWeightTrigHLT_tau80_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2016__1down, &b_TauWeightTrigHLT_tau80_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2016__1down);
   fChain->SetBranchAddress("TauWeightTrigHLT_tau80_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2016__1up", &TauWeightTrigHLT_tau80_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2016__1up, &b_TauWeightTrigHLT_tau80_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2016__1up);
   fChain->SetBranchAddress("TauWeightTrigHLT_tau80_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2017__1down", &TauWeightTrigHLT_tau80_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2017__1down, &b_TauWeightTrigHLT_tau80_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2017__1down);
   fChain->SetBranchAddress("TauWeightTrigHLT_tau80_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2017__1up", &TauWeightTrigHLT_tau80_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2017__1up, &b_TauWeightTrigHLT_tau80_medium1_tracktwo_TAUS_TRUEHADTAU_EFF_TRIGGER_SYST2017__1up);
   fChain->SetBranchAddress("TauWeight_TAUS_TRUEELECTRON_EFF_ELEOLR_STATHIGHMU__1down", &TauWeight_TAUS_TRUEELECTRON_EFF_ELEOLR_STATHIGHMU__1down, &b_TauWeight_TAUS_TRUEELECTRON_EFF_ELEOLR_STATHIGHMU__1down);
   fChain->SetBranchAddress("TauWeight_TAUS_TRUEELECTRON_EFF_ELEOLR_STATHIGHMU__1up", &TauWeight_TAUS_TRUEELECTRON_EFF_ELEOLR_STATHIGHMU__1up, &b_TauWeight_TAUS_TRUEELECTRON_EFF_ELEOLR_STATHIGHMU__1up);
   fChain->SetBranchAddress("TauWeight_TAUS_TRUEELECTRON_EFF_ELEOLR_STATLOWMU__1down", &TauWeight_TAUS_TRUEELECTRON_EFF_ELEOLR_STATLOWMU__1down, &b_TauWeight_TAUS_TRUEELECTRON_EFF_ELEOLR_STATLOWMU__1down);
   fChain->SetBranchAddress("TauWeight_TAUS_TRUEELECTRON_EFF_ELEOLR_STATLOWMU__1up", &TauWeight_TAUS_TRUEELECTRON_EFF_ELEOLR_STATLOWMU__1up, &b_TauWeight_TAUS_TRUEELECTRON_EFF_ELEOLR_STATLOWMU__1up);
   fChain->SetBranchAddress("TauWeight_TAUS_TRUEELECTRON_EFF_ELEOLR_SYST__1down", &TauWeight_TAUS_TRUEELECTRON_EFF_ELEOLR_SYST__1down, &b_TauWeight_TAUS_TRUEELECTRON_EFF_ELEOLR_SYST__1down);
   fChain->SetBranchAddress("TauWeight_TAUS_TRUEELECTRON_EFF_ELEOLR_SYST__1up", &TauWeight_TAUS_TRUEELECTRON_EFF_ELEOLR_SYST__1up, &b_TauWeight_TAUS_TRUEELECTRON_EFF_ELEOLR_SYST__1up);
   fChain->SetBranchAddress("TauWeight_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL__1down", &TauWeight_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL__1down, &b_TauWeight_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL__1down);
   fChain->SetBranchAddress("TauWeight_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL__1up", &TauWeight_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL__1up, &b_TauWeight_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL__1up);
   fChain->SetBranchAddress("TauWeight_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTUNCORR2025__1down", &TauWeight_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTUNCORR2025__1down, &b_TauWeight_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTUNCORR2025__1down);
   fChain->SetBranchAddress("TauWeight_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTUNCORR2025__1up", &TauWeight_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTUNCORR2025__1up, &b_TauWeight_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTUNCORR2025__1up);
   fChain->SetBranchAddress("TauWeight_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTUNCORR2530__1down", &TauWeight_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTUNCORR2530__1down, &b_TauWeight_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTUNCORR2530__1down);
   fChain->SetBranchAddress("TauWeight_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTUNCORR2530__1up", &TauWeight_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTUNCORR2530__1up, &b_TauWeight_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTUNCORR2530__1up);
   fChain->SetBranchAddress("TauWeight_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTUNCORR3040__1down", &TauWeight_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTUNCORR3040__1down, &b_TauWeight_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTUNCORR3040__1down);
   fChain->SetBranchAddress("TauWeight_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTUNCORR3040__1up", &TauWeight_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTUNCORR3040__1up, &b_TauWeight_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTUNCORR3040__1up);
   fChain->SetBranchAddress("TauWeight_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTUNCORRGE40__1down", &TauWeight_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTUNCORRGE40__1down, &b_TauWeight_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTUNCORRGE40__1down);
   fChain->SetBranchAddress("TauWeight_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTUNCORRGE40__1up", &TauWeight_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTUNCORRGE40__1up, &b_TauWeight_TAUS_TRUEHADTAU_EFF_JETID_1PRONGSTATSYSTUNCORRGE40__1up);
   fChain->SetBranchAddress("TauWeight_TAUS_TRUEHADTAU_EFF_JETID_3PRONGSTATSYSTUNCORR2030__1down", &TauWeight_TAUS_TRUEHADTAU_EFF_JETID_3PRONGSTATSYSTUNCORR2030__1down, &b_TauWeight_TAUS_TRUEHADTAU_EFF_JETID_3PRONGSTATSYSTUNCORR2030__1down);
   fChain->SetBranchAddress("TauWeight_TAUS_TRUEHADTAU_EFF_JETID_3PRONGSTATSYSTUNCORR2030__1up", &TauWeight_TAUS_TRUEHADTAU_EFF_JETID_3PRONGSTATSYSTUNCORR2030__1up, &b_TauWeight_TAUS_TRUEHADTAU_EFF_JETID_3PRONGSTATSYSTUNCORR2030__1up);
   fChain->SetBranchAddress("TauWeight_TAUS_TRUEHADTAU_EFF_JETID_3PRONGSTATSYSTUNCORRGE30__1down", &TauWeight_TAUS_TRUEHADTAU_EFF_JETID_3PRONGSTATSYSTUNCORRGE30__1down, &b_TauWeight_TAUS_TRUEHADTAU_EFF_JETID_3PRONGSTATSYSTUNCORRGE30__1down);
   fChain->SetBranchAddress("TauWeight_TAUS_TRUEHADTAU_EFF_JETID_3PRONGSTATSYSTUNCORRGE30__1up", &TauWeight_TAUS_TRUEHADTAU_EFF_JETID_3PRONGSTATSYSTUNCORRGE30__1up, &b_TauWeight_TAUS_TRUEHADTAU_EFF_JETID_3PRONGSTATSYSTUNCORRGE30__1up);
   fChain->SetBranchAddress("TauWeight_TAUS_TRUEHADTAU_EFF_JETID_HIGHPT__1down", &TauWeight_TAUS_TRUEHADTAU_EFF_JETID_HIGHPT__1down, &b_TauWeight_TAUS_TRUEHADTAU_EFF_JETID_HIGHPT__1down);
   fChain->SetBranchAddress("TauWeight_TAUS_TRUEHADTAU_EFF_JETID_HIGHPT__1up", &TauWeight_TAUS_TRUEHADTAU_EFF_JETID_HIGHPT__1up, &b_TauWeight_TAUS_TRUEHADTAU_EFF_JETID_HIGHPT__1up);
   fChain->SetBranchAddress("TauWeight_TAUS_TRUEHADTAU_EFF_JETID_SYST__1down", &TauWeight_TAUS_TRUEHADTAU_EFF_JETID_SYST__1down, &b_TauWeight_TAUS_TRUEHADTAU_EFF_JETID_SYST__1down);
   fChain->SetBranchAddress("TauWeight_TAUS_TRUEHADTAU_EFF_JETID_SYST__1up", &TauWeight_TAUS_TRUEHADTAU_EFF_JETID_SYST__1up, &b_TauWeight_TAUS_TRUEHADTAU_EFF_JETID_SYST__1up);
   fChain->SetBranchAddress("TauWeight_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT__1down", &TauWeight_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT__1down, &b_TauWeight_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT__1down);
   fChain->SetBranchAddress("TauWeight_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT__1up", &TauWeight_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT__1up, &b_TauWeight_TAUS_TRUEHADTAU_EFF_RECO_HIGHPT__1up);
   fChain->SetBranchAddress("TauWeight_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1down", &TauWeight_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1down, &b_TauWeight_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1down);
   fChain->SetBranchAddress("TauWeight_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1up", &TauWeight_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1up, &b_TauWeight_TAUS_TRUEHADTAU_EFF_RECO_TOTAL__1up);
   fChain->SetBranchAddress("muWeight", &muWeight, &b_muWeight);
   fChain->SetBranchAddress("muWeight_PRW_DATASF__1down", &muWeight_PRW_DATASF__1down, &b_muWeight_PRW_DATASF__1down);
   fChain->SetBranchAddress("muWeight_PRW_DATASF__1up", &muWeight_PRW_DATASF__1up, &b_muWeight_PRW_DATASF__1up);
   fChain->SetBranchAddress("Aplanarity", &Aplanarity, &b_Aplanarity);
   fChain->SetBranchAddress("Circularity", &Circularity, &b_Circularity);
   fChain->SetBranchAddress("CosChi1", &CosChi1, &b_CosChi1);
   fChain->SetBranchAddress("CosChi2", &CosChi2, &b_CosChi2);
   fChain->SetBranchAddress("Ht_Jet", &Ht_Jet, &b_Ht_Jet);
   fChain->SetBranchAddress("Ht_Lep", &Ht_Lep, &b_Ht_Lep);
   fChain->SetBranchAddress("Ht_Tau", &Ht_Tau, &b_Ht_Tau);
   fChain->SetBranchAddress("Lin_Aplanarity", &Lin_Aplanarity, &b_Lin_Aplanarity);
   fChain->SetBranchAddress("Lin_Circularity", &Lin_Circularity, &b_Lin_Circularity);
   fChain->SetBranchAddress("Lin_Planarity", &Lin_Planarity, &b_Lin_Planarity);
   fChain->SetBranchAddress("Lin_Sphericity", &Lin_Sphericity, &b_Lin_Sphericity);
   fChain->SetBranchAddress("Lin_Sphericity_C", &Lin_Sphericity_C, &b_Lin_Sphericity_C);
   fChain->SetBranchAddress("Lin_Sphericity_D", &Lin_Sphericity_D, &b_Lin_Sphericity_D);
   fChain->SetBranchAddress("MET_Centrality", &MET_Centrality, &b_MET_Centrality);
   fChain->SetBranchAddress("MET_CosMinDeltaPhi", &MET_CosMinDeltaPhi, &b_MET_CosMinDeltaPhi);
   fChain->SetBranchAddress("MET_LepTau_DeltaPhi", &MET_LepTau_DeltaPhi, &b_MET_LepTau_DeltaPhi);
   fChain->SetBranchAddress("MET_LepTau_VecSumPt", &MET_LepTau_VecSumPt, &b_MET_LepTau_VecSumPt);
   fChain->SetBranchAddress("MET_SumCosDeltaPhi", &MET_SumCosDeltaPhi, &b_MET_SumCosDeltaPhi);
   fChain->SetBranchAddress("MT2_max", &MT2_max, &b_MT2_max);
   fChain->SetBranchAddress("MT2_min", &MT2_min, &b_MT2_min);
   fChain->SetBranchAddress("Meff", &Meff, &b_Meff);
   fChain->SetBranchAddress("Meff_TauTau", &Meff_TauTau, &b_Meff_TauTau);
   fChain->SetBranchAddress("MetTST_OverSqrtHT", &MetTST_OverSqrtHT, &b_MetTST_OverSqrtHT);
   fChain->SetBranchAddress("MetTST_OverSqrtSumET", &MetTST_OverSqrtSumET, &b_MetTST_OverSqrtSumET);
   fChain->SetBranchAddress("MetTST_Significance", &MetTST_Significance, &b_MetTST_Significance);
   fChain->SetBranchAddress("MetTST_Significance_Rho", &MetTST_Significance_Rho, &b_MetTST_Significance_Rho);
   fChain->SetBranchAddress("MetTST_Significance_VarL", &MetTST_Significance_VarL, &b_MetTST_Significance_VarL);
   fChain->SetBranchAddress("MetTST_Significance_dataJER", &MetTST_Significance_dataJER, &b_MetTST_Significance_dataJER);
   fChain->SetBranchAddress("MetTST_Significance_dataJER_Rho", &MetTST_Significance_dataJER_Rho, &b_MetTST_Significance_dataJER_Rho);
   fChain->SetBranchAddress("MetTST_Significance_dataJER_VarL", &MetTST_Significance_dataJER_VarL, &b_MetTST_Significance_dataJER_VarL);
   fChain->SetBranchAddress("MetTST_Significance_dataJER_noPUJets", &MetTST_Significance_dataJER_noPUJets, &b_MetTST_Significance_dataJER_noPUJets);
   fChain->SetBranchAddress("MetTST_Significance_dataJER_noPUJets_Rho", &MetTST_Significance_dataJER_noPUJets_Rho, &b_MetTST_Significance_dataJER_noPUJets_Rho);
   fChain->SetBranchAddress("MetTST_Significance_dataJER_noPUJets_VarL", &MetTST_Significance_dataJER_noPUJets_VarL, &b_MetTST_Significance_dataJER_noPUJets_VarL);
   fChain->SetBranchAddress("MetTST_Significance_noPUJets", &MetTST_Significance_noPUJets, &b_MetTST_Significance_noPUJets);
   fChain->SetBranchAddress("MetTST_Significance_noPUJets_Rho", &MetTST_Significance_noPUJets_Rho, &b_MetTST_Significance_noPUJets_Rho);
   fChain->SetBranchAddress("MetTST_Significance_noPUJets_VarL", &MetTST_Significance_noPUJets_VarL, &b_MetTST_Significance_noPUJets_VarL);
   fChain->SetBranchAddress("MetTST_Significance_noPUJets_noSoftTerm", &MetTST_Significance_noPUJets_noSoftTerm, &b_MetTST_Significance_noPUJets_noSoftTerm);
   fChain->SetBranchAddress("MetTST_Significance_noPUJets_noSoftTerm_Rho", &MetTST_Significance_noPUJets_noSoftTerm_Rho, &b_MetTST_Significance_noPUJets_noSoftTerm_Rho);
   fChain->SetBranchAddress("MetTST_Significance_noPUJets_noSoftTerm_VarL", &MetTST_Significance_noPUJets_noSoftTerm_VarL, &b_MetTST_Significance_noPUJets_noSoftTerm_VarL);
   fChain->SetBranchAddress("MetTST_Significance_phireso_noPUJets", &MetTST_Significance_phireso_noPUJets, &b_MetTST_Significance_phireso_noPUJets);
   fChain->SetBranchAddress("MetTST_Significance_phireso_noPUJets_Rho", &MetTST_Significance_phireso_noPUJets_Rho, &b_MetTST_Significance_phireso_noPUJets_Rho);
   fChain->SetBranchAddress("MetTST_Significance_phireso_noPUJets_VarL", &MetTST_Significance_phireso_noPUJets_VarL, &b_MetTST_Significance_phireso_noPUJets_VarL);
   fChain->SetBranchAddress("Oblateness", &Oblateness, &b_Oblateness);
   fChain->SetBranchAddress("PTt", &PTt, &b_PTt);
   fChain->SetBranchAddress("Planarity", &Planarity, &b_Planarity);
   fChain->SetBranchAddress("RJW_CosThetaStarW", &RJW_CosThetaStarW, &b_RJW_CosThetaStarW);
   fChain->SetBranchAddress("RJW_GammaBetaW", &RJW_GammaBetaW, &b_RJW_GammaBetaW);
   fChain->SetBranchAddress("RJW_dPhiDecayPlaneW", &RJW_dPhiDecayPlaneW, &b_RJW_dPhiDecayPlaneW);
   fChain->SetBranchAddress("RJZ_BalMetObj_Z", &RJZ_BalMetObj_Z, &b_RJZ_BalMetObj_Z);
   fChain->SetBranchAddress("RJZ_CMS_Mass", &RJZ_CMS_Mass, &b_RJZ_CMS_Mass);
   fChain->SetBranchAddress("RJZ_H01_Tau1", &RJZ_H01_Tau1, &b_RJZ_H01_Tau1);
   fChain->SetBranchAddress("RJZ_H01_Z", &RJZ_H01_Z, &b_RJZ_H01_Z);
   fChain->SetBranchAddress("RJZ_H11_Tau1", &RJZ_H11_Tau1, &b_RJZ_H11_Tau1);
   fChain->SetBranchAddress("RJZ_H11_Z", &RJZ_H11_Z, &b_RJZ_H11_Z);
   fChain->SetBranchAddress("RJZ_H21_Tau1", &RJZ_H21_Tau1, &b_RJZ_H21_Tau1);
   fChain->SetBranchAddress("RJZ_H21_Z", &RJZ_H21_Z, &b_RJZ_H21_Z);
   fChain->SetBranchAddress("RJZ_Ht01_Tau1", &RJZ_Ht01_Tau1, &b_RJZ_Ht01_Tau1);
   fChain->SetBranchAddress("RJZ_Ht01_Z", &RJZ_Ht01_Z, &b_RJZ_Ht01_Z);
   fChain->SetBranchAddress("RJZ_Ht11_Tau1", &RJZ_Ht11_Tau1, &b_RJZ_Ht11_Tau1);
   fChain->SetBranchAddress("RJZ_Ht11_Z", &RJZ_Ht11_Z, &b_RJZ_Ht11_Z);
   fChain->SetBranchAddress("RJZ_Ht21_Tau1", &RJZ_Ht21_Tau1, &b_RJZ_Ht21_Tau1);
   fChain->SetBranchAddress("RJZ_Ht21_Z", &RJZ_Ht21_Z, &b_RJZ_Ht21_Z);
   fChain->SetBranchAddress("RJZ_R_BoostZ", &RJZ_R_BoostZ, &b_RJZ_R_BoostZ);
   fChain->SetBranchAddress("RJZ_cosTheta_Tau1", &RJZ_cosTheta_Tau1, &b_RJZ_cosTheta_Tau1);
   fChain->SetBranchAddress("RJZ_cosTheta_Tau2", &RJZ_cosTheta_Tau2, &b_RJZ_cosTheta_Tau2);
   fChain->SetBranchAddress("RJZ_cosTheta_Z", &RJZ_cosTheta_Z, &b_RJZ_cosTheta_Z);
   fChain->SetBranchAddress("RJZ_dPhiDecayPlane_Tau1", &RJZ_dPhiDecayPlane_Tau1, &b_RJZ_dPhiDecayPlane_Tau1);
   fChain->SetBranchAddress("RJZ_dPhiDecayPlane_Tau2", &RJZ_dPhiDecayPlane_Tau2, &b_RJZ_dPhiDecayPlane_Tau2);
   fChain->SetBranchAddress("RJZ_dPhiDecayPlane_Z", &RJZ_dPhiDecayPlane_Z, &b_RJZ_dPhiDecayPlane_Z);
   fChain->SetBranchAddress("Sphericity", &Sphericity, &b_Sphericity);
   fChain->SetBranchAddress("Spherocity", &Spherocity, &b_Spherocity);
   fChain->SetBranchAddress("Thrust", &Thrust, &b_Thrust);
   fChain->SetBranchAddress("ThrustMajor", &ThrustMajor, &b_ThrustMajor);
   fChain->SetBranchAddress("ThrustMinor", &ThrustMinor, &b_ThrustMinor);
   fChain->SetBranchAddress("VecSumPt_LepTau", &VecSumPt_LepTau, &b_VecSumPt_LepTau);
   fChain->SetBranchAddress("WolframMoment_0", &WolframMoment_0, &b_WolframMoment_0);
   fChain->SetBranchAddress("WolframMoment_1", &WolframMoment_1, &b_WolframMoment_1);
   fChain->SetBranchAddress("WolframMoment_10", &WolframMoment_10, &b_WolframMoment_10);
   fChain->SetBranchAddress("WolframMoment_11", &WolframMoment_11, &b_WolframMoment_11);
   fChain->SetBranchAddress("WolframMoment_2", &WolframMoment_2, &b_WolframMoment_2);
   fChain->SetBranchAddress("WolframMoment_3", &WolframMoment_3, &b_WolframMoment_3);
   fChain->SetBranchAddress("WolframMoment_4", &WolframMoment_4, &b_WolframMoment_4);
   fChain->SetBranchAddress("WolframMoment_5", &WolframMoment_5, &b_WolframMoment_5);
   fChain->SetBranchAddress("WolframMoment_6", &WolframMoment_6, &b_WolframMoment_6);
   fChain->SetBranchAddress("WolframMoment_7", &WolframMoment_7, &b_WolframMoment_7);
   fChain->SetBranchAddress("WolframMoment_8", &WolframMoment_8, &b_WolframMoment_8);
   fChain->SetBranchAddress("WolframMoment_9", &WolframMoment_9, &b_WolframMoment_9);
   fChain->SetBranchAddress("actualInteractionsPerCrossing", &actualInteractionsPerCrossing, &b_actualInteractionsPerCrossing);
   fChain->SetBranchAddress("averageInteractionsPerCrossing", &averageInteractionsPerCrossing, &b_averageInteractionsPerCrossing);
   fChain->SetBranchAddress("corr_avgIntPerX", &corr_avgIntPerX, &b_corr_avgIntPerX);
   fChain->SetBranchAddress("corr_avgIntPerX_PRW_DATASF__1down", &corr_avgIntPerX_PRW_DATASF__1down, &b_corr_avgIntPerX_PRW_DATASF__1down);
   fChain->SetBranchAddress("corr_avgIntPerX_PRW_DATASF__1up", &corr_avgIntPerX_PRW_DATASF__1up, &b_corr_avgIntPerX_PRW_DATASF__1up);
   fChain->SetBranchAddress("emt_MT2_max", &emt_MT2_max, &b_emt_MT2_max);
   fChain->SetBranchAddress("emt_MT2_min", &emt_MT2_min, &b_emt_MT2_min);
   fChain->SetBranchAddress("mu_density", &mu_density, &b_mu_density);
   fChain->SetBranchAddress("SUSYFinalState", &SUSYFinalState, &b_SUSYFinalState);
   fChain->SetBranchAddress("Vtx_n", &Vtx_n, &b_Vtx_n);
   fChain->SetBranchAddress("n_BJets", &n_BJets, &b_n_BJets);
   fChain->SetBranchAddress("n_BaseJets", &n_BaseJets, &b_n_BaseJets);
   fChain->SetBranchAddress("n_BaseTau", &n_BaseTau, &b_n_BaseTau);
   fChain->SetBranchAddress("n_SignalElec", &n_SignalElec, &b_n_SignalElec);
   fChain->SetBranchAddress("n_SignalJets", &n_SignalJets, &b_n_SignalJets);
   fChain->SetBranchAddress("n_SignalMuon", &n_SignalMuon, &b_n_SignalMuon);
   fChain->SetBranchAddress("n_SignalTau", &n_SignalTau, &b_n_SignalTau);
   fChain->SetBranchAddress("OS_BaseTauEle", &OS_BaseTauEle, &b_OS_BaseTauEle);
   fChain->SetBranchAddress("OS_BaseTauMuo", &OS_BaseTauMuo, &b_OS_BaseTauMuo);
   fChain->SetBranchAddress("OS_EleEle", &OS_EleEle, &b_OS_EleEle);
   fChain->SetBranchAddress("OS_MuoMuo", &OS_MuoMuo, &b_OS_MuoMuo);
   fChain->SetBranchAddress("OS_TauEle", &OS_TauEle, &b_OS_TauEle);
   fChain->SetBranchAddress("OS_TauMuo", &OS_TauMuo, &b_OS_TauMuo);
   fChain->SetBranchAddress("OS_TauTau", &OS_TauTau, &b_OS_TauTau);
   fChain->SetBranchAddress("TrigHLT_e120_lhloose", &TrigHLT_e120_lhloose, &b_TrigHLT_e120_lhloose);
   fChain->SetBranchAddress("TrigHLT_e140_lhloose_nod0", &TrigHLT_e140_lhloose_nod0, &b_TrigHLT_e140_lhloose_nod0);
   fChain->SetBranchAddress("TrigHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwo", &TrigHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwo, &b_TrigHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwo);
   fChain->SetBranchAddress("TrigHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwoEF", &TrigHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwoEF, &b_TrigHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwoEF);
   fChain->SetBranchAddress("TrigHLT_e17_lhmedium_nod0_ivarloose_tau25_mediumRNN_tracktwoMVA", &TrigHLT_e17_lhmedium_nod0_ivarloose_tau25_mediumRNN_tracktwoMVA, &b_TrigHLT_e17_lhmedium_nod0_ivarloose_tau25_mediumRNN_tracktwoMVA);
   fChain->SetBranchAddress("TrigHLT_e17_lhmedium_nod0_tau25_medium1_tracktwoEF_xe50", &TrigHLT_e17_lhmedium_nod0_tau25_medium1_tracktwoEF_xe50, &b_TrigHLT_e17_lhmedium_nod0_tau25_medium1_tracktwoEF_xe50);
   fChain->SetBranchAddress("TrigHLT_e17_lhmedium_nod0_tau25_medium1_tracktwo_xe50", &TrigHLT_e17_lhmedium_nod0_tau25_medium1_tracktwo_xe50, &b_TrigHLT_e17_lhmedium_nod0_tau25_medium1_tracktwo_xe50);
   fChain->SetBranchAddress("TrigHLT_e17_lhmedium_nod0_tau25_mediumRNN_tracktwoMVA_xe50", &TrigHLT_e17_lhmedium_nod0_tau25_mediumRNN_tracktwoMVA_xe50, &b_TrigHLT_e17_lhmedium_nod0_tau25_mediumRNN_tracktwoMVA_xe50);
   fChain->SetBranchAddress("TrigHLT_e24_lhmedium_L1EM20VH", &TrigHLT_e24_lhmedium_L1EM20VH, &b_TrigHLT_e24_lhmedium_L1EM20VH);
   fChain->SetBranchAddress("TrigHLT_e24_lhmedium_nod0_ivarloose_tau35_medium1_tracktwo", &TrigHLT_e24_lhmedium_nod0_ivarloose_tau35_medium1_tracktwo, &b_TrigHLT_e24_lhmedium_nod0_ivarloose_tau35_medium1_tracktwo);
   fChain->SetBranchAddress("TrigHLT_e26_lhtight_nod0_ivarloose", &TrigHLT_e26_lhtight_nod0_ivarloose, &b_TrigHLT_e26_lhtight_nod0_ivarloose);
   fChain->SetBranchAddress("TrigHLT_e60_lhmedium", &TrigHLT_e60_lhmedium, &b_TrigHLT_e60_lhmedium);
   fChain->SetBranchAddress("TrigHLT_e60_lhmedium_nod0", &TrigHLT_e60_lhmedium_nod0, &b_TrigHLT_e60_lhmedium_nod0);
   fChain->SetBranchAddress("TrigHLT_mu14_ivarloose_tau25_medium1_tracktwo", &TrigHLT_mu14_ivarloose_tau25_medium1_tracktwo, &b_TrigHLT_mu14_ivarloose_tau25_medium1_tracktwo);
   fChain->SetBranchAddress("TrigHLT_mu14_ivarloose_tau35_medium1_tracktwo", &TrigHLT_mu14_ivarloose_tau35_medium1_tracktwo, &b_TrigHLT_mu14_ivarloose_tau35_medium1_tracktwo);
   fChain->SetBranchAddress("TrigHLT_mu14_ivarloose_tau35_medium1_tracktwoEF", &TrigHLT_mu14_ivarloose_tau35_medium1_tracktwoEF, &b_TrigHLT_mu14_ivarloose_tau35_medium1_tracktwoEF);
   fChain->SetBranchAddress("TrigHLT_mu14_ivarloose_tau35_mediumRNN_tracktwoMVA", &TrigHLT_mu14_ivarloose_tau35_mediumRNN_tracktwoMVA, &b_TrigHLT_mu14_ivarloose_tau35_mediumRNN_tracktwoMVA);
   fChain->SetBranchAddress("TrigHLT_mu14_tau25_medium1_tracktwoEF_xe50", &TrigHLT_mu14_tau25_medium1_tracktwoEF_xe50, &b_TrigHLT_mu14_tau25_medium1_tracktwoEF_xe50);
   fChain->SetBranchAddress("TrigHLT_mu14_tau25_medium1_tracktwo_xe50", &TrigHLT_mu14_tau25_medium1_tracktwo_xe50, &b_TrigHLT_mu14_tau25_medium1_tracktwo_xe50);
   fChain->SetBranchAddress("TrigHLT_mu14_tau25_mediumRNN_tracktwoMVA_xe50", &TrigHLT_mu14_tau25_mediumRNN_tracktwoMVA_xe50, &b_TrigHLT_mu14_tau25_mediumRNN_tracktwoMVA_xe50);
   fChain->SetBranchAddress("TrigHLT_mu20_iloose_L1MU15", &TrigHLT_mu20_iloose_L1MU15, &b_TrigHLT_mu20_iloose_L1MU15);
   fChain->SetBranchAddress("TrigHLT_mu24_ivarmedium", &TrigHLT_mu24_ivarmedium, &b_TrigHLT_mu24_ivarmedium);
   fChain->SetBranchAddress("TrigHLT_mu26_imedium", &TrigHLT_mu26_imedium, &b_TrigHLT_mu26_imedium);
   fChain->SetBranchAddress("TrigHLT_mu26_ivarmedium", &TrigHLT_mu26_ivarmedium, &b_TrigHLT_mu26_ivarmedium);
   fChain->SetBranchAddress("TrigHLT_mu40", &TrigHLT_mu40, &b_TrigHLT_mu40);
   fChain->SetBranchAddress("TrigHLT_mu50", &TrigHLT_mu50, &b_TrigHLT_mu50);
   fChain->SetBranchAddress("TrigHLT_mu60_0eta105_msonly", &TrigHLT_mu60_0eta105_msonly, &b_TrigHLT_mu60_0eta105_msonly);
   fChain->SetBranchAddress("TrigHLT_tau125_medium1_tracktwo", &TrigHLT_tau125_medium1_tracktwo, &b_TrigHLT_tau125_medium1_tracktwo);
   fChain->SetBranchAddress("TrigHLT_tau125_medium1_tracktwoEF", &TrigHLT_tau125_medium1_tracktwoEF, &b_TrigHLT_tau125_medium1_tracktwoEF);
   fChain->SetBranchAddress("TrigHLT_tau160_medium1_tracktwo", &TrigHLT_tau160_medium1_tracktwo, &b_TrigHLT_tau160_medium1_tracktwo);
   fChain->SetBranchAddress("TrigHLT_tau160_medium1_tracktwoEF", &TrigHLT_tau160_medium1_tracktwoEF, &b_TrigHLT_tau160_medium1_tracktwoEF);
   fChain->SetBranchAddress("TrigHLT_tau160_medium1_tracktwo_L1TAU100", &TrigHLT_tau160_medium1_tracktwo_L1TAU100, &b_TrigHLT_tau160_medium1_tracktwo_L1TAU100);
   fChain->SetBranchAddress("TrigHLT_tau25_medium1_tracktwo", &TrigHLT_tau25_medium1_tracktwo, &b_TrigHLT_tau25_medium1_tracktwo);
   fChain->SetBranchAddress("TrigHLT_tau25_medium1_tracktwoEF", &TrigHLT_tau25_medium1_tracktwoEF, &b_TrigHLT_tau25_medium1_tracktwoEF);
   fChain->SetBranchAddress("TrigHLT_tau25_mediumRNN_tracktwoMVA", &TrigHLT_tau25_mediumRNN_tracktwoMVA, &b_TrigHLT_tau25_mediumRNN_tracktwoMVA);
   fChain->SetBranchAddress("TrigHLT_tau35_medium1_tracktwo", &TrigHLT_tau35_medium1_tracktwo, &b_TrigHLT_tau35_medium1_tracktwo);
   fChain->SetBranchAddress("TrigHLT_tau35_medium1_tracktwoEF", &TrigHLT_tau35_medium1_tracktwoEF, &b_TrigHLT_tau35_medium1_tracktwoEF);
   fChain->SetBranchAddress("TrigHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo", &TrigHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo, &b_TrigHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo);
   fChain->SetBranchAddress("TrigHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_L1TAU20IM_2TAU12IM", &TrigHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_L1TAU20IM_2TAU12IM, &b_TrigHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_L1TAU20IM_2TAU12IM);
   fChain->SetBranchAddress("TrigHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_xe50", &TrigHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_xe50, &b_TrigHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_xe50);
   fChain->SetBranchAddress("TrigHLT_tau50_medium1_tracktwo_L1TAU12", &TrigHLT_tau50_medium1_tracktwo_L1TAU12, &b_TrigHLT_tau50_medium1_tracktwo_L1TAU12);
   fChain->SetBranchAddress("TrigHLT_tau60_medium1_tracktwo", &TrigHLT_tau60_medium1_tracktwo, &b_TrigHLT_tau60_medium1_tracktwo);
   fChain->SetBranchAddress("TrigHLT_tau60_medium1_tracktwoEF", &TrigHLT_tau60_medium1_tracktwoEF, &b_TrigHLT_tau60_medium1_tracktwoEF);
   fChain->SetBranchAddress("TrigHLT_tau60_medium1_tracktwoEF_tau25_medium1_tracktwoEF_xe50", &TrigHLT_tau60_medium1_tracktwoEF_tau25_medium1_tracktwoEF_xe50, &b_TrigHLT_tau60_medium1_tracktwoEF_tau25_medium1_tracktwoEF_xe50);
   fChain->SetBranchAddress("TrigHLT_tau60_medium1_tracktwo_tau25_medium1_tracktwo_xe50", &TrigHLT_tau60_medium1_tracktwo_tau25_medium1_tracktwo_xe50, &b_TrigHLT_tau60_medium1_tracktwo_tau25_medium1_tracktwo_xe50);
   fChain->SetBranchAddress("TrigHLT_tau60_mediumRNN_tracktwoMVA_tau25_mediumRNN_tracktwoMVA_xe50", &TrigHLT_tau60_mediumRNN_tracktwoMVA_tau25_mediumRNN_tracktwoMVA_xe50, &b_TrigHLT_tau60_mediumRNN_tracktwoMVA_tau25_mediumRNN_tracktwoMVA_xe50);
   fChain->SetBranchAddress("TrigHLT_tau80_medium1_tracktwo", &TrigHLT_tau80_medium1_tracktwo, &b_TrigHLT_tau80_medium1_tracktwo);
   fChain->SetBranchAddress("TrigHLT_tau80_medium1_tracktwoEF", &TrigHLT_tau80_medium1_tracktwoEF, &b_TrigHLT_tau80_medium1_tracktwoEF);
   fChain->SetBranchAddress("TrigHLT_tau80_medium1_tracktwoEF_L1TAU60", &TrigHLT_tau80_medium1_tracktwoEF_L1TAU60, &b_TrigHLT_tau80_medium1_tracktwoEF_L1TAU60);
   fChain->SetBranchAddress("TrigHLT_tau80_medium1_tracktwoEF_L1TAU60_tau60_medium1_tracktwoEF_L1TAU40", &TrigHLT_tau80_medium1_tracktwoEF_L1TAU60_tau60_medium1_tracktwoEF_L1TAU40, &b_TrigHLT_tau80_medium1_tracktwoEF_L1TAU60_tau60_medium1_tracktwoEF_L1TAU40);
   fChain->SetBranchAddress("TrigHLT_tau80_medium1_tracktwo_L1TAU60", &TrigHLT_tau80_medium1_tracktwo_L1TAU60, &b_TrigHLT_tau80_medium1_tracktwo_L1TAU60);
   fChain->SetBranchAddress("TrigHLT_tau80_medium1_tracktwo_L1TAU60_tau25_medium1_tracktwo_L1TAU12", &TrigHLT_tau80_medium1_tracktwo_L1TAU60_tau25_medium1_tracktwo_L1TAU12, &b_TrigHLT_tau80_medium1_tracktwo_L1TAU60_tau25_medium1_tracktwo_L1TAU12);
   fChain->SetBranchAddress("TrigHLT_tau80_medium1_tracktwo_L1TAU60_tau50_medium1_tracktwo_L1TAU12", &TrigHLT_tau80_medium1_tracktwo_L1TAU60_tau50_medium1_tracktwo_L1TAU12, &b_TrigHLT_tau80_medium1_tracktwo_L1TAU60_tau50_medium1_tracktwo_L1TAU12);
   fChain->SetBranchAddress("TrigHLT_tau80_medium1_tracktwo_L1TAU60_tau60_medium1_tracktwo_L1TAU40", &TrigHLT_tau80_medium1_tracktwo_L1TAU60_tau60_medium1_tracktwo_L1TAU40, &b_TrigHLT_tau80_medium1_tracktwo_L1TAU60_tau60_medium1_tracktwo_L1TAU40);
   fChain->SetBranchAddress("TrigHLT_tau80_mediumRNN_tracktwoMVA_L1TAU60_tau60_mediumRNN_tracktwoMVA_L1TAU40", &TrigHLT_tau80_mediumRNN_tracktwoMVA_L1TAU60_tau60_mediumRNN_tracktwoMVA_L1TAU40, &b_TrigHLT_tau80_mediumRNN_tracktwoMVA_L1TAU60_tau60_mediumRNN_tracktwoMVA_L1TAU40);
   fChain->SetBranchAddress("TrigHLT_xe100_pufit_L1XE50", &TrigHLT_xe100_pufit_L1XE50, &b_TrigHLT_xe100_pufit_L1XE50);
   fChain->SetBranchAddress("TrigHLT_xe100_pufit_L1XE55", &TrigHLT_xe100_pufit_L1XE55, &b_TrigHLT_xe100_pufit_L1XE55);
   fChain->SetBranchAddress("TrigHLT_xe110_mht_L1XE50", &TrigHLT_xe110_mht_L1XE50, &b_TrigHLT_xe110_mht_L1XE50);
   fChain->SetBranchAddress("TrigHLT_xe110_pufit_L1XE50", &TrigHLT_xe110_pufit_L1XE50, &b_TrigHLT_xe110_pufit_L1XE50);
   fChain->SetBranchAddress("TrigHLT_xe110_pufit_L1XE55", &TrigHLT_xe110_pufit_L1XE55, &b_TrigHLT_xe110_pufit_L1XE55);
   fChain->SetBranchAddress("TrigHLT_xe110_pufit_xe65_L1XE50", &TrigHLT_xe110_pufit_xe65_L1XE50, &b_TrigHLT_xe110_pufit_xe65_L1XE50);
   fChain->SetBranchAddress("TrigHLT_xe110_pufit_xe70_L1XE50", &TrigHLT_xe110_pufit_xe70_L1XE50, &b_TrigHLT_xe110_pufit_xe70_L1XE50);
   fChain->SetBranchAddress("TrigHLT_xe120_pufit_L1XE50", &TrigHLT_xe120_pufit_L1XE50, &b_TrigHLT_xe120_pufit_L1XE50);
   fChain->SetBranchAddress("TrigHLT_xe50", &TrigHLT_xe50, &b_TrigHLT_xe50);
   fChain->SetBranchAddress("TrigHLT_xe70_mht", &TrigHLT_xe70_mht, &b_TrigHLT_xe70_mht);
   fChain->SetBranchAddress("TrigHLT_xe90_mht_L1XE50", &TrigHLT_xe90_mht_L1XE50, &b_TrigHLT_xe90_mht_L1XE50);
   fChain->SetBranchAddress("TrigHLT_xe90_pufit_L1XE50", &TrigHLT_xe90_pufit_L1XE50, &b_TrigHLT_xe90_pufit_L1XE50);
   fChain->SetBranchAddress("TrigMatchHLT_e120_lhloose", &TrigMatchHLT_e120_lhloose, &b_TrigMatchHLT_e120_lhloose);
   fChain->SetBranchAddress("TrigMatchHLT_e140_lhloose_nod0", &TrigMatchHLT_e140_lhloose_nod0, &b_TrigMatchHLT_e140_lhloose_nod0);
   fChain->SetBranchAddress("TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwo", &TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwo, &b_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwo);
   fChain->SetBranchAddress("TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwoEF", &TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwoEF, &b_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwoEF);
   fChain->SetBranchAddress("TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_mediumRNN_tracktwoMVA", &TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_mediumRNN_tracktwoMVA, &b_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_mediumRNN_tracktwoMVA);
   fChain->SetBranchAddress("TrigMatchHLT_e17_lhmedium_nod0_tau25_medium1_tracktwoEF_xe50", &TrigMatchHLT_e17_lhmedium_nod0_tau25_medium1_tracktwoEF_xe50, &b_TrigMatchHLT_e17_lhmedium_nod0_tau25_medium1_tracktwoEF_xe50);
   fChain->SetBranchAddress("TrigMatchHLT_e17_lhmedium_nod0_tau25_medium1_tracktwo_xe50", &TrigMatchHLT_e17_lhmedium_nod0_tau25_medium1_tracktwo_xe50, &b_TrigMatchHLT_e17_lhmedium_nod0_tau25_medium1_tracktwo_xe50);
   fChain->SetBranchAddress("TrigMatchHLT_e17_lhmedium_nod0_tau25_mediumRNN_tracktwoMVA_xe50", &TrigMatchHLT_e17_lhmedium_nod0_tau25_mediumRNN_tracktwoMVA_xe50, &b_TrigMatchHLT_e17_lhmedium_nod0_tau25_mediumRNN_tracktwoMVA_xe50);
   fChain->SetBranchAddress("TrigMatchHLT_e24_lhmedium_L1EM20VH", &TrigMatchHLT_e24_lhmedium_L1EM20VH, &b_TrigMatchHLT_e24_lhmedium_L1EM20VH);
   fChain->SetBranchAddress("TrigMatchHLT_e24_lhmedium_nod0_ivarloose_tau35_medium1_tracktwo", &TrigMatchHLT_e24_lhmedium_nod0_ivarloose_tau35_medium1_tracktwo, &b_TrigMatchHLT_e24_lhmedium_nod0_ivarloose_tau35_medium1_tracktwo);
   fChain->SetBranchAddress("TrigMatchHLT_e26_lhtight_nod0_ivarloose", &TrigMatchHLT_e26_lhtight_nod0_ivarloose, &b_TrigMatchHLT_e26_lhtight_nod0_ivarloose);
   fChain->SetBranchAddress("TrigMatchHLT_e60_lhmedium", &TrigMatchHLT_e60_lhmedium, &b_TrigMatchHLT_e60_lhmedium);
   fChain->SetBranchAddress("TrigMatchHLT_e60_lhmedium_nod0", &TrigMatchHLT_e60_lhmedium_nod0, &b_TrigMatchHLT_e60_lhmedium_nod0);
   fChain->SetBranchAddress("TrigMatchHLT_mu14_ivarloose_tau25_medium1_tracktwo", &TrigMatchHLT_mu14_ivarloose_tau25_medium1_tracktwo, &b_TrigMatchHLT_mu14_ivarloose_tau25_medium1_tracktwo);
   fChain->SetBranchAddress("TrigMatchHLT_mu14_ivarloose_tau35_medium1_tracktwo", &TrigMatchHLT_mu14_ivarloose_tau35_medium1_tracktwo, &b_TrigMatchHLT_mu14_ivarloose_tau35_medium1_tracktwo);
   fChain->SetBranchAddress("TrigMatchHLT_mu14_ivarloose_tau35_medium1_tracktwoEF", &TrigMatchHLT_mu14_ivarloose_tau35_medium1_tracktwoEF, &b_TrigMatchHLT_mu14_ivarloose_tau35_medium1_tracktwoEF);
   fChain->SetBranchAddress("TrigMatchHLT_mu14_ivarloose_tau35_mediumRNN_tracktwoMVA", &TrigMatchHLT_mu14_ivarloose_tau35_mediumRNN_tracktwoMVA, &b_TrigMatchHLT_mu14_ivarloose_tau35_mediumRNN_tracktwoMVA);
   fChain->SetBranchAddress("TrigMatchHLT_mu14_tau25_medium1_tracktwoEF_xe50", &TrigMatchHLT_mu14_tau25_medium1_tracktwoEF_xe50, &b_TrigMatchHLT_mu14_tau25_medium1_tracktwoEF_xe50);
   fChain->SetBranchAddress("TrigMatchHLT_mu14_tau25_medium1_tracktwo_xe50", &TrigMatchHLT_mu14_tau25_medium1_tracktwo_xe50, &b_TrigMatchHLT_mu14_tau25_medium1_tracktwo_xe50);
   fChain->SetBranchAddress("TrigMatchHLT_mu14_tau25_mediumRNN_tracktwoMVA_xe50", &TrigMatchHLT_mu14_tau25_mediumRNN_tracktwoMVA_xe50, &b_TrigMatchHLT_mu14_tau25_mediumRNN_tracktwoMVA_xe50);
   fChain->SetBranchAddress("TrigMatchHLT_mu20_iloose_L1MU15", &TrigMatchHLT_mu20_iloose_L1MU15, &b_TrigMatchHLT_mu20_iloose_L1MU15);
   fChain->SetBranchAddress("TrigMatchHLT_mu24_ivarmedium", &TrigMatchHLT_mu24_ivarmedium, &b_TrigMatchHLT_mu24_ivarmedium);
   fChain->SetBranchAddress("TrigMatchHLT_mu26_imedium", &TrigMatchHLT_mu26_imedium, &b_TrigMatchHLT_mu26_imedium);
   fChain->SetBranchAddress("TrigMatchHLT_mu26_ivarmedium", &TrigMatchHLT_mu26_ivarmedium, &b_TrigMatchHLT_mu26_ivarmedium);
   fChain->SetBranchAddress("TrigMatchHLT_mu40", &TrigMatchHLT_mu40, &b_TrigMatchHLT_mu40);
   fChain->SetBranchAddress("TrigMatchHLT_mu50", &TrigMatchHLT_mu50, &b_TrigMatchHLT_mu50);
   fChain->SetBranchAddress("TrigMatchHLT_mu60_0eta105_msonly", &TrigMatchHLT_mu60_0eta105_msonly, &b_TrigMatchHLT_mu60_0eta105_msonly);
   fChain->SetBranchAddress("TrigMatchHLT_tau125_medium1_tracktwo", &TrigMatchHLT_tau125_medium1_tracktwo, &b_TrigMatchHLT_tau125_medium1_tracktwo);
   fChain->SetBranchAddress("TrigMatchHLT_tau125_medium1_tracktwoEF", &TrigMatchHLT_tau125_medium1_tracktwoEF, &b_TrigMatchHLT_tau125_medium1_tracktwoEF);
   fChain->SetBranchAddress("TrigMatchHLT_tau160_medium1_tracktwo", &TrigMatchHLT_tau160_medium1_tracktwo, &b_TrigMatchHLT_tau160_medium1_tracktwo);
   fChain->SetBranchAddress("TrigMatchHLT_tau160_medium1_tracktwoEF", &TrigMatchHLT_tau160_medium1_tracktwoEF, &b_TrigMatchHLT_tau160_medium1_tracktwoEF);
   fChain->SetBranchAddress("TrigMatchHLT_tau160_medium1_tracktwo_L1TAU100", &TrigMatchHLT_tau160_medium1_tracktwo_L1TAU100, &b_TrigMatchHLT_tau160_medium1_tracktwo_L1TAU100);
   fChain->SetBranchAddress("TrigMatchHLT_tau25_medium1_tracktwo", &TrigMatchHLT_tau25_medium1_tracktwo, &b_TrigMatchHLT_tau25_medium1_tracktwo);
   fChain->SetBranchAddress("TrigMatchHLT_tau25_medium1_tracktwoEF", &TrigMatchHLT_tau25_medium1_tracktwoEF, &b_TrigMatchHLT_tau25_medium1_tracktwoEF);
   fChain->SetBranchAddress("TrigMatchHLT_tau25_mediumRNN_tracktwoMVA", &TrigMatchHLT_tau25_mediumRNN_tracktwoMVA, &b_TrigMatchHLT_tau25_mediumRNN_tracktwoMVA);
   fChain->SetBranchAddress("TrigMatchHLT_tau35_medium1_tracktwo", &TrigMatchHLT_tau35_medium1_tracktwo, &b_TrigMatchHLT_tau35_medium1_tracktwo);
   fChain->SetBranchAddress("TrigMatchHLT_tau35_medium1_tracktwoEF", &TrigMatchHLT_tau35_medium1_tracktwoEF, &b_TrigMatchHLT_tau35_medium1_tracktwoEF);
   fChain->SetBranchAddress("TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo", &TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo, &b_TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo);
   fChain->SetBranchAddress("TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_L1TAU20IM_2TAU12IM", &TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_L1TAU20IM_2TAU12IM, &b_TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_L1TAU20IM_2TAU12IM);
   fChain->SetBranchAddress("TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_xe50", &TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_xe50, &b_TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_xe50);
   fChain->SetBranchAddress("TrigMatchHLT_tau50_medium1_tracktwo_L1TAU12", &TrigMatchHLT_tau50_medium1_tracktwo_L1TAU12, &b_TrigMatchHLT_tau50_medium1_tracktwo_L1TAU12);
   fChain->SetBranchAddress("TrigMatchHLT_tau60_medium1_tracktwo", &TrigMatchHLT_tau60_medium1_tracktwo, &b_TrigMatchHLT_tau60_medium1_tracktwo);
   fChain->SetBranchAddress("TrigMatchHLT_tau60_medium1_tracktwoEF", &TrigMatchHLT_tau60_medium1_tracktwoEF, &b_TrigMatchHLT_tau60_medium1_tracktwoEF);
   fChain->SetBranchAddress("TrigMatchHLT_tau60_medium1_tracktwoEF_tau25_medium1_tracktwoEF_xe50", &TrigMatchHLT_tau60_medium1_tracktwoEF_tau25_medium1_tracktwoEF_xe50, &b_TrigMatchHLT_tau60_medium1_tracktwoEF_tau25_medium1_tracktwoEF_xe50);
   fChain->SetBranchAddress("TrigMatchHLT_tau60_medium1_tracktwo_tau25_medium1_tracktwo_xe50", &TrigMatchHLT_tau60_medium1_tracktwo_tau25_medium1_tracktwo_xe50, &b_TrigMatchHLT_tau60_medium1_tracktwo_tau25_medium1_tracktwo_xe50);
   fChain->SetBranchAddress("TrigMatchHLT_tau60_mediumRNN_tracktwoMVA_tau25_mediumRNN_tracktwoMVA_xe50", &TrigMatchHLT_tau60_mediumRNN_tracktwoMVA_tau25_mediumRNN_tracktwoMVA_xe50, &b_TrigMatchHLT_tau60_mediumRNN_tracktwoMVA_tau25_mediumRNN_tracktwoMVA_xe50);
   fChain->SetBranchAddress("TrigMatchHLT_tau80_medium1_tracktwo", &TrigMatchHLT_tau80_medium1_tracktwo, &b_TrigMatchHLT_tau80_medium1_tracktwo);
   fChain->SetBranchAddress("TrigMatchHLT_tau80_medium1_tracktwoEF", &TrigMatchHLT_tau80_medium1_tracktwoEF, &b_TrigMatchHLT_tau80_medium1_tracktwoEF);
   fChain->SetBranchAddress("TrigMatchHLT_tau80_medium1_tracktwoEF_L1TAU60", &TrigMatchHLT_tau80_medium1_tracktwoEF_L1TAU60, &b_TrigMatchHLT_tau80_medium1_tracktwoEF_L1TAU60);
   fChain->SetBranchAddress("TrigMatchHLT_tau80_medium1_tracktwoEF_L1TAU60_tau60_medium1_tracktwoEF_L1TAU40", &TrigMatchHLT_tau80_medium1_tracktwoEF_L1TAU60_tau60_medium1_tracktwoEF_L1TAU40, &b_TrigMatchHLT_tau80_medium1_tracktwoEF_L1TAU60_tau60_medium1_tracktwoEF_L1TAU40);
   fChain->SetBranchAddress("TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60", &TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60, &b_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60);
   fChain->SetBranchAddress("TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau25_medium1_tracktwo_L1TAU12", &TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau25_medium1_tracktwo_L1TAU12, &b_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau25_medium1_tracktwo_L1TAU12);
   fChain->SetBranchAddress("TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau50_medium1_tracktwo_L1TAU12", &TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau50_medium1_tracktwo_L1TAU12, &b_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau50_medium1_tracktwo_L1TAU12);
   fChain->SetBranchAddress("TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau60_medium1_tracktwo_L1TAU40", &TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau60_medium1_tracktwo_L1TAU40, &b_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau60_medium1_tracktwo_L1TAU40);
   fChain->SetBranchAddress("TrigMatchHLT_tau80_mediumRNN_tracktwoMVA_L1TAU60_tau60_mediumRNN_tracktwoMVA_L1TAU40", &TrigMatchHLT_tau80_mediumRNN_tracktwoMVA_L1TAU60_tau60_mediumRNN_tracktwoMVA_L1TAU40, &b_TrigMatchHLT_tau80_mediumRNN_tracktwoMVA_L1TAU60_tau60_mediumRNN_tracktwoMVA_L1TAU40);
   fChain->SetBranchAddress("TrigMatching", &TrigMatching, &b_TrigMatching);
   fChain->SetBranchAddress("Lin_Sphericity_IsValid", &Lin_Sphericity_IsValid, &b_Lin_Sphericity_IsValid);
   fChain->SetBranchAddress("MET_BiSect", &MET_BiSect, &b_MET_BiSect);
   fChain->SetBranchAddress("Sphericity_IsValid", &Sphericity_IsValid, &b_Sphericity_IsValid);
   fChain->SetBranchAddress("Spherocity_IsValid", &Spherocity_IsValid, &b_Spherocity_IsValid);
   fChain->SetBranchAddress("Thrust_IsValid", &Thrust_IsValid, &b_Thrust_IsValid);
   fChain->SetBranchAddress("WolframMoments_AreValid", &WolframMoments_AreValid, &b_WolframMoments_AreValid);
   fChain->SetBranchAddress("RandomLumiBlockNumber", &RandomLumiBlockNumber, &b_RandomLumiBlockNumber);
   fChain->SetBranchAddress("RandomRunNumber", &RandomRunNumber, &b_RandomRunNumber);
   fChain->SetBranchAddress("backgroundFlags", &backgroundFlags, &b_backgroundFlags);
   fChain->SetBranchAddress("bcid", &bcid, &b_bcid);
   fChain->SetBranchAddress("coreFlags", &coreFlags, &b_coreFlags);
   fChain->SetBranchAddress("forwardDetFlags", &forwardDetFlags, &b_forwardDetFlags);
   fChain->SetBranchAddress("larFlags", &larFlags, &b_larFlags);
   fChain->SetBranchAddress("lumiBlock", &lumiBlock, &b_lumiBlock);
   fChain->SetBranchAddress("lumiFlags", &lumiFlags, &b_lumiFlags);
   fChain->SetBranchAddress("muonFlags", &muonFlags, &b_muonFlags);
   fChain->SetBranchAddress("pixelFlags", &pixelFlags, &b_pixelFlags);
   fChain->SetBranchAddress("runNumber", &runNumber, &b_runNumber);
   fChain->SetBranchAddress("sctFlags", &sctFlags, &b_sctFlags);
   fChain->SetBranchAddress("tileFlags", &tileFlags, &b_tileFlags);
   fChain->SetBranchAddress("trtFlags", &trtFlags, &b_trtFlags);
   fChain->SetBranchAddress("eventNumber", &eventNumber, &b_eventNumber);
   fChain->SetBranchAddress("MetCST_met", &MetCST_met, &b_MetCST_met);
   fChain->SetBranchAddress("MetCST_phi", &MetCST_phi, &b_MetCST_phi);
   fChain->SetBranchAddress("MetCST_sumet", &MetCST_sumet, &b_MetCST_sumet);
   fChain->SetBranchAddress("MetTST_met", &MetTST_met, &b_MetTST_met);
   fChain->SetBranchAddress("MetTST_phi", &MetTST_phi, &b_MetTST_phi);
   fChain->SetBranchAddress("MetTST_sumet", &MetTST_sumet, &b_MetTST_sumet);
   fChain->SetBranchAddress("TruthMET_met", &TruthMET_met, &b_TruthMET_met);
   fChain->SetBranchAddress("TruthMET_phi", &TruthMET_phi, &b_TruthMET_phi);
   fChain->SetBranchAddress("TruthMET_sumet", &TruthMET_sumet, &b_TruthMET_sumet);
   fChain->SetBranchAddress("RJW_JigSawCandidates_frame", &RJW_JigSawCandidates_frame, &b_RJW_JigSawCandidates_frame);
   fChain->SetBranchAddress("RJW_JigSawCandidates_pdgId", &RJW_JigSawCandidates_pdgId, &b_RJW_JigSawCandidates_pdgId);
   fChain->SetBranchAddress("RJW_JigSawCandidates_pt", &RJW_JigSawCandidates_pt, &b_RJW_JigSawCandidates_pt);
   fChain->SetBranchAddress("RJW_JigSawCandidates_eta", &RJW_JigSawCandidates_eta, &b_RJW_JigSawCandidates_eta);
   fChain->SetBranchAddress("RJW_JigSawCandidates_phi", &RJW_JigSawCandidates_phi, &b_RJW_JigSawCandidates_phi);
   fChain->SetBranchAddress("RJW_JigSawCandidates_m", &RJW_JigSawCandidates_m, &b_RJW_JigSawCandidates_m);
   fChain->SetBranchAddress("RJZ_JigSawCandidates_CosThetaStar", &RJZ_JigSawCandidates_CosThetaStar, &b_RJZ_JigSawCandidates_CosThetaStar);
   fChain->SetBranchAddress("RJZ_JigSawCandidates_dPhiDecayPlane", &RJZ_JigSawCandidates_dPhiDecayPlane, &b_RJZ_JigSawCandidates_dPhiDecayPlane);
   fChain->SetBranchAddress("RJZ_JigSawCandidates_frame", &RJZ_JigSawCandidates_frame, &b_RJZ_JigSawCandidates_frame);
   fChain->SetBranchAddress("RJZ_JigSawCandidates_pdgId", &RJZ_JigSawCandidates_pdgId, &b_RJZ_JigSawCandidates_pdgId);
   fChain->SetBranchAddress("RJZ_JigSawCandidates_pt", &RJZ_JigSawCandidates_pt, &b_RJZ_JigSawCandidates_pt);
   fChain->SetBranchAddress("RJZ_JigSawCandidates_eta", &RJZ_JigSawCandidates_eta, &b_RJZ_JigSawCandidates_eta);
   fChain->SetBranchAddress("RJZ_JigSawCandidates_phi", &RJZ_JigSawCandidates_phi, &b_RJZ_JigSawCandidates_phi);
   fChain->SetBranchAddress("RJZ_JigSawCandidates_m", &RJZ_JigSawCandidates_m, &b_RJZ_JigSawCandidates_m);
   fChain->SetBranchAddress("dilepton_charge", &dilepton_charge, &b_dilepton_charge);
   fChain->SetBranchAddress("dilepton_pdgId", &dilepton_pdgId, &b_dilepton_pdgId);
   fChain->SetBranchAddress("dilepton_pt", &dilepton_pt, &b_dilepton_pt);
   fChain->SetBranchAddress("dilepton_eta", &dilepton_eta, &b_dilepton_eta);
   fChain->SetBranchAddress("dilepton_phi", &dilepton_phi, &b_dilepton_phi);
   fChain->SetBranchAddress("dilepton_m", &dilepton_m, &b_dilepton_m);
   fChain->SetBranchAddress("electrons_MT", &electrons_MT, &b_electrons_MT);
   fChain->SetBranchAddress("electrons_charge", &electrons_charge, &b_electrons_charge);
   fChain->SetBranchAddress("electrons_d0sig", &electrons_d0sig, &b_electrons_d0sig);
   fChain->SetBranchAddress("electrons_isol", &electrons_isol, &b_electrons_isol);
   fChain->SetBranchAddress("electrons_signal", &electrons_signal, &b_electrons_signal);
   fChain->SetBranchAddress("electrons_truthOrigin", &electrons_truthOrigin, &b_electrons_truthOrigin);
   fChain->SetBranchAddress("electrons_truthType", &electrons_truthType, &b_electrons_truthType);
   fChain->SetBranchAddress("electrons_z0sinTheta", &electrons_z0sinTheta, &b_electrons_z0sinTheta);
   fChain->SetBranchAddress("electrons_pt", &electrons_pt, &b_electrons_pt);
   fChain->SetBranchAddress("electrons_eta", &electrons_eta, &b_electrons_eta);
   fChain->SetBranchAddress("electrons_phi", &electrons_phi, &b_electrons_phi);
   fChain->SetBranchAddress("electrons_e", &electrons_e, &b_electrons_e);
   fChain->SetBranchAddress("jets_Jvt", &jets_Jvt, &b_jets_Jvt);
   fChain->SetBranchAddress("jets_MV2c10", &jets_MV2c10, &b_jets_MV2c10);
   fChain->SetBranchAddress("jets_NTrks", &jets_NTrks, &b_jets_NTrks);
   fChain->SetBranchAddress("jets_bjet", &jets_bjet, &b_jets_bjet);
   fChain->SetBranchAddress("jets_signal", &jets_signal, &b_jets_signal);
   fChain->SetBranchAddress("jets_pt", &jets_pt, &b_jets_pt);
   fChain->SetBranchAddress("jets_eta", &jets_eta, &b_jets_eta);
   fChain->SetBranchAddress("jets_phi", &jets_phi, &b_jets_phi);
   fChain->SetBranchAddress("jets_m", &jets_m, &b_jets_m);
   fChain->SetBranchAddress("muons_MT", &muons_MT, &b_muons_MT);
   fChain->SetBranchAddress("muons_charge", &muons_charge, &b_muons_charge);
   fChain->SetBranchAddress("muons_d0sig", &muons_d0sig, &b_muons_d0sig);
   fChain->SetBranchAddress("muons_isol", &muons_isol, &b_muons_isol);
   fChain->SetBranchAddress("muons_signal", &muons_signal, &b_muons_signal);
   fChain->SetBranchAddress("muons_truthOrigin", &muons_truthOrigin, &b_muons_truthOrigin);
   fChain->SetBranchAddress("muons_truthType", &muons_truthType, &b_muons_truthType);
   fChain->SetBranchAddress("muons_z0sinTheta", &muons_z0sinTheta, &b_muons_z0sinTheta);
   fChain->SetBranchAddress("muons_pt", &muons_pt, &b_muons_pt);
   fChain->SetBranchAddress("muons_eta", &muons_eta, &b_muons_eta);
   fChain->SetBranchAddress("muons_phi", &muons_phi, &b_muons_phi);
   fChain->SetBranchAddress("muons_e", &muons_e, &b_muons_e);
   fChain->SetBranchAddress("taus_BDTEleScore", &taus_BDTEleScore, &b_taus_BDTEleScore);
   fChain->SetBranchAddress("taus_BDTEleScoreSigTrans", &taus_BDTEleScoreSigTrans, &b_taus_BDTEleScoreSigTrans);
   fChain->SetBranchAddress("taus_BDTJetScore", &taus_BDTJetScore, &b_taus_BDTJetScore);
   fChain->SetBranchAddress("taus_BDTJetScoreSigTrans", &taus_BDTJetScoreSigTrans, &b_taus_BDTJetScoreSigTrans);
   fChain->SetBranchAddress("taus_ConeTruthLabelID", &taus_ConeTruthLabelID, &b_taus_ConeTruthLabelID);
   fChain->SetBranchAddress("taus_MT", &taus_MT, &b_taus_MT);
   fChain->SetBranchAddress("taus_NTrks", &taus_NTrks, &b_taus_NTrks);
   fChain->SetBranchAddress("taus_NTrksJet", &taus_NTrksJet, &b_taus_NTrksJet);
   fChain->SetBranchAddress("taus_PartonTruthLabelID", &taus_PartonTruthLabelID, &b_taus_PartonTruthLabelID);
   fChain->SetBranchAddress("taus_Quality", &taus_Quality, &b_taus_Quality);
   fChain->SetBranchAddress("taus_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwo", &taus_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwo, &b_taus_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwo);
   fChain->SetBranchAddress("taus_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwoEF", &taus_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwoEF, &b_taus_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_medium1_tracktwoEF);
   fChain->SetBranchAddress("taus_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_mediumRNN_tracktwoMVA", &taus_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_mediumRNN_tracktwoMVA, &b_taus_TrigMatchHLT_e17_lhmedium_nod0_ivarloose_tau25_mediumRNN_tracktwoMVA);
   fChain->SetBranchAddress("taus_TrigMatchHLT_e17_lhmedium_nod0_tau25_medium1_tracktwoEF_xe50", &taus_TrigMatchHLT_e17_lhmedium_nod0_tau25_medium1_tracktwoEF_xe50, &b_taus_TrigMatchHLT_e17_lhmedium_nod0_tau25_medium1_tracktwoEF_xe50);
   fChain->SetBranchAddress("taus_TrigMatchHLT_e17_lhmedium_nod0_tau25_medium1_tracktwo_xe50", &taus_TrigMatchHLT_e17_lhmedium_nod0_tau25_medium1_tracktwo_xe50, &b_taus_TrigMatchHLT_e17_lhmedium_nod0_tau25_medium1_tracktwo_xe50);
   fChain->SetBranchAddress("taus_TrigMatchHLT_e17_lhmedium_nod0_tau25_mediumRNN_tracktwoMVA_xe50", &taus_TrigMatchHLT_e17_lhmedium_nod0_tau25_mediumRNN_tracktwoMVA_xe50, &b_taus_TrigMatchHLT_e17_lhmedium_nod0_tau25_mediumRNN_tracktwoMVA_xe50);
   fChain->SetBranchAddress("taus_TrigMatchHLT_e24_lhmedium_nod0_ivarloose_tau35_medium1_tracktwo", &taus_TrigMatchHLT_e24_lhmedium_nod0_ivarloose_tau35_medium1_tracktwo, &b_taus_TrigMatchHLT_e24_lhmedium_nod0_ivarloose_tau35_medium1_tracktwo);
   fChain->SetBranchAddress("taus_TrigMatchHLT_mu14_ivarloose_tau25_medium1_tracktwo", &taus_TrigMatchHLT_mu14_ivarloose_tau25_medium1_tracktwo, &b_taus_TrigMatchHLT_mu14_ivarloose_tau25_medium1_tracktwo);
   fChain->SetBranchAddress("taus_TrigMatchHLT_mu14_ivarloose_tau35_medium1_tracktwo", &taus_TrigMatchHLT_mu14_ivarloose_tau35_medium1_tracktwo, &b_taus_TrigMatchHLT_mu14_ivarloose_tau35_medium1_tracktwo);
   fChain->SetBranchAddress("taus_TrigMatchHLT_mu14_ivarloose_tau35_medium1_tracktwoEF", &taus_TrigMatchHLT_mu14_ivarloose_tau35_medium1_tracktwoEF, &b_taus_TrigMatchHLT_mu14_ivarloose_tau35_medium1_tracktwoEF);
   fChain->SetBranchAddress("taus_TrigMatchHLT_mu14_ivarloose_tau35_mediumRNN_tracktwoMVA", &taus_TrigMatchHLT_mu14_ivarloose_tau35_mediumRNN_tracktwoMVA, &b_taus_TrigMatchHLT_mu14_ivarloose_tau35_mediumRNN_tracktwoMVA);
   fChain->SetBranchAddress("taus_TrigMatchHLT_mu14_tau25_medium1_tracktwoEF_xe50", &taus_TrigMatchHLT_mu14_tau25_medium1_tracktwoEF_xe50, &b_taus_TrigMatchHLT_mu14_tau25_medium1_tracktwoEF_xe50);
   fChain->SetBranchAddress("taus_TrigMatchHLT_mu14_tau25_medium1_tracktwo_xe50", &taus_TrigMatchHLT_mu14_tau25_medium1_tracktwo_xe50, &b_taus_TrigMatchHLT_mu14_tau25_medium1_tracktwo_xe50);
   fChain->SetBranchAddress("taus_TrigMatchHLT_mu14_tau25_mediumRNN_tracktwoMVA_xe50", &taus_TrigMatchHLT_mu14_tau25_mediumRNN_tracktwoMVA_xe50, &b_taus_TrigMatchHLT_mu14_tau25_mediumRNN_tracktwoMVA_xe50);
   fChain->SetBranchAddress("taus_TrigMatchHLT_tau125_medium1_tracktwo", &taus_TrigMatchHLT_tau125_medium1_tracktwo, &b_taus_TrigMatchHLT_tau125_medium1_tracktwo);
   fChain->SetBranchAddress("taus_TrigMatchHLT_tau125_medium1_tracktwoEF", &taus_TrigMatchHLT_tau125_medium1_tracktwoEF, &b_taus_TrigMatchHLT_tau125_medium1_tracktwoEF);
   fChain->SetBranchAddress("taus_TrigMatchHLT_tau160_medium1_tracktwo", &taus_TrigMatchHLT_tau160_medium1_tracktwo, &b_taus_TrigMatchHLT_tau160_medium1_tracktwo);
   fChain->SetBranchAddress("taus_TrigMatchHLT_tau160_medium1_tracktwoEF", &taus_TrigMatchHLT_tau160_medium1_tracktwoEF, &b_taus_TrigMatchHLT_tau160_medium1_tracktwoEF);
   fChain->SetBranchAddress("taus_TrigMatchHLT_tau160_medium1_tracktwo_L1TAU100", &taus_TrigMatchHLT_tau160_medium1_tracktwo_L1TAU100, &b_taus_TrigMatchHLT_tau160_medium1_tracktwo_L1TAU100);
   fChain->SetBranchAddress("taus_TrigMatchHLT_tau25_medium1_tracktwo", &taus_TrigMatchHLT_tau25_medium1_tracktwo, &b_taus_TrigMatchHLT_tau25_medium1_tracktwo);
   fChain->SetBranchAddress("taus_TrigMatchHLT_tau25_medium1_tracktwoEF", &taus_TrigMatchHLT_tau25_medium1_tracktwoEF, &b_taus_TrigMatchHLT_tau25_medium1_tracktwoEF);
   fChain->SetBranchAddress("taus_TrigMatchHLT_tau25_mediumRNN_tracktwoMVA", &taus_TrigMatchHLT_tau25_mediumRNN_tracktwoMVA, &b_taus_TrigMatchHLT_tau25_mediumRNN_tracktwoMVA);
   fChain->SetBranchAddress("taus_TrigMatchHLT_tau35_medium1_tracktwo", &taus_TrigMatchHLT_tau35_medium1_tracktwo, &b_taus_TrigMatchHLT_tau35_medium1_tracktwo);
   fChain->SetBranchAddress("taus_TrigMatchHLT_tau35_medium1_tracktwoEF", &taus_TrigMatchHLT_tau35_medium1_tracktwoEF, &b_taus_TrigMatchHLT_tau35_medium1_tracktwoEF);
   fChain->SetBranchAddress("taus_TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo", &taus_TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo, &b_taus_TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo);
   fChain->SetBranchAddress("taus_TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_L1TAU20IM_2TAU12IM", &taus_TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_L1TAU20IM_2TAU12IM, &b_taus_TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_L1TAU20IM_2TAU12IM);
   fChain->SetBranchAddress("taus_TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_xe50", &taus_TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_xe50, &b_taus_TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_xe50);
   fChain->SetBranchAddress("taus_TrigMatchHLT_tau50_medium1_tracktwo_L1TAU12", &taus_TrigMatchHLT_tau50_medium1_tracktwo_L1TAU12, &b_taus_TrigMatchHLT_tau50_medium1_tracktwo_L1TAU12);
   fChain->SetBranchAddress("taus_TrigMatchHLT_tau60_medium1_tracktwo", &taus_TrigMatchHLT_tau60_medium1_tracktwo, &b_taus_TrigMatchHLT_tau60_medium1_tracktwo);
   fChain->SetBranchAddress("taus_TrigMatchHLT_tau60_medium1_tracktwoEF", &taus_TrigMatchHLT_tau60_medium1_tracktwoEF, &b_taus_TrigMatchHLT_tau60_medium1_tracktwoEF);
   fChain->SetBranchAddress("taus_TrigMatchHLT_tau60_medium1_tracktwoEF_tau25_medium1_tracktwoEF_xe50", &taus_TrigMatchHLT_tau60_medium1_tracktwoEF_tau25_medium1_tracktwoEF_xe50, &b_taus_TrigMatchHLT_tau60_medium1_tracktwoEF_tau25_medium1_tracktwoEF_xe50);
   fChain->SetBranchAddress("taus_TrigMatchHLT_tau60_medium1_tracktwo_tau25_medium1_tracktwo_xe50", &taus_TrigMatchHLT_tau60_medium1_tracktwo_tau25_medium1_tracktwo_xe50, &b_taus_TrigMatchHLT_tau60_medium1_tracktwo_tau25_medium1_tracktwo_xe50);
   fChain->SetBranchAddress("taus_TrigMatchHLT_tau60_mediumRNN_tracktwoMVA_tau25_mediumRNN_tracktwoMVA_xe50", &taus_TrigMatchHLT_tau60_mediumRNN_tracktwoMVA_tau25_mediumRNN_tracktwoMVA_xe50, &b_taus_TrigMatchHLT_tau60_mediumRNN_tracktwoMVA_tau25_mediumRNN_tracktwoMVA_xe50);
   fChain->SetBranchAddress("taus_TrigMatchHLT_tau80_medium1_tracktwo", &taus_TrigMatchHLT_tau80_medium1_tracktwo, &b_taus_TrigMatchHLT_tau80_medium1_tracktwo);
   fChain->SetBranchAddress("taus_TrigMatchHLT_tau80_medium1_tracktwoEF", &taus_TrigMatchHLT_tau80_medium1_tracktwoEF, &b_taus_TrigMatchHLT_tau80_medium1_tracktwoEF);
   fChain->SetBranchAddress("taus_TrigMatchHLT_tau80_medium1_tracktwoEF_L1TAU60", &taus_TrigMatchHLT_tau80_medium1_tracktwoEF_L1TAU60, &b_taus_TrigMatchHLT_tau80_medium1_tracktwoEF_L1TAU60);
   fChain->SetBranchAddress("taus_TrigMatchHLT_tau80_medium1_tracktwoEF_L1TAU60_tau60_medium1_tracktwoEF_L1TAU40", &taus_TrigMatchHLT_tau80_medium1_tracktwoEF_L1TAU60_tau60_medium1_tracktwoEF_L1TAU40, &b_taus_TrigMatchHLT_tau80_medium1_tracktwoEF_L1TAU60_tau60_medium1_tracktwoEF_L1TAU40);
   fChain->SetBranchAddress("taus_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60", &taus_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60, &b_taus_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60);
   fChain->SetBranchAddress("taus_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau25_medium1_tracktwo_L1TAU12", &taus_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau25_medium1_tracktwo_L1TAU12, &b_taus_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau25_medium1_tracktwo_L1TAU12);
   fChain->SetBranchAddress("taus_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau50_medium1_tracktwo_L1TAU12", &taus_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau50_medium1_tracktwo_L1TAU12, &b_taus_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau50_medium1_tracktwo_L1TAU12);
   fChain->SetBranchAddress("taus_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau60_medium1_tracktwo_L1TAU40", &taus_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau60_medium1_tracktwo_L1TAU40, &b_taus_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau60_medium1_tracktwo_L1TAU40);
   fChain->SetBranchAddress("taus_TrigMatchHLT_tau80_mediumRNN_tracktwoMVA_L1TAU60_tau60_mediumRNN_tracktwoMVA_L1TAU40", &taus_TrigMatchHLT_tau80_mediumRNN_tracktwoMVA_L1TAU60_tau60_mediumRNN_tracktwoMVA_L1TAU40, &b_taus_TrigMatchHLT_tau80_mediumRNN_tracktwoMVA_L1TAU60_tau60_mediumRNN_tracktwoMVA_L1TAU40);
   fChain->SetBranchAddress("taus_Width", &taus_Width, &b_taus_Width);
   fChain->SetBranchAddress("taus_charge", &taus_charge, &b_taus_charge);
   fChain->SetBranchAddress("taus_signalID", &taus_signalID, &b_taus_signalID);
   fChain->SetBranchAddress("taus_truthOrigin", &taus_truthOrigin, &b_taus_truthOrigin);
   fChain->SetBranchAddress("taus_truthType", &taus_truthType, &b_taus_truthType);
   fChain->SetBranchAddress("taus_pt", &taus_pt, &b_taus_pt);
   fChain->SetBranchAddress("taus_eta", &taus_eta, &b_taus_eta);
   fChain->SetBranchAddress("taus_phi", &taus_phi, &b_taus_phi);
   fChain->SetBranchAddress("taus_e", &taus_e, &b_taus_e);

//   newTree = fChain->CloneTree(0);
//   newTree->SetBranchStatus("*",0);
//   newTree->SetBranchStatus("*up*",1);
//   newTree->SetBranchStatus("*down*",1);
//   newTree->SetBranchStatus("*Weight*",1);
//   newTree->SetBranchStatus("*weight*",1);
   Notify();
}

Bool_t selector::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void selector::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t selector::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}

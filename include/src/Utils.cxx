#include "Utils.h"
#include "glog/logging.h"

void Utils::roundCSV(std::ofstream& stream, double num, double err) {
	// if err >  0.095, will round the num and err to 2 digist after the point.
	if (err >= 0.095 && err < 9999.945) {
		// if num > 9999.945, will not round sum
		if (num >= 9999.945) {
			stream << std::resetiosflags(std::ios::fixed) << std::setprecision(6) << num << " +- " << std::fixed << std::setprecision(2) << err;
		} else {
			stream << std::fixed << std::setprecision(2) << num << " +- " <<  err;
		}
	}
	else {
		//else, will be kind of difficult and will round by hand
		stream << std::resetiosflags(std::ios::fixed) << std::setprecision(6) << num << " +- " << err;
	}
}

void Utils::roundLatex(std::ofstream& stream, double num, double err) {
	// if err >  0.095, will round the num and err to 2 digist after the point.
	if (err >= 0.095 && err < 9999.945) {
		// if num > 9999.945, will not round sum
		if (num >= 9999.945) {
			stream << "$  " << std::resetiosflags(std::ios::fixed) << std::setprecision(6) << num << " \\pm " << std::fixed << std::setprecision(2) << err << "  $";
		}
		else {
			stream << std::fixed << std::setprecision(2) << "$  " << num << " \\pm " << err << "  $";
		}
	}
	else {
		//else, will be kind of difficult and will round by hand
		stream << std::resetiosflags(std::ios::fixed) << std::setprecision(6) << "$  " << num << " \\pm " << err << "  $";
	}
}

template <class out_type, class in_type> out_type Utils::convert(in_type& src) {
	std::stringstream ss;
	out_type result;
	ss << src;
	ss >> result;
	return result;
}

int Utils::positionSearch(double value, std::vector<double> list) {
	int right = list.size() - 1;
	int left = 0;

	//first, make sure the value is not larger than list[right] or lower than list[left]
	if (value > list[right]) return right;
	if (value <= list[left]) return -1;

	while (right - left != 1) {
		int mid = (left + right) / 2;
		if (value > list[mid]) {
			left = mid;
		}
		else {
			right = mid;
		}
	}
	return left;
}

std::string Utils::eraseToEnd(std::string target, std::string src){
	int pos = target.rfind(src);
	if(pos > -1){
		int sEnd = target.size();
		return target.erase(pos,sEnd);
	} else{
		LOG(WARNING)<<"Warning: target string \""<<target<<"\" dosen't contain \""<<src<<"\" ! Return target string";
	}
	return target;
}

std::vector<std::string> Utils::splitStrBy(std::string names, char symbol){
	std::vector<std::string> result;
	std::stringstream ss(names);
    while (ss.good()) {
        std::string substr;
        getline(ss, substr, symbol);
        result.emplace_back(substr);
    }
	return result;
}

std::vector<std::string> Utils::regexMatch(std::regex pattern, std::string ip) 
{
    std::smatch result;
    std::vector<std::string> ret;
    bool valid = std::regex_match(ip,result,pattern); 
	DLOG(INFO)<<ip<<"  "<<(valid?"valid":"invalid");
    if(valid)
    {
    	for(unsigned int i = 0; i < result.size(); i++){
        	ret.emplace_back(result[i]);
        }   
	}   
    return ret;
}

std::vector<std::string> Utils::getFiles(std::regex pattern, std::string folder){
	if(folder[folder.length()-1] != '/'){
		folder.append(1,'/');
	}

	std::vector<std::string> strVec;
	DIR* dp;
	dp = opendir(folder.c_str());
	if (dp){
		while (auto file = readdir(dp)){
			std::string filename = (std::string)file->d_name;
			if (filename != "." && filename != ".."){			// ignore '.' and '..'
				std::vector<std::string> result = regexMatch(pattern, filename);
				if(result.size() != 0){
					strVec.emplace_back(folder + result[0]);
				}
			}
		}
		sort(strVec.begin(),strVec.end());
	}else{
		LOG(FATAL) << "Utils::getFiles: Dir not found!" << std::endl;
	}
	closedir(dp);
	return strVec;
}

std::vector<std::string> Utils::getFiles(std::regex pattern, std::string folder, std::vector<std::vector<std::string>>& extraCapture){
	if(folder[folder.length()-1] != '/'){
		folder.append(1,'/');
	}

	std::vector<std::string> strVec;
	DIR* dp;
	dp = opendir(folder.c_str());
	bool hasVec(false);
	if (dp){
		while (auto file = readdir(dp)){
			std::string filename = (std::string)file->d_name;
			if (filename != "." && filename != ".."){			// ignore '.' and '..'
				std::vector<std::string> result = regexMatch(pattern, filename);
				if(result.size() != 0){
					hasVec = true;
					strVec.emplace_back(folder + result[0]);
					extraCapture.emplace_back(result);
				}
			}
		}
		if(hasVec){
			sort(strVec.begin(),strVec.end());
			sort(extraCapture.begin(),extraCapture.end(),[](std::vector<std::string> a, std::vector<std::string> b){return a[0] < b[0];});
		}
	}else{
		LOG(FATAL) << "Dir not found!";
	}
	closedir(dp);
	return strVec;
}

std::vector<std::string> Utils::getTreeList(std::string filename, std::string filter){
	std::vector<std::string> outlist;
	TFile f(filename.c_str());
	if (f.IsZombie()) {
		LOG(FATAL) << "Error opening file";
	}

	TIter nextkey( f.GetListOfKeys() );
	TKey *key;
	while ( (key = (TKey*)nextkey())) {
		TObject *obj = key->ReadObj();
		if ( obj->IsA()->InheritsFrom( TTree::Class() ) ) {
			std::string name = obj->GetName();
			if(filter == ""){
				outlist.emplace_back(name);
			}else{
				std::regex pattern(filter);
				std::smatch result;
				if(std::regex_match(name,result,pattern)){
					outlist.emplace_back(name);
				}
			}
		}
		delete obj;
	}
	delete key;
	f.Close();
	return outlist;
}

//std::vector<std::string> Utils::getFiles(std::regex pattern, std::string folder){
//	if(folder[folder.length()-1] != '/'){
//		folder.append(1,'/');
//	}
//
//	std::vector<std::string> strVec;
//	for (auto&fe : fs::directory_iterator(folder))
//	{
//			auto fp = fe.path();
//			auto fname = fp.filename();
//			std::vector<std::string> result = regexMatch(pattern, fname);
//			if(result.size() != 0){
//				strVec.emplace_back(folder + result[0]);
//			}
//	}
//	return strVec;
//}
//
//std::vector<std::string> Utils::getFiles(std::regex pattern, std::string folder, std::vector<std::vector<std::string>>& extraCapture){
//  if(folder[folder.length()-1] != '/'){
//		folder.append(1,'/');
//  }
//
//	std::vector<std::string> strVec;
//	for (auto&fe : fs::directory_iterator(folder))
//	{
//			auto fp = fe.path();
//			auto fname = fp.filename();
//			std::vector<std::string> result = regexMatch(pattern, fname);
//			if(result.size() != 0){
//				strVec.emplace_back(folder + result[0]);
//				extraCapture.emplace_back(result);
//			}
//	}
//	return strVec;
//}

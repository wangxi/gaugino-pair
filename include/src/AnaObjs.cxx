#include "AnaObjs.h"

AnaObj operator+(const AnaObj& lhs, const AnaObj& rhs) {
	const TLorentzVector &tlhs = lhs;
	return AnaObj(COMBINED, tlhs + rhs, lhs.charge() + rhs.charge(), 0, lhs.Mt() + rhs.Mt());
}

AnaObjs operator+(const AnaObjs& lhs, const AnaObjs& rhs) {
	AnaObjs combined;
	for (const auto &cand : lhs)
		combined.emplace_back(cand);
	for (const auto &cand : rhs)
		combined.emplace_back(cand);
	combined.sortByPt();
	return combined;
}

AnaObjs AnaObjs::selectObjs(float ptCut, float etaCut, int idCut) {
	AnaObjs newObjs;
	for (auto obj : *this)
	{
		if (obj.Pt() > ptCut && fabs(obj.Eta()) <= etaCut && obj.id() == (obj.id() | idCut))
		{
			newObjs.emplace_back(obj);
		}
	}
	return newObjs;
}

void printObject(const AnaObj& obj) {
	std::cout << " Pt: " << obj.Pt() << " Phi: " << obj.Phi() << " Eta:" << obj.Eta()
		<< " charge: " << obj.charge() << " id: " << obj.id() << " type:" << obj.type() << std::endl;
}

void printObjects(const AnaObjs& objs) {
	int ii = 0;
	for (const auto& obj : objs) {
		std::cout << " " << ii;
		printObject(obj);
		ii++;
	}
}

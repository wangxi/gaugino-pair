#include "GRLDB.h"
#include "glog/logging.h"
#include "pugixml.hpp"
#include <iostream>

void GRLDB::loadGRLFile(std::string filename) {
	using namespace pugi;
	xml_document doc;
	if (!doc.load_file(filename.c_str())) {
		LOG(FATAL) << "Failed to load GRL file '" << filename << "'";
		return;
	}

	auto root_nodes = doc.select_nodes("/LumiRangeCollection/NamedLumiRange/LumiBlockCollection");
	for(auto node : root_nodes) {
		int runNumber = std::stoi(std::string(node.node().child_value("Run")));
		std::vector<Range> ranges;
		for (auto child : node.node().children("LBRange")) {
			int rStart = child.attribute("Start").as_int();
			int rEnd = child.attribute("End").as_int();
			ranges.emplace_back(Range(rStart, rEnd));
		}
		lumiBlocks[runNumber] = ranges;
	}
}

bool GRLDB::inRange(int num, std::vector<Range> ranges){
	for(auto range : ranges){
		if (num >= range.first && num <= range.second) {
			return true;
		}
	}
	return false;
}

bool GRLDB::checkInGRL(int runNumber, int lumiBlock){
	auto it = lumiBlocks.find(runNumber);
	if(it != lumiBlocks.end()){
		return inRange(lumiBlock, it->second);
	}
	return false;
}

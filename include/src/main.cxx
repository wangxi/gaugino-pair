#include <fstream>
#include <vector>
#include <string>
#include <ctime>
#include <glog/logging.h>

static clock_t c_start, c_finish;

//function declearation
void errorSignalHandle(const char* data, int size);
static int InitAnalysis();
int MainProcess(int argc, char* argv[]);
static int EndAnalysis();


void errorSignalHandle(const char* data, int size)
{
	std::ofstream fs("glog_dump.log", std::ios::app);
	std::string str = std::string(data, size);
	fs << str;
	fs.close();
	LOG(ERROR) << str;
}

int InitAnalysis() {
	//Log setting
	google::InitGoogleLogging("tauAnaFrame");
	google::SetLogFilenameExtension(".log");
	google::InstallFailureSignalHandler();
	google::InstallFailureWriter(&errorSignalHandle);
	google::SetStderrLogging(google::GLOG_INFO);
	FLAGS_colorlogtostderr = true;

	//Clock setting
	c_start = clock();
	LOG(INFO) << "*****************Start Analysis**********************";
	return 0;
}

int EndAnalysis() {
	//Clock setting
	c_finish = clock();

	int TotalTime = (int)((c_finish - c_start) / CLOCKS_PER_SEC);
	int hour = TotalTime / 3600;
	int minite = (TotalTime % 3600) / 60;
	int second = (TotalTime % 3600) % 60;

	LOG(INFO) << "********** Analysis End. Total run CPU time is: " << hour << "h," << minite << "m," << second << "s" << " ***********";

	//Log setting
	google::ShutdownGoogleLogging();
	return 0;
}

int main(int argc, char* argv[]) {
	if (InitAnalysis() != 0) {
		return -1;
	}
	if (MainProcess(argc, argv) != 0) {
		return -2;
	}
	if (EndAnalysis() != 0) {
		return -3;
	}
	return 0;
}

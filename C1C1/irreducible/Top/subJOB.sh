#rm cutflow.csv yeild.csv
path1="/publicfs/atlas/atlasnew/SUSY/users/wxin/Sample/gaugino_pair/nom_prod/bkg_data_ori/"
#path1="/publicfs/atlas/atlasnew/SUSY/users/zhucz/data/run2/Bkg/"

path2="/publicfs/atlas/atlasnew/SUSY/users/wxin/Sample/gaugino_pair/nom_prod/C1C1/C1C1_ori/"
#for preseletion
#path3="/publicfs/atlas/atlasnew/SUSY/users/wxin/C1C1/LowMass_2tau/abcd/T2/D/"
#path3="/publicfs/atlas/atlasnew/SUSY/users/wxin/C1C1/LowMass_2tau/ABCD/left_SS/"
path3="/publicfs/atlas/atlasnew/SUSY/users/wxin/gaugino_pair/C1C1/irreducible/Top-VR1/123/"
#path3="/publicfs/atlas/atlasnew/SUSY/users/wxin/C1C1/LowMass_2tau/preselection/T2/"
unset ALRB_infoProc
cd build/
rm -r *
cmake ../
make


rm ../result/*
mv selector_ana ../result/
cd ../result


#rm ../T2_123/*
#mv selector_ana ../T2_123
#cd ../T2_123

#rm ../VR1_ss/*
#mv selector_ana ../VR1_ss
#cd ../VR1_ss

#rm ../123/*
#mv selector_ana ../123
#cd ../123
#################################################
chmod u+x selector_ana
rm *log
#################################################
#for bkg
files1=`ls ${path1} | grep Multi`
files2=`ls ${path1} | grep Top`
files3=`ls ${path1} | grep Wjets`
files4=`ls ${path1} | grep Zjets`
files5=`ls ${path1} | grep Higgs`
files6=`ls ${path1} | grep data`


MultiBoson="MultiBoson"
Top="Top"
Wjets="Wjets"
Zjets="Zjets"
Higgs="Higgs"
data="data"
###############################################
#for Wjets
files_emu=`ls ${path1} | grep -E "We|Wmu"`
files_tau=`ls ${path1} | grep "Wtau"`
files_all=`ls ${path1} | grep "W"`
Wemu="Wemu"
Wtau="Wtau"
##############################################
#for sig

files_sig=`ls ${path2} | grep .root`
################################################
# for other
#files=`ls ${path3} | grep .root`
#./selector_ana  $path1 $files6 $data
###############################################

#./selector_ana  $path2 $files_sig
#for all

echo ./selector_ana  $path1 $files1 $MultiBoson \>\> bkglog > start1.sh
chmod u+x start1.sh
hep_sub start1.sh

echo ./selector_ana  $path1 $files2 $Top \>\> bkglog > start2.sh
chmod u+x start2.sh
hep_sub start2.sh

echo ./selector_ana  $path1 $files3 $Wjets \>\> bkglog > start3.sh
chmod u+x start3.sh
hep_sub start3.sh

echo ./selector_ana  $path1 $files4 $Zjets \>\> bkglog > start4.sh
chmod u+x start4.sh
hep_sub start4.sh

echo ./selector_ana  $path1 $files5 $Higgs \>\> bkglog > start5.sh
chmod u+x start5.sh
hep_sub start5.sh

echo ./selector_ana  $path1 $files6 $data \>\> bkglog > start6.sh
chmod u+x start6.sh
#hep_sub start6.sh
hep_sub start6.sh

#for signal

echo ./selector_ana  $path2 $files_sig \>\> siglog > start_sig.sh
chmod u+x start_sig.sh
hep_sub start_sig.sh

#./selector_ana  $path1 $files1
#./selector_ana  $path2 $files2
#./selector_ana  $path1 $files3 $MultiBoson


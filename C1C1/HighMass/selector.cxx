#define selector_cxx
#include "selector.h"
#include <math.h>
#include <regex>
#include <vector>
#include <fstream>
#include <TSystem.h>
#include <TMath.h>
#include <iomanip>
#include <string>
#include <sstream>
#include <algorithm>
#include <TFile.h>
#include <TROOT.h>
#include <TStyle.h>
#include <cmath>
#include "Cutflow.h"
#include "Utils.h"
#include "PhyUtils.h"
#include "MetaDB.h"
#include <TLorentzVector.h>
#include <limits>
#include "GRLDB.h"
#include "easylogging++.h"

INITIALIZE_EASYLOGGINGPP
std::string file;
std::string datamode;
double sigEvents = 0;
int N_OS = 0;
int N_SS = 0;
int k=0;
std::vector<std::string> getinfo(regex pattern, const string& ip);
void maincut(string path, string filename);
void maincut(string path, vector<string> filename,const string name);
bool getfileinfo(string pathDATA, const string& ip);

void selector::Loop()
{
   if (fChain == 0) return;

   Long64_t nentries = fChain->GetEntries();

   Long64_t nbytes = 0, nb = 0;

	//set mc channel number to 0 initially to seperate data and mc
	mcChannelNumber = 0;

	// include grl db to use full ditaumet trigger 
	GRLDB mGRLDB;
	mGRLDB.loadGRLFile("/publicfs/atlas/atlasnew/SUSY/users/zhucz/framework/run2Ditau/include/ditauMETGRL.xml");
	//define variables we will use
    int nTaus;
	//define cutflow
	Cutflow* mCutflow = new Cutflow(file);
	mCutflow->setWeight([&]{return Weight_mc;});
	//define cuts
	int Charge = 0;
	double tau1Pt, tau1Mt;
	double tau2Pt, tau2Mt;
	int nTightTau, Ljet_n;
    int nLooseTau, nMedTau;
	double dPhitt, dRtt, mt12tau, meff_tau, Mtt_12, MT2, met_sig, Ljet_pt, met_sig_tj, evt_MET;

	//Will use for WCR/VR
	double tau1Pt_W, tau1Mt_W;
	double mu1Pt, mu1Mt;
	double dPhitm, dRtm, mt12tm, meff_tm, Mtm_12, MT2_tm;

	//define cuts
	bool tauTrigger, bVeto, OS2Tau, zVeto, mllCut, mt2Cut, noemu, SS2medtau,OSTightTau,OSmedtight,SS2Tau,muTtigger;
	bool isSR1, isSR2, isWCR, isWVR, isTVR, isZVR, isQCDCR_SR1, isQCDCR_SR2, isQCDCR_WCR, isQCDCR_WVR;


	/*********************************************************************
	 * ******************** Define output Tree ***************************
	 * ******************************************************************/
	std::cout << "My tree name is " << datamode << std::endl;
	TTree* m_Tree = fChain->CloneTree(0);
	m_Tree->Branch("Weight_mc", &Weight_mc);
//	m_Tree->Branch("mergedRunNumber",&mergedRunNumber);   --> already done in preselect tree. It means the runNumber for data, randomrunnumber for MC
	m_Tree->Branch("tau1Pt", &tau1Pt);
	m_Tree->Branch("tau2Pt", &tau2Pt);
	m_Tree->Branch("tau1Mt", &tau1Mt);
	m_Tree->Branch("tau2Mt", &tau2Mt);
	m_Tree->Branch("dRtt", &dRtt);
	m_Tree->Branch("dPhitt", &dPhitt);
	m_Tree->Branch("mt12tau", &mt12tau);
	m_Tree->Branch("Mll_tt", &Mtt_12);
	m_Tree->Branch("meff_tt", &meff_tau);
	m_Tree->Branch("MT2", &MT2);

	m_Tree->Branch("Charge", &Charge);

	m_Tree->Branch("tau1Pt_W", &tau1Pt_W);
	m_Tree->Branch("mu1Pt", &mu1Pt);
	m_Tree->Branch("tau1Mt_W", &tau1Mt_W);
	m_Tree->Branch("mu1Mt", &mu1Mt);
	m_Tree->Branch("dRtm", &dRtm);
	m_Tree->Branch("dPhitm", &dPhitm);
	m_Tree->Branch("mt12tm", &mt12tm);
	m_Tree->Branch("Mll_tm", &Mtm_12);
	m_Tree->Branch("meff_tm", &meff_tm);
	m_Tree->Branch("MT2_tm", &MT2_tm);


	m_Tree->Branch("lJet_pt", &Ljet_pt);
	m_Tree->Branch("LJet_n", &Ljet_n);
	m_Tree->Branch("Evt_MET", &evt_MET);
	m_Tree->Branch("Evt_METSIG", &met_sig);
	m_Tree->Branch("Evt_METSIG_WithTauJet", &met_sig_tj);

	m_Tree->Branch("isSR1", &isSR1);
	m_Tree->Branch("isSR2", &isSR2);
	m_Tree->Branch("isWCR", &isWCR);
	m_Tree->Branch("isWVR", &isWVR);
	m_Tree->Branch("isTVR", &isTVR);
	m_Tree->Branch("isZVR", &isZVR);
	m_Tree->Branch("isQCDCR_SR1", &isQCDCR_SR1);
	m_Tree->Branch("isQCDCR_SR2", &isQCDCR_SR2);
	m_Tree->Branch("isQCDCR_WCR", &isQCDCR_WCR);
	m_Tree->Branch("isQCDCR_WVR", &isQCDCR_WVR);

	mCutflow->setFillTree(m_Tree);

	mCutflow->registerCut("baseline",[&] {return true;});
////////////////////////////////////////////////////////////////////////////////////
//cutflow
/*
	mCutflow->registerCut("baseline", [&] {return nMedTau >= 2 && tau1Pt > 50; });
	mCutflow->registerCut("OS", [&] {return (OS2Tau); });//C
	mCutflow->registerCut("bVeto", [&] {return n_BJets==0; },"hasbjet",5,0,5,[&]{return !bVeto;});
    mCutflow->registerCut("noemu", [&] {return noemu; },"noemu",3, 0, 3, [&] {return noemu; });
	mCutflow->registerCut("zVeto", [&] {return zVeto; },"Mll", 15, 0, 300, [&] {return Mtt_12; });
	mCutflow->registerCut(">= 1 tight taus", [&] {return (nTightTau >=1); });//
	mCutflow->registerCut("tau trigger & offline cut",[&]{return tauTrigger;});
	mCutflow->registerCut("mT2 > 20 GeV", [&] {return MT2 > 20; },"mT2_20", 20, 0, 100, [&] {return MT2;});
*/
//	mCutflow->registerCut("OS", [&] {return (OS2Tau); });//C
//	mCutflow->registerCut("dphitt > 0.8", [&] {return dPhitt > 0.8; },"DPhitt",20,0,4,[&] {return dPhitt;});
//	mCutflow->registerCut("drtt < 3.2", [&] {return dRtt < 3.2; },"DRtt",20,0,4,[&] {return dRtt;});
//    mCutflow->registerCut("mT2 > 70 GeV", [&] {return MT2 > 70; },"mT2_80", 20, 0, 200, [&] {return MT2;});

//////////////////////////////////////////////////////////////////////////////////////////////////////
// simplified ABCD 
//    mCutflow->registerCut("noemu", [&] {return noemu; },"noemu",3, 0, 3, [&] {return noemu; });
//left
//	mCutflow->registerCut("> 2 loose tau",[&] {return nLooseTau >= 2;}); //B 	
//	mCutflow->registerCut("no >= 2M1T(OS)",[&] {return !((nTightTau >=1)&&(nMedTau >=2)&&OS2Tau) ;}); //B E
//	mCutflow->registerCut("no >= 2T(OS)",[&] {return !((nTightTau >=2)&&OS2Tau) ;}); //B E
//right

//	mCutflow->registerCut("> 2 medium tau",[&] {return nMedTau >= 2;}); //B 	
//	mCutflow->registerCut(">= 1med_1tight", [&] {return ((nTightTau >=1)&&(nMedTau >=2)&& OS2Tau); });//
//	mCutflow->registerCut("==2 tight tau", [&] {return (nTightTau==2)&& OS2Tau; },"n_TightTau",4,0,4,[&] {return nTightTau;});

//   mCutflow->registerCut("zVeto", [&] {return zVeto; },"Mll", 15, 0, 300, [&] {return Mtt_12; });
//    mCutflow->registerCut("noemu", [&] {return noemu; },"noemu",3, 0, 3, [&] {return noemu; });
//    mCutflow->registerCut("bVeto", [&] {return n_BJets == 0; },"hasbjet",5,0,5,[&]{return !bVeto;});

//	mCutflow->registerCut("mT2 > 80 GeV", [&] {return MT2 > 80; },"mT2_80", 20, 0, 200, [&] {return MT2;});
//	mCutflow->registerCut("mT2 > 30 GeV", [&] {return MT2 > 30; },"mT2_30", 20, 0, 200, [&] {return MT2;});	

//     mCutflow->registerCut("tau trigger & offline cut",[&]{return tauTrigger;});
//	mCutflow->registerCut("Evt_MET > 50 GeV", [&] {return MetTST_met > 50000; },"evt_met",20,0,500,[&] {return evt_MET;});


///////////////////////////////////////////////////////////////////////////////////////////////////
//for ABCD
/*	
    mCutflow->registerCut("tau trigger",[&]{return tauTrigger;});
	
//	mCutflow->registerCut("> 2 loose tau(SS)",[&] {return (nLooseTau >= 2 && SS2Tau);}); //B 	
	mCutflow->registerCut(" >= 2M1T(OS)",[&] {return !((nTightTau >=1)&&(nMedTau ==2)&&OS2Tau) ;}); //B E
//	mCutflow->registerCut("==2 tight tau(OS)", [&] {return (nTightTau==2&&OS2Tau); },"n_TightTau",4,0,4,[&] {return nTightTau;});
	
	mCutflow->registerCut("noemu", [&] {return noemu; },"noemu",3, 0, 3, [&] {return noemu; });
	mCutflow->registerCut("zVeto", [&] {return zVeto; },"Mll", 15, 0, 300, [&] {return Mtt_12; });
    mCutflow->registerCut("bVeto", [&] {return n_BJets == 0; },"hasbjet",5,0,5,[&]{return !bVeto;});

*/
//	mCutflow->registerCut("Evt_MET > 40 GeV", [&] {return MetTST_met > 40000; },"evt_met",20,0,500,[&] {return evt_MET;});
//	mCutflow->registerCut("Evt_MET < 150 GeV", [&] {return MetTST_met < 150000; },"evt_met",20,0,500,[&] {return evt_MET;});

//      mCutflow->registerCut("mT2 > 10 GeV", [&] {return MT2 > 10; },"mT2_10", 20, 0, 200, [&] {return MT2;});	
//      mCutflow->registerCut("mT2 < 40 GeV", [&] {return MT2 < 40; },"mT2_40", 20, 0, 200, [&] {return MT2;});	

//      mCutflow->registerCut("MT12 > 0", [&] {return mt12tau > 0;},"mt12tau_0",20,0,1000,[&] {return mt12tau;} );
//       mCutflow->registerCut("MT12 < 175", [&] {return mt12tau < 175;},"mt12tau_175",20,0,1000,[&] {return mt12tau;} );

//       mCutflow->registerCut("mT2 > 50 GeV", [&] {return MT2 > 50; },"mT2_50", 20, 0, 200, [&] {return MT2;});	
       mCutflow->registerCut("mT2 > 80 GeV", [&] {return MT2 > 80; },"mT2_80", 20, 0, 200, [&] {return MT2;});	

//       mCutflow->registerCut("MT12 > 200", [&] {return mt12tau > 200;},"mt12tau_200",20,0,1000,[&] {return mt12tau;} );
       mCutflow->registerCut("MT12 > 550", [&] {return mt12tau > 550;},"mt12tau_550",20,0,1000,[&] {return mt12tau;} );

//      mCutflow->registerCut("MT12 > 100", [&] {return mt12tau > 100;},"mt12tau_100",20,0,1000,[&] {return mt12tau;} );
//      mCutflow->registerCut("MT12 < 550", [&] {return mt12tau < 550;},"mt12tau_550",20,0,1000,[&] {return mt12tau;} );

//       mCutflow->registerCut("mT2 > 30 GeV", [&] {return MT2 > 30; },"mT2_30", 20, 0, 200, [&] {return MT2;});	
//      mCutflow->registerCut("mT2 < 70 GeV", [&] {return MT2 < 70; },"mT2_70", 20, 0, 200, [&] {return MT2;});	
//       
/////////////////////////////////////////////////////////////////////////////////////////////////	
//SR

//   mCutflow->registerCut("tau trigger & offline cut & MET>150",[&]{return tauTrigger;});
//     mCutflow->registerCut("mT2 > 70 GeV", [&] {return MT2 > 70; },"mT2_70", 20, 0, 200, [&] {return MT2;});
//    mCutflow->registerCut("MT12 > 550", [&] {return mt12tau > 550;},"mt12tau_550",20,0,1000,[&] {return mt12tau;} );
//	mCutflow->registerCut("Evt_MET > 150 GeV", [&] {return MetTST_met > 150000; },"evt_met",20,0,500,[&] {return evt_MET;});


////////////////////////////////////////////////////////////////////////////////////////////////
/*
	mCutflow->registerCut("=2 taus",[&] {return nTaus==2;});
	
	mCutflow->registerCut("tau trigger",[&]{return tauTrigger;});
//	mCutflow->registerCut("Evt_MET > 150 GeV", [&] {return MetTST_met > 150000; },"evt_met",20,0,500,[&] {return evt_MET;});
	
//	mCutflow->registerCut(">= 2 loose tau",[&] {return nLooseTau >= 2;}); //B 	
//	mCutflow->registerCut("no = 2 medium tau(OS)",[&] {return !SS2medtau ;}); //B E
//	mCutflow->registerCut("no tight tau(OS)",[&] {return !OSTightTau ;}); //B E
//	mCutflow->registerCut("!(med1_tight1_OS)",[&] {return !OSmedtight ;}); //B E

//	mCutflow->registerCut(">= 2 medium tau",[&] {return nMedTau >= 2;}); //B 	
	mCutflow->registerCut(">= 1med_1tight", [&] {return ((nTightTau >=1)&&(nMedTau >=2)); });//
//	mCutflow->registerCut(">= 2 tight taus", [&] {return (nTightTau >=2); });//
	
//	mCutflow->registerCut("SS", [&] {return (SS2Tau); });//C
	mCutflow->registerCut("OS", [&] {return (OS2Tau); });//C
	mCutflow->registerCut("noemu", [&] {return noemu; },"noemu",3, 0, 3, [&] {return noemu; });
	mCutflow->registerCut("zVeto", [&] {return zVeto; },"Mll", 15, 0, 300, [&] {return Mtt_12; });
    mCutflow->registerCut("bVeto", [&] {return bVeto; },"hasbjet",5,0,5,[&]{return !bVeto;});
*/
//	mCutflow->registerCut("mT2 > 60 GeV", [&] {return MT2 > 60; },"mT2_60", 20, 0, 100, [&] {return MT2;});
//    mCutflow->registerCut("mT2 < 80 GeV", [&] {return MT2 < 80; },"mT2_80", 20, 0, 100, [&] {return MT2;});
	
//     mCutflow->registerCut("MT12 > 200", [&] {return mt12tau > 200;},"mt12tau_200",18,0,450,[&] {return mt12tau;} );
//     mCutflow->registerCut("MT12 < 450", [&] {return mt12tau < 450;},"mt12tau_450",18,0,450,[&] {return mt12tau;} );
//    mCutflow->registerCut("tau2pt > 60", [&] {return tau2Pt > 60;},"tau2Pt",50,0,500,[&] {return tau2Pt;} );
//	mCutflow->registerCut("mll_12 < 450 GeV",[&]{return Mtt_12 <= 450;},"mll_12",45,0,450,[&] {return Mtt_12;});
//	mCutflow->registerCut("drtt < 3.2", [&] {return dRtt < 3.2; },"DRtt",20,0,4,[&] {return dRtt;});
//	mCutflow->registerCut("dphitt > 0.8", [&] {return dPhitt > 0.8; },"DPhitt",20,0,4,[&] {return dPhitt;});
	
//	mCutflow->registerCut("Evt_MET > 40 GeV", [&] {return MetTST_met > 40000; },"evt_met_40",15,0,150,[&] {return evt_MET;});
//	mCutflow->registerCut("Evt_MET < 150 GeV", [&] {return MetTST_met < 150000; },"evt_met_150",50,0,500,[&] {return evt_MET;});
//   mCutflow->registerCut("Evt_MET < 100 GeV", [&] {return MetTST_met < 100000; },"evt_met_100",20,0,500,[&] {return evt_MET;});
//	mCutflow->registerCut("drtt < 3.2", [&] {return dRtt < 3.2; },"DRtt",20,0,4,[&] {return dRtt;});
//	mCutflow->registerCut("tau1pt > 90", [&] {return tau1Pt > 90;},"tau1Pt",50,0,500,[&] {return tau1Pt;} );
//	mCutflow->registerCut("dphitt > 0.4", [&] {return dPhitt > 0.4; },"DPhitt",20,0,4,[&] {return dPhitt;});
//	mCutflow->registerCut("mll_12 > 120 GeV",[&]{return Mtt_12 >= 120;},"mll_12",50,0,500,[&] {return Mtt_12;});
//	mCutflow->registerCut("MT12 > 350 GeV", [&] {return mt12tau > 350; },"MT12",100,0,1000,[&] {return mt12tau;});
	
	
	
//	mCutflow->registerCut("==2 tight tau", turn zVeto; },"Mll", 15, 0, 300, [&] {return Mtt_12; });
	

//	mCutflow->registerCut("drtt < 3.2", [&] {return dRtt < 3.2; },"DRtt",20,0,4,[&] {return dRtt;});
//	mCutflow->registerCut("dphitt > 1.4", [&] {return dPhitt > 1.4; },"DPhitt",20,0,4,[&] {return dPhitt;});
//	mCutflow->registerCut("Evt_MET > 75 GeV", [&] {return MetTST_met > 75000; });
//	mCutflow->registerCut("mT2 > 70 GeV", [&] {return MT2 > 70; });
//	mCutflow->registerCut("tau1pt > 120", [&] {return tau1Pt > 120000; });
//	mCutflow->registerCut("mll_12 > 13GeV",[&]{return Mtt_12 >= 130000;});
//	mCutflow->registerCut("drtt < 3.2", [&] {return dRtt < 3.2; });
//	mCutflow->registerCut("dphitt > 0.6", [&] {return dPhitt > 0.6; });
//	mCutflow->registerCut("mT2_max > 65 GeV", [&] {return MT2_max > 65000; });
//	mCutflow->registerCut("Evt_MET > 150 GeV", [&] {return MetTST_met > 150000; });
//	mCutflow->registerCut("MT12 > 250 GeV", [&] {return mt12tau > 250000; });
/*
	//ABCD
	mCutflow->registerCut("fabs(weights)<25",[&] {return fabs(weights)<25;});
	mCutflow->registerCut("tau trigger",[&] {return tauTrigger;});
	mCutflow->registerCut("bVeto", [&] {return bVeto; },"hasbjet",5,0,5,[&]{return !bVeto;});
	mCutflow->registerCut("zVeto", [&] {return zVeto; },"Mll", 15, 0, 300, [&] {return Mtt_12; });
	mCutflow->registerCut("noemu", [&] {return noemu; },"noemu",3, 0, 3, [&] {return noemu; });

//	mCutflow->registerCut(">= 2 loose tau",[&] {return nLooseTau >= 2;}); //B E
//	mCutflow->registerCut("no >= 2 medium tau(OS)",[&] {return SS2medtau ;}); //B E

	mCutflow->registerCut("==2 tight tau", [&] {return (nTightTau==2); });//C
	mCutflow->registerCut("OS", [&] {return (OS2Tau); });//C
*/
//    mCutflow->registerCut("drtt < 3.2", [&] {return dRtt < 3.2; });
//	mCutflow->registerCut("dphitt > 1.4", [&] {return dPhitt > 1.4; });

    
//	mCutflow->registerCut("Evt_MET > 75 Gev", [&] {return MetTST_met > 75000;});
//	mCutflow->registerCut("Evt_MET < 150 Gev", [&] {return MetTST_met < 150000;});
//	mCutflow->registerCut("mT2 > 10 Gev", [&] {return MT2 > 10;});
//	mCutflow->registerCut("mT2 < 30  Gev", [&] {return MT2 < 30 ;});

	auto lastCut = mCutflow->registerCut("the END", [&] {return true; });
	
//	lastCut->addHist("Evt_MET", 11, 40, 150, [&] {return evt_MET; });//50,0,500
//	lastCut->addHist("mt2", 5,20,70, [&] {return MT2; });//20,0,200
//	lastCut->addHist("MT12", 9, 100, 550, [&] {return mt12tau; });//20,0,1000
	
	lastCut->addHist("Charge", 5, -2, 3, [&] {return Charge; });
	lastCut->addHist("Evt_MET",11, 40, 150, [&] {return evt_MET; });//50,0,500
	lastCut->addHist("mt2", 8,30,70, [&] {return MT2; });//20,0,200
  lastCut->addHist("tau2pt", 50, 0, 500, [&] {return tau2Pt; });
	lastCut->addHist("MT12", 18, 100, 550, [&] {return mt12tau; });//20,0,1000
	lastCut->addHist("Mtt_12", 20, 0, 1000, [&] {return Mtt_12; });
	
	lastCut->addHist("meff_tau", 20, 0, 1000, [&] {return meff_tau; });
	lastCut->addHist("DRtt", 20, 0, 4, [&] {return dRtt; });
	lastCut->addHist("tau1pt", 50, 0, 500, [&] {return tau1Pt; });
	lastCut->addHist("tau1Mt", 20, 0, 1000, [&] {return tau1Mt; });
	lastCut->addHist("tau2Mt", 20, 0, 500, [&] {return tau2Mt; });
	lastCut->addHist("DPhitt", 20, 0, 4, [&] {return dPhitt; });
	lastCut->addHist("MET_sig", 20, 0, 50, [&] {return met_sig; });
	lastCut->addHist("met_sig_tj", 20, 0, 50, [&] {return met_sig_tj; });
	lastCut->addHist("Ljet_n", 10, 0, 10, [&] {return Ljet_n; });
	lastCut->addHist("Ljet_pt", 10, 20, 120, [&] {return Ljet_pt; });

   for (Long64_t jentry=0; jentry<nentries;jentry++) 
   {
      Long64_t ientry = LoadTree(jentry);
      if (ientry < 0) break;
      nb = fChain->GetEntry(jentry);   nbytes += nb;
      if (0 == jentry%100000)
      	cout << jentry << " entry of " << nentries << "entries" << endl;

	  //intialize all variable an cut
	  tauTrigger = false, bVeto = true, OS2Tau = false, zVeto = false, noemu = true, SS2medtau = false,OSTightTau = false,OSmedtight = false, SS2Tau = false;
	  mllCut = false, mt2Cut = false;

	  nMedTau=0,nLooseTau=0;
	  nTightTau = 0; Ljet_n = 0;
	  tau1Pt = -1; tau1Mt = -1;
	  tau2Pt = -1; tau2Mt = -1;
	  dPhitt = 999; dRtt = 999; mt12tau = -999; Mtt_12 = -999; MT2 = -1; met_sig = -1; met_sig_tj = -1; Ljet_pt = 0; 

		isSR1 = false;
		isSR2 = false;
		isWCR = false;
		isWVR = false;
		isTVR = false;
		isZVR = false;
		isQCDCR_SR1 = false;
		isQCDCR_SR2 = false;
		isQCDCR_WCR = false;
		isQCDCR_WVR = false;

		isSR1 = true;

	  evt_MET =  MetTST_met / 1000;

	  //weight setting
  

	  noemu = ((electrons_pt->size() + muons_pt->size()) == 0);
	   nTaus = taus_pt->size();
      

		  // trgger match. Will check 17 data/MC
		  bool useOldtrigger(true);
		  if(mergedRunNumber >= 325713 && mergedRunNumber <=340453){   // 17 data/MC
			if(mcChannelNumber != 0){
				if(!mGRLDB.checkInGRL(mergedRunNumber,RandomLumiBlockNumber)){
					useOldtrigger = false;
	 			}
			} else{
	 			if(!mGRLDB.checkInGRL(mergedRunNumber,lumiBlock)){
					useOldtrigger = false;
				}
			}
		  }

//tauID
		  int passMatch(0);
		  if(nTaus>=2)
		  {
			  for(int i_tau=0;i_tau<2;i_tau++)
			  {
				  if((mergedRunNumber > 341649 && taus_TrigMatchHLT_tau60_medium1_tracktwoEF_tau25_medium1_tracktwoEF_xe50->at(i_tau)) || 
						  (mergedRunNumber <= 341649 && useOldtrigger && taus_TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_xe50->at(i_tau))||
						  (mergedRunNumber <= 341649 && !useOldtrigger && taus_TrigMatchHLT_tau60_medium1_tracktwo_tau25_medium1_tracktwo_xe50->at(i_tau))
					)
				  {
					  passMatch++;
				  }
				  if ( taus_Quality->at(i_tau) >= 1 )
				  { 
					  nLooseTau++;

					  if ( taus_Quality->at(i_tau) >= 2 && taus_signalID->at(i_tau) == 1 )
					  { 
						  nMedTau++;
						  if ( taus_Quality->at(i_tau) >= 3 && taus_signalID->at(i_tau) == 1)
						  { 
							  nTightTau++;
						  }
					  }
				  }
			  }



//build tau1 &tau2		  
		  int tau1Posi = 0;
		  tau1Pt = taus_pt->at(tau1Posi);
		  double tau1Eta = taus_eta->at(tau1Posi);
		  double tau1Phi = taus_phi->at(tau1Posi);
		  double tau1E = taus_e->at(tau1Posi);
		  tau1Mt = taus_MT->at(tau1Posi);
		  double tau1Charge = taus_charge->at(tau1Posi);

          int tau2Posi = 1;
		  tau2Pt = taus_pt->at(tau2Posi);
		  double tau2Eta = taus_eta->at(tau2Posi);
		  double tau2Phi = taus_phi->at(tau2Posi);
		  double tau2E = taus_e->at(tau2Posi);
		  tau2Mt = taus_MT->at(tau2Posi);
		  double tau2Charge = taus_charge->at(tau2Posi);

		  TLorentzVector tau1, tau2, vecMET;
		  tau1.SetPtEtaPhiE(tau1Pt, tau1Eta, tau1Phi, tau1E);
		  tau2.SetPtEtaPhiE(tau2Pt, tau2Eta, tau2Phi, tau2E);
		  vecMET.SetPtEtaPhiE(MetTST_met,0,MetTST_phi,MetTST_met);
		  
		  //mll
		  Mtt_12 = PhyUtils::calcMll(tau1, tau2);
		  MT2 = PhyUtils::MT2(tau1,tau2,vecMET);
		  mt12tau = tau1Mt + tau2Mt;
		  dRtt = PhyUtils::deltaR(tau1, tau2);
		  dPhitt = PhyUtils::deltaPhi(tau1Phi, tau2Phi);
		  meff_tau = tau1Pt + tau2Pt + MetTST_met;
		  met_sig = (MetTST_met/1000) / (sqrt(tau1Pt/1000+tau2Pt/1000));
	  	
		OS2Tau = tau1Charge * tau2Charge == -1;
		  SS2Tau = tau1Charge * tau2Charge == 1;
		  Charge = tau1Charge * tau2Charge; 
		  if(SS2Tau == false)
			  N_OS +=1;
		  else 
			  N_SS +=1;
		  if (Mtt_12 > 120000) zVeto = true;

		  if(nMedTau >= 2 && OS2Tau)
		  {SS2medtau = true;}

		  if(nTightTau != 0&&nMedTau ==2 && OS2Tau)
		  {OSTightTau = true;}

		  //2018 without &&evt_MET < 200
		  if((mergedRunNumber > 341649)){
			if(tau1Pt>75000 && tau2Pt>40000 && evt_MET > 0 && TrigHLT_tau60_medium1_tracktwoEF_tau25_medium1_tracktwoEF_xe50 != 0){
				tauTrigger = (passMatch >= 2);
			}
		  }

		  //2015-2017 Old ditauMET
		  else if(useOldtrigger && tau1Pt>50000 && tau2Pt>40000 && evt_MET > 0 && TrigHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_xe50 != 0){
              tauTrigger = (passMatch >=2);
			  if(mcChannelNumber != 0){
              	Weight_mc *= (TauWeightTrigHLT_tau35_medium1_tracktwo * TauWeightTrigHLT_tau25_medium1_tracktwo);
			  }
		  }
		  
		  //2017 new ditauMET
		  else if(!useOldtrigger && tau1Pt>75000 && tau2Pt>40000 && evt_MET > 0 && TrigHLT_tau60_medium1_tracktwo_tau25_medium1_tracktwo_xe50 != 0){
			  tauTrigger =(passMatch>=2);
		  }
		
} 
    
//scale
		tau1Pt    = tau1Pt / 1000;
		tau2Pt    = tau2Pt / 1000;
		tau1Mt    = tau1Mt / 1000;
		tau2Mt    = tau2Mt / 1000;
		mt12tau   = mt12tau / 1000;
		Mtt_12    = Mtt_12 / 1000;
		meff_tau   = meff_tau / 1000;
		MT2       = MT2 / 1000;
		tau1Pt_W  = tau1Pt_W / 1000;
		mu1Pt     = mu1Pt / 1000;
		tau1Mt_W  = tau1Mt_W / 1000;
		mu1Mt     = mu1Mt / 1000;
		mt12tm    = mt12tm / 1000;
		Mtm_12    = Mtm_12 / 1000;
		meff_tm   = meff_tm / 1000;
		MT2_tm    = MT2_tm / 1000;
		Ljet_pt   = Ljet_pt / 1000;

	  
	  bool firstJet = true;
	  double sumJet(0);


//	  met_sig_tj = (MetTST_met/1000) / (sqrt(tau1Pt/1000 + tau2Pt/1000 + sumJet / 1000));


	  mCutflow->startCut(Cutflow::SHOW_HIST);
   }
   mCutflow->PrintOut();
   delete mCutflow;
}

int main(int argc, char* argv[])
{
    cout<<"*******************************Start**********************************"<<endl;

    //argv[1]表示datamode，datamode=background表示不需要分析文件名信息
    //datamode=signal表示需分析文件名信息
    //datamode=data表示使用针对data的cut
    //argv[2]表示samplemode，可为16a,16d,16aaf2,16daf2,data,bkg
    //argv[3]表示filepath
    std::string pathDATA = argv[1];

    //因为argv[0]是程序本身,argv[1],[2]是条件判断,argv[3]是path,所以这里要减4,之后要加4
//  if(pathDATA =="/publicfs/atlas/atlasnew/SUSY/users/wxin/C1C1/Sample/signal/")
//	if(pathDATA == "/publicfs/atlas/atlasnew/SUSY/users/wxin/Sample/gaugino_pair/QCD_prod/C1C1/")
	if(pathDATA == "/publicfs/atlas/atlasnew/SUSY/users/wxin/Sample/gaugino_pair/nom_prod/C1C1/C1C1_ori/")
	{
		for (int i =0; i<argc-2; i++)
		{

			std::string filename = argv[i+2];
			if(getfileinfo(pathDATA, filename)){
				maincut(pathDATA,filename);
			} 
		}
}
//	else if (pathDATA == "/publicfs/atlas/atlasnew/SUSY/users/zhucz/framework/run2Ditau/R21Validation/preselect/")
//      else if(pathDATA == "/publicfs/atlas/atlasnew/SUSY/users/wxin/Sample/gaugino_pair/QCD_prod/bkg_data/")
//  else if(pathDATA == "/publicfs/atlas/atlasnew/SUSY/users/wxin/gaugino_pair/C1C1/HighMass/ABCD/to_right/")
//  else if(pathDATA == "/publicfs/atlas/atlasnew/SUSY/users/wxin/gaugino_pair/C1C1/HighMass/ABCD/to_left/")
   else if(pathDATA == "/publicfs/atlas/atlasnew/SUSY/users/wxin/Sample/gaugino_pair/nom_prod/bkg_data/")
	{
		std::string filename = argv[argc-1]; 
	    std::cout<<filename<<"  "<<argc<<std::endl;
		vector<string> surname(argc-3);
		for (int i =2; i<argc-1; i++)
		{
			std::cout<<argv[i]<<std::endl;
			surname.push_back(argv[i]);
		}
       maincut(pathDATA,surname,filename);

	}
//else if(pathDATA == "/publicfs/atlas/atlasnew/SUSY/users/wxin/gaugino_pair/C1C1/HighMass/abcd/MT1/D/")
else if(pathDATA == "/publicfs/atlas/atlasnew/SUSY/users/wxin/gaugino_pair/C1C1/HighMass/SR/MT1/")
//else if(pathDATA == "/publicfs/atlas/atlasnew/SUSY/users/wxin/gaugino_pair/C1C1/HighMass/ABCD/to_right/")
{
		for (int i =0; i<argc-2; i++)
		{

			std::string filename = argv[i+2];
			if(getfileinfo(pathDATA, filename)){
				maincut(pathDATA,filename);
			} 
		}
}

	std::cout << "******************E N D****************************************" << std::endl;

	return 0;
}

//正则匹配，过滤出root文件
bool getfileinfo(string pathDATA, const string& ip)
{
	regex pattern;
	pattern = "(.*\\.root)";
	std::vector<std::string> result = getinfo(pattern,ip);
	return (result.size() != 0);
}

std::vector<std::string> getinfo(regex pattern, const string& ip)
{
	std::smatch result;
	std::vector<std::string> ret;
	bool valid = regex_match(ip,result,pattern); 
	cout<<ip<<"  "<<(valid?"valid":"invalid")<<endl;
	if(valid)
	{       
			for(unsigned int i = 0; i < result.size(); i++){
				ret.push_back(result[i]);
			}
	}
	return ret;
}

void maincut(string path, string filename)
{
	std::string chainname = "Staus_Nominal";
	TChain *chain = new TChain(chainname.c_str());
	file = datamode + filename;
	std::string rootfile = path + filename;
	chain->Add(rootfile.c_str());

	const char* newfile = file.c_str();

	selector* analysis = new selector(chain);

	std::cout << "**********Start cutting " << filename << "*********" << std::endl;
	analysis->Loop();
	std::cout << "**********End of cutting " << filename << "********" <<std::endl;

	delete analysis;
	delete chain;
}
void maincut(string path, vector<string> filename,const string name)
{
	using namespace std;

	std::string chainname = "Staus_Nominal";
	TChain *chain = new TChain(chainname.c_str());
	file = datamode + name;
	for(int i=0;i<filename.size();i++)
	{
	std::string rootfile = path + filename[i];
	chain->Add(rootfile.c_str());
	}

	const char* newfile = file.c_str();

	selector* analysis = new selector(chain);

	std::cout << "**********Start cutting " << name << "*********" << std::endl;
	analysis->Loop();
	std::cout << "**********End of cutting " << name << "********" <<std::endl;

	delete analysis;
	delete chain;
}

#########################################################################
# File Name: for_abcd.sh
# Author: ma6174
# mail: ma6174@163.com
# Created Time: Wed 17 Jul 2019 04:00:57 PM CST
#########################################################################
#!/bin/bash

root -l -b -q 'compare_zn.C ("mT2")'
root -l -b -q 'compare_zn.C ("Evt_MET")'
root -l -b -q 'compare_zn.C ("tau2pt")'
root -l -b -q 'compare_zn.C ("mT2_70_N_1")'
root -l -b -q 'compare_zn.C ("evt_met_80_N_1")'
#root -l -b -q 'compare_zn.C ("tau2Pt_70_N_1")'

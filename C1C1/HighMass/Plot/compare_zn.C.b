#include "AtlasUtils.C"
#include "AtlasStyle.C"
#include "TROOT.h"
#include "TLorentzVector.h"
#include <TPad.h>
#include <TCanvas.h>
#include <TAxis.h>
#include <TH1F.h>
#include <TF2.h>
#include "TH1.h"
#include <TProfile.h>
#include <TH1D.h>
#include <TLatex.h>
#include <TFile.h>
#include <TLegend.h>

#include <iostream>
#include <math.h>
#include <vector>
#include <fstream>
#include <string>
void os_compare();
TH1F* integralHist(TH1F* hist,string newname);

void compare_zn(const std::string name) {

    double Zn = RooStats::NumberCountingUtils::BinomialExpZ( 27.4575, 10.425267, sqrt(pow(0.3,2)+pow((1.67833/10.425267),2)) );
	cout<<Zn<<endl;
/*  enum{
  	dibosonColor=49,
	WColor=8,
	ZColor=7,
	ttbarColor=42,
	sig1Color=9,
	sig2Color=6,
	sig3Color=2
  };
*/
  int rebinNum=1;
  bool flipZnDirec = false;
  bool showLine = false;
  std::string folder = "../merge/";
  double abeginX(250),amidleY(17),alastX(300);
  enum{
	QCDColor=0,
  	dibosonColor=30,
	WColor=41,
	ZColor=38,
	ttbarColor=49,
	HiggsColor=kMagenta - 5,
	DYColor = kSpring + 9,
	sig1Color=2,
	sig2Color=6,
	sig3Color=9,
	arrowColor=2
  };
  if(name == "tau1pt_N_1"){
		  abeginX=80;     alastX=140;
		  showLine = true;
  }else if(name == "DRtt" || name == "CosSS" || name == "CosSS_N_1" || name == "DPhitt" || name == "JetPt" || name == "JetPt_N_1" || name == "Ljet_n"){
		  flipZnDirec = true;
		  abeginX=3;  amidleY=10;   alastX=2.2;
  } else if(name == "DRtt_N_1"){
		  flipZnDirec = true;
		  abeginX = 3.4; alastX = 2.5; amidleY = 14;
		  showLine = true;
  } else if(name == "DPhitt_N_1"){
//		  flipZnDirec = true;
		  abeginX = 1.4; alastX = 1.8; amidleY = 14;
		  showLine = true;
  } else if(name == "MT12_N_1"){
		  abeginX=420;      alastX=500;
		  showLine = true;
  } else if(name == "MET_sig_tj_N_1"){
		  abeginX=3;     alastX=7;
//		  showLine = true;
  } else if(name == "mT2_N_1"){
  		  abeginX=70; 	alastX=100;
		  showLine = true;
  } else if(name == "Mll_N_1"){
  		  abeginX=100; 	alastX=170;
		  showLine = true;
  }

  SetAtlasStyle();
  TCanvas *canvas = new TCanvas("canvas","canvas",0,0,1000,900);
  canvas->SetLogy();

  TPad*    upperPad = new TPad("upperPad", "upperPad", .001, .25, .995, .995);
  TPad*    lowerPad = new TPad("lowerPad", "lowerPad", .001, .001, .995, .25);
  upperPad->SetBottomMargin(0);
  upperPad->SetLogy();
  upperPad->Draw();
  lowerPad->SetTopMargin(0);
  lowerPad->SetBottomMargin(0.3);
  lowerPad->Draw();

//get file
   TFile* fW_1 = new TFile((folder+"Wjets.root").c_str());
   TFile* fdiboson_1 = new TFile((folder+"MultiBoson.root").c_str());
   TFile* fZtautau = new TFile((folder+"Zjets.root").c_str());
   TFile* fttbar = new TFile((folder+"Top.root").c_str());
//   TFile* fDY = new TFile((folder+"DY.root").c_str());
   TFile* fHiggs = new TFile((folder+"Higgs.root").c_str());
//   TFile* fQCD = new TFile((folder+"QCD_Medium.root").c_str());
   TFile* fSig150 = new TFile((folder+"StauStau_200_1.root").c_str());
//   TFile* fSig300 = new TFile((folder+"Herwigpp_UEEE3_CTEQ6L1_pMSSM_DStau_MSL_200_N1_000_MET100.root").c_str());
//   TFile* fSig500 = new TFile((folder+"Herwigpp_UEEE3_CTEQ6L1_pMSSM_DStau_MSL_300_N1_000_MET100.root").c_str());

//define hist
   TH1F* histfW_1 = (TH1F*)fW_1->Get(name.c_str());
   TH1F* histdiboson_1 = (TH1F*)fdiboson_1->Get(name.c_str());
   TH1F* histZtautau = (TH1F*)fZtautau->Get(name.c_str());
   TH1F* histttbar = (TH1F*)fttbar->Get(name.c_str());
//   TH1F* histQCD = (TH1F*)fQCD->Get(name.c_str());
   TH1F* histHiggs = (TH1F*)fHiggs->Get(name.c_str());
//   TH1F* histDY = (TH1F*)fDY->Get(name.c_str());
   TH1F* histSig150 = (TH1F*)fSig150->Get(name.c_str());
//   TH1F* histSig300 = (TH1F*)fSig300->Get(name.c_str());
//   TH1F* histSig500 = (TH1F*)fSig500->Get(name.c_str());

/*   string newname = "newhist";
   TH1F* histfW_1 = integralHist(rawhistfW_1,newname);
   TH1F* histdiboson_1 = integralHist(rawhistdiboson_1,newname);
   TH1F* histZtautau = integralHist(rawhistZtautau,newname);
   TH1F* histttbar = integralHist(rawhistttbar,newname);
   TH1F* histSig150 = integralHist(rawhistSig150,newname);
   TH1F* histSig300 = integralHist(rawhistSig300,newname);
   TH1F* histSig500 = integralHist(rawhistSig500,newname);
*/
   histfW_1->Sumw2();
   histdiboson_1->Sumw2();
   histZtautau->Sumw2();
   histttbar->Sumw2();
   histHiggs->Sumw2();
//   histDY->Sumw2();
//   histQCD->Sumw2();
   histSig150->Sumw2();
//   histSig300->Sumw2();
//   histSig500->Sumw2();

   histfW_1->Rebin(rebinNum);
   histdiboson_1->Rebin(rebinNum);
   histttbar->Rebin(rebinNum);
   histZtautau->Rebin(rebinNum);
   histHiggs->Rebin(rebinNum);
//   histDY->Rebin(rebinNum);
//   histQCD->Rebin(rebinNum);
   histSig150->Rebin(rebinNum);
//   histSig300->Rebin(rebinNum);
//   histSig500->Rebin(rebinNum);

   THStack *bkg = new THStack("","Distribution");

   TH1F* MCNum = (TH1F*)histdiboson_1->Clone("MCNum");
   MCNum->Sumw2();
   MCNum->Add(histZtautau);
   MCNum->Add(histHiggs);
//   MCNum->Add(histDY);
   MCNum->Add(histttbar);
   MCNum->Add(histfW_1);
//   MCNum->Add(histQCD);

//draw options
   upperPad->cd();

   histfW_1->SetFillColor(WColor);
   histttbar->SetFillColor(ttbarColor);
   histZtautau->SetFillColor(ZColor);
   histdiboson_1->SetFillColor(dibosonColor);
   histHiggs->SetFillColor(HiggsColor);
//   histDY->SetFillColor(DYColor);
//   histQCD->SetFillColor(QCDColor);
   histSig150->SetLineColor(sig1Color);
   histSig150->SetMarkerColor(sig1Color);
   histSig150->SetMarkerStyle(1);
   histSig150->SetLineStyle(2);

/*   histSig300->SetLineColor(sig2Color);
   histSig300->SetMarkerColor(sig2Color);
   histSig300->SetMarkerStyle(1);
   histSig300->SetLineStyle(2);

   histSig500->SetLineColor(sig3Color);
   histSig500->SetMarkerColor(sig3Color);
   histSig500->SetMarkerStyle(1);
   histSig500->SetLineStyle(2);
*/
//   bkg->Add(histDY);
   bkg->Add(histZtautau);
   bkg->Add(histttbar);
   bkg->Add(histfW_1);
   bkg->Add(histHiggs);
   bkg->Add(histdiboson_1);
//   bkg->Add(histQCD);
   bkg->SetMinimum(0.01);
   histSig150->Draw("l");
   histSig150->GetYaxis()->SetRangeUser(0.002,300000);

   bkg->Draw("histsame");

   histSig150->Draw("lsame");
//   histSig300->Draw("lsame");
//   histSig500->Draw("lsame");
   histSig150->GetYaxis()->SetTitle("Events");
   histSig150->GetXaxis()->SetLabelSize(30);
    histSig150->GetXaxis()->SetLabelFont(43);
    histSig150->GetXaxis()->SetTitleOffset(1);    
    histSig150->GetXaxis()->SetTitleSize(30);
    histSig150->GetXaxis()->SetTitleFont(43); 


   MCNum->SetLineColor(0);
   MCNum->SetFillColor(1);
   MCNum->SetMarkerStyle(1);
   MCNum->SetFillStyle(3004);
   MCNum->Draw("e2][same");

   TLine *line =new TLine(abeginX,0,abeginX,amidleY);
   line->SetLineWidth(4);
   line->SetLineStyle(1);
   line->SetLineColor(arrowColor);

   TArrow* arrow = new TArrow(abeginX,amidleY,alastX,amidleY,0.02,"->");
   arrow->SetLineWidth(4);
   arrow->SetLineColor(arrowColor);
   arrow->SetLineStyle(1);

   if(showLine){
   	   line->Draw();
	   arrow->Draw();
   }

  TLatex l5(0.2,0.83,"#sqrt{s} = 13 TeV, 80 fb^{-1}");
  l5.SetNDC(kTRUE);
  l5.SetTextSize(0.034);
  l5.SetTextFont(42);
  l5.Draw("same");

  TLatex l6(0.2,0.88,"#bf{#it{ATLAS}} Internal");
  l6.SetNDC(kTRUE);
  l6.SetTextSize(0.04);
  l6.SetTextFont(42);
  l6.Draw("same");

   TLegend *legend = new TLegend(0.45,0.63,0.74,0.92);
   legend->SetBorderSize(0);
   legend->SetTextFont(42);
   legend->SetTextSize(0.03);
   legend->SetFillColor(0);
   legend->SetLineColor(0);
//   legend->AddEntry(histQCD, "QCD","f");
   legend->AddEntry(histdiboson_1, "diboson","f");
   legend->AddEntry(histHiggs, "Higgs","f");
   legend->AddEntry(histfW_1, "W", "f");
   legend->AddEntry(histttbar, "ttbar","f");
//   legend->AddEntry(histDY, "DYTauTau","f");
   legend->AddEntry(histZtautau, "Z","f");
   legend->AddEntry(MCNum, "Total BKG","f");
   legend->AddEntry(histSig150, "m_{#tilde{#tau}}, m_{#tilde{#chi}_{1}^{0}}) = (200, 1) GeV","pl");
//   legend->AddEntry(histSig300, "m_{#tilde{#tau}}, m_{#tilde{#chi}_{1}^{0}}) = (200, 0) GeV","pl");
//   legend->AddEntry(histSig500, "m_{#tilde{#tau}}, m_{#tilde{#chi}_{1}^{0}}) = (300, 0) GeV","pl");   
   legend->Draw();

   TH1F *hZn1 = (TH1F*)MCNum->Clone("hZn1");
//   TH1F *hZn2 = (TH1F*)MCNum->Clone("hZn2");
//   TH1F *hZn3 = (TH1F*)MCNum->Clone("hZn3");
   Int_t NBins = hZn1->GetXaxis()->GetNbins();

   if(flipZnDirec){
   		for (int ibin=1; ibin<=NBins; ibin++){
       		double errb =0; 
  //     		double errsig3 =0; 
       		double errsig1 =0; 
  //     		double errsig2=0;
          
       		double s1 = histSig150->IntegralAndError(1,ibin,errsig1 );
//       		double s2 = histSig300->IntegralAndError(1,ibin,errsig2 );
//       		double s3 = histSig500->IntegralAndError(1,ibin,errsig3 );
       		double b = MCNum->IntegralAndError(1,ibin,errb );
      		if ( b <= 0 ){
           		hZn1->SetBinContent( ibin,0 );
  //         		hZn2->SetBinContent( ibin,0 );
  //         		hZn3->SetBinContent( ibin,0 );
        	} else{
           		//double ZnForPlot->RooStats::NumberCountingUtils::BinomialExpZ(s,b,sqrt(pow(0.3,2)+pow((err_b/b),2)));
           		double Zn1 = RooStats::NumberCountingUtils::BinomialExpZ( s1, b, sqrt(pow(0.3,2)+pow((errb/b),2)) );
    //       		double Zn2 = RooStats::NumberCountingUtils::BinomialExpZ( s2, b, sqrt(pow(0.3,2)+pow((errb/b),2)) );
    //       		double Zn3 = RooStats::NumberCountingUtils::BinomialExpZ( s3, b, sqrt(pow(0.3,2)+pow((errb/b),2)) );
              
           		hZn1->SetBinContent( ibin,Zn1 );
    //       		hZn2->SetBinContent( ibin,Zn2 );
    //       		hZn3->SetBinContent( ibin,Zn3 );
           		if (s1 <= 0){
            		hZn1->SetBinContent( ibin,-3 );
           		}
/*           		if (s2 <= 0){
         		   	hZn2->SetBinContent( ibin,-3 );
           		}
           		if(s3 <= 0){
            		hZn3->SetBinContent( ibin,-3 );
           		}*/
       		}   
   		}         
   	}else{
	   for (int ibin=1; ibin<=NBins; ibin++){
			double errb =0; 
//			double errsig3 =0; 
			double errsig1 =0;
//			double errsig2=0;
      
       		double s1 = histSig150->IntegralAndError(ibin,NBins+1,errsig1 );
//       		double s2 = histSig300->IntegralAndError(ibin,NBins+1,errsig2 );
//       		double s3 = histSig500->IntegralAndError(ibin,NBins+1,errsig3 );
       		double b = MCNum->IntegralAndError(ibin,NBins+1,errb );
       		if ( b <= 0 ){
           		hZn1->SetBinContent( ibin,0 );
  //         		hZn2->SetBinContent( ibin,0 );
  //         		hZn3->SetBinContent( ibin,0 );
        		} else{
		   		//double ZnForPlot->RooStats::NumberCountingUtils::BinomialExpZ(s,b,sqrt(pow(0.3,2)+pow((err_b/b),2)));
           		double Zn1 = RooStats::NumberCountingUtils::BinomialExpZ( s1, b, sqrt(pow(0.3,2)+pow((errb/b),2)) );
  //         		double Zn2 = RooStats::NumberCountingUtils::BinomialExpZ( s2, b, sqrt(pow(0.3,2)+pow((errb/b),2)) );
  //         		double Zn3 = RooStats::NumberCountingUtils::BinomialExpZ( s3, b, sqrt(pow(0.3,2)+pow((errb/b),2)) );
          
           		hZn1->SetBinContent( ibin,Zn1 );
    //       		hZn2->SetBinContent( ibin,Zn2 );
    //       		hZn3->SetBinContent( ibin,Zn3 );
		   		if (s1 <= 0){
		   			hZn1->SetBinContent( ibin,-3 );
		   		}
	/*	   		if (s2 <= 0){
		   			hZn2->SetBinContent( ibin,-3 );
			   }
			   if(s3 <= 0){
		   			hZn3->SetBinContent( ibin,-3 );
		   		}*/
       		}   
   		}
   }

   lowerPad->cd();
   lowerPad->SetGridy();

    hZn1->SetLineColor(sig1Color);
	hZn1->SetTitle("");
	hZn1->SetFillStyle(0);
    hZn1->SetMarkerSize(.8);
    hZn1->Draw("hist");
    gPad->RedrawAxis("sameaxis");
    hZn1->GetYaxis()->SetRangeUser(0,5);
	std::string xaxis = name;
	if(name == "tau1pt" || name == "tau1pt_N_1"){
		xaxis = "p_{T#tau1} [GeV]";
	} else if (name == "tau2pt" || name == "tau2_pt_N_1"){
		xaxis = "p_{T#tau2} [GeV]";
	} else if (name == "Evt_MET" || name == "Evt_MET_N_1"){
		xaxis = "E_{T}^{miss} [GeV]";
	} else if (name == "DRtt" || name == "DRtt_N_1"){
		xaxis = "#Delta R_{#tau#tau}";
	} else if (name == "Mll" || name == "Mll_N_1"){
		xaxis = "M_{#tau#tau} [GeV]";
	} else if (name == "MT12" || name == "MT12_N_1"){
		xaxis = "m_{T#tau1}+m_{T#tau2} [GeV]";
	} else if (name == "mT2" || name == "mT2_N_1"){
		xaxis = "m_{T2} [GeV]";
	} else if (name == "MET_sig_tj_N_1" || name == "MET_sig_tj"){
		xaxis = "E_{T}^{miss} Sig_{taujet} [#sqrt{GeV}]";
	} else if (name == "DPhitt" || name == "DPhitt_N_1"){
		xaxis = "#Delta #phi_{#tau#tau}";
	} else if (name == "meff_tau"){
		xaxis = "M_{eff} [GeV]";
	}
	hZn1->GetXaxis()->SetTitle(xaxis.c_str());
    //hZn1->GetXaxis()->SetNdivisions(404);
    hZn1->GetXaxis()->SetLabelSize(30);
    hZn1->GetXaxis()->SetLabelFont(43);
    hZn1->GetXaxis()->SetTitleOffset(4);
    hZn1->GetXaxis()->SetTitleSize(25);
    hZn1->GetXaxis()->SetTitleFont(43);

    hZn1->GetYaxis()->SetTitle("Zn");
    hZn1->GetYaxis()->SetNdivisions(505);
    hZn1->GetYaxis()->SetLabelSize(30);
    hZn1->GetYaxis()->SetLabelFont(43);
    hZn1->GetYaxis()->SetTitleOffset(1.55);
    hZn1->GetYaxis()->SetTitleSize(30);
    hZn1->GetYaxis()->SetTitleFont(43);

/*    hZn2->SetLineColor(sig2Color);
	hZn2->SetFillStyle(0);
    hZn2->SetMarkerSize(.8);
    hZn2->Draw("histsame");

    hZn3->SetLineColor(sig3Color);
	hZn3->SetFillStyle(0);
    hZn3->SetMarkerSize(.8);
    hZn3->Draw("histsame");
*/
   canvas->SetTitle(name.c_str());
   gStyle->SetTitleFontSize(0.08);
   const std::string saveName1 = name + ".png";
   const std::string saveName2 = name + ".pdf";
   canvas->SaveAs(saveName1.c_str());
   canvas->SaveAs(saveName2.c_str());
}

TH1F* integralHist(TH1F* hist,string newname){
	TH1F* newhist = (TH1F*)hist->Clone(newname.c_str());
	for(int i = 0; i <= hist->GetNbinsX(); i++){
		double histbin(0),histErr(0);
		histbin = hist->IntegralAndError(i, (hist->GetNbinsX() + 1), histErr);
		if(histbin < 0) histbin = 0;
		newhist->SetBinContent(i,histbin);
		newhist->SetBinError(i,histErr);
	}
	return newhist;
}

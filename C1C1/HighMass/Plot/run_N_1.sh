#########################################################################
# File Name: run_N_1.sh
# Author: 123
# mail: xin.wang@smail.nju.edu.cn
# Created Time: Tue 08 Jan 2019 10:07:49 AM CST
#########################################################################
#!/bin/bash

root -l -b -q 'compare_zn.C ("evt_met_80_N_1")'
root -l -b -q 'compare_zn.C ("mT2_80_N_1")'
root -l -b -q 'compare_zn.C ("mt12tau_550_N_1")'
root -l -b -q 'compare_zn.C ("tau1Pt_N_1")'
root -l -b -q 'compare_zn.C ("tau2Pt_N_1")'
root -l -b -q 'compare_zn.C ("DRtt_N_1")'
root -l -b -q 'compare_zn.C ("DPhitt_N_1")'
root -l -b -q 'compare_zn.C ("mll_12_N_1")'

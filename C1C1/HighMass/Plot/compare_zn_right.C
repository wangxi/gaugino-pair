#include "AtlasUtils.C"
#include "AtlasStyle.C"
#include "TROOT.h"
#include "TLorentzVector.h"
#include <TPad.h>
#include <TCanvas.h>
#include <TAxis.h>
#include <TH1F.h>
#include <TF2.h>
#include "TH1.h"
#include <TProfile.h>
#include <TH1D.h>
#include <TLatex.h>
#include <TFile.h>
#include <TLegend.h>
#include <map>

#include <iostream>
#include <math.h>
#include <vector>
#include <fstream>
#include <string>
int rebinNum=1;

enum{
  QCDColor = 0,
  dibosonColor=30,
  WColor=41,
  ZColor=38,
  ttbarColor=49,
  higgsColor=kViolet-8,
  sig1Color=9,
  sig2Color=6,
  sig3Color=2,
  dataColor=1,
  arrowColor=2
};

enum{
	DATA = 0,
	BKG = 1,
	SIGNAL = 2
};

void os_compare();
TH1F* integralHist(TH1F* hist,string newname);
TH1F* getHist(string file, string name, int type = BKG, int color = dataColor);
void myText(Double_t x,Double_t y,Color_t color,string text, Double_t tsize);


void compare_zn(const std::string name) {
  
  bool flipZnDirec = false;
  bool showLine = false;
  std::string folder = "../pre-2tight/";
  double abeginX(250),amidleY(20),alastX(300);
  double legenX1S(0.2);
  double legenX2S(0.55),legenX2E(0.70);
  double legenX3S(0.55),legenX3E(0.92);
  double legenX4S(0.60),legenX4E(0.85);
   string yTitile = "Events / 10 GeV";
  if(name == "tau1Pt_N_1"){
		  abeginX=200;     alastX=280;
		  yTitile = "Events / 50 GeV";
//		  showLine = true;
  }else if(name == "tau2Pt_N_1"){
	      abeginX=75;     alastX=150;
//		  showLine = true;
		  yTitile = "Events / 25 GeV";
  }else if(name == "DRtt" || name == "DRttP" || name == "DPhittP" || name == "DPhittP1" || name == "Ljet_pt_N_1" || name == "Ljet_n"){
		  flipZnDirec = true;
		  abeginX=3;  amidleY=70;   alastX=2.2;
  } else if(name == "DRtt_N_1"){
		  flipZnDirec = true;
		  abeginX = 3.2; alastX = 2.5; amidleY = 9;
		  showLine = true;
		  yTitile = "Events / 0.2";
  } else if(name == "DPhitt_N_1"){
//		  flipZnDirec = true;
//		  rebinNum = 2;
		  abeginX = 1.4; alastX = 2; amidleY = 20;
		  showLine = true;
		  yTitile = "Events / 0.4";
//		  legenX1S = 0.567;
//		  legenX2S = 0.2; legenX2E = 0.38;
//		  legenX3S = 0.2; legenX3E = 0.56;
//		  legenX4S = 0.2; legenX4E = 0.5;
  } else if(name == "MT12_N_1"){
		  abeginX=800;      alastX=1000;
//		  showLine = true;
		  yTitile = "Events / 400 GeV";
  } else if(name == "MET_sig_tj_N_1"){
		  abeginX=3;     alastX=7;
//		  showLine = true;
  } else if(name == "mT2_N_1"){
  		  abeginX=70; 	alastX=120;
		  showLine = true;
		  yTitile = "Events / 40 GeV";
  } else if(name == "MET_sig_tj_N_1"){
  } else if(name == "Evt_MET_N_1"){
  		  abeginX=75; 	alastX=120; amidleY = 10;
		  showLine = true;
		  yTitile = "Events / 25 GeV";
  } else if(name == "Ljet_pt_N_1"){
		  flipZnDirec = true;		  
		  rebinNum = 10;
		  abeginX=10;  alastX=0;
//		  showLine = true;
		  yTitile = "Events / 10 GeV";
  }


  SetAtlasStyle();
  TCanvas *canvas = new TCanvas("canvas","canvas",0,0,1000,800);
  canvas->SetLogy();

  TPad*    upperPad = new TPad("upperPad", "upperPad", .001, .25, 1.0, 1.0);
  TPad*    lowerPad = new TPad("lowerPad", "lowerPad", .001, .001, 1.0, .25);
  upperPad->SetBottomMargin(0.01);
  upperPad->SetLeftMargin(0.12);
  upperPad->SetRightMargin(0.05);
  upperPad->Draw();
  upperPad->SetLogy();
  lowerPad->SetTopMargin(0);
  lowerPad->SetBottomMargin(0.47);
  lowerPad->SetLeftMargin(0.12);
  lowerPad->SetRightMargin(0.05);
  lowerPad->Draw();

//get file

   TH1F* histfW_1 = getHist(folder+"Wjets.root",name,BKG,WColor);
   TH1F* histmultiboson  = getHist(folder+"MultiBoson.root",name,BKG,dibosonColor);
   TH1F* histttbar = getHist(folder+"Top.root",name,BKG,ttbarColor);
   TH1F* histZtautau = getHist(folder+"Zjets.root",name,BKG,ZColor);
   TH1F* histHiggs = getHist(folder+"Higgs.root",name,BKG,higgsColor);
   TH1F* histQCD = getHist(folder+"QCD.root",name,BKG,QCDColor);
   TH1F* histdata = getHist(folder+"data.root",name,DATA,dataColor);
   TH1F* histsig1 = getHist(folder+"StauStau_100_1.root",name,SIGNAL,sig1Color);
   TH1F* histsig2 = getHist(folder+"StauStau_200_1.root",name,SIGNAL,sig2Color);
   TH1F* histsig3 = getHist(folder+"StauStau_280_1.root",name,SIGNAL,sig3Color);

//   TH1F* histQCD = getHist(folder2+"data.root",name,BKG,QCDColor);
//   histQCD->Scale(0.008480);
/*
    for(int i = 0; i <= histQCD->GetNbinsX(); i++){
         // relative bin_err = bin_err/bin_content
         // relative bin_err(scaled) = (a_err/a_content) * (a_content/b_content) = a_err/b_content
         if(histQCD->GetBinContent(i) != 0){
			 double oldErr = histQCD->GetBinError(i);
			 double newError = sqrt(oldErr*oldErr+histQCD->GetBinContent(i)*histQCD->GetBinContent(i));
             histQCD->SetBinError(i,newError); 
         }
    }
*/

   TH1F* MCNum = (TH1F*)histmultiboson->Clone("MCNum");
   MCNum->Sumw2();
   MCNum->Add(histHiggs);
   MCNum->Add(histttbar);
   MCNum->Add(histZtautau);
   MCNum->Add(histfW_1);
   MCNum->Add(histQCD);

//draw options
   upperPad->cd();

//   histdata->Draw("Ep");

   THStack *bkg = new THStack("","Distribution");
   bkg->Add(histHiggs);
   bkg->Add(histZtautau);
   bkg->Add(histttbar);
   bkg->Add(histfW_1);
   bkg->Add(histmultiboson);
   bkg->Add(histQCD);

   histsig1->Draw("l");
   bkg->Draw("histsame");
   
   histdata->Draw("Epsame");
   histdata->GetYaxis()->SetTitle("Events");
   histdata->GetXaxis()->SetLabelSize(30);
    histdata->GetXaxis()->SetLabelFont(43);

	histsig1->GetYaxis()->SetRangeUser(0.002,1000000000);
    histsig1->GetYaxis()->SetTitle("Events");
    histsig1->GetYaxis()->SetLabelSize(0.07);
    histsig1->GetXaxis()->SetLabelOffset(0.005);
    histsig1->GetYaxis()->SetTitleOffset(0.9);    
    histsig1->GetYaxis()->SetTitleSize(0.07);

    histdata->GetXaxis()->SetTitleOffset(1);    
    histdata->GetXaxis()->SetTitleSize(30);
    histdata->GetXaxis()->SetTitleFont(43); 


   histsig1->Draw("lsame");
   histsig2->Draw("lsame");
   histsig3->Draw("lsame");
   MCNum->SetLineColor(0);
   MCNum->SetFillColor(1);
   MCNum->SetMarkerStyle(1);
   MCNum->SetFillStyle(3004);
   MCNum->Draw("e2][same");

   TLine *line =new TLine(abeginX,0,abeginX,amidleY);
   line->SetLineWidth(4);
   line->SetLineStyle(1);
   line->SetLineColor(arrowColor);

   TArrow* arrow = new TArrow(abeginX,amidleY,alastX,amidleY,0.02,"->");
   arrow->SetLineWidth(4);
   arrow->SetLineColor(arrowColor);
   arrow->SetLineStyle(1);

   if(showLine){
	line->Draw();
   	arrow->Draw();
   }


  myText(      0.17,0.76, 1, "#sqrt{s} = 13 TeV, 128 fb^{-1}", 0.06);

  TLatex l; 
  l.SetNDC();
  l.SetTextFont(72); l.SetTextSize(0.06); 
  l.DrawLatex(0.17, 0.84,"ATLAS");
  TLatex p; 
  p.SetNDC();
  p.SetTextFont(42); p.SetTextSize(0.06); 
  //p.DrawLatex(x+0.1,y,"Simulation Preliminary");
  p.DrawLatex(0.17+0.12,0.84,"Internal");
 
   TLegend *legendMC = new TLegend(legenX2S,0.85,legenX2E,0.93);
   legendMC->AddEntry(MCNum, "Total SM","f");
   legendMC->SetBorderSize(0);
   legendMC->SetTextFont(42);
   legendMC->SetTextSize(0.055);
   //legend->SetTextSize(0.03);
   legendMC->SetFillColor(0);
   legendMC->SetLineColor(0);
   legendMC->Draw("same");

   TLegend *legend = new TLegend(legenX3S,0.65,legenX3E,0.85);
   legend->SetBorderSize(0);
   legend->SetTextFont(42);
   legend->SetTextSize(0.055);
   //legend->SetTextSize(0.03);
   legend->SetFillColor(0);
   legend->SetLineColor(0);
   legend->SetNColumns(2);
   legend->AddEntry(histQCD, "Multi-jet","f");
   legend->AddEntry(histmultiboson, "multi-boson","f");
   legend->AddEntry(histfW_1, "W+jets", "f");
   legend->AddEntry(histttbar, "Top","f");
//   legend->AddEntry(histDY, "DYTauTau","f");
   legend->AddEntry(histHiggs, "Higgs","f");
   legend->AddEntry(histZtautau, "Z+jets","f");
   legend->AddEntry(histdata, "data","f");

   TLegend *legend2 = new TLegend(0.67,0.48,0.87,0.64);
   legend2->SetBorderSize(0);
   legend2->SetTextFont(42);
   legend2->SetTextSize(0.05);
   legend2->SetFillColor(0);
   legend2->SetLineColor(0);
   TLatex l7(0.5,0.60,"m_{#tilde{#tau}}, m_{#tilde{#chi}_{1}^{0}} =");
   l7.SetNDC(kTRUE);
   l7.SetTextSize(0.05);
   //l6.SetTextSize(0.04);
   l7.SetTextFont(42);
   legend2->AddEntry(histsig1, "(100, 1) GeV","pl");
   legend2->AddEntry(histsig2, "(200, 1) GeV","pl");
   legend2->AddEntry(histsig3, "(280, 1) GeV","pl");
   
   legend->Draw();
   legend2->Draw();
   l7.Draw("same");

  /*
   TLegend *legend3 = new TLegend(0.45,0.44,0.60,0.60);
   legend3->SetBorderSize(0);
   legend3->SetTextFont(42);
   legend3->SetTextSize(0.05);
   legend3->SetFillColor(0);
   legend3->SetLineColor(0);
   legend3->AddEntry(histsig, "(200, 1) GeV","pl");   
   legend3->Draw("same");
*/

/*
  TLatex l5(0.2,0.83,"#sqrt{s} = 13 TeV, 128 fb^{-1}");
  l5.SetNDC(kTRUE);
  l5.SetTextSize(0.034);
  l5.SetTextFont(42);
  l5.Draw("same");

  TLatex l6(0.2,0.88,"#bf{#it{ATLAS}} Internal");
  l6.SetNDC(kTRUE);
  l6.SetTextSize(0.04);
  l6.SetTextFont(42);
  l6.Draw("same");

   TLegend *legend = new TLegend(0.45,0.63,0.74,0.92);
//   TLegend *legend = new TLegend(0.2,0.53,0.45,0.8);
   legend->SetBorderSize(0);
   legend->SetTextFont(42);
   legend->SetTextSize(0.03);
   legend->SetFillColor(0);
   legend->SetLineColor(0);
//   legend->AddEntry(histdata, "data","pl");
   legend->AddEntry(histttbar, "ttbar","f");
   legend->AddEntry(histfW_1, "W", "f");
   legend->AddEntry(histdiboson_1, "Multi-Boson","f");
   legend->AddEntry(histZtautau, "Z","f");
   legend->AddEntry(histHiggs, "Higgs","f");
   legend->AddEntry(histQCD, "QCD","f");
   legend->AddEntry(MCNum, "Total BKG","f");
   legend->AddEntry(histsig, "m_{#tilde{#tau}}, m_{#tilde{#chi}_{1}^{0}}) = (200, 1) GeV","pl");
   legend->Draw();
*/



   TH1F *hZn1 = (TH1F*)MCNum->Clone("hZn1");
   TH1F *hZn2 = (TH1F*)MCNum->Clone("hZn2");
   TH1F *hZn3 = (TH1F*)MCNum->Clone("hZn3");
   Int_t NBins = hZn1->GetXaxis()->GetNbins();

   if(flipZnDirec)
   {
   		for (int ibin=1; ibin<=NBins; ibin++)
		{
       		double errb =0; 
       		double errsig1 =0; 
       		double errsig2=0;
       		double errsig3 =0; 
          
       		double s1 = histsig1->IntegralAndError(1,ibin,errsig1 );
       		double s2 = histsig2->IntegralAndError(1,ibin,errsig2 );
       		double s3 = histsig3->IntegralAndError(1,ibin,errsig3 );
       		double b = MCNum->IntegralAndError(1,ibin,errb );
      		if ( b <= 0 )
			{
           		hZn1->SetBinContent( ibin,0 );
           		hZn2->SetBinContent( ibin,0 );
           		hZn3->SetBinContent( ibin,0 );
        	} 
			else
			{
           // 		double ZnForPlot->RooStats::NumberCountingUtils::BinomialExpZ(s,b,sqrt(pow(0.3,2)+pow((err_b/b),2)));
           		double Zn1 = RooStats::NumberCountingUtils::BinomialExpZ( s1, b, sqrt(pow(0.3,2)+pow((errb/b),2)) );
           		double Zn2 = RooStats::NumberCountingUtils::BinomialExpZ( s2, b, sqrt(pow(0.3,2)+pow((errb/b),2)) );
           		double Zn3 = RooStats::NumberCountingUtils::BinomialExpZ( s3, b, sqrt(pow(0.3,2)+pow((errb/b),2)) );
              
           		hZn1->SetBinContent( ibin,Zn1 );
           		hZn2->SetBinContent( ibin,Zn2 );
           		hZn3->SetBinContent( ibin,Zn3 );
           		if (s1 <= 0)
				{
            		hZn1->SetBinContent( ibin,-3 );
           		}
           		if (s2 <= 0)
				{
         		   	hZn2->SetBinContent( ibin,-3 );
           		}
           		if(s3 <= 0){
            		hZn3->SetBinContent( ibin,-3 );
           		}
       		}   
   		}         
   	}
   else
   {
	   for (int ibin=1; ibin<=NBins; ibin++)
	   {
			double errb =0; 
			double errsig1 =0;
			double errsig2=0;
			double errsig3 =0; 
      
       		double s1 = histsig1->IntegralAndError(ibin,NBins+1,errsig1 );
       		double s2 = histsig2->IntegralAndError(ibin,NBins+1,errsig2 );
       		double s3 = histsig3->IntegralAndError(ibin,NBins+1,errsig3 );
			double b = MCNum->IntegralAndError(ibin,NBins+1,errb );
			if ( b <= 0 )
			{
				hZn1->SetBinContent( ibin,0 );
				hZn2->SetBinContent( ibin,0 );
				hZn3->SetBinContent( ibin,0 );
			}
			else
			{
		   		//double ZnForPlot->RooStats::NumberCountingUtils::BinomialExpZ(s,b,sqrt(pow(0.3,2)+pow((err_b/b),2)));
           		double Zn1 = RooStats::NumberCountingUtils::BinomialExpZ( s1, b, sqrt(pow(0.3,2)+pow((errb/b),2)) );
           	 	double Zn2 = RooStats::NumberCountingUtils::BinomialExpZ( s2, b, sqrt(pow(0.3,2)+pow((errb/b),2)) );
           		double Zn3 = RooStats::NumberCountingUtils::BinomialExpZ( s3, b, sqrt(pow(0.3,2)+pow((errb/b),2)) );
          
            	hZn1->SetBinContent( ibin,Zn1 );
           		hZn2->SetBinContent( ibin,Zn2 );
           		hZn3->SetBinContent( ibin,Zn3 );
	 	   		if (s1 <= 0){
		   			hZn1->SetBinContent( ibin,-3 );
		   		}
		   		if (s2 <= 0){
		   			hZn2->SetBinContent( ibin,-3 );
			   }
			   if(s3 <= 0){
 		   			hZn3->SetBinContent( ibin,-3 );
	 	   		}
       		}   
   		}
   }

   lowerPad->cd();
   lowerPad->SetGridy();

    hZn1->SetLineColor(sig1Color);
	hZn1->SetTitle("");
	hZn1->SetFillStyle(0);
    hZn1->SetMarkerSize(.8);
    hZn1->Draw("hist");

    hZn2->SetLineColor(sig2Color);
	hZn2->SetTitle("");
	hZn2->SetFillStyle(0);
    hZn2->SetMarkerSize(.8);
    hZn2->Draw("histsame");
    
	
    hZn3->SetLineColor(sig3Color);
	hZn3->SetTitle("");
	hZn3->SetFillStyle(0);
    hZn3->SetMarkerSize(.8);
    hZn3->Draw("histsame");

	hZn1->GetYaxis()->SetRangeUser(0,2);
	std::string xaxis = name;
    if(name == "tau1pt" || name == "tau1pt_N_1"){
        xaxis = "p_{T#tau} [GeV]";
    } else if (name == "tau2pt" || name == "tau2pt_N_1"){
        xaxis = "p_{T#tau2} [GeV]";
    } else if (name == "mu1Pt" || name == "mu1Pt_pt_N_1"){
        xaxis = "p_{T#mu} [GeV]";
    } else if (name == "Evt_MET" || name == "Evt_MET_N_1"){
        xaxis = "E_{T}^{miss} [GeV]";
    } else if (name == "DRtt" || name == "DRtt_N_1"){
        xaxis = "#Delta R_{#tau#tau}";
    } else if (name == "DPhitt_N_1" || name == "DPhitt"){
        xaxis = "#Delta #phi_{#tau#tau}";
    } else if (name == "dRtm" || name == "dRtm_N_1"){
        xaxis = "#Delta R_{#tau#mu}";
    } else if (name == "Mll_12" || name == "Mll_N_1"){
        xaxis = "M_{#tau#tau} [GeV]";
    } else if (name == "MT12" || name == "MT12_N_1"){
        xaxis = "m_{T#tau1}+m_{T#tau2} [GeV]";
    } else if (name == "mT2" || name == "mT2_N_1" || name == "MT21_N_1"){
        xaxis = "m_{T2} [GeV]";
    } else if (name == "met_sig_tj_N_1" || name == "met_sig_tj"){
        xaxis = "E_{T}^{miss} Sig_{taujet} [#sqrt{GeV}]";
    } else if (name == "dPhitm"){
		xaxis = "#Delta #phi_{#tau#mu}";
    } else if (name == "meff_tau" || name == "meff"){
		xaxis = "M_{eff} [GeV]";
    } else if (name == "muMt"){
		xaxis = "M_{T#mu} [GeV]";
    } else if (name == "tau1Mt"){
		xaxis = "M_{T#tau1} [GeV]";
    } else if (name == "tau2Mt"){
		xaxis = "M_{T#tau2} [GeV]";
    } else if (name == "tauMt"){
		xaxis = "M_{T#tau} [GeV]";
    }
	hZn1->GetXaxis()->SetTitle(xaxis.c_str());
    hZn1->GetXaxis()->SetLabelSize(0.2);
    hZn1->GetXaxis()->SetTitleOffset(1.1);
    hZn1->GetXaxis()->SetTitleSize(0.2);
	hZn1->GetXaxis()->SetLabelOffset(0.02);

    hZn1->GetYaxis()->SetTitle("Z_{N} (#sigma_{bg}=30%)");
    hZn1->GetYaxis()->SetNdivisions(505);
    hZn1->GetYaxis()->SetLabelSize(0.18);
    hZn1->GetYaxis()->SetTitleOffset(0.3);
	hZn1->GetYaxis()->SetLabelOffset(0.02);
    hZn1->GetYaxis()->SetTitleSize(0.16);
	hZn1->GetYaxis()->SetNdivisions(503);
 
   canvas->SetTitle(name.c_str());
   gStyle->SetTitleFontSize(0.08);
   const std::string saveName1 =  name + ".png";
   const std::string saveName2 =  name + ".pdf";
   canvas->SaveAs(saveName1.c_str());
   canvas->SaveAs(saveName2.c_str());
   
   delete histdata;
   delete histsig1;
   delete histsig2;
   delete histsig3;
}

TH1F* integralHist(TH1F* hist,string newname){
	TH1F* newhist = (TH1F*)hist->Clone(newname.c_str());
	for(int i = 0; i <= hist->GetNbinsX(); i++){
		double histbin(0),histErr(0);
		histbin = hist->IntegralAndError(i, (hist->GetNbinsX() + 1), histErr);
		if(histbin < 0) histbin = 0;
		newhist->SetBinContent(i,histbin);
		newhist->SetBinError(i,histErr);
	}
	return newhist;
}

TH1F* getHist(string file, string name, int type, int color){
   TFile* tfile = new TFile(file.c_str());
   TH1F* hist = (TH1F*)tfile->Get(name.c_str());
   hist->Sumw2();
   hist->Rebin(rebinNum);

   if(type == BKG){
		hist->SetFillColor(color);
   }else if(type == SIGNAL){
		hist->SetMarkerColor(color);
		hist->SetMarkerStyle(1);
		hist->SetLineColor(color);
		hist->SetLineStyle(2);
   }else{
		hist->SetMarkerStyle(20);
		hist->SetLineColor(color);
		hist->SetMarkerColor(color);
   }
   return hist;
}

void myText(Double_t x,Double_t y,Color_t color,string text, Double_t tsize) {

  //Double_t tsize=0.05;
  TLatex l; l.SetTextAlign(12); l.SetTextSize(tsize); 
  l.SetNDC();
  l.SetTextColor(color);
  l.DrawLatex(x,y,text.c_str());
}


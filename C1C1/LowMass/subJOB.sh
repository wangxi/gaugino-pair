#rm cutflow.csv yeild.csv
path_1="/publicfs/atlas/atlasnew/SUSY/users/wxin/Sample/gaugino_pair/nom_prod/bkg_data/"
path_11="/publicfs/atlas/atlasnew/SUSY/users/wxin/Sample/gaugino_pair/nom_prod/C1C1/C1C1_ori/"
#path_1="/publicfs/atlas/atlasnew/SUSY/users/wxin/Sample/gaugino_pair/QCD_prod/bkg_data/"
#path_11="/publicfs/atlas/atlasnew/SUSY/users/wxin/Sample/gaugino_pair/QCD_prod/C1C1/"
#path_1="/publicfs/atlas/atlasnew/SUSY/users/zhucz/framework/run2Ditau/R21Validation/preselect/"
#path3="/publicfs/atlas/atlasnew/SUSY/users/wxin/gaugino_pair/C1C1/LowMass/abcd/T2/D/"
#path_11="/publicfs/atlas/atlasnew/SUSY/users/wxin/C1C1/Sample/signal/"
'''for ABCD'''
#path3="/publicfs/atlas/atlasnew/SUSY/users/wxin/gaugino_pair/C1C1/LowMass/ABCD/to_left/"
path3="/publicfs/atlas/atlasnew/SUSY/users/wxin/gaugino_pair/C1C1/LowMass/abcd/T2_2/d/"
#path3="/publicfs/atlas/atlasnew/SUSY/users/wxin/gaugino_pair/C1C1/LowMass/abcd/T2/D/"
#path3="/publicfs/atlas/atlasnew/SUSY/users/wxin/gaugino_pair/C1C1/LowMass/SR/T2_2/"
unset ALRB_infoProc
cd build/
rm -r *
cmake ../
make


#rm ../abcd/T2_2/c/*
#mv selector_ana ../abcd/T2_2/c/
#cd ../abcd/T2_2/c

#rm ../abcd/T2/B/*
#mv selector_ana ../abcd/T2/B/
#cd ../abcd/T2/B

#rm ../ABCD/E/*
#mv selector_ana ../ABCD/E
#cd ../ABCD/E/

#rm ../old/*
#mv selector_ana ../old
#cd ../old

#rm ../PRE/T2_old/*
#mv selector_ana ../PRE/T2_old
#cd ../PRE/T2_old

#rm ../valid_abcd/left_SS/*
#mv selector_ana ../valid_abcd/left_SS
#cd ../valid_abcd/left_SS

#rm ../SR/T2_2/*
#mv selector_ana ../SR/T2_2
#cd ../SR/T2_2

rm ../123/*
mv selector_ana ../123
cd ../123

#rm ../3/*
#mv selector_ana ../3
#cd ../3
################################################
chmod u+x selector_ana
rm *log
#################################################
#for bkg
#for new
files1=`ls ${path_1} | grep Multi`
files2=`ls ${path_1} | grep Top`
files3=`ls ${path_1} | grep Wjets`
files4=`ls ${path_1} | grep Zjets`
files5=`ls ${path_1} | grep Higgs`
files6=`ls ${path_1} | grep data`
#for old
#files1=`ls ${path_1} | grep VV`
#files2=`ls ${path_1} | grep _T`
#files3=`ls ${path_1} | grep W`
#files4=`ls ${path_1} | grep Z`
#files5=`ls ${path_1} | grep Higgs`
#files6=`ls ${path_1} | grep data`

MultiBoson="MultiBoson"
Top="Top"
Wjets="Wjets"
Zjets="Zjets"
Higgs="Higgs"
data="data"
###############################################
#for Wjets
#files_emu=`ls ${path1} | grep -E "We|Wmu"`
#files_tau=`ls ${path1} | grep "Wtau"`
#files_all=`ls ${path1} | grep "W"`
Wemu="Wemu"
Wtau="Wtau"
##############################################
#for sig

files_sig=`ls ${path_11} | grep .root`
################################################
# for other
files=`ls ${path3} | grep root`
#./selector_ana  $path3 $files
###############################################

#./selector_ana  $path2 $files_sig
#for all

#echo ./selector_ana  $path_1 $files3 $Wjets \>\> bkglog > start3.sh
#chmod u+x start3.sh
#./start3.sh

echo ./selector_ana  $path_1 $files1 $MultiBoson \>\> bkglog > start1.sh
chmod u+x start1.sh
hep_sub start1.sh
#./start1.sh
'''
echo ./selector_ana  $path_1 $files2 $Top \>\> bkglog > start2.sh
chmod u+x start2.sh
hep_sub start2.sh
#./start2.sh

echo ./selector_ana  $path_1 $files3 $Wjets \>\> bkglog > start3.sh
chmod u+x start3.sh
hep_sub start3.sh
#./start3.sh

echo ./selector_ana  $path_1 $files4 $Zjets \>\> bkglog > start4.sh
chmod u+x start4.sh
hep_sub start4.sh
#./start4.sh

echo ./selector_ana  $path_1 $files5 $Higgs \>\> bkglog > start5.sh
chmod u+x start5.sh
hep_sub start5.sh
#./start5.sh

echo ./selector_ana  $path_1 $files6 $data \>\> bkglog > start6.sh
chmod u+x start6.sh
hep_sub start6.sh
#./start6.sh

#for signal

echo ./selector_ana  $path_11 $files_sig \>\> siglog > start_sig.sh
chmod u+x start_sig.sh
hep_sub start_sig.sh
#./start_sig.sh
'''
#./selector_ana  $path1 $files1
#./selector_ana  $path2 $files2
#./selector_ana  $path1 $files3 $MultiBoson

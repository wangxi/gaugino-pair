#########################################################################
# File Name: run_N_1.sh
# Author: 123
# mail: xin.wang@smail.nju.edu.cn
# Created Time: Tue 08 Jan 2019 10:07:49 AM CST
#########################################################################
#!/bin/bash

root -l -b -q 'compare_zn.C ("hasbjet_N_1")'
root -l -b -q 'compare_zn.C ("n_TightTau_N_1")'
root -l -b -q 'compare_zn.C ("Mll_N_1")'
root -l -b -q 'compare_zn.C ("tau2Pt_N_1")'
root -l -b -q 'compare_zn.C ("tau1Pt_N_1")'
root -l -b -q 'compare_zn.C ("noemu_N_1")'
root -l -b -q 'compare_zn.C ("n_SignalTau_N_1")'
root -l -b -q 'compare_zn.C ("evt_met150_N_1")'
